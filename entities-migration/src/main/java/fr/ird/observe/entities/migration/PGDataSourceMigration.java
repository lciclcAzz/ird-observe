/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.migration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * PGDataSourceMigration
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class PGDataSourceMigration extends AbstractDataSourceMigration {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(PGDataSourceMigration.class);

    public static final String TYPE = "PG";

    public PGDataSourceMigration() {
        super(new ObserveMigrationCallBackForVersionResolver(TYPE));
    }

    protected static String getUniqueConstraintName(TopiaSqlSupport  tx, final String tableName, final String columnName) {

        // recherche du nom de la constrainte
        final List<String> result = new ArrayList<>();

        tx.doSqlWork(connection -> {

            // get table oid
            String oid = null;

            String sqlOid = "select oid from pg_class where relname=?;";
            {
                PreparedStatement ps = connection.prepareStatement(sqlOid);
                ps.setString(1, tableName.toLowerCase());
                try {
                    ResultSet set = ps.executeQuery();
                    if (set.next()) {
                        oid = set.getString(1);
                        if (log.isDebugEnabled())
                            log.debug("found table oid " + tableName + ": " + oid);
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain oid for table" + tableName, e);
                } finally {
                    ps.close();
                }
            }

            // get attribute num
            String attNumSql = "select attnum from pg_attribute where attrelid=? AND attname =?";
            String attNum = null;
            {
                PreparedStatement ps = connection.prepareStatement(attNumSql);
                ps.setInt(1, Integer.valueOf(oid));
                ps.setString(2, columnName.toLowerCase());
                try {
                    ResultSet set = ps.executeQuery();
                    if (set.next()) {
                        attNum = set.getString(1);
                        if (log.isDebugEnabled())
                            log.debug("found attribute " + columnName + " attNum : " + attNum);
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain attNum for column" + columnName, e);
                } finally {
                    ps.close();
                }
            }

            String sql = "SELECT conname FROM pg_constraint where contype='u' and conrelid= ? and conkey = '{" + attNum + "}';";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, Integer.valueOf(oid));
            try {
                ResultSet set = ps.executeQuery();

                if (set.next()) {
                    String constraintName = set.getString(1);
                    if (log.isDebugEnabled())
                        log.debug("found constraint of type unique for table " + tableName + " and column " + columnName + ": " + constraintName);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraint unique for table" + tableName + " and column " + columnName, e);
            } finally {
                ps.close();
            }
        });

        if (result.isEmpty()) {
            throw new TopiaException("No unique constraint found for table " + tableName + " and column " + columnName);
        }

        return result.get(0);
    }

    public static String getFirstTableUniqueConstraintName(TopiaSqlSupport  tx, final String tableName) {

        // recherche du nom de la constrainte
        //();
        final List<String> result = new ArrayList<>();

        tx.doSqlWork(connection -> {
            String sql = "SELECT conname FROM pg_constraint where contype='u' and conrelid= (select oid from pg_class where relname=?);";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, tableName.toLowerCase());
            try {
                ResultSet set = ps.executeQuery();
                if (set.next()) {
                    String constraintName = set.getString(1);
                    if (log.isDebugEnabled())
                        log.debug("found constraint of type unique for table " + tableName + " : " + constraintName);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraint unique for table " + tableName, e);
            } finally {
                ps.close();
            }
        });

        if (result.isEmpty()) {
            throw new TopiaException("Aucune contrainte de type unique trouvée sur la table " + tableName);
        }

        return result.get(0);
    }

    public static Set<String> getConstraintNames(TopiaSqlSupport  tx, final String tableName) {

        final Set<String> result = new HashSet<>();

        tx.doSqlWork(connection -> {
            String sql = String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='u' OR contype='f' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName.toLowerCase());
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String constraintName = set.getString(1);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraints for table " + tableName, e);
            } finally {
                ps.close();
            }
        });

        return result;
    }

    public static Set<String> getForeignKeyConstraintNames(TopiaSqlSupport  tx, final String tableName) {

        final Set<String> result = new HashSet<>();

        tx.doSqlWork(connection -> {
            String sql = String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='f' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName.toLowerCase());
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String constraintName = set.getString(1);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraints for table " + tableName, e);
            } finally {
                ps.close();
            }
        });

        return result;
    }


    public static String getForeignKeyConstraintName(TopiaSqlSupport  tx, final String schemaName, final String tableName, final String columnName, final boolean mustExists) {

        final List<String> result = new ArrayList<>();

        tx.doSqlWork(connection -> {


            // get table oid
            String oid = null;

            String sqlOid = "select oid from pg_class where relnamespace = (select oid from pg_catalog.pg_namespace where nspname=?) AND relname=?;";
            {
                PreparedStatement ps = connection.prepareStatement(sqlOid);
                ps.setString(1, schemaName.toLowerCase());
                ps.setString(2, tableName.toLowerCase());
                try {
                    ResultSet set = ps.executeQuery();
                    if (set.next()) {
                        oid = set.getString(1);
                        if (log.isDebugEnabled())
                            log.debug("found table oid " + tableName + ": " + oid);
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain oid for table" + tableName, e);
                } finally {
                    ps.close();
                }
            }

            // get attribute num
            String attNumSql = "select attnum from pg_attribute where attrelid=? AND attname =?";
            String attNum = null;
            {
                PreparedStatement ps = connection.prepareStatement(attNumSql);
                ps.setInt(1, Integer.valueOf(oid));
                ps.setString(2, columnName.toLowerCase());
                try {
                    ResultSet set = ps.executeQuery();
                    if (set.next()) {
                        attNum = set.getString(1);
                        if (log.isDebugEnabled())
                            log.debug("found attribute " + columnName + " attNum : " + attNum);
                    }
                } catch (Exception e) {
                    throw new SQLException("Could not obtain attNum for column" + columnName, e);
                } finally {
                    ps.close();
                }
            }

            String sql = "SELECT DISTINCT conname FROM pg_constraint WHERE contype='f' AND conrelid = ? AND conkey = '{" + attNum + "}';";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, Integer.valueOf(oid));

            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String constraintName = set.getString(1);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraints for table " + tableName, e);
            } finally {
                ps.close();
            }
        });

        String constraintName;

        if (result.isEmpty()) {

            if (mustExists) {
                throw new IllegalStateException("Could not find constraint name for " + schemaName + "." + tableName + "." + columnName);
            }

            constraintName = null;

        } else {

            constraintName = result.get(0);

        }

        return constraintName;

    }

    public static Set<String> getUniqueKeyConstraintNames(TopiaSqlSupport  tx, final String tableName) {

        final Set<String> result = new HashSet<>();

        tx.doSqlWork(connection -> {
            String sql = String.format("SELECT DISTINCT conname FROM pg_constraint WHERE ( contype='u' ) AND conrelid = (SELECT oid FROM pg_class WHERE relname='%s');", tableName.toLowerCase());
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String constraintName = set.getString(1);
                    result.add(constraintName);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain constraints for table " + tableName, e);
            } finally {
                ps.close();
            }
        });

        return result;
    }

    public static void removeFK(TopiaSqlSupport tx, String tableName, List<String> queries) {

        // Get fk constraints
        Set<String> fkNames = getForeignKeyConstraintNames(tx, tableName);

        // remove constraints
        for (String contrainstName : fkNames) {
            queries.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, contrainstName));
        }

    }

    public static void removeFK(TopiaSqlSupport  tx, String schemaName, String tableName, String columnName, List<String> queries) {

        // Get fk constraints
        String contrainstName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, true);

        // remove constraints
        queries.add(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, contrainstName));

    }

    public static void removeFKIfExists(TopiaSqlSupport  tx, String schemaName, String tableName, String columnName, List<String> queries) {

        // Get fk constraints
        String contrainstName = getForeignKeyConstraintName(tx, schemaName, tableName, columnName, false);

        if (contrainstName != null) {

            // remove constraints
            queries.add(String.format("ALTER TABLE %s.%s DROP CONSTRAINT %s;", schemaName, tableName, contrainstName));

        }

    }

    public static void removeUK(TopiaSqlSupport tx, String tableName, List<String> queries) {

        // Get uk constraints
        Set<String> uKNames = getUniqueKeyConstraintNames(tx, tableName);

        // remove constraints
        for (String contrainstName : uKNames) {
            queries.add(String.format("ALTER TABLE %s DROP CONSTRAINT %s;", tableName, contrainstName));
        }
    }

}
