package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.ObserveTopiaIdFactory;
import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 6/8/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.1
 */
public abstract class DataSourceMigrationForVersion_4_0_1 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_4_0_1(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_4_0_1.getVersion(), callBack, scriptSuffix);
    }

    protected abstract void removeForeignKeys(TopiaSqlSupport tx, List<String> queries);

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport tx,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {

        // See https://forge.codelutin.com/issues/7229
        removeForeignKeys(tx, queries);
        addScript("02", "add-missing-fk", queries);

        // See https://forge.codelutin.com/issues/7350
        migrateGearUseFeaturesSeineMeasurementIds(tx, queries);

        // See https://forge.codelutin.com/issues/7328
        recomputeLonglineHaulingIds(tx, queries);

    }

    protected void recomputeLonglineHaulingIds(TopiaSqlSupport tx, List<String> queries) {

        class SetData {

            String id;

            int totalSectionsCount;

            int basketsPerSectionCount;

            int branchlinesPerBasketCount;

        }

        // L'ensemble des opérations de pêche où l'on doit regénerer les générer les haulingIds
        final Set<SetData> setIds = new LinkedHashSet<>();
        tx.doSqlWork(connection -> {
            String sql = "SELECT topiaId, totalSectionsCount, basketsPerSectionCount, branchlinesPerBasketCount FROM OBSERVE_LONGLINE.SET WHERE HAULINGBREAKS = 0 AND haulingDirectionSameAsSetting IS FALSE;";
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    SetData setData = new SetData();
                    setData.id = set.getString(1);
                    setData.totalSectionsCount = set.getInt(2);
                    setData.basketsPerSectionCount = set.getInt(3);
                    setData.branchlinesPerBasketCount = set.getInt(4);
                    setIds.add(setData);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain SET data", e);
            } finally {
                ps.close();
            }
        });

        for (SetData setData : setIds) {

            queries.add(String.format("UPDATE OBSERVE_LONGLINE.SECTION SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE set = '%s'", setData.totalSectionsCount, setData.id));
            queries.add(String.format("UPDATE OBSERVE_LONGLINE.BASKET SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE section IN  ( SELECT topiaid FROM OBSERVE_LONGLINE.SECTION WHERE set ='%s' )", setData.basketsPerSectionCount, setData.id));
            queries.add(String.format("UPDATE OBSERVE_LONGLINE.BRANCHLINE SET haulingIdentifier = (%s - settingIdentifier + 1 ) WHERE basket IN ( SELECT distinct b.topiaid FROM OBSERVE_LONGLINE.BASKET b, OBSERVE_LONGLINE.SECTION s WHERE b.section = s.topiaid AND s.set  = '%s' )", setData.branchlinesPerBasketCount, setData.id));

        }
    }

    protected void migrateGearUseFeaturesSeineMeasurementIds(TopiaSqlSupport tx, List<String> queries) {

        final Multimap<String, String> gearUseFeaturesSeineAndMeasurementIds = LinkedHashMultimap.create();
        tx.doSqlWork(connection -> {
            String sql = "SELECT GEARUSEFEATURES, topiaId FROM OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT;";
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String gearUseFeaturesSeineId = set.getString(1);
                    String gearUseFeaturesMeasurementSeineId = set.getString(2);
                    gearUseFeaturesSeineAndMeasurementIds.put(gearUseFeaturesSeineId, gearUseFeaturesMeasurementSeineId);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain GEARUSEFEATURESMEASUREMENT ids", e);
            } finally {
                ps.close();
            }
        });

        ObserveTopiaIdFactory topiaIdFactory = new ObserveTopiaIdFactory();
        for (String gearUseFeaturesSeineId : gearUseFeaturesSeineAndMeasurementIds.keySet()) {


            String newGearUseFeaturesSeineId = topiaIdFactory.newTopiaId(GearUseFeaturesSeine.class);

            queries.add(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEAR, NUMBER) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referentiel.Gear#1239832686125#0.20', 1)", newGearUseFeaturesSeineId));
            queries.add(String.format("UPDATE OBSERVE_SEINE.GEARUSEFEATURES SET TRIP = (SELECT TRIP FROM OBSERVE_SEINE.GEARUSEFEATURES WHERE topiaId='%s') WHERE topiaId='%s';", gearUseFeaturesSeineId, newGearUseFeaturesSeineId));

            Collection<String> gearUseFeaturesMeasurementSeineIds = gearUseFeaturesSeineAndMeasurementIds.get(gearUseFeaturesSeineId);

            for (String gearUseFeaturesMeasurementSeineId : gearUseFeaturesMeasurementSeineIds) {

                String newGearUseFeaturesMeasurementSeineId = topiaIdFactory.newTopiaId(GearUseFeaturesMeasurementSeine.class);
                queries.add(String.format("UPDATE OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT SET GEARUSEFEATURES = '%s', topiaid = '%s' WHERE topiaid='%s'", newGearUseFeaturesSeineId, newGearUseFeaturesMeasurementSeineId, gearUseFeaturesMeasurementSeineId));

            }

            queries.add(String.format("DELETE FROM OBSERVE_SEINE.GEARUSEFEATURES WHERE topiaId='%s'", gearUseFeaturesSeineId));

        }

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_1 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport tx, List<String> queries, boolean showSql, boolean showProgression) throws TopiaException {

            // See https://forge.codelutin.com/issues/7226
            addScript("01", "remove-gender-column", queries);

            super.prepareMigrationScript(tx, queries, showSql, showProgression);

        }

        @Override
        protected void removeForeignKeys(TopiaSqlSupport tx, List<String> queries) {

            H2DataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTTYPE", queries);
            H2DataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTFATE", queries);
            H2DataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTOPERATION", queries);
            H2DataSourceMigration.removeFKIfExists(tx, "observe_seine", "ACTIVITY_OBSERVEDSYSTEM", "OBSERVEDSYSTEM", queries);
            H2DataSourceMigration.removeFKIfExists(tx, "observe_seine", "TRIP", "OBSERVER", queries);

        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_1 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void removeForeignKeys(TopiaSqlSupport tx, List<String> queries) {

            PGDataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTTYPE", queries);
            PGDataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTFATE", queries);
            PGDataSourceMigration.removeFKIfExists(tx, "observe_seine", "FLOATINGOBJECT", "OBJECTOPERATION", queries);
            PGDataSourceMigration.removeFKIfExists(tx, "observe_seine", "ACTIVITY_OBSERVEDSYSTEM", "OBSERVEDSYSTEM", queries);
            PGDataSourceMigration.removeFKIfExists(tx, "observe_seine", "TRIP", "OBSERVER", queries);

        }

    }

}
