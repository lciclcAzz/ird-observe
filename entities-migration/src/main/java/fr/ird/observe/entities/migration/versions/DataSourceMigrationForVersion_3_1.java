package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Migration class for version {@code 3.1}.
 *
 * Created on 10/10/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
public class DataSourceMigrationForVersion_3_1 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_3_1(AbstractDataSourceMigration callBack) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_3_1.getVersion(), callBack, "");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {

        // Doublons et absence de clé primaire dans especefaune_ocean et especethon_ocean
        // (see http://forge.codelutin.com/issues/3398)
        updateReferentielEspece("thon", topiaSqlSupport, queries);
        updateReferentielEspece("faune", topiaSqlSupport, queries);
    }

    protected void updateReferentielEspece(String speciesType, TopiaSqlSupport topiaSqlSupport,
                                           List<String> queries) {

        GetAllEspeceOceanDoublonQuery request = new GetAllEspeceOceanDoublonQuery(speciesType);

        List<Pair<String, String>> entities = topiaSqlSupport.findMultipleResult(request);

        String deleteQuery = "DELETE FROM espece%1$s_ocean WHERE espece%1$s='%2$s' AND ocean='%3$s';";
        String insertQuery = "INSERT INTO espece%1$s_ocean VALUES('%2$s','%3$s');";
        String addConstraintQuery = "ALTER TABLE espece%1$s_ocean ADD CONSTRAINT espece%1$s_ocean_unique_key UNIQUE ( espece%1$s, ocean );";

        for (Pair<String, String> tuple : entities) {
            String speciesId = tuple.getLeft();
            String oceanId = tuple.getRight();

            queries.add(String.format(deleteQuery, speciesType, speciesId, oceanId));
            queries.add(String.format(insertQuery, speciesType, speciesId, oceanId));
        }

        queries.add(String.format(addConstraintQuery, speciesType));
    }

    static class GetAllEspeceOceanDoublonQuery extends TopiaSqlQuery<Pair<String, String>> {

        private final String tableName;

        private GetAllEspeceOceanDoublonQuery(String tableName) {
            this.tableName = tableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String sql = "SELECT e.espece%1$s, e.ocean, count(e.*) " +
                         "FROM  espece%1$s_ocean e \n" +
                         "GROUP BY e.espece%1$s, e.ocean\n" +
                         "HAVING count(e.*) > 1\n" +
                         "ORDER BY e.espece%1$s, e.ocean;";
            return connection.prepareStatement(String.format(sql, tableName));
        }

        @Override
        public Pair<String, String> prepareResult(ResultSet set) throws SQLException {
            return Pair.of(set.getString(1), set.getString(2));
        }
    }

}
