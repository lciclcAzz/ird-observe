package fr.ird.observe.entities.migration;

/*-
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 8/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public abstract class AbstractObserveMigrationCallBack extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractObserveMigrationCallBack.class);

    private final String scriptSuffix;

    public AbstractObserveMigrationCallBack(Version version, TopiaMigrationCallbackByClass callBack, String scriptSuffix) {
        super(version, callBack);
        this.scriptSuffix = scriptSuffix;
    }

    protected Set<String> getTopiaIds(TopiaSqlSupport topiaSqlSupport, final String tableName) {

        final Set<String> result = new HashSet<>();

        topiaSqlSupport.doSqlWork(connection -> {
            String sql = String.format("SELECT topiaId FROM %s;", tableName);
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    String id = set.getString(1);
                    result.add(id);
                }
            }
        });

        return result;
    }


    protected void addScript(String rank, String prefix, List<String> queries) {

        String[] migrationQueries = loadScript(rank, prefix);
        queries.addAll(Lists.newArrayList(migrationQueries));

    }

    private String[] loadScript(String rank, String prefix) {

        String migrationScript = prefix + "-" + scriptSuffix + ".sql";
        String scriptPath = "/db/migration/V" + version.getValidName() + "_" + rank + "_" + migrationScript;

        if (log.isInfoEnabled()) {
            log.info("Load migration script: " + scriptPath);
        }

        try (InputStream stream = getClass().getResourceAsStream(scriptPath)) {
            String content = IOUtils.toString(stream, Charsets.UTF_8);
            Set<String> result = new LinkedHashSet<>();
            for (String statement : content.split("\n")) {

                if (statement.startsWith("--")) {
                    // commentaire
                    continue;
                }
                result.add(statement);
            }

            return result.toArray(new String[result.size()]);

        } catch (IOException e) {
            throw new TopiaException("Could not load migration script: " + migrationScript, e);
        }
    }
}
