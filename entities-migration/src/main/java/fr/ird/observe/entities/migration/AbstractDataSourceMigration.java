/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.migration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;

import java.util.List;

/**
 * AbstractDataSourceMigration
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class AbstractDataSourceMigration extends TopiaMigrationCallbackByClass {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractDataSourceMigration.class);

    protected AbstractDataSourceMigration(MigrationCallBackForVersionResolver callBackResolver) {
        super(callBackResolver);
    }

    @Override
    public Version[] getAvailableVersions() {
        return ObserveMigrationConfigurationProvider.get().getAvailableVersions();
    }

    @Override
    public boolean askUser(Version dbVersion, List<Version> versions) {
        // on autorise les migrations dès quelles sont demandée pour une version égale ou superieur à la V3.0.
        return dbVersion.afterOrEquals(ObserveMigrationConfigurationProvider.get().getMinimumVersion());
    }

}
