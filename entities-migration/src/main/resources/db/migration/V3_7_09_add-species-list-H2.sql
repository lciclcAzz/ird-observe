---
-- #%L
-- ObServe :: Entities Migration
-- %%
-- Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
DELETE FROM OBSERVE_COMMON.SPECIES_SPECIESLIST WHERE SPECIESLIST ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3';
DELETE FROM OBSERVE_COMMON.SPECIES_SPECIESLIST WHERE SPECIESLIST ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4';
DELETE FROM OBSERVE_COMMON.SPECIES_SPECIESLIST WHERE SPECIESLIST ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5';
DELETE FROM OBSERVE_COMMON.SPECIESLIST WHERE topiaid ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5';
UPDATE OBSERVE_COMMON.SPECIESLIST SET code = 4, topiaid ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5' WHERE topiaid ='fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.6';
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683846#0.9864285434180587', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683725#0.39445809291491807', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684201#0.30554150296793947', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683841#0.25560065674558863', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683731#0.3892121873590658', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684531#0.01962657179799221', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684549#0.4997270804575126', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685240#0.16727135240325364', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685240#0.16727135240325364', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684953#0.5908437547687754', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684537#0.2397229787936519', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685039#0.9885160092180597', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685039#0.9885160092180597', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683867#0.5251675316716491', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685119#0.05669776907577062', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685119#0.05669776907577062', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684904#0.7999183854146987', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#201007241100#0.12345371', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685094#0.8862082966884622', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685094#0.8862082966884622', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683991#0.18251172983518738', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683991#0.18251172983518738', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685477#0.2673009297087321', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685113#0.7014499115802707', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685113#0.7014499115802707', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684795#0.6053463019344153', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684807#0.8492371601281514', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684971#0.8622121191938249', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684813#0.23032822909041595', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#201007241100#0.12345247', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#201007241100#0.12345248', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683785#0.49501050869628815', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684831#0.9665297005737801', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684819#0.5710822650999473', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684837#0.36870634574294836', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683883#0.6583610159371419', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684885#0.5770224986830752', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683888#0.6223394488789843', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683835#0.03404189793534684', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684036#0.959050780883495', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684644#0.6230088204735412', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684861#0.36396067375732244', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683748#0.9215143239720961', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685052#0.41637903112663477', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685052#0.41637903112663477', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683996#0.008204938561974351', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684327#0.8415482323514992', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684296#0.3383076607094946', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684332#0.5394989374753841', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683774#0.3664090332253188', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683791#0.20975568563021063', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832683985#0.48335145439788996', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684959#0.8280432216962142', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684940#0.3941475292396889', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684946#0.29129778706560716', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684928#0.20119509838828453', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832685474#0.8943253454598569', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST(SPECIES, SPECIESLIST) VALUES ('fr.ird.observe.entities.referentiel.Species#1239832684892#0.9272035028442585', 'fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3');
