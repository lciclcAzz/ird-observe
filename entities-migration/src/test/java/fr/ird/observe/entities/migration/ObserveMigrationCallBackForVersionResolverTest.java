/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.migration;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;

/**
 * Test de le résolveur de classe de migration par version
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ObserveMigrationCallBackForVersionResolver
 * @since 2.0
 */
public class ObserveMigrationCallBackForVersionResolverTest {

    @Test
    public void instanceForH2() throws Exception {
        ObserveMigrationCallBackForVersionResolver resolver
                = new ObserveMigrationCallBackForVersionResolver("H2");
        Assert.assertNotNull(resolver);

        for (Version version : ObserveMigrationConfigurationProvider.get().getAvailableVersions()) {
            Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion> callBack = resolver.getCallBack(version);
            Assert.assertNotNull(callBack);
        }
    }

    @Test
    public void instanceForPG() throws Exception {
        ObserveMigrationCallBackForVersionResolver resolver =
                new ObserveMigrationCallBackForVersionResolver("PG");
        Assert.assertNotNull(resolver);
        for (Version version : ObserveMigrationConfigurationProvider.get().getAvailableVersions()) {
            Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion> callBack = resolver.getCallBack(version);
            Assert.assertNotNull(callBack);
        }
    }

    @Test(expected = IllegalStateException.class)
    public void instanceForDUmmy() throws Exception {
        ObserveMigrationCallBackForVersionResolver resolver =
                new ObserveMigrationCallBackForVersionResolver("Dummy");
        Assert.assertNotNull(resolver);
        for (Version version : ObserveMigrationConfigurationProvider.get().getAvailableVersions()) {
            Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion> callBack = resolver.getCallBack(version);
            Assert.assertNotNull(callBack);
        }
    }

}
