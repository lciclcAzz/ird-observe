package fr.ird.observe.application.swing.validators;

/*
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <!-- START SNIPPET: javadoc --> VesselActivityFieldValidator vérifie que
 * l'activity vessel d'une activité est cohérente. <!-- END SNIPPET: javadoc
 * -->
 *
 *
 * <!-- START SNIPPET: parameters --> <ul> <li>fieldName - The field name this
 * validator is validating. Required if using Plain-Validator Syntax otherwise
 * not required</li> </ul> <!-- END SNIPPET: parameters -->
 *
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="invalidLochMatin"&gt;
 *             &lt;param name="fieldName"&gt;startLogValue&lt;/param&gt;
 *             &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *         &lt;/validator&gt;
 *
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="startLogValue"&gt;
 *         	  &lt;field-validator type="invalidLochMatin"&gt;
 *                 &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivityFinDeVeilleExistsDtoValidator extends FieldValidatorSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ActivityFinDeVeilleExistsDtoValidator.class);

    /**
     * l'état attendu : la route possède-t-ell ou non une activity de fin de
     * veille.
     *
     * Si le drapeau vaut {@code true}, la route est valide si elle possède une
     * activité de fin de veille (cas de vérification de la présence de
     * l'activité sur l'ensemble au niveau de sa route).
     *
     * Si le drapeau vaut {@code false}, la route est valide si elle ne possède
     * déjà d'activité de fin de veille (cas de création d'une nouvelle
     * activité).
     */
    private Boolean required;

    @Override
    public String getValidatorType() {
        return "activityFinDeVeilleExists";
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (required == null) {
            throw new ValidationException("le parametre required est obligatoire");
        }

        if (object == null) {

            // pas d'objet, donc rien a faire
            return;
        }

        if (object instanceof RouteDto) {

            // on verifie qu'il existe bien une activity de fin de veille
            // parmi les activitys de la route

            RouteDto route = (RouteDto) object;

            checkAgainstRequired(route, route);
            return;
        }

        if (object instanceof ActivitySeineDto) {

            // on verifie qu'il n'existe pas d'activity de fin de veille

            ActivitySeineDto activitySeine = (ActivitySeineDto) object;

            if (!activitySeine.isActivityFinDeVeille()) {

                // rien a valider (on est pas sur une activity de fin de veille
                return;
            }

            // l'activity est une activite de fin de veille
            // on doit vérifier qu'il n'existe pas déjà une autre activité de
            // fin de veille

            RouteDto route = (RouteDto) stack.findValue("routeEntity");
            if (route == null) {

                if (log.isWarnEnabled()) {
                    log.warn("COULD NOT FIND DATA CONTEXT! [routeEntity]");
                }
                return;
            }

            checkAgainstRequired(route, activitySeine);
        }
    }

    protected void checkAgainstRequired(RouteDto route, Object object) {

        if (log.isInfoEnabled()) {

            log.info("check [required : " + required +
                             "?] activity fin de veille sur la route " +
                             route.getId() + ":" + route.getDate()
                             + "sur " + route.sizeActivitySeine() + " activity(s).");
        }

        boolean detected = route.isActivityFindDeVeilleFound();
        boolean valid = required == detected;
        if (log.isDebugEnabled()) {
            log.debug("detected activity fin de veille " + detected);
            log.debug("is valid = " + valid);
        }

        if (valid) {
            return;
        }

        String fieldName = getFieldName();
        if (log.isDebugEnabled()) {
            log.debug("not valid , fieldName : " + fieldName);
        }
        addFieldError(fieldName, object);
    }
}
