###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#!/bin/sh

#
# script pour creer une base obstuna en v2.
# script pour creation de la base obstuna :
#  - creation du role proprietaire de la base
#  - creation de la base
#  - creation des roles
#
# Note : Ce script doit etre lance par l'utilisateur postgres sur la machine
# hebergeant la base.
#
# Usage :
# ./create-empty-obstuna.sh nomDeLaBase admin [role1, role2, ...]

if [ ! $# -eq 3 ] ; then
    echo "le nombre de paramètres n'est pas correct."
    echo ""
    echo "usage $0 dbname owner \"role1 role2 ...\""
    exit 1
fi

# parameter 1 : db name
OBSTUNA_DB=$1

# parameter 2 : owner
OBSTUNA_OWNER=$2

# parameter 3 : other roles
OBSTUNA_ROLES="$3"

echo "Création du role proprietaire '$OBSTUNA_OWNER'..."
createuser -sdRP "$OBSTUNA_OWNER"
if [ $? -eq 1 ] ; then
    echo "KO"
else
  echo "OK"
fi

for role in $OBSTUNA_ROLES ;
do
    echo "Création du role '$role'..."
    createuser -SDRP "$role"
    if [ $? -eq 1 ] ; then
        echo "KO"
    else
       echo "OK"
    fi
done

echo -n "Création de la base '$OBSTUNA_DB' (proprietaire $OBSTUNA_OWNER)..."
createdb -E UTF-8 -O "$OBSTUNA_OWNER" "$OBSTUNA_DB"
if [ $? -eq 1 ] ; then
    echo "KO"
    exit 1
fi
echo "OK"