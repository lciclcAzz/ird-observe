###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#! /bin/sh

#
# création du script sql qui permet d'ajouter pour un role donné les droits sur
# la base obstuna :
# - lecture des données observateurs
# - lecture sur le référentiel

# parametre 1 : les tables
TABLES=$1

# parametre 2 : le role
ROLE=$2

for  table in $TABLES 
do
    echo "revoke ALL on $table FROM $ROLE CASCADE;"
    echo "grant SELECT on $table TO $ROLE;"
    echo "revoke INSERT, UPDATE on $table FROM $ROLE CASCADE;"
done;
