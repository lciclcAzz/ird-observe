<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

  <h3>Type de source de données sélectionné</h3>

  <#if dbMode.name() == "USE_LOCAL">
    Utiliser une base locale de type H2
  <#elseif dbMode.name() == "CREATE_LOCAL">
    Creer une base locale de type H2
  <#elseif dbMode.name() == "USE_REMOTE">
    Utiliser une base distante de type postgres
  <#elseif dbMode.name() == "USE_SERVER">
    Utiliser un service web distant
  <#else>
    Aucun type de source de données sélectionné
  </#if>

  <h3>Mode de création sélectionné</h3>

  <#if dbMode.name() == "CREATE_LOCAL">
    <#if !creationMode??>
      Aucun mode de création sélectionné
    <#elseif creationMode.name() == "EMPTY">
      Générer une nouvelle base locale vide. Cette base n'aura pas de référentiel et il vous faudra ensuite faire un import de référentiel.
    <#elseif creationMode.name() == "IMPORT_INTERNAL_DUMP">
      Générer une nouvelle base locale à partir de la dernière version de la base embarquée.
    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">
      Créer une nouvelle base locale à partir d'une précédente sauvegarde de l'application
    <#elseif creationMode.name() == "IMPORT_LOCAL_STORAGE">
      Générer une nouvelle base locale et y importer le référentiel d'une autre base locale.
    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">
      Générer une nouvelle base locale et y importer le référentiel d'une autre base distante.
    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">
      Générer une nouvelle base locale et y importer le référentiel depuis un service web distant.
    </#if>
  <#else>
    Non requis.
  </#if>
</body>
</html>
