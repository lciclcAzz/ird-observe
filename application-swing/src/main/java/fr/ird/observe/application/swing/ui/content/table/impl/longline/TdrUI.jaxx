<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI superGenericType='SetLonglineTdrDto, TdrDto'
                                                contentTitle='{n("observe.content.tdr.title")}'
                                                saveNewEntryText='{n("observe.content.tdr.action.create")}'
                                                saveNewEntryTip='{n("observe.content.tdr.action.create.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.DataReference
    fr.ird.observe.services.dto.longline.BasketDto
    fr.ird.observe.services.dto.longline.BranchlineDto
    fr.ird.observe.services.dto.longline.TdrDto
    fr.ird.observe.services.dto.longline.SectionDto
    fr.ird.observe.services.dto.longline.SetLonglineTdrDto
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto
    fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto
    fr.ird.observe.services.dto.referential.longline.SensorBrandDto
    fr.ird.observe.application.swing.ui.content.table.*

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox
    org.nuiton.jaxx.widgets.select.FilterableDoubleList

    org.nuiton.jaxx.widgets.datetime.DateTimeEditor

    java.util.Collection

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <TdrUIHandler id='handler'/>

  <!-- model -->
  <TdrUIModel id='model'/>

  <!-- edit bean -->
  <SetLonglineTdrDto id='bean'/>

  <!-- table edit bean -->
  <TdrDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator' context='ui-update-tdr'
                 beanClass='fr.ird.observe.services.dto.longline.SetLonglineTdrDto'
                 errorTableModel='{getErrorTableModel()}'>
  </BeanValidator>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable' autoField='true' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.longline.TdrDto'
                 errorTableModel='{getErrorTableModel()}'
                 parentValidator='{validator}'/>

  <Table id='editorPanel' fill='both' insets='0'>
    <row>
      <cell weightx="1" weighty="1">
        <JTabbedPane id='editTabPane'>

          <tab id='caracteristicsTab'>

            <JPanel layout='{new BorderLayout()}'>
              <Table fill='both' constraints='BorderLayout.NORTH' insets="0">

                <!-- homeId -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='homeIdLabel'/>
                  </cell>
                  <cell fill='both' weightx='1'>
                    <JPanel layout='{new BorderLayout()}'>
                      <JToolBar id='homeIdToolbar' constraints='BorderLayout.WEST'>
                        <JButton id='resetHomeId' styleClass='resetButton'/>
                      </JToolBar>
                      <JTextField id='homeId' constraints='BorderLayout.CENTER'/>
                    </JPanel>
                  </cell>
                </row>

                <!-- serialNo -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='serialNoLabel'/>
                  </cell>
                  <cell fill='both' weightx='1'>
                    <JPanel layout='{new BorderLayout()}'>
                      <JToolBar id='serialNoToolbar' constraints='BorderLayout.WEST'>
                        <JButton id='resetSerialNo' styleClass='resetButton'/>
                      </JToolBar>
                      <JTextField id='serialNo' constraints='BorderLayout.CENTER'/>
                    </JPanel>
                  </cell>
                </row>

                <!-- sensorBrand -->
                <row>
                  <cell>
                    <JLabel id='sensorBrandLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='sensorBrand' constructorParams='this' genericType='ReferentialReference&lt;SensorBrandDto&gt;' _entityClass='SensorBrandDto.class'/>
                  </cell>
                </row>

                <!-- dataLocation -->
                <row>
                  <cell>
                    <JLabel id='dataLocationLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <JPanel layout='{new BorderLayout()}'>
                      <JToolBar id='dataLocationToolbar' constraints='BorderLayout.WEST'>
                        <JButton id='resetDataLocation' styleClass='resetButton'/>
                      </JToolBar>
                      <JTextField id='dataLocation' constraints='BorderLayout.CENTER'/>
                    </JPanel>
                  </cell>
                </row>

                <!-- data -->
                <row>
                  <cell>
                    <JLabel id='dataLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <JPanel layout='{new GridLayout()}'>
                      <JButton id="importDataButton" onActionPerformed="getHandler().importData()"/>
                      <JButton id="exportDataButton" onActionPerformed="getHandler().exportData()"/>
                      <JButton id="deleteDataButton" onActionPerformed="getHandler().deleteData()"/>
                    </JPanel>
                  </cell>
                </row>

              </Table>
            </JPanel>

          </tab>

          <tab id='localisationTab'>

            <JPanel layout='{new BorderLayout()}'>
              <Table fill='both' constraints='BorderLayout.NORTH' insets="0">

                <!-- location on longline -->
                <row>
                  <cell columns="2">
                    <Table id="locationOnLonglinePanel">
                      <row>
                        <cell anchor="west">
                          <JLabel id='sectionLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='section' genericType='DataReference&lt;SectionDto&gt;' _entityClass='SectionDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='basketLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='basket' genericType='DataReference&lt;BasketDto&gt;' _entityClass='BasketDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                      <row>
                        <cell anchor="west">
                          <JLabel id='branchlineLabel'/>
                        </cell>
                        <cell fill="both" weightx="1">
                          <BeanComboBox id='branchline' genericType='DataReference&lt;BranchlineDto&gt;' _entityClass='BranchlineDto.class' constructorParams='this'/>
                        </cell>
                      </row>
                    </Table>
                  </cell>
                </row>

                <!-- itemHorizontalPosition -->
                <row>
                  <cell>
                    <JLabel id='itemHorizontalPositionLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='itemHorizontalPosition' constructorParams='this'
                                  genericType='ReferentialReference&lt;ItemHorizontalPositionDto&gt;' _entityClass='ItemHorizontalPositionDto.class'/>
                  </cell>
                </row>

                <!-- itemVerticalPosition -->
                <row>
                  <cell>
                    <JLabel id='itemVerticalPositionLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='itemVerticalPosition' constructorParams='this'
                                  genericType='ReferentialReference&lt;ItemVerticalPositionDto&gt;' _entityClass='ItemVerticalPositionDto.class'/>
                  </cell>
                </row>

                <!-- floatline1Length -->
                <row>
                  <cell>
                    <JLabel id='floatline1LengthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='floatline1Length' constructorParams='this' styleClass='float'/>
                  </cell>
                </row>

                <!-- floatline2Length -->
                <row>
                  <cell>
                    <JLabel id='floatline2LengthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='floatline2Length' constructorParams='this' styleClass='float'/>
                  </cell>
                </row>

              </Table>
            </JPanel>

          </tab>

          <tab id='timestampTab'>

            <JPanel layout='{new BorderLayout()}'>
              <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.NORTH'>

                <JCheckBox id='enableTimestamp' constraints='BorderLayout.NORTH'
                           onMouseClicked='getHandler().changeEnableTimestamp(enableTimestamp.isSelected())'/>

                <Table id='timestampPanel' fill='both' constraints='BorderLayout.CENTER' insets="0">

                  <!-- deployementStart -->
                  <row>
                    <cell weightx="1" fill="both">
                      <DateTimeEditor id='deployementStart' constructorParams='this'/>
                    </cell>
                  </row>
                  <!-- fishingStart -->
                  <row>
                    <cell>
                      <DateTimeEditor id='fishingStart' constructorParams='this'/>
                    </cell>
                  </row>
                  <!-- fishingEnd -->
                  <row>
                    <cell>
                      <DateTimeEditor id='fishingEnd' constructorParams='this'/>
                    </cell>
                  </row>
                  <!-- deployementEnd -->
                  <row>
                    <cell>
                      <DateTimeEditor id='deployementEnd' constructorParams='this'/>
                    </cell>
                  </row>
                </Table>
              </JPanel>

            </JPanel>
          </tab>

          <tab id='keyDataTab'>

            <JPanel layout='{new BorderLayout()}'>
              <Table fill='both' constraints='BorderLayout.NORTH'>

                <!-- fishingStartDepth -->
                <row>
                  <cell>
                    <JLabel id='fishingStartDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='fishingStartDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- fishingEndDepth -->
                <row>
                  <cell>
                    <JLabel id='fishingEndDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='fishingEndDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- minFishingDepth -->
                <row>
                  <cell>
                    <JLabel id='minFishingDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='minFishingDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- maxFishingDepth -->
                <row>
                  <cell>
                    <JLabel id='maxFishingDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='maxFishingDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- meanFishingDepth -->
                <row>
                  <cell>
                    <JLabel id='meanFishingDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='meanFishingDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- medianFishingDepth -->
                <row>
                  <cell>
                    <JLabel id='medianFishingDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='medianFishingDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- meanDeployementDepth -->
                <row>
                  <cell>
                    <JLabel id='meanDeployementDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='meanDeployementDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
                <!-- medianDeployementDepth -->
                <row>
                  <cell>
                    <JLabel id='medianDeployementDepthLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='medianDeployementDepth' constructorParams='this' styleClass='float2'/>
                  </cell>
                </row>
              </Table>

            </JPanel>

          </tab>

          <tab id='speciesTab'>

            <!-- species -->
            <JPanel layout='{new BorderLayout()}'>
              <Table fill='both' constraints='BorderLayout.NORTH'>
                <row>
                  <cell weighty="1" weightx="1">
                    <FilterableDoubleList id='species'
                                          genericType='ReferentialReference&lt;SpeciesDto&gt;'
                                          _entityClass='SpeciesDto.class'/>
                  </cell>
                </row>
              </Table>
            </JPanel>

          </tab>

        </JTabbedPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
