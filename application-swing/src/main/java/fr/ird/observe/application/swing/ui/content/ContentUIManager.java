/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content;

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.impl.longline.LonglineDetailCompositionUI;
import fr.ird.observe.application.swing.ui.content.impl.longline.LonglineGlobalCompositionUI;
import fr.ird.observe.application.swing.ui.content.impl.longline.SetLonglineUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.FloatingObjectTransmittingBuoyOperationUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.FloatingObjectUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.SetSeineUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.RoutesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.TripSeinesUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.TripLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.RouteUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.TripSeineUI;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI;
import fr.ird.observe.application.swing.ui.content.ref.ReferenceHomeUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.CatchLonglineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.EncounterUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.GearUseFeaturesLonglineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.SensorUsedUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.TdrUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.GearUseFeaturesSeineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.NonTargetCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.NonTargetSampleUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.ObjectObservedSpeciesUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.ObjectSchoolEstimateUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.SchoolEstimateUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetDiscardCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetSampleCaptureUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetSampleRejeteUI;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialHelper;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.DiscardedTargetCatchDto;
import fr.ird.observe.services.dto.seine.DiscardedTargetSampleDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.KeptTargetCatchDto;
import fr.ird.observe.services.dto.seine.KeptTargetSampleDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.CardLayout2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.Component;
import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.n;

/**
 * Manager des ecrans d'editions
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ContentUIManager {

    private static final String REFERENCE = "reference";

    public static final String DATA = "data";

    /** Logger */
    static private final Log log = LogFactory.getLog(ContentUIManager.class);

    private final Map<String, Class<? extends ObserveContentUI<?>>> mapping;

    public ContentUIManager() {

        mapping = new TreeMap<>();

        // --- Seine data --- //
        addMapping(DATA, ProgramDto.class, GearType.seine.name(), TripSeinesUI.class);

        addMapping(DATA, TripSeineDto.class, null, TripSeineUI.class);
        addMapping(DATA, TripSeineDto.class, n("observe.type.tripSeine.unsaved"), TripSeineUI.class);
        addMapping(DATA, TripSeineDto.class, GearUseFeaturesSeineDto.class, GearUseFeaturesSeineUI.class);
        addMapping(DATA, TripSeineDto.class, RouteDto.class, RoutesUI.class);

        addMapping(DATA, RouteDto.class, null, RouteUI.class);
        addMapping(DATA, RouteDto.class, n("observe.type.route.unsaved"), RouteUI.class);
        addMapping(DATA, RouteDto.class, ActivitySeineDto.class, ActivitySeinesUI.class);

        addMapping(DATA, ActivitySeineDto.class, null, ActivitySeineUI.class);
        addMapping(DATA, ActivitySeineDto.class, n("observe.type.activitySeine.unsaved"), ActivitySeineUI.class);

        addMapping(DATA, SetSeineDto.class, null, SetSeineUI.class);
        addMapping(DATA, SetSeineDto.class, n("observe.type.setSeine.unsaved"), SetSeineUI.class);
        addMapping(DATA, SetSeineDto.class, SchoolEstimateDto.class, SchoolEstimateUI.class);
        addMapping(DATA, SetSeineDto.class, KeptTargetCatchDto.class, TargetCatchUI.class);
        addMapping(DATA, SetSeineDto.class, DiscardedTargetCatchDto.class, TargetDiscardCatchUI.class);
        addMapping(DATA, SetSeineDto.class, NonTargetCatchDto.class, NonTargetCatchUI.class);

        addMapping(DATA, SetSeineDto.class, KeptTargetSampleDto.class, TargetSampleCaptureUI.class);
        addMapping(DATA, SetSeineDto.class, DiscardedTargetSampleDto.class, TargetSampleRejeteUI.class);
        addMapping(DATA, SetSeineDto.class, NonTargetSampleDto.class, NonTargetSampleUI.class);

        addMapping(DATA, FloatingObjectDto.class, null, FloatingObjectUI.class);
        addMapping(DATA, FloatingObjectDto.class, n("observe.type.floatingObject.unsaved"), FloatingObjectUI.class);
        addMapping(DATA, FloatingObjectDto.class, ObjectOperationDto.class, FloatingObjectTransmittingBuoyOperationUI.class);
        addMapping(DATA, FloatingObjectDto.class, ObjectObservedSpeciesDto.class, ObjectObservedSpeciesUI.class);
        addMapping(DATA, FloatingObjectDto.class, ObjectSchoolEstimateDto.class, ObjectSchoolEstimateUI.class);

        // --- Longline data --- //

        addMapping(DATA, ProgramDto.class, GearType.longline.name(), TripLonglinesUI.class);

        addMapping(DATA, TripLonglineDto.class, null, TripLonglineUI.class);
        addMapping(DATA, TripLonglineDto.class, n("observe.type.tripLongline.unsaved"), TripLonglineUI.class);
        addMapping(DATA, TripLonglineDto.class, GearUseFeaturesLonglineDto.class, GearUseFeaturesLonglineUI.class);
        addMapping(DATA, TripLonglineDto.class, ActivityLonglineDto.class, ActivityLonglinesUI.class);

        addMapping(DATA, ActivityLonglineDto.class, null, ActivityLonglineUI.class);
        addMapping(DATA, ActivityLonglineDto.class, n("observe.type.activityLongline.unsaved"), ActivityLonglineUI.class);
        addMapping(DATA, ActivityLonglineDto.class, EncounterDto.class, EncounterUI.class);
        addMapping(DATA, ActivityLonglineDto.class, SensorUsedDto.class, SensorUsedUI.class);

        addMapping(DATA, SetLonglineDto.class, null, SetLonglineUI.class);
        addMapping(DATA, SetLonglineDto.class, n("observe.type.setLongline.unsaved"), SetLonglineUI.class);
        addMapping(DATA, SetLonglineDto.class, SetLonglineGlobalCompositionDto.class, LonglineGlobalCompositionUI.class);
        addMapping(DATA, SetLonglineDto.class, SetLonglineDetailCompositionDto.class, LonglineDetailCompositionUI.class);

        addMapping(DATA, SetLonglineDto.class, CatchLonglineDto.class, CatchLonglineUI.class);
        addMapping(DATA, SetLonglineDto.class, TdrDto.class, TdrUI.class);

        // --- Referential --- //

        loadReferentialsMapping(ReferentialHelper.REFERENCE_COMMON_DTOS, "impl");
        loadReferentialsMapping(ReferentialHelper.REFERENCE_SEINE_DTOS, "impl.seine");
        loadReferentialsMapping(ReferentialHelper.REFERENCE_LONGLINE_DTOS, "impl.longline");

        addMapping(REFERENCE, null, null, ReferenceHomeUI.class);

    }

    public Class<? extends ObserveContentUI<?>> convertNodeToContentUI(ObserveNode node) {
        if (log.isDebugEnabled()) {
            log.debug("Entrer for node = " + node);
        }
        if (node.isRoot()) {
            return null;
        }

        Class<?> editType = node.getInternalClass();
        String prefix = node.isReferentielNode() ? REFERENCE : DATA;
        String context = node.getContext();

        String mappingKey;

        if (node.isReferentielNode()) {

            if (String.class.equals(editType)) {
                editType = null;
            }

        } else {

            // noeud de donnee
            if (String.class.equals(editType)) {
                editType = node.getContainerNode().getInternalClass();
            }
        }

        mappingKey = getMappingKey(prefix, editType, context);
        if (mappingKey == null) {
            throw new NullPointerException("Could not find mappingKey for node " + node);
        }
        if (log.isDebugEnabled()) {
            log.debug("mappingKey = [" + mappingKey + "] for node " + node);
        }

        return mapping.get(mappingKey);
    }

    public <U extends ObserveContentUI<?>> U getContent(Class<U> uiClass) {

        CardLayout2 layout = getLayout();
        JPanel layoutContent = getLayoutContent();
        String constraints = uiClass.getName();

        if (!layout.contains(constraints)) {

            // pas trouvé
            return null;
        }

        U content = (U) layout.getComponent(layoutContent, constraints);

        if (log.isDebugEnabled()) {
            log.debug("Will use existing content [" + constraints + "] : " + content.getClass().getName());
        }
        return content;
    }

    public <U extends ObserveContentUI<?>> U createContent(Class<U> uiClass) {

        String constraints = uiClass.getName();

//        if (getLayout().contains(constraints)) {
//            throw new IllegalStateException("Already existing constrainst [" + constraints + "]");
//        }
        U result;
        try {
            Constructor<U> constructor = uiClass.getConstructor(JAXXContext.class);
            if (log.isDebugEnabled()) {
                log.debug("create new content : " + uiClass);
            }
            result = constructor.newInstance(new JAXXInitialContext().add(getMainUI()));
        } catch (Exception e) {
            throw new IllegalStateException("Could not create content ui " + uiClass, e);
        }

        try {

            // ajout du content dans son parent
            getLayoutContent().add((JComponent) result, constraints);

            if (log.isDebugEnabled()) {
                log.debug("Add new content [" + constraints + "] : " + result.getClass().getName());
            }

            // initialisation du content
            result.init();

            return result;
        } catch (Exception e) {
            throw new IllegalStateException("Could not init content ui " + uiClass, e);
        }
    }

    public ObserveContentUI<?> getCurrentContent() {
        return (ObserveContentUI<?>)
                getLayout().getVisibleComponent(getLayoutContent());
    }

    public void openContent(ObserveContentUI<?> content) {

        String constraints = content.getClass().getName();

        if (log.isDebugEnabled()) {
            log.debug("Will open ui [" + constraints + "] : " + content.getClass());
        }

        // on ouvre l'ui
        try {
            content.open();

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {

            // on affiche l'ui quoi qu'il arrive ?
            getLayout().show(getLayoutContent(), constraints);
        }
    }

    public void close() {
        getLayout().reset(getLayoutContent());
    }

    public ContentUI<?> getSelectedContentUI() {
        return getSelectedContentUI(getMainUI());
    }

    public boolean closeSelectedContentUI() {
        return closeSelectedContentUI(getMainUI());
    }

    /**
     * Essaye de fermer l'écran d'édition s'il existe.
     *
     * @param mainUI l'ui principale
     * @return {@code true} si le contenu a bien été fermé, {@code false} si on
     * ne peut pas fermer l'écran
     * @since 1.5
     */
    public boolean closeSelectedContentUI(ObserveMainUI mainUI) {
        ContentUI<?> ui = getSelectedContentUI(mainUI);
        if (ui == null) {
            // no content ui
            return true;
        }
        boolean closed = false;
        try {
            closed = ui.close();
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
        return closed;
    }

    public void removeSelectedContentUI() {

        ContentUI<?> selectedContentUI = getSelectedContentUI();
        if (selectedContentUI != null) {
            getLayout().removeLayoutComponent(selectedContentUI, selectedContentUI.getClass().getName());
            getLayoutContent().remove(selectedContentUI);
            selectedContentUI.destroy();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }

    private void loadReferentialsMapping(ImmutableSet<Class<? extends ReferentialDto>> types, String packagePrefix) {
        for (Class<? extends ReferentialDto> editType : types) {
            String simpleName = StringUtils.removeEnd(editType.getSimpleName(), "Dto");
            String fqn = ContentReferenceUI.class.getPackage().getName() + "." + packagePrefix + "." + simpleName + "UI";
            Class<?> result;
            try {
                result = Class.forName(fqn);
                addMapping(REFERENCE, null, editType, result);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Could not find " + fqn);
            }
        }

    }

    private JPanel getLayoutContent() {
        return getMainUI().getContent();
    }

    private CardLayout2 getLayout() {
        return getMainUI().getContentLayout();
    }

    private ObserveMainUI getMainUI() {
        return ObserveSwingApplicationContext.get().getMainUI();
    }

    private ContentUI<?> getSelectedContentUI(ObserveMainUI ui) {

        if (ui == null) {
            // no ui, so no modification
            return null;
        }

        ContentUI<?> result = null;
        CardLayout2 layout = ui.getContentLayout();
        JPanel container = ui.getContent();
        Component currentContent = layout.getVisibleComponent(container);
        if (currentContent != null && currentContent instanceof ContentUI<?>) {

            result = (ContentUI<?>) currentContent;
        }
        return result;
    }

    private void addMapping(String prefix, Class<?> klass, Object context, Class<?> contentClass) {

        String contextValue = context instanceof Class ? ((Class) context).getName() : (context == null ? null : context.toString());
        String key = getMappingKey(prefix, klass, contextValue);
        if (log.isDebugEnabled()) {
            log.debug("Add key: " + key + " → " + contentClass.getName());
        }
        mapping.put(key, (Class<? extends ContentUI<?>>) contentClass);
    }

    private String getMappingKey(String prefix, Class<?> klass, String context) {
        String key = prefix + ".";
        if (klass != null) {
            key += klass.getName();
        }
        if (context != null) {
            if (klass != null) {
                key += "#";
            }
            key += context;
        }
        return key;
    }
}
