package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import jaxx.runtime.SwingUtil;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class CloseHelpAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public CloseHelpAction(ObserveMainUI ui) {

        super(null, SwingUtil.getUIManagerActionIcon("closeTab"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.quitHelp.tip"));
        putValue(MNEMONIC_KEY, (int) 'F');

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        ObserveUIMode oldMode = ui.getContextValue(ObserveUIMode.class, "oldMode");
        if (oldMode == null) {

            // on regarde si une base est chargee
            ObserveSwingDataSource mainStorage = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

            if (mainStorage == null) {
                oldMode = ObserveUIMode.NO_DB;
            } else {
                oldMode = ObserveUIMode.DB;
            }
        }
        ui.setMode(oldMode);

    }

}
