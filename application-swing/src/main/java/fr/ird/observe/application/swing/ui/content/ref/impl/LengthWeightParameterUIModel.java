package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class LengthWeightParameterUIModel extends ContentReferenceUIModel<LengthWeightParameterDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_OTHER_TAB_VALID = "otherTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(LengthWeightParameterDto.PROPERTY_URI,
                                               LengthWeightParameterDto.PROPERTY_CODE,
                                               LengthWeightParameterDto.PROPERTY_STATUS,
                                               LengthWeightParameterDto.PROPERTY_SEX,
                                               LengthWeightParameterDto.PROPERTY_OCEAN,
                                               LengthWeightParameterDto.PROPERTY_SPECIES,
                                               LengthWeightParameterDto.PROPERTY_NEED_COMMENT,
                                               LengthWeightParameterDto.PROPERTY_START_DATE,
                                               LengthWeightParameterDto.PROPERTY_END_DATE).build();

    public static final Set<String> OTHER_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(LengthWeightParameterDto.PROPERTY_MEAN_LENGTH,
                                               LengthWeightParameterDto.PROPERTY_MEAN_WEIGHT,
                                               LengthWeightParameterDto.PROPERTY_WEIGHT_LENGTH_FORMULA,
                                               LengthWeightParameterDto.PROPERTY_WEIGHT_LENGTH_FORMULA_VALID,
                                               LengthWeightParameterDto.PROPERTY_LENGTH_WEIGHT_FORMULA,
                                               LengthWeightParameterDto.PROPERTY_LENGTH_WEIGHT_FORMULA_VALID,
                                               LengthWeightParameterDto.PROPERTY_COEFFICIENTS).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean otherTabValid;

    public LengthWeightParameterUIModel() {
        super(LengthWeightParameterDto.class,
              null,
              new String[]{
                      LengthWeightParameterDto.PROPERTY_SEX,
                      LengthWeightParameterDto.PROPERTY_OCEAN,
                      LengthWeightParameterDto.PROPERTY_SPECIES,
                      LengthWeightParameterDto.PROPERTY_START_DATE,
              },
              null
        );
    }

    public boolean isOtherTabValid() {
        return otherTabValid;
    }

    public void setOtherTabValid(boolean otherTabValid) {
        Object oldValue = isOtherTabValid();
        this.otherTabValid = otherTabValid;
        firePropertyChange(PROPERTY_OTHER_TAB_VALID, oldValue, otherTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
