/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.open.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.referential.PersonHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.VesselHelper;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineHelper;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIHandler;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIModel;
import fr.ird.observe.application.swing.ui.util.tripMap.TripMapUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
class TripSeineUIHandler extends ContentOpenableUIHandler<TripSeineDto> {

    /** Logger */
    static private final Log log = LogFactory.getLog(TripSeineUIHandler.class);

    private boolean buildTripMap = true;

    TripSeineUIHandler(TripSeineUI ui) {
        super(ui,
              DataContextType.Program,
              DataContextType.TripSeine,
              n("observe.content.tripSeine.message.not.open"));
    }


    @Override
    public TripSeineUI getUi() {
        return (TripSeineUI) super.getUi();
    }

    @Override
    public boolean doCloseData() {
        boolean result = getOpenDataManager().isOpenTripSeine(getSelectedId());
        if (result) {
            getOpenDataManager().closeTripSeine(getSelectedId());
        }
        return result;
    }

    @Override
    public void initUI() {
        super.initUI();

        TripSeineUI ui = getUi();
        TripMapUI tripMap = ui.getTripMap();
        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();

        tripMap.getHandler().setConfig(config);

        getUi().getTripSeineTabPane().addChangeListener(e -> {
            JTabbedPane tripSeineTabPane = (JTabbedPane) e.getSource();
            TripSeineUI ui1 = getUi();
            TripMapUI tripMap1 = ui1.getTripMap();
            if (tripSeineTabPane.getSelectedComponent().equals(tripMap1)) {
                ui1.getActions().setVisible(false);

                if (buildTripMap) {
                    SwingUtilities.invokeLater(() -> {
                        ObserveSwingApplicationContext.get().getMainUI().setBusy(true);
                        try {
                            TripSeineUI ui11 = getUi();
                            TripMapUI tripMap11 = ui11.getTripMap();
                            TripMapDto tripSeineMap = getTripSeineService().getTripSeineMap(getSelectedId());
                            tripMap11.getHandler().doOpenMap(tripSeineMap);
                        } finally {
                            ObserveSwingApplicationContext.get().getMainUI().setBusy(false);
                        }
                    });
                    buildTripMap = false;
                }

            } else {
                ui1.getActions().setVisible(true);
            }
        });

        getUi().getVessel().getIndexes().setSelectedButton(1);
        getUi().getDepartureHarbour().getIndexes().setSelectedButton(1);
        getUi().getLandingHarbour().getIndexes().setSelectedButton(1);

    }

    @Override
    public void openUI() {
        super.openUI();

        ContentMode mode = computeContentMode();

        String programId = getSelectedParentId();
        final String tripId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "programId = " + programId);
            log.info(prefix + "tripId    = " + tripId);
            log.info(prefix + "mode      = " + mode);
        }

        TripSeineDto bean = getBean();

        boolean create = tripId == null;

        Form<TripSeineDto> form;
        if (create) {

            // create mode
            if (log.isInfoEnabled()) {
                log.info(prefix + "create a new trip");
            }
            form = getTripSeineService().preCreate(programId);

        } else {

            // update mode
            if (log.isInfoEnabled()) {
                log.info(prefix + "load existing trip " + tripId);
            }
            form = getTripSeineService().loadForm(tripId);

        }

        setContentMode(mode);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        TripSeineHelper.copyTripSeineDto(form.getObject(), bean);

        getUi().getTripSeineTabPane().setSelectedIndex(0);
        getUi().getTripMap().getHandler().doCloseMap();
        buildTripMap = true;

        finalizeOpenUI(mode, create);
    }

    @Override
    public void startEditUI(String... binding) {

        TripSeineUI ui = getUi();

        ContentOpenableUIModel<TripSeineDto> model = getModel();

        ContentMode mode = model.getMode();

        boolean create = mode == ContentMode.CREATE;

        String contextName = getValidatorContextName(mode);
        ui.getValidator().setContext(contextName);

        if (create) {

            addInfoMessage(t("observe.content.tripSeine.message.creating"));
        } else {
            addInfoMessage(t("observe.content.tripSeine.message.updating"));
            if (model.isHistoricalData()) {

                addInfoMessage(t("observe.message.historical.data"));
            }
        }

        // date is current day
        if (model.getMode() == ContentMode.UPDATE) {
            if (getBean().getEndDate() == null) {
                Date date = DateUtil.getEndOfDay(new Date());
                getBean().setEndDate(date);
                if (log.isDebugEnabled()) {
                    log.debug("end date : " + date);
                }
            }
        }

        super.startEditUI(TripSeineUI.BINDING_VESSEL_SELECTED_ITEM,
                          TripSeineUI.BINDING_OBSERVER_SELECTED_ITEM,
                          TripSeineUI.BINDING_CAPTAIN_SELECTED_ITEM,
                          TripSeineUI.BINDING_DATA_ENTRY_OPERATOR_SELECTED_ITEM,
                          TripSeineUI.BINDING_OCEAN_SELECTED_ITEM,
                          TripSeineUI.BINDING_START_DATE_DATE,
                          TripSeineUI.BINDING_END_DATE_DATE,
                          TripSeineUI.BINDING_COMMENT2_TEXT,
                          TripSeineUI.BINDING_ERS_ID_TEXT,
                          TripSeineUI.BINDING_CLOSE_ENABLED,
                          TripSeineUI.BINDING_CLOSE_AND_CREATE_ENABLED);
        model.setModified(create);
    }

    @Override
    protected boolean doOpenData() {
        boolean result = getOpenDataManager().canOpenTripSeine();
        if (result) {
            getOpenDataManager().openTripSeine(getSelectedParentId(), getSelectedId());
        }
        return result;
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String tripSeineId = getSelectedId();

        if (tripSeineId == null) {

            // maree en cours de creation
            return ContentMode.CREATE;
        }

        if (getOpenDataManager().isOpenTripSeine(tripSeineId)) {

            // maree ouverte
            return ContentMode.UPDATE;
        }

        addInfoMessage(t(closeMessage));
        return ContentMode.READ;

    }

    @Override
    protected boolean doSave(TripSeineDto bean) throws Exception {

        boolean notPersisted = bean.isNotPersisted();

        // on force toujours la date a etre sans heure, minute,...
        Date startDate = DateUtil.getDay(bean.getStartDate());
        if (log.isDebugEnabled()) {
            log.debug("startDate = " + startDate);
        }
        bean.setStartDate(startDate);

        Date endDate = bean.getEndDate();
        if (log.isDebugEnabled()) {
            log.debug("endDate   = " + endDate);
        }

        SaveResultDto saveResult = getTripSeineService().save(bean);
        saveResult.toDto(bean);

        // recuperation de la position de la maree dans le program
        obtainChildPosition(bean);

        // ouverture de la marée
        if (notPersisted) {
            getOpenDataManager().openTripSeine(getSelectedParentId(), bean.getId());
        }

        return true;
    }

    protected int getOpenablePosition(String parentId, TripSeineDto bean) {

        return getTripSeineService().getTripSeinePositionInProgram(parentId, bean.getId());
    }

    @Override
    protected boolean doDelete(TripSeineDto bean) {

        if (askToDelete(bean)) {
            return false;
        }
        if (log.isInfoEnabled()) {
            log.info("Will delete Trip " + bean.getId());
        }

        getTripSeineService().delete(bean.getId());
        getOpenDataManager().closeTripSeine(bean.getId());

        if (log.isInfoEnabled()) {
            log.info("Delete done for Trip " + bean.getId());
        }
        return true;
    }

    @Override
    protected boolean obtainCanReopen(boolean create) {
        return !create && getOpenDataManager().canOpenTripSeine();
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case TripSeineDto.PROPERTY_CAPTAIN: {
                result = (List) PersonHelper.filterCaptainReferences((List) result);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }
            break;

            case TripSeineDto.PROPERTY_OBSERVER: {
                result = (List) PersonHelper.filterObserverReferences((List) result);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }
            break;

            case TripSeineDto.PROPERTY_DATA_ENTRY_OPERATOR: {
                result = (List) PersonHelper.filterDataEntryOperatorReferences((List) result);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }
            break;

            case TripSeineDto.PROPERTY_VESSEL: {
                ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();
                result = (List) VesselHelper.filterVesselReferencesByVesselTypeIds((List) result, config.getSeineVesselTypeIds());
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }
            break;

        }

        return result;
    }

    private TripSeineService getTripSeineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
    }

}
