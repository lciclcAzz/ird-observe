/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveDataProvider;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;

import java.util.Objects;
import java.util.Set;

/**
 * Un object pour charger les fils d'un noeud.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class AbstractNodeChildLoador<T, O> extends NavTreeNodeChildLoador<T, O, ObserveNode> {

    private static final long serialVersionUID = 1L;

    protected AbstractNodeChildLoador(Class<O> beanType) {
        super(beanType);
    }

    protected ObserveSwingDataSource getDataSource(NavDataProvider dataProvider) {
        ObserveDataProvider provider = (ObserveDataProvider) dataProvider;
        return provider.getDataSource();
    }

    protected DataSelectionModel getSelectionModel(NavDataProvider dataProvider) {
        ObserveDataProvider provider = (ObserveDataProvider) dataProvider;
        return provider.getSelectionModel();
    }

    ObserveNode createNode0(Set<Class> pluralizeProperties, Class data) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        if (pluralizeProperties.contains(data)) {
            return createPluralizeStringNode(data, null);
        } else {
            return createStringNode(data, null);
        }
    }

    <L extends AbstractNodeChildLoador<?, ?>> ObserveNode createStringNode(Class<?> context, Class<L> loadorType) {
        Objects.requireNonNull(context, "Ne peut pas ajouter un context null");
        L childLoador = loadorType == null ? null : ObserveTreeHelper.getChildLoador(loadorType);

        String propertyLabel = ObserveI18nDecoratorHelper.getTypeI18nKey(context);
        return new ObserveNode(String.class,
                               propertyLabel,
                               context.getName(),
                               childLoador,
                               false);

    }

    <L extends AbstractNodeChildLoador<?, ?>> ObserveNode createPluralizeStringNode(Class<?> context, Class<L> loadorType) {
        Objects.requireNonNull(context, "Ne peut pas ajouter un context null");
        L childLoador = loadorType == null ? null : ObserveTreeHelper.getChildLoador(loadorType);

        String propertyLabel = ObserveI18nDecoratorHelper.getTypePluralI18nKey(context);
        return new ObserveNode(String.class,
                               propertyLabel,
                               context.getName(),
                               childLoador,
                               false);
    }


}
