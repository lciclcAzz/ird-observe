package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ObjectSchoolEstimateUIModel extends ContentTableUIModel<FloatingObjectSchoolEstimateDto, ObjectSchoolEstimateDto> {

    private static final long serialVersionUID = 1L;

    public ObjectSchoolEstimateUIModel(ObjectSchoolEstimateUI ui) {

        super(FloatingObjectSchoolEstimateDto.class,
              ObjectSchoolEstimateDto.class,
              new String[]{
                      FloatingObjectSchoolEstimateDto.PROPERTY_OBJECT_SCHOOL_ESTIMATE,
                      FloatingObjectSchoolEstimateDto.PROPERTY_COMMENT},
              new String[]{
                      ObjectSchoolEstimateDto.PROPERTY_SPECIES,
                      ObjectSchoolEstimateDto.PROPERTY_TOTAL_WEIGHT}
        );

        List<ContentTableMeta<ObjectSchoolEstimateDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(ObjectSchoolEstimateDto.class, ObjectSchoolEstimateDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(ObjectSchoolEstimateDto.class, ObjectSchoolEstimateDto.PROPERTY_TOTAL_WEIGHT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<FloatingObjectSchoolEstimateDto, ObjectSchoolEstimateDto> createTableModel(
            ObserveContentTableUI<FloatingObjectSchoolEstimateDto, ObjectSchoolEstimateDto> ui,
            List<ContentTableMeta<ObjectSchoolEstimateDto>> contentTableMetas) {

        return new ContentTableModel<FloatingObjectSchoolEstimateDto, ObjectSchoolEstimateDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<ObjectSchoolEstimateDto> getChilds(FloatingObjectSchoolEstimateDto bean) {
                return bean.getObjectSchoolEstimate();
            }

            @Override
            protected void load(ObjectSchoolEstimateDto source, ObjectSchoolEstimateDto target) {
                ObjectSchoolEstimateHelper.copyObjectSchoolEstimateDto(source, target);
            }

            @Override
            protected void setChilds(FloatingObjectSchoolEstimateDto parent, List<ObjectSchoolEstimateDto> childs) {
                parent.setObjectSchoolEstimate(childs);
            }
        };
    }
}
