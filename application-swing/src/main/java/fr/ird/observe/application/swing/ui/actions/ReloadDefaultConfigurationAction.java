package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUICallback;
import jaxx.runtime.SwingUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ReloadDefaultConfigurationAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReloadDefaultConfigurationAction.class);

    private final ObserveMainUI ui;

    public ReloadDefaultConfigurationAction(ObserveMainUI ui) {

        super(t("observe.action.reloadDefaultConfiguration"), SwingUtil.getUIManagerActionIcon("application-reload"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.reloadDefaultConfiguration.tip"));
        putValue(MNEMONIC_KEY, (int) 'R');

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if (log.isInfoEnabled()) {
            log.info("ObServe reloading default configuration...");
        }

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {
            ObserveSwingApplicationConfig config = ui.getConfig();

            File directory = config.getResourcesDirectory();

            if (log.isInfoEnabled()) {
                log.info(t("observe.message.delete.directory", directory));
            }

            // suppression du répertoire des ressources
            try {
                FileUtils.deleteDirectory(directory);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not delete resources directory: " + directory, e);
                }
            }

            ObserveUICallback.application.run();
        }

    }
}
