package fr.ird.observe.application.swing.ui.content.open.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIHandler;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivityLonglineNodeChildLoador;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineHelper;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.SwingUtilities;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 8/29/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
class ActivityLonglineUIHandler extends ContentOpenableUIHandler<ActivityLonglineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ActivityLonglineUIHandler.class);

    static {

        //FIXME Remove this
        n("observe.common.latitude");
        n("observe.common.longitude");

    }

    public ActivityLonglineUIHandler(ActivityLonglineUI ui) {
        super(ui,
              DataContextType.TripLongline,
              DataContextType.ActivityLongline,
              n("observe.storage.activityLongline.message.not.open"));
    }

    @Override
    public ActivityLonglineUI getUi() {
        return (ActivityLonglineUI) super.getUi();
    }

    @Override
    protected boolean doOpenData() {
        boolean result = getOpenDataManager().canOpenActivityLongline(getSelectedParentId());
        if (result) {
            getOpenDataManager().openActivityLongline(getSelectedParentId(), getSelectedId());
        }
        return result;
    }

    @Override
    public boolean doCloseData() {
        boolean result = getOpenDataManager().isOpenActivityLongline(getSelectedId());
        if (result) {
            getOpenDataManager().closeActivityLongline(getSelectedId());
        }
        return result;
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String activityId = getSelectedId();

        if (activityId == null) {

            // mode creation
            return ContentMode.CREATE;
        }

        // l'activity existe en base
        if (getOpenDataManager().isOpenActivityLongline(getSelectedId())) {

            // l'activity est ouverte, donc modifiable
            return ContentMode.UPDATE;
        }

        ActivityLonglineUI ui = getUi();

        // l'activity n'est pas ouverte, donc pas éditable
        if (!getOpenDataManager().isOpenTripLongline(getSelectedParentId())) {

            // la marée n'est pas ouverte
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(TripLonglineDto.class),
                       t("observe.content.tripLongline.message.not.open"));

            if (getModel().isHistoricalData()) {

                addInfoMessage(t("observe.message.historical.data"));
            }

        } else {

            // seule l'activity n'est pas ouverte
            addInfoMessage(t(closeMessage));
        }

        return ContentMode.READ;
    }

    @Override
    public void openUI() {
        super.openUI();

        getUi().getCoordinatesEditor().resetModel();

        String tripId = getSelectedParentId();
        String activityId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "tripId     = " + tripId);
            log.info(prefix + "activityId = " + activityId);
        }

        ContentMode mode = computeContentMode();
        if (log.isInfoEnabled()) {
            log.info(prefix + "content mode " + mode);
        }
        ActivityLonglineDto bean = getBean();

        boolean create = activityId == null;

        Form<ActivityLonglineDto> form;
        if (create) {

            // create mode
            form = getActivityLonglineService().preCreate(tripId);

        } else {

            // update mode
            form = getActivityLonglineService().loadForm(activityId);

        }

        setContentMode(mode);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        ActivityLonglineHelper.copyActivityLonglineDto(form.getObject(), bean);

        if (log.isDebugEnabled()) {
            log.debug("           long  - lat  = " + bean.getLongitude() +
                              " - " + bean.getLatitude());
        }
        finalizeOpenUI(mode, create);

        // Mise à jour du composant de coordonnées
        // 1. Mise à jour latitude/longitude:
        getUi().getCoordinatesEditor().setLatitudeAndLongitude(bean.getLatitude(), bean.getLongitude());

        // 2. Mise à jour du quadrant :
        // Si le bean de données contient un quadrant, on met simplement à jour le composant de coordonnées pour sélectionner le quadrant voulu
        // sinon, on réinitialise les quadrants du composant afin qu'aucun d'eux ne soit sélectionné (par exemple dans le cas de la création de la première activité d'une marée)
        if (bean.getQuadrant() == null) {
            getUi().getCoordinatesEditor().resetQuadrant();
        } else {
            getUi().getCoordinatesEditor().setQuadrant(bean.getQuadrant());
        }

        // on annule la modification engendree par ce binding
        getModel().setModified(create);
    }

    @Override
    public void startEditUI(String... binding) {
        ActivityLonglineUI ui = getUi();

        ContentUIModel<ActivityLonglineDto> model = getModel();

        boolean create = model.getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(model.getMode());
        ui.getValidator().setContext(contextName);
        if (create) {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivityLonglineDto.class),
                       t("observe.storage.activityLongline.message.creating"));
        } else {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivityLonglineDto.class),
                       t("observe.storage.activityLongline.message.updating"));
        }

        super.startEditUI(
                ActivityLonglineUI.BINDING_TIME_STAMP_DATE,
                ActivityLonglineUI.BINDING_SEA_SURFACE_TEMPERATURE_MODEL,
                ActivityLonglineUI.BINDING_COMMENT2_TEXT,
                ActivityLonglineUI.BINDING_VESSEL_ACTIVITY_LONGLINE_SELECTED_ITEM,
                ActivityLonglineUI.BINDING_FPA_ZONE_SELECTED_ITEM,
                ActivityLonglineUI.BINDING_CLOSE_ENABLED,
                ActivityLonglineUI.BINDING_CLOSE_AND_CREATE_ENABLED);
        model.setModified(create);
    }

    @Override
    protected boolean doSave(ActivityLonglineDto bean) throws Exception {

        boolean notPersisted = bean.isNotPersisted();

        if (log.isDebugEnabled()) {
            log.debug("           long  - lat = " + bean.getLongitude() + " - " + bean.getLatitude());
        }

        String tripId = getSelectedParentId();

        TripChildSaveResultDto saveResult = getActivityLonglineService().save(tripId, getModel().getBean());
        saveResult.toDto(bean);

        setUpdateMareeNodeTag(saveResult.isTripEndDateUpdated());

        obtainChildPosition(bean);

        if (notPersisted) {
            // ouverture de l'activité après création
            getOpenDataManager().openActivityLongline(getSelectedParentId(), bean.getId());

            if (ActivityLonglineNodeChildLoador.FISHING_OPERATION_ID.equals(bean.getVesselActivityLongline().getId())) {

                // création de l'opération de pêche
                SwingUtilities.invokeLater(() -> getUi().getAddSet().doClick());
            }
        }

        return true;
    }

    @Override
    protected int getOpenablePosition(String parentId, ActivityLonglineDto bean) {
        return getActivityLonglineService().getActivityLonglinePositionInTripLongline(parentId, bean.getId());
    }

    @Override
    protected boolean doDelete(ActivityLonglineDto bean) {

        if (askToDelete(bean)) {
            return false;
        }
        if (log.isInfoEnabled()) {
            log.info("Will delete Activity " + bean.getId());
        }

        String tripId = getSelectedParentId();
        boolean wasTripEndDateUpdated = getActivityLonglineService().delete(tripId, bean.getId());
        if (log.isInfoEnabled()) {
            log.info("Delete done for Activity " + bean.getId());
        }
        getOpenDataManager().closeActivityLongline(bean.getId());

        setUpdateMareeNodeTag(wasTripEndDateUpdated);
        return true;

    }

    @Override
    protected void afterSave(boolean refresh) {
        super.afterSave(refresh);
        repaintTripNode();
    }

    @Override
    protected void afterDelete() {
        super.afterDelete();
        repaintTripNode();
    }

    @Override
    protected boolean obtainCanReopen(boolean create) {

        return !create && getOpenDataManager().canOpenActivityLongline(getSelectedParentId());

    }

    private ActivityLonglineService getActivityLonglineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLonglineService();
    }
}
