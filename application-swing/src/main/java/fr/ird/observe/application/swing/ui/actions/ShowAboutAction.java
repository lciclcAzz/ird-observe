package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.vladsch.flexmark.ast.Node;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import jaxx.runtime.SwingUtil;
import org.nuiton.jaxx.widgets.about.AboutUI;
import org.nuiton.jaxx.widgets.about.AboutUIBuilder;
import org.nuiton.util.Resource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ShowAboutAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public ShowAboutAction(ObserveMainUI ui) {

        super(t("observe.action.about"), SwingUtil.getUIManagerActionIcon("about"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.about.tip"));
        putValue(MNEMONIC_KEY, KeyEvent.VK_P);

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        String name = ObserveRunner.getRunner().getRunnerName();

        File csvFile = new File(ui.getConfig().getI18nDirectory(), name + "-i18n.csv");
        String translateText;
        try {
            translateText = t("observe.about.translate.content", csvFile.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new ObserveSwingTechnicalException("Could not obtain url from " + csvFile, e);
        }

        AboutUI about;
        JPanel topPanel;
        String changelog = null;
        try {
            try (Reader resource = new InputStreamReader(getClass().getClassLoader().getResourceAsStream("META-INF/" + name + "-CHANGELOG.md"), StandardCharsets.UTF_8)) {
                Parser parser = Parser.builder().build();
                Node node = parser.parseReader(resource);
                HtmlRenderer htmlRenderer = HtmlRenderer.builder().build();
                changelog = htmlRenderer.render(node);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        about = AboutUIBuilder.builder(ui)
                .setIconPath("/icons/logo.png")
                .setTitle(t("observe.title.about"))
                .setBottomText(ui.getConfig().getCopyrightText())
                .addAboutTab(t("observe.about.message"), true)
                .addDefaultLicenseTab(name, false)
                .addDefaultThirdPartyTab(name, false)
                .addTab(t("aboutframe.changelog"), changelog, true)
                .addTab(t("observe.about.translate.title"), translateText, true)
                .build();

        // tweak top panel
        topPanel = about.getTopPanel();
        topPanel.removeAll();
        topPanel.setLayout(new BorderLayout());
        topPanel.add(new JLabel(Resource.getIcon("/icons/logo-small.png")), BorderLayout.WEST);
        topPanel.add(new JLabel(Resource.getIcon("/icons/logo_ird.png")), BorderLayout.EAST);

        about.display();

    }
}
