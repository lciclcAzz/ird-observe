package fr.ird.observe.application.swing.ui.actions;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.AbstractAction;
import javax.swing.Icon;
import java.beans.PropertyChangeListener;

/**
 * Created on 03/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class AbstractObserveAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    protected AbstractObserveAction(String name, Icon icon) {
        super(name, icon);
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (changeSupport != null) {
            changeSupport.addPropertyChangeListener(propertyName,listener);
        }

    }

    public synchronized void removePropertyChangeListener(String propertyName,PropertyChangeListener listener) {
        if (changeSupport != null) {
            changeSupport.removePropertyChangeListener(propertyName,listener);
        }
    }
}
