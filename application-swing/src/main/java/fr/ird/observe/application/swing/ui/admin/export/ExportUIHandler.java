/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import com.google.common.base.Preconditions;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUI;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.ImportTripResult;
import fr.ird.observe.services.service.trip.TripManagementService;
import jaxx.runtime.swing.editor.MyDefaultCellEditor;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ExportUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ExportUIHandler.class);

    public ExportUIHandler(AdminTabUI ui) {
        super(ui);
    }

    public void initTabUI(AdminUI ui, ExportUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        tabUI.getPrepareAction().setText(t("observe.actions.synchro.prepare.operation", t(tabUI.getStep().getOperationLabel())));
        tabUI.getStartAction().setText(t("observe.actions.synchro.launch.operation", t(tabUI.getStep().getOperationLabel())));

        ConfigUI configabUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);

        configabUI.getCentralSourceInfoLabel().setText(t("observe.action.config.export.required.write.data"));
        configabUI.getCentralSourceLabel().setText(t("observe.action.config.export.required.write.data"));

        SelectDataUI selectTabUI = (SelectDataUI) ui.getStepUI(AdminStep.SELECT_DATA);

        tabUI.getModel().addPropertyChangeListener(AdminUIModel.SELECTION_MODEL_CHANGED_PROPERTY_NAME, evt -> {
            AdminUIModel model1 = (AdminUIModel) evt.getSource();
//                if (!getModel().containsStep(tabUI.getStep())) {
//                    // avoid multi-cast
//                    return;
//                }

            DataSelectionModel value = (DataSelectionModel) evt.getNewValue();
            if (log.isInfoEnabled()) {
                log.info("selection model changed to " + value);
            }

            selectTabUI.getSelectionModel().clearSelection();
            selectTabUI.getSelectionRenderer().setExistingTrips(model1.getExportModel().getExistingTrips());
            updateSelectionModel(selectTabUI);
        });

        // tableau de l'export de données

        final JTable table4 = tabUI.getTrips();
        table4.setRowHeight(24);

        UIHelper.fixTableColumnWidth(table4, 0, 20);
        UIHelper.fixTableColumnWidth(table4, 3, 20);

        DefaultTableCellRenderer renderer5 = new DefaultTableCellRenderer();

        UIHelper.setI18nTableHeaderRenderer(
                table4,
                n("observe.actions.exportData.table.selected"),
                n("observe.actions.exportData.table.selected.tip"),
                n("observe.actions.exportData.table.program.label"),
                n("observe.actions.exportData.table.program.label.tip"),
                n("observe.actions.exportData.table.trip.label"),
                n("observe.actions.exportData.table.trip.label.tip"),
                n("observe.actions.exportData.table.exist.label"),
                n("observe.actions.exportData.table.exist.label.tip"));

        UIHelper.setTableColumnRenderer(table4, 0, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnRenderer(table4, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer5, ProgramDto.class));
        UIHelper.setTableColumnRenderer(table4, 2, UIHelper.newDecorateTableCellRenderer(renderer5, DataReference.class, DecoratorService.TRIP_CONTEXT));
        UIHelper.setTableColumnRenderer(table4, 3, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnEditor(table4, 0, MyDefaultCellEditor.newBooleanEditor(false));

        // pour tout selectionner - deselectionner dans l'entete du tableau
        table4.getTableHeader().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                int colIndex = table4.getTableHeader().columnAtPoint(e.getPoint());
                colIndex = table4.convertColumnIndexToModel(colIndex);
                if (colIndex == 0) {
                    TripToExportTableModel model = (TripToExportTableModel) table4.getModel();
                    boolean oldValue = model.isSelectAll();
                    // toggle selectAll
                    model.setSelectAll(!oldValue);
                }
            }
        });
    }

    public void updateState(ExportUI tabUI, WizardState newState) {
        super.updateState(tabUI, newState);
        if (newState == WizardState.NEED_FIX) {
            // met a jour la liste des marées
            tabUI.tripsModel.init(tabUI.getStepModel().getData());
        }
    }

    public void doPrepareAction() {

        addAdminWorker(((ExportUI) ui).getPrepareAction().getToolTipText(), this::doPrepareAction0);
    }

    public void doStartAction() {

        ExportUI tabUI = (ExportUI) ui;

        int[] rows = tabUI.getTripsModel().getSelected();
        tabUI.getModel().getExportModel().setExportDataSelectedIndex(rows);

        addAdminWorker(tabUI.getStartAction().getToolTipText(), this::doStartAction0);

    }

    private WizardState doPrepareAction0() throws Exception {

        DecoratorService decoratorProvider = getDecoratorService();
        ExportModel stepModel = model.getExportModel();

        ReferentialReferenceDecorator<ProgramDto> pDecorator = decoratorProvider.getReferentialReferenceDecorator(ProgramDto.class);
        DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator = decoratorProvider.getDataReferenceDecorator(TripLonglineDto.class);
        DataReferenceDecorator<TripSeineDto> tripSeineDecorator = decoratorProvider.getDataReferenceDecorator(TripSeineDto.class);
        stepModel.setProgramDecorator(pDecorator);
        stepModel.setTripSeineDecorator(tripSeineDecorator);
        stepModel.setTripLonglineDecorator(tripLonglineDecorator);

        ObserveSwingDataSource centralSource = model.getSafeCentralSource(false);

        stepModel.setCentralSource(centralSource);

        if (!centralSource.canWriteData()) {
            // l'utilisateur n'a pas le droit d'écrire sur la base distante
            sendMessage(t("observe.message.can.not.write.data"));
            return WizardState.FAILED;
        }

        ObserveSwingDataSource source = model.getSafeLocalSource(false);

        stepModel.setSource(source);

        // ouverture de la base distante
        openSource(centralSource);

        // ouverture base source
        openSource(source);

        // il s'agit de la première passe : préparation des données

        sendMessage(t("observe.actions.exportData.message.prepare.data"));

        // récupération des couples (program, marees) sur la base temporaire
        DataSelectionModel selectionModel = model.getSelectionDataModel();
        stepModel.setData(selectionModel);

        List<TripEntry> entries = stepModel.getData();

        if (entries.isEmpty()) {
            // pas de données possible à exporter
            sendMessage(t("observe.actions.exportData.message.not.possible"));
            sendMessage(t("observe.actions.operation.message.done", new Date()));
            return WizardState.CANCELED;
        }

        sendMessage(t("observe.actions.exportData.message.operation.needFix", new Date()));

        // on rend la main à l'ui pour que l'utilisateur selectionne les marees a exporter
        return WizardState.NEED_FIX;
    }

    private WizardState doStartAction0() throws Exception {

        ExportModel stepModel = model.getExportModel();

        // on filtre les marées sélectionnées pour export
        List<TripEntry> tripEntries = stepModel.getSelectedTrips();

        Preconditions.checkState(CollectionUtils.isNotEmpty(tripEntries));

        DecoratorService decoratorService = getDecoratorService();
        ReferentialReferenceDecorator<ProgramDto> programDecorator = decoratorService.getReferentialReferenceDecorator(ProgramDto.class);

        try (ObserveSwingDataSource localDataSource = openSource(stepModel.getSource())) {

            TripManagementService localTripManagementService = localDataSource.newTripManagementService();

            try (ObserveSwingDataSource centralDataSource = openSource(stepModel.getCentralSource())) {

                TripManagementService centralTripManagementService = centralDataSource.newTripManagementService();

                for (TripEntry tripEntry : tripEntries) {

                    ExportTripRequest exportTripRequest = new ExportTripRequest(false, tripEntry.getProgramId(), tripEntry.getTripId());
                    ExportTripResult exportTripResult = localTripManagementService.exportTrip(exportTripRequest);
                    logExportResult(n("observe.actions.exportData.result.export.trip"),
                                    exportTripResult,
                                    programDecorator,
                                    tripEntry.getProgram(),
                                    tripEntry.getTrip());

                    ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                    ImportTripResult importTripResult = centralTripManagementService.importTrip(importTripRequest);
                    logImportResult(n("observe.actions.exportData.result.import.trip"),
                                    n("observe.actions.exportData.result.delete.trip"),
                                    importTripResult,
                                    programDecorator,
                                    tripEntry.getProgram(),
                                    tripEntry.getTrip());

                }

            }
        }

        sendMessage(t("observe.actions.operation.message.done", new Date()));

        return WizardState.SUCCESSED;
    }


}
