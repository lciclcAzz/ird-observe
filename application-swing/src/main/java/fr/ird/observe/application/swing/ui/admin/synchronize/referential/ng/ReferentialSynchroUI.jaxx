<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!-- ************************************************************* -->
<!-- L'écran de synchronisation bi-directionnel des données        -->
<!-- ************************************************************* -->

<fr.ird.observe.application.swing.ui.admin.AdminTabUI>

  <import>
    fr.ird.observe.application.swing.ui.UIHelper
    fr.ird.observe.application.swing.ui.admin.AdminUI
    fr.ird.observe.application.swing.ui.admin.AdminStep
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.ApplyAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.RegisterCopyTaskAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.RegisterDeleteTaskAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.RegisterDesactivateTaskAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.RegisterRevertTaskAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action.RegisterSkipTaskAction
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel
    fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeCellRenderer
    fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper
    fr.ird.observe.application.swing.ui.util.tree.BeanTreeHeader
    javax.swing.ListSelectionModel

    static org.nuiton.i18n.I18n.t;

  </import>

  <ObserveTreeHelper id='leftTreeHelper'/>
  <ObserveTreeHelper id='rightTreeHelper'/>

  <ReferentialSynchroUIHandler id='handler' constructorParams='this'/>

  <ReferentialSynchroModel id='stepModel' javaBean='getModel().getReferentialSynchroModel()'/>

  <ReferentialSynchronizeTreeModel id="leftTreeModel" javaBean="getStepModel().getLeftTreeModel()"/>
  <ReferentialSynchronizeTreeModel id="rightTreeModel" javaBean="getStepModel().getRightTreeModel()"/>
  <JPanel id='invisiblePanel'>

  </JPanel>

  <script><![CDATA[
public ReferentialSynchroUI(AdminUI parentContext) {
    super(AdminStep.REFERENTIAL_SYNCHRONIZE, parentContext);
}

public void initUI(AdminUI ui) {
    getHandler().initTabUI(ui, this);
}

]]>
  </script>

  <JPanel id='PENDING_content'>
    <Table constraints='BorderLayout.CENTER' fill='both' weightx='1'
           weighty='1'>
      <row>
        <cell>
          <JButton id='startAction' onActionPerformed="getHandler().doStartAction()"/>
        </cell>
      </row>
    </Table>
  </JPanel>
  <JPanel id='NEED_FIX_content'>
    <JSplitPane id="contentSplitPane" constraints='BorderLayout.CENTER'>
      <Table id='contentNorth' fill="both" weighty="1">
        <row>
          <cell weightx="0.5">
            <JScrollPane id='leftTreePane'  columnHeaderView='{leftTreeHeader}'>
              <JTree id='leftTree'/>
              <BeanTreeHeader id='leftTreeHeader' tree='{leftTree}'/>
            </JScrollPane>
          </cell>
          <cell>
            <JPanel layout="{new BorderLayout()}" border='{new TitledBorder("")}'>
              <JToolBar id="middleActions" layout="{new GridLayout(0, 2)}" constraints='BorderLayout.CENTER'>
                <JButton id="copyLeft" opaque="true"/>
                <JButton id="copyRight" opaque="true"/>
                <JButton id="revertLeft" opaque="true"/>
                <JButton id="revertRight" opaque="true"/>
                <JButton id="desactivateLeft" opaque="false"/>
                <JButton id="desactivateRight" opaque="false"/>
                <JButton id="desactivateWithReplaceLeft" opaque="false"/>
                <JButton id="desactivateWithReplaceRight" opaque="false"/>
                <JButton id="deleteLeft" opaque="false"/>
                <JButton id="deleteRight" opaque="false"/>
                <JButton id="skipLeft" opaque="false"/>
                <JButton id="skipRight" opaque="false"/>
              </JToolBar>
            </JPanel>
          </cell>
          <cell weightx="0.5">
            <JScrollPane id='rightTreePane' columnHeaderView='{rightTreeHeader}'>
              <JTree id='rightTree'/>
              <BeanTreeHeader id='rightTreeHeader' tree='{rightTree}'/>
            </JScrollPane>
          </cell>
        </row>
      </Table>
      <Table id='contentSouth' fill="both">
        <row weighty="1">
          <cell weightx="1">
            <JScrollPane id='actionsToConsumePane'>
              <JList id="actionsToConsume"/>
            </JScrollPane>
          </cell>
        </row>
        <row>
          <cell>
            <JPanel layout="{new BorderLayout()}">
              <JButton id='applyAction' constraints='BorderLayout.CENTER'/>
            </JPanel>
          </cell>
        </row>
      </Table>
    </JSplitPane>

  </JPanel>

</fr.ird.observe.application.swing.ui.admin.AdminTabUI>
