package fr.ird.observe.application.swing.ui.util.table;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.io.Serializable;

/**
 * Created on 15/03/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class InlineTableAutotSelectRowAndShowPopupAction<E extends Serializable, M extends EditableTableModelSupport<E>> extends AutotSelectRowAndShowPopupActionSupport {

    private final JMenuItem addWidget;
    private final JMenuItem deleteWidget;
    private final M tableModel;

    public InlineTableAutotSelectRowAndShowPopupAction(JScrollPane scrollPane,
                                                       JTable table,
                                                       M tableModel,
                                                       JPopupMenu popup,
                                                       JMenuItem addWidget,
                                                       JMenuItem deleteWidget) {
        super(scrollPane, table, popup);
        this.addWidget = addWidget;
        this.deleteWidget = deleteWidget;
        this.tableModel = tableModel;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {

        boolean canAdd = tableModel.isValid();
        boolean canDelete = !tableModel.isSelectionEmpty();

        if (canDelete) {

            // check also that the row is not empty
            E selectedData = tableModel.getSelectedRow();
            canDelete = tableModel.isRowNotEmpty(selectedData);

        }

        addWidget.setEnabled(canAdd);
        deleteWidget.setEnabled(canDelete);

    }

}
