/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.open;

import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;

/**
 * Le modèle pour un écran d'édition avec des fils.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public abstract class ContentOpenableUIModel<E extends IdDto> extends ContentUIModel<E> {

    public static final String PROPERTY_CAN_REOPEN = "canReopen";

    public static final String PROPERTY_HISTORICAL_DATA = "historicalData";

    private static final long serialVersionUID = 1L;

    protected boolean canReopen;

    protected boolean historicalData;

    public ContentOpenableUIModel(Class<E> beanType) {
        super(beanType);
    }

    public boolean isCanReopen() {
        return canReopen;
    }

    public void setCanReopen(boolean canReopen) {
        boolean old = isCanReopen();
        this.canReopen = canReopen;
        firePropertyChange(PROPERTY_CAN_REOPEN, old, canReopen);
    }

    public boolean isHistoricalData() {
        return historicalData;
    }

    public void setHistoricalData(boolean historicalData) {
        boolean old = isHistoricalData();
        this.historicalData = historicalData;
        firePropertyChange(PROPERTY_HISTORICAL_DATA, old, historicalData);
    }
}
