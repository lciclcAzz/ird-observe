<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
  superGenericType='TargetSampleDto, TargetLengthDto'
  abstract="true">

  <style source="AbstractSampleUI.jcss"/>

  <import>
    fr.ird.observe.services.dto.seine.TargetSampleDto
    fr.ird.observe.services.dto.seine.TargetLengthDto
    fr.ird.observe.application.swing.ui.content.table.impl.seine.CodeMesureEnum
    fr.ird.observe.application.swing.ui.content.table.impl.seine.ModeSaisieEchantillonEnum
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.application.swing.ui.content.table.*

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <TargetSampleUIHandler id='handler' initializer='null'/>

  <!-- model -->
  <TargetSampleUIModel id='model' constructorParams='this'/>

  <!-- edit bean -->
  <TargetSampleDto id='bean'/>

  <!-- table edit bean -->
  <TargetLengthDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator'
                 beanClass='fr.ird.observe.services.dto.seine.TargetSampleDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'>
    <!-- clef unique -->
    <field name="targetLength" component="editorPanel"/>
    <field name='comment'/>
  </BeanValidator>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable'
                 autoField='true'
                 beanClass='fr.ird.observe.services.dto.seine.TargetLengthDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'
                 parentValidator='{validator}'/>

  <ButtonGroup id='acquisitionModeGroup'
               onStateChanged='getHandler().updateModeSaisie((ModeSaisieEchantillonEnum) acquisitionModeGroup.getSelectedValue())'/>

  <script><![CDATA[

public static final String POIDS_COMPUTED_TIP = n("observe.common.weight.computed.tip");
public static final String POIDS_OBSERVED_TIP = n("observe.common.weight.observed.tip");
public static final String LONGUEUR_COMPUTED_TIP = n("observe.common.length.computed.tip");
public static final String LONGUEUR_OBSERVED_TIP = n("observe.common.length.observed.tip");

public String getWeightDataTip(boolean computed) {
    return computed ? t(POIDS_COMPUTED_TIP) : t(POIDS_OBSERVED_TIP);
}

public String getLengthDataTip(boolean computed) {
    return computed ? t(LONGUEUR_COMPUTED_TIP) : t(LONGUEUR_OBSERVED_TIP);
}
]]>
  </script>
  <Table id='editorPanel' fill='both' insets='1'>

    <!-- mode de saisie -->
    <row>
      <cell columns="3">
        <JPanel id="modeAndCodePanel">
          <JPanel id='acquisitionModePanel' constraints="BorderLayout.CENTER">
            <JRadioButton id='acquisitionModeEffectif'/>
            <JRadioButton id='acquisitionModeIndividu'/>
          </JPanel>
          <JPanel id='measureTypePanel' layout='{new BorderLayout()}'>
            <EnumEditor id='measureType'
                        constraints="BorderLayout.CENTER"
                        constructorParams='CodeMesureEnum.class'
                        genericType='CodeMesureEnum'
                        onItemStateChanged='getTableEditBean().setMeasureType(measureType.getSelectedIndex())'/>
          </JPanel>
        </JPanel>
      </cell>
    </row>

    <!-- species thon -->
    <row>
      <cell>
        <JLabel id='speciesLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='species' genericType='ReferentialReference&lt;SpeciesDto&gt;' _entityClass='SpeciesDto.class' constructorParams='this'/>
      </cell>
    </row>

    <!-- length -->
    <row>
      <cell>
        <JLabel id='lengthLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='length' constructorParams='this'/>
      </cell>
      <cell anchor='east'>
        <JToolBar id='lengthSourceAction'>
          <JButton id='lengthSourceInformation'
                   onActionPerformed='getHandler().resetLengthSource()'/>
        </JToolBar>
      </cell>
    </row>

    <!-- weight individuel -->
    <row>
      <cell>
        <JLabel id='weightLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='weight' constructorParams='this'/>
      </cell>
      <cell anchor='east'>
        <JToolBar id='weightSourceAction'>
          <JButton id='weightSourceInformation'
                   onActionPerformed='getHandler().resetWeightSource()'/>
        </JToolBar>
      </cell>
    </row>

    <!-- count -->
    <row>
      <cell>
        <JLabel id='countLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='count' constructorParams='this'/>
      </cell>
    </row>
  </Table>

  <Table id='extraZone' fill='both' weightx='1' insets='0'>
    <row>
      <cell weighty='1'>
        <JScrollPane id='comment'
                     onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2'
                     onKeyReleased='getBean().setComment(comment2.getText())'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
