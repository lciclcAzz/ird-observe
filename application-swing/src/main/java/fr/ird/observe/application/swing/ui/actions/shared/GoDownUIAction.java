/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ObserveContentUI;
import fr.ird.observe.application.swing.ui.tree.renderer.AbstractObserveTreeCellRenderer;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class GoDownUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "goDown";

    public GoDownUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.goDown"),
              n("observe.action.goDown.tip"),
              "go-down"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {

        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            JPopupMenu p = (JPopupMenu)
                    c.getClientProperty("popup");
            if (p == null) {
                throw new IllegalStateException(
                        "could not find client property " +
                        "popup on component" + c);
            }
            p.show(c, 2, c.getHeight());
        });
    }

    @Override
    public void initAction(ObserveContentUI<?> ui, AbstractButton editor) {
        super.initAction(ui, editor);
        editor.putClientProperty("popup", getMainUI().getScopeDownPopup());
    }

    @Override
    public void updateAction(ObserveContentUI<?> ui, AbstractButton editor) {
        ObserveMainUI mainUI = getMainUI();

        ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();

        AbstractObserveTreeCellRenderer render = treeHelper.getTreeCellRenderer();

        ObserveNode node;

        super.updateAction(ui, editor);

        JPopupMenu scopePopup = mainUI.getScopeDownPopup();
        Action action =
                mainUI.getRootPane().getActionMap().get(SelectNodeUIAction.ACTION_NAME);
        node = treeHelper.getSelectedNode();
        ObserveNode nodeOriginal = node;
        scopePopup.removeAll();
        Enumeration<?> e = node.children();
        while (e.hasMoreElements()) {
            node = (ObserveNode) e.nextElement();
            String text = render.getNodeText(node);
            if (text == null) {
                continue;
            }
            Color color =
                    render.getNavigationTextColor(node);
            Icon icon = render.getNavigationIcon(node, "-16");
            JMenuItem mi = new JMenuItem();
            mi.setAction(action);
            mi.setText(text.trim());
            mi.setIcon(icon);
            mi.setForeground(color);
            mi.putClientProperty("node", node);
            mi.putClientProperty("ui", ui);
            mi.setAction(action);
            scopePopup.add(mi);
        }

        if (nodeOriginal.getChildCount() == 1 &&
            !nodeOriginal.getChildAt(0).isLeaf()) {
            e = nodeOriginal.getChildAt(0).children();
            while (e.hasMoreElements()) {
                node = (ObserveNode) e.nextElement();
                Color color = render.getNavigationTextColor(
                        node);
                Icon icon = render.getNavigationIcon(node, "-16");
                String text = render.getNodeText(node);
                JMenuItem mi = new JMenuItem();
                mi.setAction(action);
                mi.setText(text.trim());
                mi.setIcon(icon);
                mi.setForeground(color);
                mi.putClientProperty("node", node);
                mi.putClientProperty("ui", ui);
                mi.setAction(action);
                scopePopup.add(mi);
            }
        }

        int nbNodes = scopePopup.getComponentCount();

        editor.setEnabled(nbNodes > 0);
    }
}
