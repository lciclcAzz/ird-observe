package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceProduceSqlsRequest;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeResources;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ReferentialSynchronizeTaskWithReplaceSupport<R extends ReferentialDto> extends ReferentialSynchronizeTaskSupport<R> {

    protected final String replaceDataStr;
    private final ReferentialReference<R> replaceData;

    protected ReferentialSynchronizeTaskWithReplaceSupport(ReferentialSynchronizeResources resource, boolean left, ReferentialReference<R> data, ReferentialReference<R> replaceData) {
        super(resource, left, data);
        this.replaceData = replaceData;
        this.replaceDataStr = replaceData == null ? null : ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(replaceData.getType()).toString(replaceData);
    }

    public String getReplaceId() {
        return replaceData == null ? null : replaceData.getId();
    }

    @Override
    public String getLabel() {
        return t(i18nKey, typeStr, dataStr, replaceDataStr);
    }

    public void registerTask(ReferentialSynchronizeServiceProduceSqlsRequest.Builder builder) {
        builder.addTask(isLeft(), getTaskType(), getType(), getId(), getReplaceId());
    }
}
