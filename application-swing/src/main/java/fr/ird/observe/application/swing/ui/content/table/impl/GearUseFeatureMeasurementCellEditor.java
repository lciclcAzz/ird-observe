package fr.ird.observe.application.swing.ui.content.table.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeHelper;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;

import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeatureMeasurementCellEditor implements TableCellEditor {

    protected final int caracteristicColumn;

    protected TableCellEditor editor;

    protected Map<String, TableCellEditor> editorsByGearCaracteristicId;


    public GearUseFeatureMeasurementCellEditor(int caracteristicColumn) {
        super();
        this.caracteristicColumn = caracteristicColumn;

    }

    @Override
    public Object getCellEditorValue() {
        return editor.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return editor.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return editor.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        editor.cancelCellEditing();
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        editor.addCellEditorListener(l);
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        editor.removeCellEditorListener(l);
    }

    public Map<String, TableCellEditor> getEditorsByGearCaracteristicId(JTable table) {
        if (editorsByGearCaracteristicId == null) {
            editorsByGearCaracteristicId = new TreeMap<>();

            {
                // texte
                TableCellEditor editor = table.getDefaultEditor(Object.class);
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.1", editor);
            }
            {
                // boolean
                TableCellEditor editor = table.getDefaultEditor(Boolean.class);
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.2", editor);
            }
            {
                // entier signé
                NumberCellEditor editor = new NumberCellEditor<>(Integer.class, true);
                editor.getNumberEditor().setSelectAllTextOnError(true);
                editor.getNumberEditor().setNumberPattern("\\d{0,6}?");
                editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.3", editor);
            }
            {
                // décimal signé
                NumberCellEditor editor = new NumberCellEditor<>(Float.class, true);
                editor.getNumberEditor().setSelectAllTextOnError(true);
                editor.getNumberEditor().setNumberPattern("\\d{0,6}(\\.\\d{0,4})?");
                editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.4", editor);
            }
            {
                // entier non signé
                NumberCellEditor editor = new NumberCellEditor<>(Integer.class, false);
                editor.getNumberEditor().setSelectAllTextOnError(true);
                editor.getNumberEditor().setNumberPattern("\\d{0,6}?");
                editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.5", editor);
            }
            {
                // décimal non signé
                NumberCellEditor editor = new NumberCellEditor<>(Float.class, false);
                editor.getNumberEditor().setSelectAllTextOnError(true);
                editor.getNumberEditor().setNumberPattern("\\d{0,6}(\\.\\d{0,4})?");
                editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
                editorsByGearCaracteristicId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.6", editor);
            }

        }
        return editorsByGearCaracteristicId;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        ReferentialReference<GearCaracteristicDto> caracteristicRef = (ReferentialReference<GearCaracteristicDto>) table.getModel().getValueAt(row, caracteristicColumn);
        if (caracteristicRef == null) {

            // can't edit a null value ?
            editor = table.getDefaultEditor(Object.class);

        } else {

            String gearCaracteristicTypeId = (String) caracteristicRef.getPropertyValue(GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE);

            Map<String, TableCellEditor> editors = getEditorsByGearCaracteristicId(table);
            editor = editors.get(gearCaracteristicTypeId);

            value = GearCaracteristicTypeHelper.getTypeValue(gearCaracteristicTypeId, value);

        }

        return editor.getTableCellEditorComponent(table, value, isSelected, row, column);

    }

}
