package fr.ird.observe.application.swing.ui.tree.menu;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.node.ProgramLonglineNode;
import fr.ird.observe.application.swing.ui.tree.node.ProgramSeineNode;
import fr.ird.observe.application.swing.ui.tree.actions.ChangeTripProgramActionListener;
import fr.ird.observe.application.swing.ui.tree.actions.NodeChangeActionListener;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveTripNodeMenuPopulator extends MoveNodeMenuPopulator {

    @Override
    public NodeChangeActionListener createChangeActionListener(ObserveTreeHelper treeHelper,
                                                               ObserveSwingDataSource dataSource,
                                                               String id,
                                                               String parentId) {
        return new ChangeTripProgramActionListener(treeHelper, dataSource, id, parentId);
    }

    @Override
    public List<DecoratedNodeEntity> getPossibleParentNodes(ObserveNode tripNode, ObserveTreeHelper treeHelper) {

        // noeud du programme parent
        ObserveNode parentNode = tripNode.getParent();

        // programmes du même type que le noeud de marée, sans le parent actuel
        List<DecoratedNodeEntity> possibleParents = new ArrayList<>();

        // noeud longline ?
        GearType gearType = IdHelper.isLonglineId(tripNode.getId()) ? GearType.longline : GearType.seine;

        // racine
        ObserveNode rootNode = treeHelper.getRootNode();

        createPossibleParents(parentNode.getId(), possibleParents, gearType, rootNode);

        return possibleParents;
    }

    public static void createPossibleParents(String oldProgramId, List<DecoratedNodeEntity> possibleParents, GearType gearType, ObserveNode rootNode) {

        ReferentialReferenceDecorator<ProgramDto> programDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(ProgramDto.class);

        for (int i = 0, n = rootNode.getChildCount(); i < n; i++) {

            ObserveNode programNode = rootNode.getChildAt(i);
            String programId = programNode.getId();

            // si le noeud programme n'est pas le même que le parent actuel
            // si le noeud est bien un noeud de programme
            if (IdHelper.isProgramId(programId) && !oldProgramId.equals(programId)) {

                if (programNode instanceof ProgramSeineNode && GearType.seine == gearType) {

                    ProgramSeineNode node = (ProgramSeineNode) programNode;
                    possibleParents.add(DecoratedNodeEntity.newDecoratedNodeEntity(node, programDecorator));

                } else if (programNode instanceof ProgramLonglineNode && GearType.longline == gearType) {

                    ProgramLonglineNode node = (ProgramLonglineNode) programNode;
                    possibleParents.add(DecoratedNodeEntity.newDecoratedNodeEntity(node, programDecorator));

                }

            }
        }
    }

}
