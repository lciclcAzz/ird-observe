package fr.ird.observe.application.swing.ui.content;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.IdDto;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.swing.BlockingLayerUI;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;

import javax.swing.Icon;
import javax.swing.JToolBar;

/**
 * Created on 10/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public interface ObserveContentUI<E extends IdDto> extends JAXXObject {

    String CLIENT_PROPERTY_FORCE_LOAD = "forceLoad";

    String CLIENT_PROPERTY_LIST_NO_LOAD = "listNoLoad";

    String CLIENT_PROPERTY_ENTITY_CLASS = "entityClass";

    ContentUIModel<E> getModel();

    DataContext getDataContext();

    ObserveSwingDataSource getDataSource();

    SwingValidatorMessageTableModel getErrorTableModel();

    void setContentIcon(Icon icon);

    void init();

    void open();

    boolean close();

    void destroy();

    void stopEdit();

    void startEdit(E bean);

    void restartEdit();

    void resetEdit();

    void save(boolean refresh);

    void delete();

    JToolBar getTitleRightToolBar();

    BlockingLayerUI getBlockLayerUI();

}
