package fr.ird.observe.application.swing.ui.content.impl.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import jaxx.runtime.validator.swing.SwingValidator;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorEvent;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorListener;

import javax.swing.JComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LonglineDetailCompositionValidationContext implements SimpleBeanValidatorListener {

    SectionDto section;

    BasketDto basket;

    BranchlineDto branchline;

    String contextLabel;

    final List<SwingValidatorMessage> messages;

    final DecoratorService decoratorService;

    private final Map<SwingValidator, JComponent> validatorEditors;

    LonglineDetailCompositionValidationContext(DecoratorService decoratorService, Map<SwingValidator, JComponent> validatorEditors) {
        this.decoratorService = decoratorService;
        this.validatorEditors = validatorEditors;
        this.messages = new ArrayList<>();
    }

    public List<SwingValidatorMessage> getMessages() {
        return messages;
    }

    public void setSection(SectionDto section) {
        this.section = section;
        this.basket = null;
        this.branchline = null;
        updateContextLabel();
    }

    public void setBasket(BasketDto basket) {
        this.basket = basket;
        this.branchline = null;
        updateContextLabel();
    }

    public void setBranchline(BranchlineDto branchline) {
        this.branchline = branchline;
        updateContextLabel();
    }

    protected void updateContextLabel() {

        contextLabel = "";
        if (section != null) {
            contextLabel += "S " + section.getSettingIdentifier();
            if (basket != null) {
                contextLabel += " Ba " + basket.getSettingIdentifier();
                if (branchline != null) {
                    contextLabel += " Br " + branchline.getSettingIdentifier();
                }
            }
        }

    }

    @Override
    public void onFieldChanged(SimpleBeanValidatorEvent event) {

        String[] messagesToAdd = event.getMessagesToAdd();
        if (messagesToAdd != null) {

            String field = event.getField();
            NuitonValidatorScope scope = event.getScope();
            SwingValidator<?> validator = (SwingValidator<?>) event.getSource();

            for (String messageToAdd : messagesToAdd) {
                addMessage(validator, scope, field, messageToAdd);
            }

        }

    }

    public void addMessage(SwingValidator validator, NuitonValidatorScope scope, String field, String messageToAdd) {

        String propertyLabel = ObserveI18nDecoratorHelper.getPropertyI18nKey(field);
        SwingValidatorMessage message = new SwingValidatorMessage(
                validator,
                contextLabel + " - " + t(propertyLabel),
                messageToAdd,
                scope,
                validatorEditors.get(validator)
        );
        messages.add(message);

    }

}
