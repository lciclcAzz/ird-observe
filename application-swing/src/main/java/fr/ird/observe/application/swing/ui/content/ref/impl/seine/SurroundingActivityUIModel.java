package fr.ird.observe.application.swing.ui.content.ref.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class SurroundingActivityUIModel extends ContentReferenceUIModel<SurroundingActivityDto> {

    private static final long serialVersionUID = 1L;

    public SurroundingActivityUIModel() {
        super(SurroundingActivityDto.class);
    }

}
