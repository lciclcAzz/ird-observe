package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.AddedReferenceReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.RootReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.TypeReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.UpdatedReferenceReferentialSynchroNode;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiff;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffState;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffs;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialSynchronizeTreeModelsBuilder {

    private final ReferentialSynchronizeDiffsEngine engine;
    private final RootReferentialSynchroNode leftRootNode;
    private final RootReferentialSynchroNode rightRootNode;
    private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> leftIdsBuilder = ImmutableSetMultimap.builder();
    private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> rightIdsBuilder = ImmutableSetMultimap.builder();

    public ReferentialSynchronizeTreeModelsBuilder(ReferentialSynchronizeMode synchronizeMode, ReferentialSynchronizeDiffsEngine engine) {
        Objects.nonNull(synchronizeMode);
        Objects.nonNull(engine);
        this.engine = engine;
        this.leftRootNode = new RootReferentialSynchroNode(true, synchronizeMode.isLeftWrite());
        this.rightRootNode = new RootReferentialSynchroNode(false, synchronizeMode.isRightWrite());
    }

    public Pair<ReferentialSynchronizeTreeModel, ReferentialSynchronizeTreeModel> build() {

        ReferentialSynchronizeDiffs synchronizeDiffs = engine.build();

        ImmutableSet<Class<? extends ReferentialDto>> referentialNames = synchronizeDiffs.getReferentialNames();

        ReferentialSynchronizeDiff leftDiff = synchronizeDiffs.getLeftDiff();
        ReferentialSynchronizeDiff rightDiff = synchronizeDiffs.getRightDiff();

        boolean rightCanWrite = rightRootNode.isCanWrite();
        boolean leftCanWrite = leftRootNode.isCanWrite();

        CreateAddNode leftAddNode = new CreateAddNode(rightCanWrite, leftCanWrite, false);
        CreateAddNode rightAddNode = new CreateAddNode(leftCanWrite, rightCanWrite, false);
        CreateNode leftUpdateNode = new CreateUpdateNode(rightCanWrite, false, leftCanWrite);
        CreateNode rightUpdateNode = new CreateUpdateNode(leftCanWrite, false, rightCanWrite);

        for (Class<? extends ReferentialDto> referentialName : referentialNames) {

            {
                // Tous les référentiels ajoutés à gauche peuvent être copié à droite
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = leftDiff.getAddedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromLeft(leftRootNode, optionalDiffStates.get(), referentialName, leftAddNode);
                }
            }
            {
                // Tous les référentiels mises à jour à gauche peuvent être copié à droite
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = leftDiff.getUpdatedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromLeft(leftRootNode, optionalDiffStates.get(), referentialName, leftUpdateNode);
                }
            }
            {
                // Tous les référentiels ajoutés à droite peuvent être supprimé ou désactivés
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = rightDiff.getAddedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromRight(rightRootNode, optionalDiffStates.get(), referentialName, rightAddNode);
                }
            }
            {
                // Tous les référentiels mises à jour à droite peuvent être remis en arrière
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = rightDiff.getUpdatedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromRight(rightRootNode, optionalDiffStates.get(), referentialName, rightUpdateNode);
                }
            }

        }

        ReferentialSynchronizeTreeModel leftTreeModel = new ReferentialSynchronizeTreeModel(leftRootNode, leftAddNode.getIds());
        ReferentialSynchronizeTreeModel rightTreeModel = new ReferentialSynchronizeTreeModel(rightRootNode, rightAddNode.getIds());
        return Pair.of(leftTreeModel, rightTreeModel);

    }

    private <R extends ReferentialDto> void addFromLeft(RootReferentialSynchroNode rootNode, ImmutableSet<ReferentialSynchronizeDiffState> diffStates, Class<R> referentialName, CreateNode createNode) {
        ReferentialReferenceSet<R> referenceSet = engine.getLeftReferentialReferenceSet(referentialName, diffStates);
        add0(rootNode, referenceSet, referentialName, createNode);
    }

    private <R extends ReferentialDto> void addFromRight(RootReferentialSynchroNode rootNode, ImmutableSet<ReferentialSynchronizeDiffState> diffStates, Class<R> referentialName, CreateNode createNode) {
        ReferentialReferenceSet<R> referenceSet = engine.getRightReferentialReferenceSet(referentialName, diffStates);
        add0(rootNode, referenceSet, referentialName, createNode);
    }

    private <R extends ReferentialDto> void add0(RootReferentialSynchroNode rootNode, ReferentialReferenceSet<R> referenceSet, Class<R> referentialName, CreateNode createNode) {

        List<ReferentialReference<R>> references = new ArrayList<>(referenceSet.getReferences());
        if (!references.isEmpty()) {
            TypeReferentialSynchroNode typeNode = rootNode.getOrAddTypeNode(referentialName);
            if (references.get(0).getPropertyNames().contains(ReferentialDto.PROPERTY_CODE)) {
                Collections.sort(references, new ReferentialReferenceComparatorFromCode<>());
            } else {
                DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
                ReferentialReferenceDecorator<R> decorator = decoratorService.getReferentialReferenceDecorator(referentialName);
                Collections.sort(references, new ReferentialReferenceComparatorFromDecorator<>(decorator));
            }
            for (ReferentialReference<R> reference : references) {
                createNode.createNode(typeNode, reference);
            }
        }
    }

    private static abstract class CreateNode {

        protected final boolean canCopy;
        protected final boolean canDelete;
        protected final boolean canRevert;

        protected CreateNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            this.canCopy = canCopy;
            this.canDelete = canDelete;
            this.canRevert = canRevert;
        }

        public abstract void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference);

    }

    private static class CreateAddNode extends CreateNode {

        private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> idsBuilder = ImmutableSetMultimap.builder();

        protected CreateAddNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            super(canCopy, canDelete, canRevert);
        }

        @Override
        public void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference) {
            ReferenceReferentialSynchroNodeSupport node = new AddedReferenceReferentialSynchroNode(reference, canCopy, false, canDelete, canRevert);
            typeNode.add(node);
            idsBuilder.put(reference.getType(), reference.getId());
        }

        public ImmutableSetMultimap<Class<? extends ReferentialDto>, String> getIds() {
            return idsBuilder.build();
        }
    }

    private static class CreateUpdateNode extends CreateNode {

        protected CreateUpdateNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            super(canCopy, canDelete, canRevert);
        }

        @Override
        public void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference) {
            ReferenceReferentialSynchroNodeSupport node = new UpdatedReferenceReferentialSynchroNode(reference, false, canCopy, canDelete, canRevert);
            typeNode.add(node);
        }
    }

    private static class ReferentialReferenceComparatorFromCode<R extends ReferentialDto> implements Comparator<ReferentialReference<R>> {

        @Override
        public int compare(ReferentialReference<R> o1, ReferentialReference<R> o2) {
            String o1Code = o1.getCode();
            String o2Code = o2.getCode();
            if (o1Code == null) {
                return o2Code == null ? 0 : -1;
            }
            if (o2Code == null) {
                return 1;
            }
            return StringUtils.leftPad(o1Code, 6, "0").compareTo(StringUtils.leftPad(o2Code, 6, "0"));
        }
    }

    private static class ReferentialReferenceComparatorFromDecorator<R extends ReferentialDto> implements Comparator<ReferentialReference<R>> {

        private final ReferentialReferenceDecorator<R> decorator;

        public ReferentialReferenceComparatorFromDecorator(ReferentialReferenceDecorator<R> decorator) {
            this.decorator = decorator;
        }

        @Override
        public int compare(ReferentialReference<R> o1, ReferentialReference<R> o2) {
            return decorator.toString(o1).compareTo(decorator.toString(o2));
        }
    }
}
