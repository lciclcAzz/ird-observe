<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
  superGenericType='TripLonglineGearUseDto, GearUseFeaturesLonglineDto'
  contentTitle='{n("observe.content.gearUseFeaturesLongline.title")}'
  saveNewEntryText='{n("observe.content.action.gearUseFeaturesLongline")}'
  saveNewEntryTip='{n("observe.content.action.gearUseFeaturesLongline.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.CommentableDto
    fr.ird.observe.services.dto.longline.TripLonglineDto
    fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto
    fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.GearDto
    fr.ird.observe.application.swing.ui.content.table.*
    fr.ird.observe.application.swing.ui.util.BooleanEditor

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <GearUseFeaturesLonglineUIHandler id='handler' constructorParams='this'/>

  <!-- model -->
  <GearUseFeaturesLonglineUIModel id='model' constructorParams='this'/>

  <GearUseFeaturesMeasurementLonglinesTableModel id='measurementsTableModel'
                                                 initializer="getModel().getMeasurementsTableModel()"/>

  <!-- edit bean -->
  <TripLonglineDto id='bean'/>

  <!-- table edit bean -->
  <GearUseFeaturesLonglineDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator'
                 beanClass='fr.ird.observe.services.dto.longline.TripLonglineGearUseDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'/>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable'
                 autoField='true'
                 beanClass='fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'/>

  <JPopupMenu id='measurementsTablePopup'>
    <JMenuItem id='addMeasurement' onActionPerformed='getHandler().addMeasurement()'/>
    <JMenuItem id='deleteSelectedMeasurement' onActionPerformed='getHandler().deleteSelectedMeasurement()'/>
  </JPopupMenu>

  <Table id='editorPanel' fill='both' insets='0' weighty="1">
    <row>
      <cell weightx="1" weighty="0.9">

        <JTabbedPane id='gearUseFeaturesTabPane'>

          <tab id='generalTab'>

            <JPanel layout='{new BorderLayout()}'>

              <Table id='editForm' fill='both' insets='1' constraints="BorderLayout.NORTH">

                <!-- gear -->
                <row>
                  <cell>
                    <JLabel id='gearLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BeanComboBox id='gear' constructorParams='this' genericType='ReferentialReference&lt;GearDto&gt;' _entityClass='GearDto.class'/>
                  </cell>
                </row>

                <!-- number -->
                <row>
                  <cell>
                    <JLabel id='numberLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <NumberEditor id='number' constructorParams='this'/>
                  </cell>
                </row>

                <!-- usedInTrip -->
                <row>
                  <cell>
                    <JLabel id='usedInTripLabel'/>
                  </cell>
                  <cell weightx='1' anchor='east'>
                    <BooleanEditor id='usedInTrip'/>
                  </cell>
                </row>

              </Table>

            </JPanel>


          </tab>

          <tab id='measurementsFormTab'>

            <JScrollPane id='measurementsScrollPane'>
              <JTable id='measurementsTable'/>
            </JScrollPane>

          </tab>
        </JTabbedPane>

      </cell>
    </row>
    <row>
      <cell weighty='0.1'>
        <JScrollPane id='comment' onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
