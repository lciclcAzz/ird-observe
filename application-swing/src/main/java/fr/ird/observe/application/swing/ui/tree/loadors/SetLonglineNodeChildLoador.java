package fr.ird.observe.application.swing.ui.tree.loadors;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created on 8/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class SetLonglineNodeChildLoador extends AbstractNodeChildLoador<Class, String> {

    private static final long serialVersionUID = 1L;

    private static final Set<Class> PLURALIZE_PROPERTIES = Sets.newHashSet(CatchLonglineDto.class, TdrDto.class);

    public SetLonglineNodeChildLoador() {
        super(String.class);
    }

    @Override
    public List<Class> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) {
        return Arrays.asList(SetLonglineGlobalCompositionDto.class,
                             SetLonglineDetailCompositionDto.class,
                             CatchLonglineDto.class,
                             TdrDto.class);
    }

    @Override
    public ObserveNode createNode(Class data, NavDataProvider dataProvider) {
        return createNode0(PLURALIZE_PROPERTIES, data);
    }

}
