package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionHelper;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 */
public class BaitsCompositionUIModel extends ContentTableUIModel<SetLonglineGlobalCompositionDto, BaitsCompositionDto> {

    private static final long serialVersionUID = 1L;

    public BaitsCompositionUIModel(BaitsCompositionUI ui) {
        super(SetLonglineGlobalCompositionDto.class,
              BaitsCompositionDto.class,
              new String[]{
                      SetLonglineGlobalCompositionDto.PROPERTY_BAITS_COMPOSITION
              },
              new String[]{BaitsCompositionDto.PROPERTY_BAIT_TYPE,
                           BaitsCompositionDto.PROPERTY_BAIT_SETTING_STATUS,
                           BaitsCompositionDto.PROPERTY_INDIVIDUAL_SIZE,
                           BaitsCompositionDto.PROPERTY_INDIVIDUAL_WEIGHT,
                           BaitsCompositionDto.PROPERTY_PROPORTION});

        List<ContentTableMeta<BaitsCompositionDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(BaitsCompositionDto.class, BaitsCompositionDto.PROPERTY_BAIT_TYPE, false),
                ContentTableModel.newTableMeta(BaitsCompositionDto.class, BaitsCompositionDto.PROPERTY_BAIT_SETTING_STATUS, false),
                ContentTableModel.newTableMeta(BaitsCompositionDto.class, BaitsCompositionDto.PROPERTY_INDIVIDUAL_SIZE, false),
                ContentTableModel.newTableMeta(BaitsCompositionDto.class, BaitsCompositionDto.PROPERTY_INDIVIDUAL_WEIGHT, false),
                ContentTableModel.newTableMeta(BaitsCompositionDto.class, BaitsCompositionDto.PROPERTY_PROPORTION, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<SetLonglineGlobalCompositionDto, BaitsCompositionDto> createTableModel(
            ObserveContentTableUI<SetLonglineGlobalCompositionDto, BaitsCompositionDto> ui,
            List<ContentTableMeta<BaitsCompositionDto>> contentTableMetas) {

        return new ContentTableModel<SetLonglineGlobalCompositionDto, BaitsCompositionDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<BaitsCompositionDto> getChilds(SetLonglineGlobalCompositionDto bean) {
                return bean.getBaitsComposition();
            }

            @Override
            protected void load(BaitsCompositionDto source, BaitsCompositionDto target) {
                BaitsCompositionHelper.copyBaitsCompositionDto(source, target);
            }

            @Override
            protected void setChilds(SetLonglineGlobalCompositionDto parent, List<BaitsCompositionDto> childs) {
                parent.setBaitsComposition(childs);
            }
        };
    }
}
