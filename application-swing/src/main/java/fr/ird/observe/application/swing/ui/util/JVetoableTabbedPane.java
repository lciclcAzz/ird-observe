package fr.ird.observe.application.swing.ui.util;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTabbedPane;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.10
 */
public class JVetoableTabbedPane extends JTabbedPane {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(JVetoableTabbedPane.class);

    protected transient ChangeSelectedIndex changeSelectedIndex;

    protected int previousIndex = -1;

    public void setChangeSelectedIndex(ChangeSelectedIndex changeSelectedIndex) {
        this.changeSelectedIndex = changeSelectedIndex;
    }

    public int getPreviousIndex() {
        return previousIndex;
    }

    @Override
    public void setSelectedIndex(int index) {

        int selectedIndex = getSelectedIndex();

        boolean canChange = true;

        if (changeSelectedIndex != null) {

            canChange = changeSelectedIndex.canChangeTab(selectedIndex, index);

        }

        if (canChange) {

            previousIndex = selectedIndex;

            if (log.isDebugEnabled()) {
                log.debug("User accept to change from " + previousIndex + " tab to " + index + " tab.");
            }

            super.setSelectedIndex(index);

        } else {

            if (log.isDebugEnabled()) {
                log.debug("User refuse to change from " + selectedIndex + " tab to " + index + " tab.");
            }

        }

    }

    public interface ChangeSelectedIndex {

        /**
         * Ask to change tab from {@code selectedIndex} tab to {@code index}.
         *
         * If response is {@code false}, then we should not quit the current tab.
         *
         * @param currentSelectedIndex the current index of the selected tab
         * @param newSelectedIndex         the index of the tab we want to go on
         * @return {@code true} if we can do the change, {@code false} otherwise.
         */
        boolean canChangeTab(int currentSelectedIndex, int newSelectedIndex);

    }
}
