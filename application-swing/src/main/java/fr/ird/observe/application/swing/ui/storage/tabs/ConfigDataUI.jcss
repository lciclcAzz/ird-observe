/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

.creationMode {
  buttonGroup:"creationMode";
}

#configContent {
  layout:{configLayout};
  border:{new TitledBorder(t("observe.storage.config.data.storage"))};
}

#dataModeConfig {
  layout:{new GridLayout(0,1)};
  border:{new TitledBorder(t("observe.storage.config.data.mode"))};
}

#noImportData {
  value:{CreationMode.EMPTY};
  text:"observe.storage.no.data.import";
  selected:{model.getDataImportMode() == CreationMode.EMPTY};
}

#importDataFromBackup {
  value:{CreationMode.IMPORT_EXTERNAL_DUMP};
  text:"observe.storage.import.data.fromBackup";
  selected:{model.getDataImportMode() == CreationMode.IMPORT_EXTERNAL_DUMP};
}

#importDataFromRemote {
  value:{CreationMode.IMPORT_REMOTE_STORAGE};
  text:"observe.storage.import.data.fromRemoteStorage";
  selected:{model.getDataImportMode() == CreationMode.IMPORT_REMOTE_STORAGE};
}

#importDataFromServer {
  value:{CreationMode.IMPORT_SERVER_STORAGE};
  text:"observe.storage.import.data.fromServerStorage";
  selected:{model.getDataImportMode() == CreationMode.IMPORT_SERVER_STORAGE};
}

#noImportDataConfig {
  enabled: false;
  text:"observe.storage.noImportData.config";
}

#centralSourceLabel {
  _no:{n("observe.storage.no.remote.storage")};
  text:{getHandler().updateStorageLabel(centralSourceModel, centralSourceModel.isValid(), centralSourceLabel, true)};
}

#configureCentralSource {
  text:"observe.action.configure";
  actionIcon:"config";
}

#centralSourceStatus {
  icon:{(Icon) getClientProperty(centralSourceModel.isValid() ? "successIcon" : "failedIcon")};
}

#centralSourcePolicy {
  text:{getHandler().updateDataSourcePolicy(centralSourceModel, centralSourceModel.isValid(), true)}
}

#centralSourceInfoLabel {
  actionIcon:"information";
  text:"observe.storage.config.export.required.read.data";
}

#centralSourceServerLabel {
  _no:{n("observe.storage.no.server.storage")};
  text:{getHandler().updateStorageLabel(centralSourceModel, centralSourceModel.isValid(), centralSourceLabel, false)};
}

#configureCentralSourceServer {
  text:"observe.action.configure";
  actionIcon:"config";
}

#centralSourceServerStatus {
  icon:{(Icon) getClientProperty(centralSourceModel.isValid() ? "successIcon" : "failedIcon")};
}

#centralSourceServerPolicy {
  text:{getHandler().updateDataSourcePolicy(centralSourceModel, centralSourceModel.isValid(), false)}
}

#centralSourceServerInfoLabel {
  actionIcon:"information";
  text:"observe.storage.config.export.required.read.data";
}


#fileChooserAction {
  actionIcon:"fileChooser";
}

#dumpFile {
  text:{centralSourceModel.getDumpFile()+""};
}
