package fr.ird.observe.application.swing.ui.content.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.application.swing.ui.content.ObserveContentUI;
import jaxx.runtime.swing.BlockingLayerUI;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.*;

/**
 * Created on 10/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public interface ObserveContentTableUI<E extends IdDto, D extends IdDto> extends ObserveContentUI<E> {

    ContentTableUIHandler<E, D> getHandler();

    ContentTableUIModel<E, D> getModel();

    ContentTableModel<E, D> getTableModel();

    JTable getTable();


    JComponent getBody();

    JComponent getExtraZone();

    JComponent getEditor();

    JComponent getHideForm();

    JComponent getShowForm();

    ListSelectionModel getSelectionModel();

    BlockingLayerUI getBlockLayerUI();

    BlockingLayerUI getEditorBlockLayerUI();

    SwingValidator<E> getValidator();

    SwingValidator<D> getValidatorTable();
}
