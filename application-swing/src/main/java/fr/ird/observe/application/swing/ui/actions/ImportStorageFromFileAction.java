package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUI;
import fr.ird.observe.application.swing.ui.storage.StorageUIHandler;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import jaxx.runtime.SwingUtil;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ImportStorageFromFileAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public ImportStorageFromFileAction(ObserveMainUI ui) {

        super(t("observe.action.load.from.file"), SwingUtil.getUIManagerActionIcon("local-import"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.load.from.file.tip"));
        putValue(MNEMONIC_KEY, (int) 'I');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            new StorageUILauncher(ui, ui, t("observe.title.import.localDB")) {
                @Override
                protected void init(StorageUI ui) {
                    super.init(ui);
                    StorageUIModel model = ui.getModel();

                    model.setDumpFile(ImportStorageFromFileAction.this.ui.getConfig().getImportDirectory());
                    model.setCanCreateLocalService(true);
                    model.setCanUseLocalService(false);
                    model.setCanUseRemoteService(false);
                    model.setCanUseServerService(false);
                    model.setDbMode(DbMode.CREATE_LOCAL);
                    model.setCreationMode(CreationMode.IMPORT_EXTERNAL_DUMP);
                    if (model.isLocalStorageExist()) {
                        // sauvegarde base locale possible
                        model.setSteps(StorageStep.CONFIG,
                                       StorageStep.BACKUP,
                                       StorageStep.CONFIRM);

                        // et requise par défaut
                        model.setDoBackup(true);
                    } else {
                        model.setSteps(StorageStep.CONFIG, StorageStep.CONFIRM);
                    }
                }

                @Override
                protected void doAction(StorageUI ui) {
                    super.doAction(ui);
                    StorageUIHandler handler = getStorageUIHandler();
                    handler.doChangeStorage(ui.getModel());

                    File importDirectory = ui.getModel().getDumpFile().getParentFile();
                    ObserveSwingApplicationConfig config = ImportStorageFromFileAction.this.ui.getConfig();
                    config.setImportDirectory(importDirectory);
                    config.saveForUser();
                }

            }.start();
        }

    }

}
