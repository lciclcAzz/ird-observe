package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class LonglineDetailCompositionUIModel extends ContentUIModel<SetLonglineDetailCompositionDto> {

    public static final String PROPERTY_CAN_GENERATE = "canGenerate";

    public static final String PROPERTY_GENERATE_TAB_VALID = "generateTabValid";

    public static final String PROPERTY_COMPOSITION_TAB_VALID = "compositionTabValid";

    public static final String PROPERTY_BRANCHLINE_DETAIL_TAB_VALID = "branchlineDetailTabValid";

    private static final long serialVersionUID = 1L;

    protected final SectionTemplatesTableModel sectionTemplatesTableModel;

    protected final SectionsTableModel sectionsTableModel;

    protected final BasketsTableModel basketsTableModel;

    protected final BranchlinesTableModel branchlinesTableModel;

    protected boolean canGenerate;

    protected boolean generateTabValid;

    protected boolean compositionTabValid;

    protected boolean branchlineDetailTabValid;

    public LonglineDetailCompositionUIModel() {

        super(SetLonglineDetailCompositionDto.class);
        this.sectionTemplatesTableModel = new SectionTemplatesTableModel();
        this.sectionsTableModel = new SectionsTableModel(this);
        this.basketsTableModel = new BasketsTableModel(this);
        this.branchlinesTableModel = new BranchlinesTableModel(this);
    }

    public SectionTemplatesTableModel getSectionTemplatesTableModel() {
        return sectionTemplatesTableModel;
    }

    public BasketsTableModel getBasketsTableModel() {
        return basketsTableModel;
    }

    public SectionsTableModel getSectionsTableModel() {
        return sectionsTableModel;
    }

    public BranchlinesTableModel getBranchlinesTableModel() {
        return branchlinesTableModel;
    }

    public boolean isHaulingdirectionSameAsSettings() {
        Boolean haulingDirectionSameAsSetting = getBean().getHaulingDirectionSameAsSetting();
        return haulingDirectionSameAsSetting != null && haulingDirectionSameAsSetting;
    }

    public boolean isGenerateHaulingIds() {
        return getBean().getHaulingBreaks() == 0;
    }

    public boolean isCanGenerate() {
        return canGenerate;
    }

    public void setCanGenerate(boolean canGenerate) {
        this.canGenerate = canGenerate;
        firePropertyChange(PROPERTY_CAN_GENERATE, null, canGenerate);
    }

    public boolean isBranchlineDetailTabValid() {
        return branchlineDetailTabValid;
    }

    public void setBranchlineDetailTabValid(boolean branchlineDetailTabValid) {
        this.branchlineDetailTabValid = branchlineDetailTabValid;
        firePropertyChange(PROPERTY_BRANCHLINE_DETAIL_TAB_VALID, null, branchlineDetailTabValid);
    }

    public boolean isCompositionTabValid() {
        return compositionTabValid;
    }

    public void setCompositionTabValid(boolean compositionTabValid) {
        this.compositionTabValid = compositionTabValid;
        firePropertyChange(PROPERTY_COMPOSITION_TAB_VALID, null, compositionTabValid);
    }

    public boolean isGenerateTabValid() {
        return generateTabValid;
    }

    public void setGenerateTabValid(boolean generateTabValid) {
        this.generateTabValid = generateTabValid;
        firePropertyChange(PROPERTY_GENERATE_TAB_VALID, null, generateTabValid);
    }

}
