/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.ObserveMainUIHandler;
import fr.ird.observe.application.swing.ui.UIHelper;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.swing.wizard.BusyChangeListener;
import jaxx.runtime.swing.wizard.WizardModel;
import jaxx.runtime.swing.wizard.WizardUILancher;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Window;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Le wizard de base pour les opération sur le service de données.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StorageUILauncher extends WizardUILancher<StorageStep, StorageUIModel, StorageUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(StorageUILauncher.class);

    protected final String title;

    public StorageUILauncher(JAXXContext context,
                             Window frame,
                             StorageUIModel model,
                             String title) {
        super(context, frame, StorageUI.class, StorageUIModel.class, model);
        this.title = title;
    }

    public StorageUILauncher(JAXXContext context,
                             Window frame,
                             String title) {
        super(context, frame, StorageUI.class, StorageUIModel.class);
        this.title = title;
    }

    @Override
    protected void init(StorageUI ui) {
        if (log.isInfoEnabled()) {
            log.info("Will init " + ui.getName());
        }
        super.init(ui);
        if (title != null) {
            ui.setTitle(title);
        }
        BusyChangeListener listener = new BusyChangeListener(ui);
        UIHelper.setLayerUI(ui.getTabs(), ui.getBusyBlockLayerUI());
        listener.setBlockingUI(ui.getBusyBlockLayerUI());
        ui.getModel().addPropertyChangeListener(WizardModel.BUSY_PROPERTY_NAME, listener);
        ui.setContextValue(this);
    }

    @Override
    public void start() {
        super.start();
        ui.getModel().setBusy(false);
    }

    @Override
    protected void doCancel(StorageUI ui) {
        if (log.isDebugEnabled()) {
            log.debug("Will cancel " + ui.getName());
        }
        ui.getModel().setBusy(true);
        super.doCancel(ui);
        ObserveMainUIHandler.restartEdit();
    }

    @Override
    protected void doAction(StorageUI ui) {
        ui.getModel().setBusy(true);
        super.doAction(ui);
    }

    protected StorageUIHandler getStorageUIHandler() {
        return ui.getContextValue(StorageUIHandler.class);
    }

    @Override
    protected void doClose(StorageUI ui, boolean wasCanceled) {
        if (log.isDebugEnabled()) {
            log.debug("Will close " + ui.getName() + " (was canceled ? " + wasCanceled + ")");
        }
        super.doClose(ui, wasCanceled);
        ui.getModel().setBusy(false);
        ui.dispose();

        ObserveRunner.cleanMemory();

    }

    /**
     * Méthode pour lancer le changement de source de données.
     *
     * @param rootContext le context applicatif
     * @param mainUI      main ui
     * @param modes       les modes optionnel à utiliser
     * @param title       le titre de la fenetre
     * @see StorageUIHandler
     * @see StorageUI
     */
    public static void changeStorage(final JAXXContext rootContext,
                                     final Window mainUI,
                                     final Set<DbMode> modes,
                                     final String title) {

        new StorageUILauncher(rootContext, mainUI, title) {

            @Override
            protected void init(StorageUI ui) {
                super.init(ui);
                if (log.isDebugEnabled()) {
                    log.debug("Incoming dbmode : " + modes.stream().map(DbMode::name).collect(Collectors.joining(", ")));
                }
                StorageUIModel model = ui.getModel();
                if (CollectionUtils.isNotEmpty(modes)) {
                    if (log.isInfoEnabled()) {
                        log.info("will use incoming mode " + modes.stream().map(DbMode::name).collect(Collectors.joining(", ")));
                    }

                    model.setExcludeSteps(
                            Arrays.asList(StorageStep.SELECT_DATA,
                                          StorageStep.BACKUP,
                                          StorageStep.CONFIG_REFERENTIEL,
                                          StorageStep.CONFIG_DATA,
                                          StorageStep.ROLES)
                    );
                    model.setCanCreateLocalService(modes.contains(DbMode.CREATE_LOCAL));
                    model.setCanUseLocalService(modes.contains(DbMode.USE_LOCAL));
                    model.setCanUseRemoteService(modes.contains(DbMode.USE_REMOTE));
                    model.setCanUseServerService(modes.contains(DbMode.USE_SERVER));

                    model.updateUniverse();
                    model.setDbMode(modes.stream().findFirst().get());

                } else {
                    ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();
                    Boolean localOpened = config.getMainStorageOpenedLocal();
                    if (model.isLocalStorageExist() && localOpened != null && localOpened) {
                        if (log.isDebugEnabled()) {
                            log.debug("Can not use local db (already opened)");
                        }
                        model.setCanUseLocalService(false);
                    }
                    model.setCanCreateLocalService(true);
                    model.setCanUseRemoteService(true);
                    model.setCanUseServerService(true);
                }
                model.updateUniverse();
            }

            @Override
            protected void doAction(StorageUI ui) {
                super.doAction(ui);
                ui.getHandler().doChangeStorage(ui.getModel());
            }

        }.start();
    }

    /**
     * Méthode pour lancer l'action de connexion à une base distante.
     *
     * Cette méthode doit être appelée avant toute action avec une base distante
     * :
     *
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenetre principale parent (peut etre null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainRemoteConnexion(final JAXXContext context,
                                             Window mainUI,
                                             final StorageUIModel model) {
        obtainConnexion(context, mainUI, model, t("observe.title.connect.remoteDB"), DbMode.USE_REMOTE);
    }

    /**
     * Méthode pour lancer l'action de connexion à une base distante.
     *
     * Cette méthode doit être appelée avant toute action avec un server distant
     * :
     *
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenetre principale parent (peut etre null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainServerConnexion(final JAXXContext context,
                                             Window mainUI,
                                             final StorageUIModel model) {
        obtainConnexion(context, mainUI, model, t("observe.title.connect.serverDB"), DbMode.USE_SERVER);
    }

    /**
     * Méthode pour lancer l'action de connexion à une source distante.
     *
     * Cette méthode doit être appelée avant toute action avec un source distante
     * :
     *
     * synchronisation, récupération du référentiel distant...
     *
     * @param context le context applicatif
     * @param mainUI  la fenetre principale parent (peut etre null)
     * @param model   le modèle de source de données à utiliser
     * @param title   le titre de la fenêtre
     * @param model   le type deconnexion (base distante ou serveur distant)
     * @see StorageUI
     */
    public static void obtainConnexion(final JAXXContext context,
                                       Window mainUI,
                                       final StorageUIModel model,
                                       String title,
                                       DbMode dbMode) {


        if (mainUI == null) {
            mainUI = ObserveSwingApplicationContext.get().getMainUI();
        }
        addStorageUIHandler(context);
        StorageUILauncher launcher = new StorageUILauncher(
                context,
                mainUI,
                model,
                title) {

            @Override
            protected void init(StorageUI ui) {
                StorageUIModel model = ui.getModel();

                model.setCanCreateLocalService(false);
                model.setCanUseLocalService(false);
                model.setCanUseRemoteService(true);
                model.setCanUseServerService(true);

                model.setExcludeSteps(Arrays.asList(
                        StorageStep.CHOOSE_DB_MODE,
                        StorageStep.CONFIG_REFERENTIEL,
                        StorageStep.CONFIG_DATA,
                        StorageStep.SELECT_DATA,
                        StorageStep.ROLES,
                        StorageStep.BACKUP,
                        StorageStep.CONFIRM)
                );
                model.setSteps(StorageStep.CONFIG);
                model.updateUniverse();

                model.setDbMode(dbMode);
            }

            @Override
            protected StorageUI createUI(JAXXContext context,
                                         Window mainUI,
                                         Class<StorageUI> storageUIClass,
                                         Class<StorageUIModel> modelClass,
                                         StorageUIModel model) throws Exception {
                if (!(mainUI instanceof JAXXObject)) {
                    mainUI = null;
                }
                return super.createUI(context,
                                      mainUI,
                                      storageUIClass,
                                      modelClass,
                                      model
                );
            }

            @Override
            protected void doAction(StorageUI ui) {
                super.doAction(ui);
                if (log.isDebugEnabled()) {
                    log.debug("Apply new remote connexion to " + model);
                }
                ui.getModel().copyTo(model);
                model.validate();
            }
        };

        launcher.start();
    }

    /**
     * Méthode pour lancer l'action de connexion à une base locale.
     *
     * @param context le context applicatif
     * @param mainUI  la fenetre principale parent (peut etre null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainLocalConnexion(final JAXXContext context,
                                            Window mainUI,
                                            final StorageUIModel model) {

        if (mainUI == null) {
            mainUI = ObserveSwingApplicationContext.get().getMainUI();
        }

        addStorageUIHandler(context);

        StorageUILauncher launcher = new StorageUILauncher(
                context,
                mainUI,
                model,
                t("observe.title.connect.localDB")) {

            @Override
            protected StorageUI createUI(JAXXContext context,
                                         Window mainUI,
                                         Class<StorageUI> storageUIClass,
                                         Class<StorageUIModel> modelClass,
                                         StorageUIModel model) throws Exception {
                if (!(mainUI instanceof JAXXObject)) {
                    // sinon on a pas de context propagé...
                    mainUI = null;
                }
                return super.createUI(context, mainUI, storageUIClass, modelClass, model);
            }

            @Override
            protected void init(StorageUI ui) {
                StorageUIModel model = ui.getModel();

                int nbModes = 0;

                DbMode mode = null;
                if (model.isCanCreateLocalService()) {
                    nbModes++;
                    mode = DbMode.CREATE_LOCAL;
                }
                if (model.isCanUseLocalService()) {
                    nbModes++;
                    mode = DbMode.USE_LOCAL;
                }
                if (model.isCanUseRemoteService()) {
                    nbModes++;
                    mode = DbMode.USE_REMOTE;
                }
                if (model.isCanUseServerService()) {
                    nbModes++;
                    mode = DbMode.USE_SERVER;
                }

                if (nbModes == 1) {

                    if (log.isDebugEnabled()) {
                        log.debug("Only one mode available [" + mode +
                                          "], set it in model");
                    }

                    // un seul mode possible, on le sélectionne
                    model.setDbMode(mode);
                }

                // on supprime des étapes
                model.setExcludeSteps(Arrays.asList(
                        StorageStep.BACKUP,
                        StorageStep.CONFIG_REFERENTIEL,
                        StorageStep.CONFIG_DATA,
                        StorageStep.SELECT_DATA,
                        StorageStep.CONFIRM));

                model.updateUniverse();
            }

            @Override
            protected void doAction(StorageUI ui) {
                super.doAction(ui);
                if (log.isDebugEnabled()) {
                    log.debug("Apply new local connexion to " + model);
                }
                ui.getModel().copyTo(model);
                model.validate();
            }
        };

        launcher.start();
    }

    /**
     * Méthode pour lancer l'action de connexion à une base locale ou distante.
     *
     * @param context le context applicatif
     * @param mainUI  la fenetre principale parent (peut etre null)
     * @param model   le modèle de source de données à utiliser
     * @see StorageUI
     */
    public static void obtainConnexion(final JAXXContext context,
                                       Window mainUI,
                                       final StorageUIModel model) {

        if (mainUI == null) {
            mainUI = ObserveSwingApplicationContext.get().getMainUI();
        }
        addStorageUIHandler(context);
        StorageUILauncher launcher = new StorageUILauncher(
                context,
                mainUI,
                model,
                t("observe.title.connect.existingDB")) {

            @Override
            protected void init(StorageUI ui) {
                StorageUIModel model = ui.getModel();
                DbMode dbMode = model.getDbMode();

                // exclusion des étapes non requises
                model.setExcludeSteps(Arrays.asList(
                        StorageStep.SELECT_DATA,
                        StorageStep.CONFIG_REFERENTIEL,
                        StorageStep.CONFIG_DATA,
                        StorageStep.ROLES,
                        StorageStep.BACKUP,
                        StorageStep.CONFIRM)
                );
                model.setSteps(StorageStep.CONFIG);
                model.updateUniverse();

                // par defaut, on se positionne sur la base distante ?
                model.setDbMode(dbMode);
            }

            @Override
            public void start() {
                super.start();
            }

            @Override
            protected StorageUI createUI(JAXXContext context,
                                         Window mainUI,
                                         Class<StorageUI> storageUIClass,
                                         Class<StorageUIModel> modelClass,
                                         StorageUIModel model) throws Exception {
                if (!(mainUI instanceof JAXXObject)) {
                    mainUI = null;
                }
                return super.createUI(context,
                                      mainUI,
                                      storageUIClass,
                                      modelClass,
                                      model
                );
            }

            @Override
            protected void doAction(StorageUI ui) {
                super.doAction(ui);
                if (log.isDebugEnabled()) {
                    log.debug("Apply new remote connexion to " + model);
                }
                ui.getModel().copyTo(model);
                model.validate();
            }
        };

        launcher.start();
    }

    protected static void addStorageUIHandler(JAXXContext context) {
        StorageUIHandler handler =
                context.getContextValue(StorageUIHandler.class);
        if (handler == null) {

            handler = new StorageUIHandler();
            context.setContextValue(handler);

            if (log.isWarnEnabled()) {
                log.warn("Register in context an storage handler : " + handler);
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Use existing handler " + handler);
            }
        }
    }

    public String getTitle() {
        return title;
    }

}
