package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public class ChangeActivityTripActionListener extends NodeChangeActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeActivityTripActionListener.class);

    public ChangeActivityTripActionListener(ObserveTreeHelper treeHelper,
                                            ObserveSwingDataSource dataSource,
                                            String activityId,
                                            String tripLonglineId) {
        super(treeHelper, activityId, tripLonglineId);
    }

    @Override
    protected void closeNode(String activityId) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();

        if (openDataManager.isOpenActivityLongline(activityId)) {
            openDataManager.closeActivityLongline(activityId);
        }
    }

    @Override
    protected ObserveNode getParentNode(ObserveNode node) {
        return node.getParent().getParent();
    }

    @Override
    protected ObserveNode getNewParentNode(ObserveNode grandParentNode, String parentNodeId) {
        ObserveNode tripLonglineNode = getTreeHelper().getChild(grandParentNode, parentNodeId);
        String activitiesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(ActivityLonglineDto.class);
        return getTreeHelper().getChild(tripLonglineNode, activitiesNodeId);
    }

    @Override
    protected int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId) {
        int position;

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

        ActivityLonglineService service = applicationContext.getMainDataSourceServicesProvider().newActivityLonglineService();
        position = service.moveActivityLonglineToTripLongline(nodeId, parentNodeId);

        return position;
    }

}
