/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.save;

import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Le modèle d'une opération d'export de données observers.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SaveLocalModel extends AdminActionModel {

    public static final String BACKUP_FILE_PROPERTY_NAME = "backupFile";

    public static final String DO_BACKUP_PROPERTY_NAME = "doBackup";

    public static final String CAN_SAVE_LOCAL_PROPERTY_NAME = "canSaveLocal";

    public static final String LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME = "localSourceNeedSave";

    /** Logger */
    private static final Log log = LogFactory.getLog(SaveLocalModel.class);

    /** le fichier de sauvegarde de la base locale */
    protected File backupFile = new File("");

    /**
     * un drapeau pour savoir si on doit effectuer une sauvegarde de la base
     * locale
     */
    protected boolean doBackup;

    /** un drapeau pour savoir si la source locale doit être sauvée */
    protected boolean localSourceNeedSave;

    /** la liste des etapes qui ont demande une sauvegarde */
    protected Set<AdminStep> stepsForSave;

    public SaveLocalModel() {
        super(AdminStep.SAVE_LOCAL);
    }

    public boolean isLocalSourceNeedSave() {
        return localSourceNeedSave;
    }

    public void setLocalSourceNeedSave(boolean localSourceNeedSave) {
        this.localSourceNeedSave = localSourceNeedSave;
        firePropertyChange(LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME, null,
                           localSourceNeedSave);
    }

    public boolean isDoBackup() {
        return doBackup;
    }

    public void setDoBackup(boolean doBackup) {
        boolean canSave = isCanSaveLocal();
        boolean oldValue = this.doBackup;
        this.doBackup = doBackup;
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, oldValue, doBackup);
        firePropertyChange(CAN_SAVE_LOCAL_PROPERTY_NAME, canSave, isCanSaveLocal());
    }

    public File getBackupFile() {
        return backupFile;
    }

    public void setBackupFile(File backupFile) {
        boolean canSave = isCanSaveLocal();
        File oldValue = this.backupFile;
        this.backupFile = backupFile;
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, oldValue, backupFile);
        firePropertyChange(CAN_SAVE_LOCAL_PROPERTY_NAME, canSave, isCanSaveLocal());
    }

    public Set<AdminStep> getStepsForSave() {
        if (stepsForSave == null) {
            stepsForSave = new HashSet<>();
        }
        return stepsForSave;
    }

    public boolean containsStepForsave(AdminStep step) {
        return stepsForSave != null && stepsForSave.contains(step);
    }

    /**
     * @return {@code true} si l'on peut reporter les modifications vers la base
     * locale.
     */
    public boolean isCanSaveLocal() {
        boolean validate = !doBackup ||
                           backupFile != null &&
                           !backupFile.exists() &&
                           backupFile.getName().endsWith(".sql.gz") &&
                           backupFile.getParentFile().exists();

        if (log.isDebugEnabled()) {
            log.debug("can save ? " + validate);
        }
        return validate;
    }

    public void addStepForSave(AdminStep step) {
        getStepsForSave().add(step);

        // la sauvegarde est obligatoire
        setLocalSourceNeedSave(true);
    }

}
