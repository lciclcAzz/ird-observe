package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

/**
 * Created on 1/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public class HarbourUIModel extends ContentReferenceUIModel<HarbourDto> {

    private static final long serialVersionUID = 1L;

    public HarbourUIModel() {
        super(HarbourDto.class,
              new String[]{HarbourDto.PROPERTY_NAME,
                           HarbourDto.PROPERTY_COUNTRY,
                           HarbourDto.PROPERTY_LOCODE,
                           HarbourDto.PROPERTY_LATITUDE,
                           HarbourDto.PROPERTY_LONGITUDE,
                           HarbourDto.PROPERTY_QUADRANT},
              new String[]{HarbourUI.BINDING_COUNTRY_SELECTED_ITEM,
                           HarbourUI.BINDING_HARBOUR_NAME_TEXT,
                           HarbourUI.BINDING_LOCODE_TEXT,
                           HarbourUI.BINDING_COORDINATES_LATITUDE,
                           HarbourUI.BINDING_COORDINATES_LONGITUDE,
                           HarbourUI.BINDING_COORDINATES_QUADRANT}
        );
    }

}
