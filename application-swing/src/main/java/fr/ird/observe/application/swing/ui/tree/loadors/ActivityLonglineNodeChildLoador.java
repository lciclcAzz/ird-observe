package fr.ird.observe.application.swing.ui.tree.loadors;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ui.tree.node.ActivityLonglineNode;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.node.SetLonglineNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeBridge;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created on 8/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class ActivityLonglineNodeChildLoador extends AbstractNodeChildLoador<Class, String> {

    private static final long serialVersionUID = 1L;

    public final static String FISHING_OPERATION_ID =
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.1"; // operation de peche

    private final static ImmutableSet<String> ENCOUNTERS_ID = ImmutableSet.of(
            FISHING_OPERATION_ID, // operation de peche
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.4" // rencontre interaction
    );
    private final static ImmutableSet<String> SENSOR_USED_ID = ImmutableSet.of(
            FISHING_OPERATION_ID, // operation de peche
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.3" // station oceanographique
    );

    public ActivityLonglineNodeChildLoador() {
        super(String.class);
    }

    @Override
    public void loadChilds(NavTreeBridge<ObserveNode> model,
                           ObserveNode parentNode,
                           NavDataProvider dataProvider) throws Exception {

        ObserveNode containerNode = parentNode.getContainerNode();

        if (containerNode == null) {
            throw new IllegalStateException("Could not find containerNode of " + parentNode);
        }

        DataReference<ActivityLonglineDto> activityLonglineRef = ((ActivityLonglineNode) parentNode).getEntity();

        DataReference<SetLonglineDto> setLonglineRef = (DataReference) activityLonglineRef.getPropertyValue(ActivityLonglineDto.PROPERTY_SET_LONGLINE);
        if (setLonglineRef != null) {
            parentNode.add(createSetNode(setLonglineRef));
        }

        String vesselActivityId = (String) activityLonglineRef.getPropertyValue(ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE + "Id");

        if (ENCOUNTERS_ID.contains(vesselActivityId)) {
            parentNode.add(createNode(EncounterDto.class, dataProvider));
        }
        if (SENSOR_USED_ID.contains(vesselActivityId)) {
            parentNode.add(createNode(SensorUsedDto.class, dataProvider));
        }
    }

    @Override
    public List<Class> getData(Class<?> parentClass, String parentId, NavDataProvider dataService) {
        return Collections.emptyList();
    }

    @Override
    public ObserveNode createNode(Class data, NavDataProvider dataProvider) {
        return createPluralizeStringNode(data, null);
    }

    public ObserveNode createSetNode(DataReference<SetLonglineDto> data) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new SetLonglineNode(data);
    }
}
