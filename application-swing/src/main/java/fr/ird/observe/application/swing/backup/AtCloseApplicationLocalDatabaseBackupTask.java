package fr.ird.observe.application.swing.backup;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

/**
 * Created on 19/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AtCloseApplicationLocalDatabaseBackupTask extends LocalDatabaseBackupTaskSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AtCloseApplicationLocalDatabaseBackupTask.class);

    @Override
    public void run() {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

        if (!applicationContext.getConfig().isLocalStorageExist()) {
            if (log.isInfoEnabled()) {
                log.info("Skip, no local database found.");
            }
            return;
        }

        boolean mustClose = false;
        ObserveSwingDataSource mainDataSource = applicationContext.getDataSourcesManager().getMainDataSource();
        if (mainDataSource == null || !mainDataSource.isLocal()) {

            mainDataSource = applicationContext.getDataSourcesManager().newLocalDatasource(applicationContext.getConfig());
            mustClose = true;
        }

        try {
            File file = doBackup(applicationContext, mainDataSource);
            applicationContext.getBackupsManager().addAutomaticBackup(file.toPath());
        } finally {

            if (mustClose) {
                mainDataSource.close();
            }
        }


    }

}
