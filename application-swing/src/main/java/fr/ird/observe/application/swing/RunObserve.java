/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Date;

/**
 * Pour lancer le client swing {@code ObServe} en mode administrateur.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class RunObserve extends ObserveRunner {

    /** Logger */
    private static final Log log = LogFactory.getLog(RunObserve.class);

    @Override
    public String getRunnerName() {
        return "observe";
    }

    public RunObserve(String... args) {
        super(args);
    }

    public static void main(String... args) {

        log.info("ObServe client launch at " + new Date() + " args: " + Arrays.toString(args));

        new RunObserve(args).launch();
    }

}
