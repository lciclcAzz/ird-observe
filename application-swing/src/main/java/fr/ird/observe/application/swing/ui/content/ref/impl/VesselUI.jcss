/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#generalTab {
  title:{t("observe.content.vessel.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#otherTab {
  title:{t("observe.content.vessel.tab.other")};
  icon:{getHandler().getErrorIconIfFalse(model.isOtherTabValid())};
}

#editI18nTable2 {
  border:{new TitledBorder(t("observe.common.libelles"))};
}

#codeStatusLabel {
  font-style:"italic";
}

#keelCodeFlotteLabel {
  text:"observe.common.keelCodeFlotte";
  labelFor:{keelCode};
}

#keelCode {
  property:{VesselDto.PROPERTY_KEEL_CODE};
  model:{bean.getKeelCode()};
}

#fleetCountry {
  property:{VesselDto.PROPERTY_FLEET_COUNTRY};
  model:{bean.getFleetCountry()};
}

#vesselTypeLabel {
  text:"observe.common.vesselType";
  labelFor:{vesselType};
}

#vesselType {
  property:{VesselDto.PROPERTY_VESSEL_TYPE};
  bean:{bean};
  selectedItem:{bean.getVesselType()};
}

#vesselSizeCategoryLabel {
  text:"observe.common.vesselSizeCategory";
  labelFor:{vesselSizeCategory};
}

#vesselSizeCategory {
  property:{VesselDto.PROPERTY_VESSEL_SIZE_CATEGORY};
  selectedItem:{bean.getVesselSizeCategory()};
}

#flagCountryLabel {
  text:"observe.common.flagCountry";
  labelFor:{flagCountry};
}

#flagCountry {
  property:{VesselDto.PROPERTY_FLAG_COUNTRY};
  selectedItem:{bean.getFlagCountry()};
}

#lengthLabel {
  text:"observe.common.length";
  labelFor:{length};
}

#length {
  property:{VesselDto.PROPERTY_LENGTH};
  model:{bean.getLength()};
}

#capacityLabel {
  text:"observe.common.capacity";
  labelFor:{capacity};
}

#capacity {
  property:{VesselDto.PROPERTY_CAPACITY};
  model:{bean.getCapacity()};
}

#powerLabel {
  text:"observe.common.power";
  labelFor:{power};
}

#power {
  property:{VesselDto.PROPERTY_POWER};
  model:{bean.getPower()};
}

#searchMaximumLabel {
  text:"observe.common.searchMaximum";
  labelFor:{searchMaximum};
}

#searchMaximum {
  property:{VesselDto.PROPERTY_SEARCH_MAXIMUM};
  model:{bean.getSearchMaximum()};
}

#yearServiceLabel {
  text:"observe.common.yearService";
  labelFor:{yearService};
}

#yearService {
  property:{VesselDto.PROPERTY_YEAR_SERVICE};
  model:{bean.getYearService()};
}

#changeDateLabel {
  text:"observe.common.changeDate";
  labelFor:{changeDate};
}

#changeDate {
  date:{bean.getChangeDate()};
}
