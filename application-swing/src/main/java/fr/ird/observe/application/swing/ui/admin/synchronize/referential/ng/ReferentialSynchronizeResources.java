package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTaskType;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;

import javax.swing.Icon;
import java.util.function.Predicate;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum ReferentialSynchronizeResources {

    ADD(
            "copyToRight",
            "copyToLeft",
            n("observe.actions.synchro.referential.task.addToRight"),
            n("observe.actions.synchro.referential.task.addToLeft"),
            null,
            null,
            null,
            null,
            ReferentialSynchronizeTaskType.ADD,
            ReferenceReferentialSynchroNodeSupport::isCanAdd),
    UPDATE(
            "copyToRight",
            "copyToLeft",
            n("observe.actions.synchro.referential.task.updateToRight"),
            n("observe.actions.synchro.referential.task.updateToLeft"),
            null,
            null,
            null,
            null,
            ReferentialSynchronizeTaskType.UPDATE,
            ReferenceReferentialSynchroNodeSupport::isCanUpdate),
    COPY(
            "copyToRight",
            "copyToLeft",
            null,
            null,
            n("observe.actions.synchro.referential.action.copyToRight.tip"),
            n("observe.actions.synchro.referential.action.copyToLeft.tip"),
            ReferentialSynchroModel.COPY_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.COPY_RIGHT_PROPERTY_NAME,
            null,
            node -> false),
    DELETE(
            "deleteFromLeft",
            "deleteFromRight",
            n("observe.actions.synchro.referential.task.deleteFromLeft"),
            n("observe.actions.synchro.referential.task.deleteFromRight"),
            n("observe.actions.synchro.referential.action.deleteFromLeft.tip"),
            n("observe.actions.synchro.referential.action.deleteFromRight.tip"),
            ReferentialSynchroModel.DELETE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DELETE_RIGHT_PROPERTY_NAME,
            ReferentialSynchronizeTaskType.DELETE,
            ReferenceReferentialSynchroNodeSupport::isCanDelete),
    REVERT(
            "revertFromLeft",
            "revertFromRight",
            n("observe.actions.synchro.referential.task.revertFromLeft"),
            n("observe.actions.synchro.referential.task.revertFromRight"),
            n("observe.actions.synchro.referential.action.revertFromLeft.tip"),
            n("observe.actions.synchro.referential.action.revertFromRight.tip"),
            ReferentialSynchroModel.REVERT_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.REVERT_RIGHT_PROPERTY_NAME,
            ReferentialSynchronizeTaskType.REVERT,
            ReferenceReferentialSynchroNodeSupport::isCanRevert),
    SKIP(
            "skipFromLeft",
            "skipFromRight",
            n("observe.actions.synchro.referential.task.skipFromLeft"),
            n("observe.actions.synchro.referential.task.skipFromRight"),
            n("observe.actions.synchro.referential.action.skipFromLeft.tip"),
            n("observe.actions.synchro.referential.action.skipFromRight.tip"),
            ReferentialSynchroModel.SKIP_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.SKIP_RIGHT_PROPERTY_NAME,
            null,
            node -> true),
    DESACTIVATE(
            "desactivateFromLeft",
            "desactivateFromRight",
            n("observe.actions.synchro.referential.task.desactivateWithReplaceFromLeft"),
            n("observe.actions.synchro.referential.task.desactivateWithReplaceFromRight"),
            n("observe.actions.synchro.referential.action.desactivateFromLeft.tip"),
            n("observe.actions.synchro.referential.action.desactivateFromRight.tip"),
            ReferentialSynchroModel.DESACTIVATE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DESACTIVATE_RIGHT_PROPERTY_NAME,
            ReferentialSynchronizeTaskType.DESACTIVATE,
            ReferenceReferentialSynchroNodeSupport::isCanDelete),
    DESACTIVATE_WITH_REPLACEMENT(
            "desactivateFromLeft",
            "desactivateFromRight",
            n("observe.actions.synchro.referential.task.desactivateWithReplaceFromLeftWithReplacement"),
            n("observe.actions.synchro.referential.task.desactivateWithReplaceFromRightWithReplacement"),
            n("observe.actions.synchro.referential.action.desactivateFromLeftWithReplacement.tip"),
            n("observe.actions.synchro.referential.action.desactivateFromRightWithReplacement.tip"),
            ReferentialSynchroModel.DESACTIVATE_LEFT_PROPERTY_NAME,
            ReferentialSynchroModel.DESACTIVATE_RIGHT_PROPERTY_NAME,
            ReferentialSynchronizeTaskType.DESACTIVATE,
            ReferenceReferentialSynchroNodeSupport::isCanDelete);

    private final String leftActionName;
    private final String leftTaskI18nKey;
    private final String rightActionName;
    private final String leftActionTipI18nKey;
    private final String rightTaskI18nKey;
    private final String rightActionTipI18nKey;
    private final String leftPropertyName;
    private final String rightPropertyName;
    private final Predicate<ReferenceReferentialSynchroNodeSupport> predicate;
    private final ReferentialSynchronizeTaskType taskType;
    private transient Icon actionIcon;

    ReferentialSynchronizeResources(String leftActionName,
                                    String rightActionName,
                                    String leftTaskI18nKey,
                                    String rightTaskI18nKey,
                                    String leftActionTipI18nKey,
                                    String rightActionTipI18nKey,
                                    String leftPropertyName,
                                    String rightPropertyName,
                                    ReferentialSynchronizeTaskType taskType,
                                    Predicate<ReferenceReferentialSynchroNodeSupport> predicate) {
        this.leftActionName = leftActionName;
        this.rightActionName = rightActionName;
        this.leftTaskI18nKey = leftTaskI18nKey;
        this.rightTaskI18nKey = rightTaskI18nKey;
        this.leftActionTipI18nKey = leftActionTipI18nKey;
        this.rightActionTipI18nKey = rightActionTipI18nKey;
        this.leftPropertyName = leftPropertyName;
        this.rightPropertyName = rightPropertyName;
        this.taskType = taskType;
        this.predicate = predicate;
    }

    public Predicate<ReferenceReferentialSynchroNodeSupport> getPredicate() {
        return predicate;
    }

    public Icon getIcon(boolean left) {
        if (actionIcon == null) {
            actionIcon = UIHelper.getUIManagerActionIcon(left ? leftActionName : rightActionName);
        }
        return actionIcon;
    }

    public String getTaskLabel(boolean left) {
        return left ? leftTaskI18nKey : rightTaskI18nKey;
    }

    public String getActionTip(boolean left) {
        return left ? leftActionTipI18nKey : rightActionTipI18nKey;
    }

    public String getActionName(boolean left) {
        return left ? leftActionName : rightActionName;
    }

    public String getPropertyName(boolean left) {
        return left ? leftPropertyName : rightPropertyName;
    }

    public ReferentialSynchronizeTaskType getTaskType() {
        return taskType;
    }
}
