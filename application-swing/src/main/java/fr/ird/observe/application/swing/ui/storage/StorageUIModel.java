/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingApplicationDataSourcesManager;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.storage.tabs.SecurityModel;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.runner.ObserveDataSourceConfigurationMainFactory;
import fr.ird.observe.services.security.BadObserveWebUserPasswordException;
import fr.ird.observe.services.security.ObserveWebSecurityExceptionSupport;
import fr.ird.observe.services.security.UnknownObserveWebUserException;
import fr.ird.observe.services.security.UnknownObserveWebUserForDatabaseException;
import fr.ird.observe.services.security.UserLoginNotFoundException;
import fr.ird.observe.services.security.UserPasswordNotFoundException;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.PingService;
import fr.ird.observe.services.service.SqlScriptProducerService;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.wizard.WizardModel;
import jaxx.runtime.swing.wizard.WizardUILancher;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static fr.ird.observe.application.swing.ui.admin.AdminUIModel.LOG_PROPERTY_CHANGE_LISTENER;
import static org.nuiton.i18n.I18n.t;

/**
 * Le modele de l'ui pour changer de source de données.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StorageUIModel extends WizardModel<StorageStep> {

    /** Logger */
    private static final Log log = LogFactory.getLog(StorageUIModel.class);

    public static final String DB_MODE_PROPERTY_NAME = "dbMode";

    public static final String CREATION_MODE_PROPERTY_NAME = "creationMode";

    public static final String DO_BACKUP_PROPERTY_NAME = "doBackup";

    public static final String BACKUP_FILE_PROPERTY_NAME = "backupFile";

    public static final String DUMP_FILE_PROPERTY_NAME = "dumpFile";

    public static final String LOCAL_STORAGE_EXIST_PROPERTY_NAME = "localStorageExist";

    public static final String USE_SSL_PROPERTY_NAME = "useSsl";

    public static final String REMOTE_URL_ROPERTY_NAME = "remoteUrl";

    public static final String SERVER_DATABASE_PROPERTY_NAME = "serverDatabase";

    public static final String REMOTE_LOGIN_ROPERTY_NAME = "remoteLogin";

    public static final String REMOTE_PASSWORD_PROPERTY_NAME = "remotePassword";

    public static final String STORE_REMOTE_CONFIG_PROPERTY_NAME = "storeRemoteConfig";

    public static final String CONNEXION_STATUS_PROPERTY_NAME = "connexionStatus";

    public static final String PREVIOUS_SERVICE_PROPERTY_NAME = "previousSource";

    public static final String CAN_MIGRATE_PROPERTY_NAME = "canMigrate";

    public static final String SHOW_MIGRATION_SQL_PROPERTY_NAME = "showMigrationSql";

    public static final String SHOW_MIGRATION_PROGRESSION_PROPERTY_NAME = "showMigrationProgression";

    private static final String CAN_USE_LOCALE_SERVICE_PROPERTY_NAME = "canUseLocalService";

    private static final String CAN_CREATE_LOCALE_SERVICE_PROPERTY_NAME = "canCreateLocalService";

    private static final String CAN_USE_REMOTE_SERVICE_PROPERTY_NAME = "canUseRemoteService";

    private static final String CAN_USE_SERVER_SERVICE_PROPERTY_NAME = "canUseServerService";

    public static final String REFERENTIEL_IMPORT_MODE_PROPERTY_NAME = "referentielImportMode";

    public static final String DATA_IMPORT_MODE_PROPERTY_NAME = "dataImportMode";

    public static final String EDIT_REMOTE_CONFIG_PROPERTY_NAME = "editRemoteConfig";

    public static final String EDIT_SERVER_CONFIG_PROPERTY_NAME = "editServerConfig";

    public static final String VALID_PROPERTY_NAME = "valid";

    public static final String ALREADY_APPLIED_PROPERTY_NAME = "alreadyApplied";

    private static final char[] EMPTY_PASSWORD = new char[0];

    private static final String LOGIN_REFERENTIEL = "referentiel";

    /** Le dbMode de connexion requis. */
    protected DbMode dbMode;

    protected boolean editServerConfig;

    protected boolean editRemoteConfig;

    /** Un drapeau pour savoir si une base locale existe. */
    protected boolean localStorageExist;

    /**
     * Un drapeau pour savoir si l'utilisateur a demandé une sauvegarde de la
     * base locale.
     *
     * Cette option n'est active uniquement si le drapeau localStorageExist est
     * à true.
     */
    protected boolean doBackup;

    /**
     * Un drapeau pour savoir si on a déjà lancé l'action Appliquer (pour éviter le code ré-entrant).
     *
     * @since 4.0.4
     */
    protected boolean alreadyApplied;

    /**
     * le fichier ou effectuer la sauvegarde de la base locale si le drapeau
     * doBackup est active.
     */
    protected File backupFile = new File("");

    /** un drapeau pour savoir s'il faut sauver la configuration à distante */
    protected boolean storeRemoteConfig;

    /** le storage precedemment utilise */
    protected ObserveDataSourceConfiguration previousDataSourceConfiguration;

    protected ConnexionStatus connexionStatus;
    protected String connexionStatusError;

    /** un drapeau pour definir si on peut utiliser la base locale */
    protected boolean canUseLocalService = true;

    /** un drapeau pour definir si on peut créer une base locale */
    protected boolean canCreateLocalService = true;

    /** un drapeau pour savoir si on a le droit d'utiliser une connexion distante */
    protected boolean canUseRemoteService = true;

    /** un drapeau pour savoir si on a le droit d'utiliser un serveur distant */
    protected boolean canUseServerService = true;

    /** le modèle de sélection de données (utilisé pour les exports) */
    protected DataSelectionModel selectDataModel;

    /** le modèle de sécurité (utilisé pour la mise à jour sécurité sur base distante) */
    protected SecurityModel securityModel;

    /** la configuration d'une base locale */
    protected ObserveDataSourceConfigurationTopiaH2 h2Config;

    /** la configuration d'une base distante */
    protected ObserveDataSourceConfigurationTopiaPG pgConfig;

    /** la configuration d'un serveur web */
    protected ObserveDataSourceConfigurationRest restConfig;

    protected String serverUrl;

    /** Les information de connextion a la base **/
    protected ObserveDataSourceInformation dataSourceInformation;

    /** Les information de connextion a la base **/
    protected ObserveDataSourceInformation h2DataSourceInformation;

    /** le fichier d'import des données */
    protected File dumpFile;

    /** le mode de creation d'une base */
    protected CreationMode creationMode;

    /** la configuration pour impoter les données */
    protected ObserveDataSourceConfiguration importDataConfig;

    /** l'action d'administration de base obstuna */
    protected ObstunaAdminAction adminAction;

    /** la configuration de la base de referentiel à importer */
    protected StorageUIModel centralSourceModel;

    /** la configuration de la base de données à importer */
    protected StorageUIModel dataSourceModel;

    /**
     * Le mode d'import de référentiel (uniquement pour la création de base distante).
     *
     * @since 3.0
     */
    protected CreationMode referentielImportMode;

    /**
     * Le mode d'import des données (uniquement pour la création de base distante).
     *
     * @since 3.6
     */
    protected CreationMode dataImportMode;

    public StorageUIModel() {
        super(StorageStep.class,
              StorageStep.CHOOSE_DB_MODE,
              StorageStep.BACKUP,
              StorageStep.CONFIRM
        );
        selectDataModel = new DataSelectionModel();
        securityModel = new SecurityModel();

        connexionStatus = ConnexionStatus.UNTESTED;

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationMainFactory = applicationContext.getObserveDataSourceConfigurationMainFactory();

        Version modelVersion = applicationContext.getConfig().getModelVersion();

        h2Config = configurationMainFactory.createObserveDataSourceConfigurationTopiaH2(
                t("observe.storage.label.local"),
                null,
                ObserveSwingApplicationConfig.DB_NAME,
                "",
                EMPTY_PASSWORD,
                false,
                false,
                modelVersion
        );

        pgConfig = configurationMainFactory.createObserveDataSourceConfigurationTopiaPG(
                t("observe.storage.label.remote"),
                "",
                "",
                EMPTY_PASSWORD,
                false,
                false,
                false,
                modelVersion
        );

        restConfig = configurationMainFactory.createObserveDataSourceConfigurationRest(
                t("observe.storage.label.server"),
                null,
                "",
                EMPTY_PASSWORD,
                null,
                modelVersion
        );

        PropertyChangeListener clearStatus = evt -> {
            setConnexionStatus(ConnexionStatus.UNTESTED);
            dataSourceInformation = null;
        };

        addPropertyChangeListener(REMOTE_URL_ROPERTY_NAME, clearStatus);
        addPropertyChangeListener(REMOTE_LOGIN_ROPERTY_NAME, clearStatus);
        addPropertyChangeListener(REMOTE_PASSWORD_PROPERTY_NAME, clearStatus);
        addPropertyChangeListener(SERVER_DATABASE_PROPERTY_NAME, clearStatus);
        addPropertyChangeListener(USE_SSL_PROPERTY_NAME, clearStatus);
    }

    @Override
    public void setStep(StorageStep step) {
        setAlreadyApplied(false);
        super.setStep(step);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'un service existant.
     *
     * @param context le context applicatif
     * @param source  le service existant
     */
    public void init(JAXXContext context, ObserveDataSourceConfiguration source) {

        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();

        StorageUIModel incomingModel = WizardUILancher.newModelEntry(StorageUIModel.class).getContextValue(context);

        if (incomingModel != null) {

            if (log.isDebugEnabled()) {
                log.debug("from a incoming model " + incomingModel.getLabel() + " : " + incomingModel);
            }

            // on initialie a partir d'un autre modèle
            incomingModel.copyTo(this);

            return;
        }

        StorageUIHandler storageUIHandler = context.getContextValue(StorageUIHandler.class);

        // ajout paramétrage depuis la configuration

        boolean isLocalStorageExist = config.isLocalStorageExist();
        setLocalStorageExist(isLocalStorageExist);
        if (!isLocalStorageExist) {

            // on force a ne pas pouvoir utilisaer la base locale
            setCanUseLocalService(false);
        }

        if (log.isDebugEnabled()) {
            log.debug("Local storage exists     ? " + isLocalStorageExist());
            log.debug("can use local storage    ? " + isCanUseLocalService());
            log.debug("can create local storage ? " + isCanCreateLocalService());
            log.debug("can use remote   storage ? " + isCanUseRemoteService());
            log.debug("can use server   storage ? " + isCanUseServerService());
            log.debug("previous service         ? " + source);
        }

        setStoreRemoteConfig(config.isStoreRemoteStorage());
        setBackupFile(config.newBackupDataFile());
        setDumpFile(config.getBackupDirectory());
        setShowMigrationProgression(config.isShowMigrationProgression());
        setShowMigrationSql(config.isShowMigrationSql());
        setDoBackup(false);
        setPreviousDataSourceConfiguration(source);

        if (log.isDebugEnabled()) {
            log.debug("canMigrate ? " + isCanMigrate());
        }

        CreationMode mode = config.getDefaultCreationMode();

        // on initialise les configuration avec les valeurs par défaut

        ObserveSwingApplicationDataSourcesManager dataSourcesManager = ObserveSwingApplicationContext.get().getDataSourcesManager();

        h2Config = dataSourcesManager.newH2DataSourceConfiguration(config, t("observe.storage.label.local"));
        pgConfig = dataSourcesManager.newPGDataSourceConfiguration(config, t("observe.storage.label.remote"));
        restConfig = dataSourcesManager.newRestDataSourceConfiguration(config, t("observe.storage.label.server"));

        if (source != null) {

            // on initialise le modèle à partir d'un service existant

            if (log.isDebugEnabled()) {
                log.debug("from a previous service " + source);
            }

            if (source instanceof ObserveDataSourceConfigurationTopiaH2) {

                // on est actuellement connecte sur une base locale
                fromStorageConfig((ObserveDataSourceConfigurationTopiaH2) source);

            } else if (source instanceof ObserveDataSourceConfigurationTopiaPG) {

                if (!isLocalStorageExist) {
                    // aucune base locale, on positionne le mode de création
                    // de base locale par défaut.

                    setCreationMode(mode);
                }

                // on est sur une base distante
                fromStorageConfig((ObserveDataSourceConfigurationTopiaPG) source);

            } else if (source instanceof ObserveDataSourceConfigurationRest) {

                // on est sur une base distante
                fromStorageConfig((ObserveDataSourceConfigurationRest) source);
            }

            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("no service found, using default configuration");
        }

        DbMode newDbMode = config.getDefaultDbMode();
        if (getAdminAction() != null) {

            // une action d'admin obstuna toujours sur base distante
            newDbMode = DbMode.USE_REMOTE;
        } else {
            if (isCanUseLocalService()) {
                mode = null;
            } else if (isCanCreateLocalService()) {
                mode = CreationMode.IMPORT_EXTERNAL_DUMP;
            } else if (isCanUseRemoteService()) {
                mode = CreationMode.IMPORT_REMOTE_STORAGE;
            } else if (isCanUseServerService()) {
                mode = CreationMode.IMPORT_SERVER_STORAGE;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Will use creationMode : " + mode);
        }
        setDbMode(newDbMode);
        setCreationMode(mode);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'une configuration de source de donnée existante.
     *
     * @param context        le context applicatif
     * @param previousConfig la configuration de service existant
     * @since 2.0
     */
    public void initFromPreviousConfig(JAXXContext context,
                                       ObserveDataSourceConfiguration previousConfig,
                                       ObserveDataSourceInformation previousInfo) {

        Objects.requireNonNull(previousConfig, "previousConfig parameter can not be null in method initFromPreviousConfig");

        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();

        StorageUIHandler storageUIHandler = context.getContextValue(StorageUIHandler.class);

        // ajout paramétrage depuis la configuration

        boolean isLocalStorageExist = config.isLocalStorageExist();
        setLocalStorageExist(isLocalStorageExist);

        if (log.isDebugEnabled()) {
            log.debug("Local storage exists     ? " + isLocalStorageExist());
            log.debug("can use local storage    ? " + isCanUseLocalService());
            log.debug("can create local storage ? " + isCanCreateLocalService());
            log.debug("can use remote   storage ? " + isCanUseRemoteService());
            log.debug("can use server   storage ? " + isCanUseServerService());
            log.debug("previous service         ? " + previousConfig);
        }

        setStoreRemoteConfig(config.isStoreRemoteStorage());
        setBackupFile(config.newBackupDataFile());
        setDumpFile(config.getBackupDirectory());
        setShowMigrationProgression(config.isShowMigrationProgression());
        setShowMigrationSql(config.isShowMigrationSql());
        setDoBackup(false);

        if (log.isDebugEnabled()) {
            log.debug("canMigrate ? " + isCanMigrate());
        }

        ObserveSwingApplicationDataSourcesManager dataSourcesManager = ObserveSwingApplicationContext.get().getDataSourcesManager();

        // on initialise les configuration avec les valeurs par défaut
        h2Config = dataSourcesManager.newH2DataSourceConfiguration(config, t("observe.storage.label.local"));
        pgConfig = dataSourcesManager.newPGDataSourceConfiguration(config, t("observe.storage.label.remote"));
        restConfig = dataSourcesManager.newRestDataSourceConfiguration(config, t("observe.storage.label.server"));

        if (previousConfig instanceof ObserveDataSourceConfigurationTopiaH2) {

            // on est actuellement connecte sur une base locale
            fromStorageConfig((ObserveDataSourceConfigurationTopiaH2) previousConfig);
            h2DataSourceInformation = previousInfo;
        } else if (previousConfig instanceof ObserveDataSourceConfigurationTopiaPG) {

            if (!isLocalStorageExist) {

                // aucune base locale, on ne peut pas utiliser la base locale
                setCanUseLocalService(false);
            }

            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationTopiaPG) previousConfig);

        } else if (previousConfig instanceof ObserveDataSourceConfigurationRest) {

            // on est sur une base distante
            fromStorageConfig((ObserveDataSourceConfigurationRest) previousConfig);
        }
        setDataSourceInformation(previousInfo);
    }

    /**
     * La méthode pour initialiser le modèle à partir du context applicatif et
     * d'un service existant.
     */
    public void initFromModel() {

        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();


        // ajout paramétrage depuis la configuration
        boolean isLocalStorageExist = config.isLocalStorageExist();
        setLocalStorageExist(isLocalStorageExist);
        if (!isLocalStorageExist) {

            // on force a ne pas pouvoir utilisaer la base locale
            setCanUseLocalService(false);
        }

        if (log.isDebugEnabled()) {
            log.debug("Local storage exists     ? " + isLocalStorageExist());
            log.debug("can use local storage    ? " + isCanUseLocalService());
            log.debug("can create local storage ? " + isCanCreateLocalService());
            log.debug("can use remote   storage ? " + isCanUseRemoteService());
            log.debug("can use server   storage ? " + isCanUseServerService());
        }

        setStoreRemoteConfig(config.isStoreRemoteStorage());
        setDumpFile(config.getBackupDirectory());
        setShowMigrationProgression(config.isShowMigrationProgression());
        setShowMigrationSql(config.isShowMigrationSql());
        setDoBackup(false);
        setPreviousDataSourceConfiguration(null);

        if (log.isDebugEnabled()) {
            log.debug("canMigrate ? " + isCanMigrate());
        }

        CreationMode mode = config.getDefaultCreationMode();

        if (log.isDebugEnabled()) {
            log.debug("no service found, using default configuration");
        }

        // aucun service de persistance ouvert,
        // on utilise la configuration par defaut
        // ce cas ne devrait arriver uniquement tant qu'aucune
        // base locale est crée

        ObserveSwingApplicationDataSourcesManager dataSourcesManager = ObserveSwingApplicationContext.get().getDataSourcesManager();

        h2Config = dataSourcesManager.newH2DataSourceConfiguration(config, t("observe.storage.label.local"));
        pgConfig = dataSourcesManager.newPGDataSourceConfiguration(config, t("observe.storage.label.remote"));
        restConfig = dataSourcesManager.newRestDataSourceConfiguration(config, t("observe.storage.label.server"));

        DbMode newDbMode = config.getDefaultDbMode();
        if (getAdminAction() != null) {

            // une action d'admin obstuna toujours sur base distante
            newDbMode = DbMode.USE_REMOTE;
        } else {
            if (isCanUseLocalService()) {
                mode = null;
            } else if (isCanCreateLocalService()) {
                mode = CreationMode.IMPORT_EXTERNAL_DUMP;
            } else if (isCanUseRemoteService()) {
                mode = CreationMode.IMPORT_REMOTE_STORAGE;
            } else if (isCanUseServerService()) {
                mode = CreationMode.IMPORT_SERVER_STORAGE;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Will use creationMode : " + mode);
        }
        setDbMode(newDbMode);
        setCreationMode(mode);
    }

    protected void startCentralSourceModel() {

        if (!isNeedReferentielDataSource()) {

            // pas besoin de la base distante
            return;
        }

        // par default, on tente d'utiliser la base distance
        getCentralSourceModel().initFromModel();
        centralSourceModel.setCanCreateLocalService(false);
        centralSourceModel.setCanUseLocalService(false);
        centralSourceModel.setCanUseRemoteService(true);
        centralSourceModel.setCanUseServerService(true);
        centralSourceModel.start(DbMode.USE_REMOTE);

        if (log.isDebugEnabled()) {
            centralSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            centralSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }

        // on teste si la connexion distante existe
        if (centralSourceModel.isValid()) {
            centralSourceModel.testRemote();
        }
    }

    protected void startCentralDataSourceModel() {

        if (!isNeedDataDataSource()) {

            // pas besoin de la base distante
            return;
        }

        // par default, on tente d'utiliser la base distance
        getDataSourceModel().initFromModel();
        dataSourceModel.setCanCreateLocalService(false);
        dataSourceModel.setCanUseLocalService(false);
        dataSourceModel.setCanUseRemoteService(true);
        dataSourceModel.setCanUseServerService(true);
        dataSourceModel.start(DbMode.USE_REMOTE);

        if (log.isDebugEnabled()) {
            dataSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            dataSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }

        // on teste si la connexion distante existe
        if (dataSourceModel.isValid()) {
            dataSourceModel.testRemote();
        }
    }

    public boolean isNeedReferentielDataSource() {
        return adminAction != null &&
                adminAction == ObstunaAdminAction.CREATE;
    }

    public boolean isNeedDataDataSource() {
        return adminAction != null &&
                adminAction == ObstunaAdminAction.CREATE;
    }

    public void start(DbMode dbMode) {

        if (log.isDebugEnabled()) {
            log.debug("Will use dbMode       : " + dbMode + " vs previous mode " + getDbMode());
        }
        setDbMode(dbMode);

        startCentralSourceModel();
        startCentralDataSourceModel();

        start();
        firePropertyChange(DB_MODE_PROPERTY_NAME, getDbMode());
        firePropertyChange(CREATION_MODE_PROPERTY_NAME, getCreationMode());
        firePropertyChange(DUMP_FILE_PROPERTY_NAME, getDumpFile());
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, getBackupFile());
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, isDoBackup());
        firePropertyChange(CAN_USE_LOCALE_SERVICE_PROPERTY_NAME, isCanUseLocalService());
        firePropertyChange(CAN_CREATE_LOCALE_SERVICE_PROPERTY_NAME, isCanCreateLocalService());
        firePropertyChange(CAN_USE_REMOTE_SERVICE_PROPERTY_NAME, isCanUseRemoteService());
        firePropertyChange(CAN_USE_SERVER_SERVICE_PROPERTY_NAME, isCanUseServerService());
        firePropertyChange(CONNEXION_STATUS_PROPERTY_NAME, getConnexionStatus());
        firePropertyChange(PREVIOUS_SERVICE_PROPERTY_NAME, getPreviousDataSourceConfiguration());
        firePropertyChange(LOCAL_STORAGE_EXIST_PROPERTY_NAME, isLocalStorageExist());
        firePropertyChange(STORE_REMOTE_CONFIG_PROPERTY_NAME, isStoreRemoteConfig());
        firePropertyChange(REMOTE_URL_ROPERTY_NAME, getRemoteUrl());
        firePropertyChange(REMOTE_LOGIN_ROPERTY_NAME, getRemoteLogin());
        firePropertyChange(REMOTE_PASSWORD_PROPERTY_NAME, getRemotePassword());
        firePropertyChange(SERVER_DATABASE_PROPERTY_NAME, getServerDatabase());

        firePropertyChange(EDIT_REMOTE_CONFIG_PROPERTY_NAME, isEditRemoteConfig());
        firePropertyChange(EDIT_SERVER_CONFIG_PROPERTY_NAME, isEditServerConfig());

        firePropertyChange(USE_SSL_PROPERTY_NAME, null, isUseSsl());
        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, isCanMigrate());
        firePropertyChange(SHOW_MIGRATION_SQL_PROPERTY_NAME, isShowMigrationSql());
        firePropertyChange(SHOW_MIGRATION_PROGRESSION_PROPERTY_NAME, isShowMigrationProgression());
    }

    public boolean isCanUseLocalService() {
        return canUseLocalService;
    }

    public boolean isCanCreateLocalService() {
        return canCreateLocalService;
    }

    public boolean isCanUseRemoteService() {
        return canUseRemoteService;
    }

    public boolean isCanUseServerService() {
        return canUseServerService;
    }

    public SecurityModel getSecurityModel() {
        return securityModel;
    }

    public ObstunaAdminAction getAdminAction() {
        return adminAction;
    }

    public String getAdminActionLabel() {
        return I18nEnumHelper.getLabel(getAdminAction());
    }

    public DataSelectionModel getSelectDataModel() {
        return selectDataModel;
    }

    public void setSelectDataModel(DataSelectionModel selectDataModel) {
        this.selectDataModel = selectDataModel;
    }

    public CreationMode getReferentielImportMode() {
        return referentielImportMode;
    }

    public CreationMode getDataImportMode() {
        return dataImportMode;
    }

    public boolean isImportReferentiel() {
        return referentielImportMode != null && CreationMode.EMPTY != referentielImportMode;
    }

    public boolean isImportData() {
        return dataImportMode != null && CreationMode.EMPTY != dataImportMode;
    }

    public StorageUIModel getCentralSourceModel() {
        if (centralSourceModel == null) {
            centralSourceModel = new StorageUIModel() {

                @Override
                public String getLabel() {
                    String txt = t("observe.storage.label.import.referentiel");
                    if (getDbMode() == DbMode.CREATE_LOCAL) {
                        txt = t("observe.storage.label.import.referentiel", getH2Config().getDirectory().getAbsolutePath());
                    } else if (isRemote()) {
                        txt = t("observe.storage.label.import.referentiel.remote", getRemoteUrl());
                    } else if (isServer()) {
                        txt = t("observe.storage.label.import.referentiel.server", getServerUrl());
                    }
                    return txt;
                }

                @Override
                public void validate() {
                    super.validate();

                    // on declanche la revalidation du modèle
                    StorageUIModel.this.firePropertyChange(VALID_PROPERTY_NAME, isValid());
                }
            };
        }
        return centralSourceModel;
    }

    public StorageUIModel getDataSourceModel() {
        if (dataSourceModel == null) {
            dataSourceModel = new StorageUIModel() {

                @Override
                public String getLabel() {
                    String txt = t("observe.storage.label.import.data");
                    if (getDbMode() == DbMode.CREATE_LOCAL) {
                        txt = t("observe.storage.label.import.data", getH2Config().getDirectory().getAbsolutePath());
                    } else if (isRemote()) {
                        txt = t("observe.storage.label.import.data.remote", getRemoteUrl());
                    } else if (isServer()) {
                        txt = t("observe.storage.label.import.data.server", getServerUrl());
                    }
                    return txt;
                }

                @Override
                public void validate() {
                    super.validate();

                    // on declanche la revalidation du modèle
                    StorageUIModel.this.firePropertyChange(VALID_PROPERTY_NAME, isValid());
                }
            };
        }
        return dataSourceModel;
    }

    public void setAdminAction(ObstunaAdminAction adminAction) {
        this.adminAction = adminAction;
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

    @Override
    public void updateUniverse() {
        if (dbMode == null) {

            // pas de mode choisi donc l'univers ne change pas
            return;
        }
        List<StorageStep> universe = new ArrayList<>();
        if (adminAction == null) {

            // when doing an admin mode we do not choose db mode, we always work on remote
            universe.add(StorageStep.CHOOSE_DB_MODE);
        }

        boolean canBackup = localStorageExist;
        switch (dbMode) {
            case CREATE_LOCAL:
                CreationMode creationMode = getCreationMode();

                if (creationMode != null) {
                    switch (creationMode) {
                        case IMPORT_EXTERNAL_DUMP:
                            universe.add(StorageStep.CONFIG);
                            break;
                        //case IMPORT_LOCAL_STORAGE:
                        case IMPORT_REMOTE_STORAGE:
                            importDataConfig = pgConfig;
                            universe.add(StorageStep.CONFIG);
                            break;
                        case IMPORT_SERVER_STORAGE:
                            importDataConfig = restConfig;
                            universe.add(StorageStep.CONFIG);
                            break;
                    }
                }
                break;
            case USE_LOCAL:
                // pas de backup si on veut utiliser la base locale
                canBackup = false;
                break;
            case USE_REMOTE:
                canBackup = false;
                universe.add(StorageStep.CONFIG);
                break;
            case USE_SERVER:
                canBackup = false;
                universe.add(StorageStep.CONFIG);
                break;
        }
        if (canBackup) {
            universe.add(StorageStep.BACKUP);

            // when backup is possible always 
            setDoBackup(true);
        }
        if (adminAction != null) {
            if (adminAction == ObstunaAdminAction.CREATE) {
                universe.add(StorageStep.CONFIG_REFERENTIEL);

                if (getReferentielImportMode() != null && getReferentielImportMode() != CreationMode.EMPTY) {

                    // can import data only if import a referentiel
                    universe.add(StorageStep.CONFIG_DATA);
                }

                if (getDataImportMode() != null && getDataImportMode() != CreationMode.EMPTY) {
                    universe.add(StorageStep.SELECT_DATA);
                }
            }
            universe.add(StorageStep.ROLES);
        }
        universe.add(StorageStep.CONFIRM);
        if (excludeSteps != null) {
            universe.removeAll(excludeSteps);
        }
        if (log.isDebugEnabled()) {
            log.debug("steps to use : " + universe);
        }
        setSteps(universe.toArray(new StorageStep[universe.size()]));
    }

    @Override
    public boolean validate(StorageStep s) {
        boolean validate = super.validate(s);
        if (validate) {
            switch (s) {
                case CHOOSE_DB_MODE:
                    validate = dbMode != null;
                    if (validate) {
                        if (!isCanUseRemoteService() && dbMode.equals(DbMode.USE_REMOTE)) {
                            validate = false;
                        } else if (!isCanUseLocalService() && dbMode.equals(DbMode.USE_LOCAL)) {
                            validate = false;
                        } else if (!isCanCreateLocalService() && dbMode.equals(DbMode.CREATE_LOCAL)) {
                            validate = false;
                        } else if (!isCanUseServerService() && dbMode.equals(DbMode.USE_SERVER)) {
                            validate = false;
                        }
                    }
                    if (validate && isLocal()) {
                        if (dbMode == DbMode.CREATE_LOCAL) {
                            validate = getCreationMode() != null;
                        }
                    }
                    break;
                case CONFIG:
                    if (DbMode.USE_REMOTE.equals(dbMode)
                            || DbMode.USE_SERVER.equals(dbMode)
                            || DbMode.CREATE_LOCAL.equals(dbMode)
                            && (CreationMode.IMPORT_SERVER_STORAGE.equals(creationMode)
                            || CreationMode.IMPORT_REMOTE_STORAGE.equals(creationMode))) {
                        validate = ConnexionStatus.SUCCESS.equals(getConnexionStatus());
                        //FIXME Il faut savoir si l'utilisateur est le
                        //FIXME propriétaire de la base
//                        if (validate) {
//                            if (getAdminAction() != null) {
//
//                                // il faut des droits rw sur la base distante
//                                validate = dataSourceInformation != null &&
//                                        dataSourceInformation.canReadReferential() &&
//                                        dataSourceInformation.canWriteReferential() &&
//                                        dataSourceInformation.canWriteData() &&
//                                        dataSourceInformation.canReadData();
//                            }
//                        }
                    } else if (DbMode.CREATE_LOCAL.equals(dbMode) && CreationMode.IMPORT_EXTERNAL_DUMP.equals(creationMode)) {
                        validate = isValidDumpFile(dumpFile);
                    }
                    break;
                case CONFIG_REFERENTIEL:

                    switch (getReferentielImportMode()) {

                        case EMPTY:
                            validate = true;
                            break;
                        case IMPORT_LOCAL_STORAGE:
                        case IMPORT_INTERNAL_DUMP:

                            break;
                        case IMPORT_EXTERNAL_DUMP:
                            // external dumb must be filled
                            validate = isValidDumpFile(centralSourceModel.getDumpFile());
                            break;
                        case IMPORT_REMOTE_STORAGE:
                        case IMPORT_SERVER_STORAGE:
                            // remote db connexion must be valid
                            validate = centralSourceModel.isValid();

                            if (validate) {

                                // check remote db != remote import db
                                validate = !centralSourceModel.getRemoteUrl().equals(
                                        getRemoteUrl());
                            }
                            if (validate) {

                                validate = centralSourceModel.getDataSourceInformation().canReadReferential();
                            }
                            break;
                    }

                    break;

                case CONFIG_DATA:

                    switch (getDataImportMode()) {

                        case EMPTY:
                            validate = true;
                            break;
                        case IMPORT_LOCAL_STORAGE:
                        case IMPORT_INTERNAL_DUMP:

                            break;
                        case IMPORT_EXTERNAL_DUMP:
                            // external dumb must be filled
                            validate = isValidDumpFile(dataSourceModel.getDumpFile());
                            break;
                        case IMPORT_REMOTE_STORAGE:
                            // remote db connexion must be valid
                            validate = dataSourceModel.isValid();

                            if (validate) {

                                // check remote db != remote import db
                                validate = !dataSourceModel.getRemoteUrl().equals(getRemoteUrl());

                            }

                            if (validate) {

                                validate = dataSourceModel.getDataSourceInformation().canReadData();

                            }

                            break;
                    }

                    break;
                case BACKUP:
                    validate = !doBackup ||
                            backupFile != null &&
                                    !backupFile.exists() &&
                                    backupFile.getName().endsWith(".sql.gz");
                    break;
                case SELECT_DATA:
                    // chemit 20100525 : aucune contrainte dans ce cas
                    validate = selectDataModel != null;
                    break;
                case ROLES:

                    Set<ObserveDbUserDto> role = getSecurityModel().getUsers();

                    int assigned = getSecurityModel().getAssigned();

                    validate = role.size() == assigned;
                    break;
                case CONFIRM:
                    validate = true;
                    break;
            }
        }
        return validate;
    }

    public void validate() {
        super.validate();

        // toujours forcer la propagation
        firePropertyChange(VALID_PROPERTY_NAME, isValid());
    }

    public boolean isValid() {
        boolean result = false;
        if (dbMode != null) {
            switch (dbMode) {

                case CREATE_LOCAL:
                    result = getCreationMode() != null &&
                            validate(StorageStep.CONFIG);
                    break;
                case USE_REMOTE:
                case USE_SERVER:
                    result = validate(StorageStep.CONFIG);
                    break;
                case USE_LOCAL:
                    //TODO  A tester...
                    result = h2Config.getDatabaseFile().exists();
                    break;
            }
        }
        return result;
    }

    public boolean isAlreadyApplied() {
        return alreadyApplied;
    }

    public void setAlreadyApplied(boolean alreadyApplied) {
        boolean oldValue = isAlreadyApplied();
        this.alreadyApplied = alreadyApplied;
        firePropertyChange(ALREADY_APPLIED_PROPERTY_NAME, oldValue, alreadyApplied);
    }

    public boolean isUseSelectData() {
        return getSteps() != null &&
                getSteps().contains(StorageStep.SELECT_DATA);
    }

    public boolean isBackupAction() {
        return getSteps() != null && getStepIndex(StorageStep.BACKUP) == 0;
    }

    public boolean isLocal() {
        return dbMode != null && (dbMode == DbMode.CREATE_LOCAL || dbMode == DbMode.USE_LOCAL);
    }

    public boolean isRemote() {
        return dbMode != null && dbMode == DbMode.USE_REMOTE;
    }

    public boolean isServer() {
        return dbMode != null && dbMode == DbMode.USE_SERVER;
    }

    public boolean isCanMigrate() {
        boolean result = false;
        if (dbMode != null) {
            switch (dbMode) {
                case USE_LOCAL:
                    result = true;
                    break;
                case CREATE_LOCAL:
                    // en cration de base local on autorise les migration uniquement pour les imports de dump
                    result = CreationMode.IMPORT_EXTERNAL_DUMP.equals(creationMode) || CreationMode.IMPORT_INTERNAL_DUMP.equals(creationMode);
                    break;
                case USE_REMOTE:
                case USE_SERVER:
                    result = ObstunaAdminAction.UPDATE.equals(adminAction);
                    break;
            }
        }
        return result;
    }

    public String getLabel() {
        String txt;
        if (dbMode == DbMode.CREATE_LOCAL) {
            txt = h2Config.getLabel();
        } else {
            if (isRemote()) {
                txt = pgConfig.getLabel();
            } else {
                txt = restConfig.getLabel();
            }
        }
        return txt;
    }

    public boolean isLocalStorageExist() {
        return localStorageExist;
    }

    public boolean isStoreRemoteConfig() {
        return storeRemoteConfig;
    }

    public File getBackupFile() {
        return backupFile;
    }

    public boolean isDoBackup() {
        return doBackup;
    }

    public Version getModelVersion() {
        return ObserveSwingApplicationContext.get().getConfig().getModelVersion();
    }

    public File getInitialDbDump() {
        return ObserveSwingApplicationContext.get().getConfig().getInitialDbDump();
    }

    public DbMode getDbMode() {
        return dbMode;
    }

    public boolean isEditRemoteConfig() {
        return editRemoteConfig;
    }

    public boolean isEditServerConfig() {
        return editServerConfig;
    }

    public ObserveDataSourceConfiguration getPreviousDataSourceConfiguration() {
        return previousDataSourceConfiguration;
    }

    public void setDbMode(DbMode dbMode) {
        DbMode oldValue = this.dbMode;
        this.dbMode = dbMode;
        firePropertyChange(DB_MODE_PROPERTY_NAME, oldValue, dbMode);
        updateEditConfig();
        if (oldValue != dbMode) {

            // on recalcule l'univers des étapes disponibles
            updateUniverse();
        }

        // validation du modèle
        validate();

        // le drapeau canMigrate peut avoir changé, on doit notifié les écouteurs
        firePropertyChange(CAN_MIGRATE_PROPERTY_NAME, null, isCanMigrate());
    }

    protected void updateEditConfig() {

        boolean oldValueRemote = isEditRemoteConfig();
        boolean oldValueServer = isEditServerConfig();

        editRemoteConfig = DbMode.USE_REMOTE.equals(dbMode) ||
                DbMode.CREATE_LOCAL.equals(dbMode) && CreationMode.IMPORT_REMOTE_STORAGE.equals(creationMode);
        firePropertyChange(EDIT_REMOTE_CONFIG_PROPERTY_NAME, oldValueRemote, editRemoteConfig);

        editServerConfig = DbMode.USE_SERVER.equals(dbMode) ||
                DbMode.CREATE_LOCAL.equals(dbMode) && CreationMode.IMPORT_SERVER_STORAGE.equals(creationMode);
        firePropertyChange(EDIT_SERVER_CONFIG_PROPERTY_NAME, oldValueServer, editServerConfig);
    }

    public void updateEditConfigValues() {

        if (isEditRemoteConfig()) {
            // bind from pg config model
            setRemoteLogin(pgConfig.getUsername());
            setRemoteUrl(pgConfig.getJdbcUrl());
            setRemotePassword(pgConfig.getPassword());
        }

        if (isEditServerConfig()) {
            // bind from rest config model
            setRemoteLogin(restConfig.getLogin());
            setRemoteUrl(restConfig.getServerUrl() == null ? "" : restConfig.getServerUrl().toString());
            setRemotePassword(restConfig.getPassword());
            setServerDatabase(restConfig.getOptionalDatabaseName().orElse(null));
        }

    }

    public void setLocalStorageExist(boolean localStorageExist) {
        boolean oldValue = this.localStorageExist;
        this.localStorageExist = localStorageExist;
        firePropertyChange(LOCAL_STORAGE_EXIST_PROPERTY_NAME, oldValue,
                           localStorageExist);
        validate();
    }

    public void setStoreRemoteConfig(boolean storeRemoteConfig) {
        boolean oldValue = this.storeRemoteConfig;
        this.storeRemoteConfig = storeRemoteConfig;
        firePropertyChange(STORE_REMOTE_CONFIG_PROPERTY_NAME, oldValue,
                           storeRemoteConfig);
    }

    public void setDoBackup(boolean doBackup) {
        boolean oldValue = this.doBackup;
        this.doBackup = doBackup;
        firePropertyChange(DO_BACKUP_PROPERTY_NAME, oldValue, doBackup);
    }

    public void setBackupFile(File backupFile) {
        File oldValue = this.backupFile;
        this.backupFile = backupFile;
        firePropertyChange(BACKUP_FILE_PROPERTY_NAME, oldValue, backupFile);
        validate();
    }

    public void setReferentielImportMode(CreationMode referentielImportMode) {
        CreationMode oldValue = this.referentielImportMode;
        this.referentielImportMode = referentielImportMode;
        firePropertyChange(REFERENTIEL_IMPORT_MODE_PROPERTY_NAME,
                           oldValue,
                           referentielImportMode);

        if (oldValue != referentielImportMode) {
            updateUniverse();
        }
        validate();
    }

    public void setDataImportMode(CreationMode dataImportMode) {
        CreationMode oldValue = this.dataImportMode;
        this.dataImportMode = dataImportMode;
        firePropertyChange(DATA_IMPORT_MODE_PROPERTY_NAME,
                           oldValue,
                           dataImportMode);
        if (oldValue != dataImportMode) {

            // reset selected data to import
            setSelectDataModel(null);

            updateUniverse();
        }
        validate();
    }

    public void setExcludeSteps(List<StorageStep> excludeSteps) {
        this.excludeSteps = excludeSteps;
    }

    public void destroy() {
        super.destroy();
    }

    public void setShowMigrationSql(boolean showMigrationSql) {
        boolean oldValue = pgConfig.isShowMigrationSql();
        pgConfig.setShowMigrationSql(showMigrationSql);
        h2Config.setShowMigrationSql(showMigrationSql);
        firePropertyChange(SHOW_MIGRATION_SQL_PROPERTY_NAME, oldValue, showMigrationSql);
    }

    public void setShowMigrationProgression(boolean showMigrationProgression) {
        boolean oldValue = pgConfig.isShowMigrationProgression();
        pgConfig.setShowMigrationProgression(showMigrationProgression);
        h2Config.setShowMigrationProgression(showMigrationProgression);
        firePropertyChange(SHOW_MIGRATION_PROGRESSION_PROPERTY_NAME, oldValue, showMigrationProgression);
    }

    // ----------------------------------------------------------
    // -- h2Config delegate methods
    // ----------------------------------------------------------

    public CreationMode getCreationMode() {
        return creationMode;
    }

    public File getDumpFile() {
        return dumpFile;
    }

    public void setCreationMode(CreationMode creationMode) {
        CreationMode oldValue = getCreationMode();
        this.creationMode = creationMode;
        firePropertyChange(CREATION_MODE_PROPERTY_NAME, oldValue, creationMode);
        updateEditConfig();
        if (oldValue != creationMode) {
            updateUniverse();
        }
        if (CreationMode.IMPORT_REMOTE_STORAGE.equals(creationMode) || CreationMode.IMPORT_SERVER_STORAGE.equals(creationMode)) {
            //TC-20100308 : dans le cas d'un import du référentiel distant
            //TC-20100308 : on utilise toujours l'utilisateur referentiel
            String s = getRemoteLogin();
            if (s != null && !LOGIN_REFERENTIEL.equals(s)) {
                setRemoteLogin(LOGIN_REFERENTIEL);
                setRemotePassword(EMPTY_PASSWORD);
            }
        }
        validate();
    }

    public void setDumpFile(File dumpFile) {
        File oldValue = this.dumpFile;
        this.dumpFile = dumpFile;
        firePropertyChange(DUMP_FILE_PROPERTY_NAME, oldValue, dumpFile);
        validate();
    }

    // ----------------------------------------------------------
    // -- pg2Config delegate methods
    // ----------------------------------------------------------

    public String getRemoteLogin() {
        String login = "";

        if (isEditRemoteConfig()) {

            login = pgConfig.getUsername();

        } else if (isEditServerConfig()) {

            login = restConfig.getLogin();

        }
        return login;
    }

    public char[] getRemotePassword() {
        char[] password = EMPTY_PASSWORD;

        if (isEditRemoteConfig()) {

            password = pgConfig.getPassword();

        } else if (isEditServerConfig()) {

            password = restConfig.getPassword();

        }
        return password;
    }

    public String getRemoteUrl() {
        String url = "";

        if (isEditRemoteConfig()) {

            url = pgConfig.getJdbcUrl();

        } else if (isEditServerConfig()) {

            url = serverUrl;

        }
        return url;
    }

    protected String getServerUrl() {
        return serverUrl;
    }

    protected void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public boolean isUseSsl() {
        return pgConfig.isUseSsl();
    }

    public String getServerDatabase() {
        return restConfig.getOptionalDatabaseName().orElse(null);
    }

    public ConnexionStatus getConnexionStatus() {
        return connexionStatus;
    }

    public void setPreviousDataSourceConfiguration(ObserveDataSourceConfiguration previousDataSourceConfiguration) {
        this.previousDataSourceConfiguration = previousDataSourceConfiguration;
    }

    public ObserveDataSourceConfigurationTopiaH2 getH2Config() {
        return h2Config;
    }

    public ObserveDataSourceConfigurationTopiaPG getPgConfig() {
        return pgConfig;
    }

    public ObserveDataSourceConfigurationRest getRestConfig() {
        return restConfig;
    }

    public void setRemoteLogin(String remoteLogin) {

//        String oldValue = getRemoteLogin();

        if (isEditRemoteConfig()) {

            pgConfig.setUsername(remoteLogin);

        } else if (isEditServerConfig()) {

            restConfig.setLogin(remoteLogin);

        }

        firePgConfigChanged(REMOTE_LOGIN_ROPERTY_NAME, null, remoteLogin);
    }

    public void setRemotePassword(char... remotePassword) {
//        char[] oldValue = getRemotePassword();

        if (isEditRemoteConfig()) {

            pgConfig.setPassword(remotePassword);

        } else if (isEditServerConfig()) {

            restConfig.setPassword(remotePassword);

        }
        firePgConfigChanged(REMOTE_PASSWORD_PROPERTY_NAME, null, remotePassword);
    }

    public void setCanUseLocalService(boolean canUseLocalService) {
        this.canUseLocalService = canUseLocalService;
        firePgConfigChanged(CAN_USE_LOCALE_SERVICE_PROPERTY_NAME, null,
                            canUseLocalService);
    }

    public void setCanCreateLocalService(boolean canCreateLocalService) {
        this.canCreateLocalService = canCreateLocalService;
        firePgConfigChanged(CAN_CREATE_LOCALE_SERVICE_PROPERTY_NAME, null,
                            canCreateLocalService);
    }

    public void setCanUseRemoteService(boolean canUseRemoteService) {
        this.canUseRemoteService = canUseRemoteService;
        firePropertyChange(CAN_USE_REMOTE_SERVICE_PROPERTY_NAME, null, canUseRemoteService);
    }

    public void setCanUseServerService(boolean canUseServerService) {
        this.canUseServerService = canUseServerService;
        firePropertyChange(CAN_USE_SERVER_SERVICE_PROPERTY_NAME, null, canUseServerService);
    }

    public void setRemoteUrl(String remoteUrl) {
        if (isEditRemoteConfig()) {

            pgConfig.setJdbcUrl(remoteUrl);

        } else if (isEditServerConfig()) {

            serverUrl = remoteUrl;

        }
        firePgConfigChanged(REMOTE_URL_ROPERTY_NAME, null, remoteUrl);
    }

    public void setUseSsl(boolean useSsl) {
        pgConfig.setUseSsl(useSsl);
        firePgConfigChanged(USE_SSL_PROPERTY_NAME, null, useSsl);
    }

    public void setServerDatabase(String database) {
        if (StringUtils.isBlank(database)) {
            restConfig.setOptionalDatabaseName(null);
        } else {
            restConfig.setOptionalDatabaseName(database);
        }
        firePgConfigChanged(SERVER_DATABASE_PROPERTY_NAME, null, database);
    }

    // ----------------------------------------------------------
    // -- StorageConfigSupport implementation methods
    // ----------------------------------------------------------

    public void fromStorageConfig(ObserveDataSourceConfigurationTopiaH2 sourceConfig) {

        setDbMode(sourceConfig.getDatabaseFile().exists() ? DbMode.USE_LOCAL : DbMode.CREATE_LOCAL);
        h2Config = sourceConfig;
    }

    public void fromStorageConfig(ObserveDataSourceConfigurationTopiaPG sourceConfig) {
        setDbMode(DbMode.USE_REMOTE);
        pgConfig = sourceConfig;
    }

    public void fromStorageConfig(ObserveDataSourceConfigurationRest sourceConfig) {
        setDbMode(DbMode.USE_SERVER);
        restConfig = sourceConfig;
    }

    public DataSourceCreateConfigurationDto getCreationConfigurationDto() throws IOException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        DataSourceCreateConfigurationDto result = null;

        if (getDbMode() == DbMode.CREATE_LOCAL) {
            result = new DataSourceCreateConfigurationDto();

            if (CreationMode.IMPORT_EXTERNAL_DUMP.equals(creationMode) || CreationMode.IMPORT_INTERNAL_DUMP.equals(creationMode)) {

                File dumpFile;

                if (CreationMode.IMPORT_EXTERNAL_DUMP.equals(creationMode)) {
                    dumpFile = getDumpFile();
                } else {
                    dumpFile = ObserveSwingApplicationContext.get().getConfig().getInitialDbDump();
                }

                byte[] dump = Files.readAllBytes(dumpFile.toPath());
                result.setImportDatabase(dump);

            } else {

                ObserveDataSourceConfiguration configSrc = null;

                switch (getCreationMode()) {
                    case IMPORT_LOCAL_STORAGE:
                        configSrc = h2Config;
                        break;
                    case IMPORT_REMOTE_STORAGE:
                        configSrc = pgConfig;
                        break;
                    case IMPORT_SERVER_STORAGE:
                        configSrc = restConfig;
                        break;
                }

                if (configSrc != null) {
                    try (ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(configSrc)) {

                        source.open();

                        SqlScriptProducerService dumpService = source.newSqlScriptProducerService();

                        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential();
                        byte[] dump = dumpService.produceAddSqlScript(request).getSqlCode();

                        result.setImportDatabase(dump);
                    }

                }

            }

        }

        return result;
    }

    public ObserveDataSourceConfigurationTopiaH2 toH2StorageConfig(String label) {

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationFactory = context.getObserveDataSourceConfigurationMainFactory();
        return configurationFactory.createObserveDataSourceConfigurationTopiaH2(
                label,
                h2Config.getDirectory(),
                h2Config.getDbName(),
                h2Config.getUsername(),
                h2Config.getPassword(),
                h2Config.isShowMigrationProgression(),
                h2Config.isShowMigrationSql(),
                h2Config.getModelVersion()
        );
    }

    public ObserveDataSourceConfigurationTopiaPG toPGStorageConfig(String label) {

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationFactory = context.getObserveDataSourceConfigurationMainFactory();
        return configurationFactory.createObserveDataSourceConfigurationTopiaPG(
                label,
                pgConfig.getJdbcUrl(),
                pgConfig.getUsername(),
                pgConfig.getPassword(),
                pgConfig.isUseSsl(),
                pgConfig.isShowMigrationProgression(),
                pgConfig.isShowMigrationSql(),
                pgConfig.getModelVersion()
        );
    }

    public ObserveDataSourceConfigurationRest toRestStorageConfig(String label) {

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationFactory = context.getObserveDataSourceConfigurationMainFactory();
        return configurationFactory.createObserveDataSourceConfigurationRest(
                label,
                restConfig.getServerUrl(),
                restConfig.getLogin(),
                restConfig.getPassword(),
                restConfig.getOptionalDatabaseName().orElse(null),
                restConfig.getModelVersion()
        );
    }

    protected void copyTo(StorageUIModel dst) {
        dst.setLocalStorageExist(isLocalStorageExist());
        dst.setBackupFile(getBackupFile());
        dst.setDoBackup(isDoBackup());
        dst.setStoreRemoteConfig(isStoreRemoteConfig());
        dst.setPreviousDataSourceConfiguration(getPreviousDataSourceConfiguration());
        dst.setCanCreateLocalService(isCanCreateLocalService());
        dst.setCanUseLocalService(isCanUseLocalService());
        dst.setCanUseRemoteService(isCanUseRemoteService());
        dst.setCanUseServerService(isCanUseServerService());
        dst.fromStorageConfig(h2Config);
        dst.fromStorageConfig(pgConfig);
        dst.fromStorageConfig(restConfig);
        dst.setDbMode(getDbMode());
        dst.setSelectDataModel(getSelectDataModel());
        dst.setSecurityModel(getSecurityModel());
        dst.setAdminAction(getAdminAction());

        dst.updateEditConfig();

        dst.setUseSsl(isUseSsl());
        dst.setRemoteUrl(getRemoteUrl());
        dst.setRemoteLogin(getRemoteLogin());
        dst.setRemotePassword(getRemotePassword());
        dst.setServerDatabase(getServerDatabase());

        dst.setDataSourceInformation(getDataSourceInformation());
        dst.setConnexionStatus(getConnexionStatus());

    }

    public void firePgConfigChanged(String propertyName,
                                    Object oldValue,
                                    Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
        validate();
    }

    /**
     * Teste une connexion à distance à partir du modèle passé en paramètre.
     *
     * @return {@code true} si la connexion a pu être établie,
     * {@code false} sinon.
     */
    public boolean testRemote() {

        ObserveDataSourceConfiguration config = null;
        boolean result = false;
        boolean error = false;
        connexionStatusError = "";
        setBusy(true);

        try {

            if (isEditRemoteConfig()) {
                config = pgConfig;
            } else if (isEditServerConfig()) {
                try {
                    URL url = new URL(serverUrl);

                    restConfig.setServerUrl(url);

                    config = restConfig;

                } catch (MalformedURLException e) {

                    connexionStatusError = t("observe.storage.error.badUrl", serverUrl);

                    setConnexionStatus(ConnexionStatus.FAILED);

                }

            }

            if (config != null) {

                ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(config);

                try {

                    if (DbMode.USE_SERVER.equals(getDbMode())
                            || DbMode.CREATE_LOCAL.equals(getDbMode()) && CreationMode.IMPORT_SERVER_STORAGE.equals(creationMode)) {

                        PingService pingService = dataSource.newPingService();

                        Version modelServerVersion = pingService.ping();

                        if (!getModelVersion().equals(modelServerVersion)) {

                            connexionStatusError = t("observe.storage.error.serverVersionMismatch", modelServerVersion, getModelVersion());

                            setConnexionStatus(ConnexionStatus.FAILED);

                            error = true;
                        }

                    }

                    if (!error) {

                        dataSourceInformation = dataSource.checkCanConnect();

                        Version versionDataSource = dataSourceInformation.getVersion();

                        // en mise a jour de la base on ne test pas la version
                        if (getModelVersion().equals(versionDataSource) || isCanMigrate() || ObstunaAdminAction.CREATE.equals(adminAction)) {

                            setConnexionStatus(ConnexionStatus.SUCCESS);

                            result = true;

                        } else {

                            connexionStatusError = t("observe.storage.error.dbVersionMismatch", versionDataSource, getModelVersion());

                            setConnexionStatus(ConnexionStatus.FAILED);

                        }
                    }

                } catch (UnknownObserveWebUserException e) {

                    connexionStatusError = t("observe.storage.error.rest.user.unknown", e.getUserLogin());
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (BadObserveWebUserPasswordException e) {

                    connexionStatusError = t("observe.storage.error.rest.password.bad", e.getUserLogin());
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (UnknownObserveWebUserForDatabaseException e) {

                    connexionStatusError = t("observe.storage.error.rest.database.unknownForUser", e.getDatabaseName(), e.getRole());
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (UserLoginNotFoundException e) {

                    connexionStatusError = t("observe.storage.error.rest.user.required");
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (UserPasswordNotFoundException e) {

                    connexionStatusError = t("observe.storage.error.rest.pasword.required");
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (ObserveWebSecurityExceptionSupport e) {

                    connexionStatusError = e.getClass().getSimpleName();
                    setConnexionStatus(ConnexionStatus.FAILED);

                } catch (Throwable e) {

                    if (e instanceof UndeclaredThrowableException) {
                        e = ((UndeclaredThrowableException)e).getUndeclaredThrowable();
                    }
                    connexionStatusError = e.getMessage();

                    if (log.isErrorEnabled()) {
                        log.error("Error in test remote", e);
                    }

                    setConnexionStatus(ConnexionStatus.FAILED);

                } finally {
                    if (dataSource.isOpen()) {
                        dataSource.close();
                    }
                }
            }

        } finally {
            setBusy(false);
        }

        return result;
    }


    public String getConnexionStatusError() {
        return connexionStatusError;
    }

    public void setSecurityModel(SecurityModel securityModel) {
        this.securityModel = securityModel;
    }

    public boolean isShowMigrationSql() {
        return pgConfig.isShowMigrationSql();
    }

    public boolean isShowMigrationProgression() {
        return pgConfig.isShowMigrationProgression();
    }

    public void setConnexionStatus(ConnexionStatus connexionStatus) {
        ConnexionStatus oldValue = this.connexionStatus;
        this.connexionStatus = connexionStatus;
        firePgConfigChanged(CONNEXION_STATUS_PROPERTY_NAME, oldValue, connexionStatus);
    }

    public void checkImportDbVersion(ObserveSwingDataSource source) {

        Version importServiceDbVersion = source.getVersion();
        Version currentDbVersion = getModelVersion();
        if (importServiceDbVersion.before(currentDbVersion)) {
            throw new IllegalStateException("Import db version (" + importServiceDbVersion + ") is not compatible with the current database version (" + currentDbVersion + ")");
        }

    }

    public DataSourceCreateConfigurationDto toImportReferentielSourceConfig() {

        DataSourceCreateConfigurationDto importReferenceConfig = new DataSourceCreateConfigurationDto();

        // on peut créer une base vide
        importReferenceConfig.setCanCreateEmptyDatabase(true);

        if (isImportReferentiel()) {

            String dbLabel = t("observe.storage.label.reference.import.db");

            switch (getReferentielImportMode()) {

                case IMPORT_EXTERNAL_DUMP:

                    if (ObstunaAdminAction.CREATE.equals(getAdminAction())) {

                        // on import que le référentiel donc on créé un base temporaire pour cette import
                        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

                        DataSourceCreateConfigurationDto createConfigurationDto = new DataSourceCreateConfigurationDto();

                        File dumpFile = getCentralSourceModel().getDumpFile();

                        try (FileInputStream inputStream = new FileInputStream(dumpFile)) {

                            byte[] bytes = IOUtils.toByteArray(inputStream);

                            createConfigurationDto.setImportDatabase(bytes);

                        } catch (Exception e) {

                            throw new RuntimeException("Could not read dump file", e);

                        }

                        try (ObserveSwingDataSource importDataSource = context.getDataSourcesManager().newTemporaryH2Datasource(dbLabel)) {

                            try {
                                importDataSource.create(createConfigurationDto);
                            } catch (IncompatibleDataSourceCreateConfigurationException
                                    | DataSourceCreateWithNoReferentialImportException
                                    | DatabaseNotFoundException
                                    | DatabaseConnexionNotAuthorizedException
                                    | BabModelVersionException e) {
                                throw new ObserveSwingTechnicalException("Could not create import data source", e);
                            }

                            importReferenceConfig.setImportReferentialDataSourceConfiguration(importDataSource.getConfiguration());

                        }


                    } else {

                        File dumpFile = getDumpFile();

                        try (FileInputStream inputStream = new FileInputStream(dumpFile)) {

                            byte[] bytes = IOUtils.toByteArray(inputStream);

                            importReferenceConfig.setImportDatabase(bytes);

                        } catch (IOException e) {

                            throw new RuntimeException("Could not read dump file", e);

                        }

                    }


                    break;
                case IMPORT_REMOTE_STORAGE:

                    // import referentiel from a remote db
                    ObserveDataSourceConfigurationTopiaPG pgConfig = getCentralSourceModel().toPGStorageConfig(dbLabel);

                    pgConfig.setCanMigrate(false);

                    importReferenceConfig.setImportReferentialDataSourceConfiguration(pgConfig);

                    break;

                case IMPORT_SERVER_STORAGE:

                    // import referentiel from a server db
                    ObserveDataSourceConfigurationRest restConfig = getCentralSourceModel().toRestStorageConfig(dbLabel);

                    importReferenceConfig.setImportReferentialDataSourceConfiguration(restConfig);

                    break;

                default:
                    throw new IllegalStateException("Can't come here");

            }

        }

        return importReferenceConfig;


    }

    public ObserveSwingDataSource toImportDataSourceConfig() {

        ObserveSwingDataSource importDataSource;

        if (isImportData()) {

            String dbLabel = t("observe.storage.label.data.import.db");

            switch (getDataImportMode()) {

                case IMPORT_EXTERNAL_DUMP:

                    ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

                    DataSourceCreateConfigurationDto createConfigurationDto = new DataSourceCreateConfigurationDto();

                    File dumpFile = ObstunaAdminAction.CREATE.equals(getAdminAction()) ? getDataSourceModel().getDumpFile() : getDumpFile();

                    try (FileInputStream inputStream = new FileInputStream(dumpFile)) {

                        byte[] bytes = IOUtils.toByteArray(inputStream);

                        createConfigurationDto.setImportDatabase(bytes);


                    } catch (Exception e) {

                        throw new RuntimeException("Could not read dump file", e);

                    }

                    try (ObserveSwingDataSource importDataSource2 = context.getDataSourcesManager().newTemporaryH2Datasource(dbLabel)) {

                        importDataSource = importDataSource2;

                        try {
                            importDataSource2.create(createConfigurationDto);
                        } catch (IncompatibleDataSourceCreateConfigurationException
                                | DataSourceCreateWithNoReferentialImportException
                                | DatabaseNotFoundException
                                | DatabaseConnexionNotAuthorizedException
                                | BabModelVersionException e) {
                            throw new ObserveSwingTechnicalException("Could not create import data source", e);
                        }

                    }

                    break;
                case IMPORT_REMOTE_STORAGE: {

                    // import referentiel from a remote db
                    ObserveDataSourceConfigurationTopiaPG dataSourceConfig = getDataSourceModel().toPGStorageConfig(dbLabel);

                    dataSourceConfig.setCanMigrate(false);

                    importDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(dataSourceConfig);

                }
                break;

                case IMPORT_SERVER_STORAGE: {

                    // import referentiel from a server db
                    ObserveDataSourceConfigurationRest dataSourceConfig = getDataSourceModel().toRestStorageConfig(dbLabel);

                    importDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(dataSourceConfig);

                }
                break;
                default:
                    throw new IllegalStateException("Can't come here");
            }

        } else {

            importDataSource = null;

        }

        return importDataSource;

    }

    public ObserveDataSourceInformation getDataSourceInformation() {
        if (DbMode.USE_LOCAL.equals(dbMode)) {
            return getH2DataSourceInformation();
        }

        return dataSourceInformation;
    }

    public ObserveDataSourceInformation getH2DataSourceInformation() {
        if (h2DataSourceInformation == null && localStorageExist) {
            ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(h2Config);
            try {
                h2DataSourceInformation = dataSource.checkCanConnect();
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("error on load data source information for local storage", e);
                }
            }
        }

        return h2DataSourceInformation;
    }

    public void setDataSourceInformation(ObserveDataSourceInformation dataSourceInformation) {
        this.dataSourceInformation = dataSourceInformation;
    }

    private boolean isValidDumpFile(File dumpFile) {
        return dumpFile != null &&
                dumpFile.exists() &&
                dumpFile.getName().endsWith(".sql.gz");
    }
}
