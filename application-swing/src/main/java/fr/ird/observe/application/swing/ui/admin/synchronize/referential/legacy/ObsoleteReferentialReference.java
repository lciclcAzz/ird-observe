package fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.util.Objects;

/**
 * Représente une référence obsolète à remplacer.
 *
 * Created on 13/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ObsoleteReferentialReference<R extends ReferentialDto> {

    private final ReferentialReference<R> referentialReference;

    public ObsoleteReferentialReference(Class<R>referentialName, ReferentialReference<R> referentialReference) {
        this.referentialReference = referentialReference;
    }

    public Class<R> getReferentialName() {
        return getType();
    }

    public ReferentialReference<R> getReferentialReference() {
        return referentialReference;
    }

    public String getId() {
        return referentialReference.getId();
    }

    public Class<R> getType() {
        return referentialReference.getType();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObsoleteReferentialReference that = (ObsoleteReferentialReference) o;
        return Objects.equals(getReferentialReference(), that.getReferentialReference());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReferentialReference());
    }
}
