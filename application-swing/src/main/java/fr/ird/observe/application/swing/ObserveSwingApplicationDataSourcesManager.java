package fr.ird.observe.application.swing;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.backup.BackupStorage;
import fr.ird.observe.application.swing.backup.BackupsManager;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceListenerAdapter;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import fr.ird.observe.application.swing.ui.actions.ChangeStorageAction;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.runner.ObserveDataSourceConfigurationMainFactory;

import javax.swing.*;
import java.io.Closeable;
import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;

import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig.DB_NAME;
import static fr.ird.observe.application.swing.ui.UIHelper.*;
import static jaxx.runtime.swing.editor.bean.BeanUIUtil.PopupHandler.log;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Permet de gérer les différentes data sources utilisées dans l'application.
 * <p>
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ObserveSwingApplicationDataSourcesManager implements Closeable {

    private final List<ObserveSwingDataSource> dataSources = new LinkedList<>();

    private ObserveSwingDataSource dataSource;

    /**
     * Construit une source de données sur la base locale de l'application.
     * <p>
     * <b>Note:</b> La base locale doit exister, sinon on soulève une
     * exeception
     *
     * @param config la configuration à utiliser
     * @return la service de persistance initialisé (mais non ouvert)
     */
    public ObserveSwingDataSource newLocalDatasource(ObserveSwingApplicationConfig config) {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = newH2DataSourceConfiguration(config, t("observe.storage.label.local"));

        Preconditions.checkState(dataSourceConfigurationH2.getDatabaseFile().exists(), "local base must exist, when using this method (" +
                dataSourceConfigurationH2.getDirectory() + ')');
        return newDataSource(dataSourceConfigurationH2);
    }

    public ObserveDataSourceConfigurationTopiaH2 newH2DataSourceConfiguration(ObserveSwingApplicationConfig config, String label) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationMainFactory = applicationContext.getObserveDataSourceConfigurationMainFactory();

        File dbDirectory = new File(config.getLocalDBDirectory(), DB_NAME);

        return configurationMainFactory.createObserveDataSourceConfigurationTopiaH2(
                label,
                dbDirectory,
                DB_NAME,
                config.getH2Login(),
                config.getH2Password().toCharArray(),
                config.isShowMigrationProgression(),
                config.isShowMigrationSql(),
                config.getModelVersion()
        );
    }

    public ObserveSwingDataSource newTemporaryH2Datasource(String label) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveSwingApplicationConfig config = applicationContext.getConfig();
        ObserveDataSourceConfigurationMainFactory configurationMainFactory = applicationContext.getObserveDataSourceConfigurationMainFactory();

        File tmpDirectory = config.getTmpDirectory();

        File dbDirectory = new File(tmpDirectory, ObserveSwingApplicationConfig.DB_NAME + UUID.randomUUID().toString());

        return new ObserveSwingDataSource(configurationMainFactory.createObserveDataSourceConfigurationTopiaH2(
                label,
                dbDirectory,
                ObserveSwingApplicationConfig.DB_NAME,
                config.getH2Login(),
                config.getH2Password().toCharArray(),
                false,
                false,
                config.getModelVersion()
        ));

    }

    public ObserveDataSourceConfigurationTopiaPG newPGDataSourceConfiguration(ObserveSwingApplicationConfig config, String label) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationMainFactory = applicationContext.getObserveDataSourceConfigurationMainFactory();

        return configurationMainFactory.createObserveDataSourceConfigurationTopiaPG(
                label,
                config.getObstunaUrl(),
                config.getObstunaLogin(),
                config.getObstunaPassword().toCharArray(),
                config.isObstunaUseSslCert(),
                config.isShowMigrationProgression(),
                config.isShowMigrationSql(),
                config.getModelVersion()
        );
    }

    public ObserveDataSourceConfigurationRest newRestDataSourceConfiguration(ObserveSwingApplicationConfig config, String label) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveDataSourceConfigurationMainFactory configurationMainFactory = applicationContext.getObserveDataSourceConfigurationMainFactory();

        return configurationMainFactory.createObserveDataSourceConfigurationRest(
                label,
                config.getServerUrl(),
                config.getServerLogin(),
                config.getServerPassword().toCharArray(),
                config.getServerDatabaseName(),
                config.getModelVersion()
        );
    }

    public ObserveSwingDataSource getMainDataSource() {
        return dataSource;
    }

    public void setMainDataSource(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ObserveSwingDataSource newDataSource(ObserveDataSourceConfiguration configuration) {
        ObserveSwingDataSource dataSource = new ObserveSwingDataSource(configuration);

        dataSource.addObserveSwingDataSourceListener(new ObserveSwingDataSourceListenerAdapter() {

            @Override
            public void onOpened(ObserveSwingDataSourceEvent event) {
                super.onOpened(event);
                ObserveSwingDataSource dataSource = event.getSource();
                dataSources.add(dataSource);

                if (log.isInfoEnabled()) {
                    log.info("Data source opened : " + dataSource.getConfiguration() + " (" + dataSources.size() + " datas sources open)");
                }
            }

            @Override
            public void onClosed(ObserveSwingDataSourceEvent event) {
                super.onClosed(event);
                ObserveSwingDataSource dataSource = event.getSource();
                dataSources.remove(dataSource);
                if (log.isInfoEnabled()) {
                    log.info("Data source closed : " + dataSource.getConfiguration() + " (" + dataSources.size() + " datas sources open)");
                }
            }
        });

        return dataSource;
    }

    @Override
    public void close() {

        // fermeture de touts les context de donnée ouvert
        for (ObserveSwingDataSource dataSource : Lists.newArrayList(dataSources)) {
            if (log.isInfoEnabled()) {
                log.info("Closing dataSource : " + dataSource.getConnection());
            }
            try {
                dataSource.close();
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close data source: " + dataSource, e);
                }
            }
        }
        setMainDataSource(null);

    }

    void initStorage(ObserveSwingApplicationConfig config, ObserveMainUI mainUI, ObserveSwingApplicationContext applicationContext) {


        BackupsManager backupsManager = applicationContext.getBackupsManager();

        BackupStorage lastAutomaticBackup = backupsManager.getAutomaticBackups().stream().findFirst().orElse(null);

        boolean askToCreate = true;
        if (config.isLocalStorageExist()) {

            // une base locale existe, on l'ouvre

            ObserveSwingDataSource dataSource = newLocalDatasource(config);

            try {

                ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect();

                dataSource.migrateDataIfPossible(dataSourceInformation, config.getModelVersion());

                // la source sera utilisée dans les ui
                prepareMainStorage(dataSource, true);

                if (log.isInfoEnabled()) {
                    log.info(t("observe.init.local.db.detected", dataSource.getLabel()));
                }

                // la base locale existe, on l'ouvre
                dataSource.open();

                // on peut retourner sur cette base
                mainUI.setMode(ObserveUIMode.DB);

                askToCreate = false;
            } catch (Exception e) {

                if (lastAutomaticBackup != null) {

                    log.error("Could not load local database", e);

                    InitStorageModel initStorageModel = new InitStorageModel(config.getLocalDBDirectory(), lastAutomaticBackup);

                    JOptionPane.showMessageDialog(
                            mainUI,
                            t("observe.init.local.db.error", initStorageModel.getBackupDate()),
                            t("observe.title.error.dialog"),
                            JOptionPane.WARNING_MESSAGE
                    );
                    loadBackup(config, mainUI, lastAutomaticBackup);
                    askToCreate = false;

                }

            }

        }

        if (askToCreate) {

            askToCreateLocalDatabase(config, mainUI, applicationContext, lastAutomaticBackup);
        }

        if (log.isInfoEnabled()) {
            log.info(t("observe.init.storage.done"));
        }
    }

    /**
     * Prepare le storage principal qui servira dans les ui.
     *
     * @param dataSource la source de données a preparer
     */
    public void prepareMainStorage(ObserveSwingDataSource dataSource, boolean setAsMainDataSource) {

        if (setAsMainDataSource) {

            setMainDataSource(dataSource);

        }

        dataSource.addObserveSwingDataSourceListener(new ObserveSwingDataSourceListenerAdapter() {

            final ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

            @Override
            public void onOpening(ObserveSwingDataSourceEvent event) {
                ObserveSwingDataSource s = event.getSource();
                displayInfo(t("observe.message.db.loading", s.getLabel()));
            }

            @Override
            public void onOpened(ObserveSwingDataSourceEvent event) {

                // le service est disponible, on enregistre les listeners
                ObserveSwingDataSource source = event.getSource();

                try {
                    openOnUI(source);
                } catch (Exception e) {

                    // la base n'a pas pu être chargée proprement
                    // ceci peut être due a une base dans une version pas
                    // assez recente, on doit donc refermer cette base
                    handlingError("Could not obtain open datas from " + source.getLabel(), e);

                    // fermeture de la source
                    source.close();
                }

            }

            @Override
            public void onClosing(ObserveSwingDataSourceEvent event) {

                super.onClosing(event);

                ObserveMainUI mainUI = applicationContext.getMainUI();
                DataContext dataContext = applicationContext.getDataContext();
                dataContext.setEnabled(false);

                // suppresion des opens dans le context de données
                applicationContext.getConfig().setTreeOpenNodeIds(dataContext.getOpenIds());
                dataContext.populateOpens();

                mainUI.getTreeHelper().cleanNavigationUI(mainUI);

                // on met a jour l'état dans la config
                applicationContext.getConfig().setMainStorageOpened(false);

            }


            @Override
            public void onClosed(ObserveSwingDataSourceEvent event) {
                // le service est indisponible, il faut supprimer toutes les
                // references vers le service
                ObserveSwingDataSource source = event.getSource();

                // on ferme la marée, la route ou l'activité ouvertes
                applicationContext.getOpenDataManager().close();

                ObserveMainUI mainUI = applicationContext.getMainUI();

                // nettoyage de l'ui ( suppression navigation et autres )
                if (mainUI != null) {

                    if (log.isDebugEnabled()) {
                        log.debug("dispose ui from storage " + source.getLabel() + ": " + mainUI.getName());
                    }

                    mainUI.setMode(ObserveUIMode.NO_DB);

                    displayInfo(t("observe.message.db.closed", source.getLabel()));

                }

                source.removeObserveSwingDataSourceListener(this);

            }

            protected void openOnUI(ObserveSwingDataSource source) {
                // on rend le service disponible dans le service de validation
                //getValidationContext().setMainDataSource(source);

                // toutes les données sont chargées, on peut declarer le service
                // comme ouvert dans la configuration
                ObserveSwingApplicationConfig config = applicationContext.getConfig();
                config.setMainStorageOpened(true);
                config.setMainStorageOpenedLocal(source.isLocal());

                if (source.isLocal()) {
                    config.setLocalStorageExist(true);
                }

                // remplissage de l'ui
                ObserveMainUI mainUI = applicationContext.getMainUI();

                if (mainUI != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("loading ui for storage " + source.getLabel() + ": " + mainUI.getName());
                    }

                    DataContext dataContext = mainUI.getDataContext();
                    dataContext.setEnabled(true);

                    ObserveTreeHelper treeHelper = mainUI.getTreeHelper();

                    treeHelper.cleanNavigationUI(mainUI);

                    String[] openIds = config.getTreeOpenNodeIds();

                    if (source.canReadData()) {
                        applicationContext.getOpenDataManager().sanitizeOpenIds(openIds);
                    }

                    dataContext.populateOpens(openIds);
                    treeHelper.loadNavigationUI(source);

                    mainUI.setMode(ObserveUIMode.DB);

                    mainUI.getStatus().setStatus(t("observe.message.db.loaded", source.getLabel()));

                }
            }
        });
    }

    public static class InitStorageModel {
        private final File localDb;
        private final BackupStorage lastAutomaticBackup;

        private final SimpleDateFormat simpleDateFormat;

        InitStorageModel(File localDb, BackupStorage lastAutomaticBackup) {
            this.localDb = localDb;
            this.lastAutomaticBackup = lastAutomaticBackup;
            simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
        }

        public File getLocalDb() {
            return localDb;
        }

        public boolean isWithBackup() {
            return lastAutomaticBackup != null;
        }

        public String getBackupDate() {
            return Optional.ofNullable(lastAutomaticBackup).map(l -> simpleDateFormat.format(l.getDate())).orElse("");
        }
    }

    private void askToCreateLocalDatabase(ObserveSwingApplicationConfig config,
                                          ObserveMainUI mainUI,
                                          ObserveSwingApplicationContext applicationContext,
                                          BackupStorage lastAutomaticBackup) {

        InitStorageModel initStorageModel = new InitStorageModel(config.getLocalDBDirectory(), lastAutomaticBackup);

        String text = applicationContext.getTextGenerator().getInitStorage(initStorageModel);

        Object[] options;
        int defaultOption;
        boolean noAutomaticBackup = lastAutomaticBackup == null;
        if (noAutomaticBackup) {
            options = new Object[]{
                    t("observe.choice.useRemoteStorage"),
                    t("observe.choice.createLocalStorage"),
                    t("observe.choice.doNothing")};
            defaultOption = 1;

        } else {
            options = new Object[]{
                    t("observe.choice.useRemoteStorage"),
                    t("observe.choice.createLocalStorage"),
                    t("observe.choice.loadLastAutomaticBackup"),
                    t("observe.choice.doNothing")};
            defaultOption = 2;

        }

        JLabel label = new JLabel();
        label.setText(text);
        int response = askUser(
                null,
                t("observe.title.no.local.db.found"),
                label,
                JOptionPane.QUESTION_MESSAGE,
                options,
                defaultOption
        );
        if (log.isDebugEnabled()) {
            log.debug("response : " + response);
        }

        if (noAutomaticBackup) {
            Set<DbMode> dbModes = EnumSet.noneOf(DbMode.class);
            String title = null;
            if (response != JOptionPane.CLOSED_OPTION && response < 2) {

                if (response == 1) {
                    // creation de la base locale
                    dbModes.add(DbMode.CREATE_LOCAL);
                    title = n("observe.title.create.local.db");
                } else {
                    // connexion à une base distante
                    dbModes.add(DbMode.USE_REMOTE);
                    dbModes.add(DbMode.USE_SERVER);
                    title = n("observe.title.load.remote.db");
                }
            }
            if (!dbModes.isEmpty()) {
                new ChangeStorageAction(mainUI, dbModes, title).run();
            }
        } else {

            Set<DbMode> dbModes = EnumSet.noneOf(DbMode.class);
            String title = null;
            if (response != JOptionPane.CLOSED_OPTION && response < 3) {

                switch (response) {
                    case 2:
                        // load last backup
                        loadBackup(config, mainUI, lastAutomaticBackup);
                        break;
                    case 1:
                        // create local db
                        // creation de la base locale
                        dbModes.add(DbMode.CREATE_LOCAL);
                        title = n("observe.title.create.local.db");
                        break;
                    case 0:
                        // use remote db
                        dbModes.add(DbMode.USE_REMOTE);
                        dbModes.add(DbMode.USE_SERVER);
                        title = n("observe.title.load.remote.db");
                        break;
                }
            }
            if (!dbModes.isEmpty()) {
                new ChangeStorageAction(mainUI, dbModes, title).run();
            }
        }

    }

    private void loadBackup(ObserveSwingApplicationConfig config, ObserveMainUI mainUI, BackupStorage backupStorage) {
        log.info("Will load last backup: " + backupStorage.getFile());
        try {
            byte[] dump = Files.readAllBytes(backupStorage.getFile().toPath());
            DataSourceCreateConfigurationDto createDto = new DataSourceCreateConfigurationDto();
            createDto.setImportDatabase(dump);

            ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = newH2DataSourceConfiguration(config, t("observe.storage.label.local"));

            ObserveSwingDataSource dataSource = newDataSource(dataSourceConfigurationH2);
            prepareMainStorage(dataSource, true);

            dataSource.create(createDto);

            mainUI.setMode(ObserveUIMode.DB);

        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Impossible de créer la base locale", e);
        }
    }

}
