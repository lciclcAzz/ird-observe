<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI superGenericType='SpeciesStatusDto'>

  <style source="../ReferenceEntity.jcss"/>
  <style source="../I18nReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.constants.ReferenceStatus
    fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto
    fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto'
                 context='ui-create'
                 errorTableModel='{getErrorTableModel()}'/>

  <!-- model -->
  <SpeciesStatusUIModel id='model'/>

  <!-- edit bean -->
  <SpeciesStatusDto id='bean'/>

  <Table id='editTable'>

    <!-- uri -->
    <row>
      <cell anchor="west">
        <JLabel id='uriLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='uri' onKeyReleased='getBean().setUri(uri.getText())'/>
      </cell>
    </row>

    <!-- code / status -->
    <row>
      <cell anchor="west">
        <JLabel id='codeStatusLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
          <JTextField id='code' constraints='BorderLayout.WEST'
                      onKeyReleased='getBean().setCode(code.getText())'/>
          <EnumEditor id='status' constraints='BorderLayout.CENTER'
                      constructorParams='ReferenceStatus.class'
                      genericType='ReferenceStatus'
                      onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'/>
        </JPanel>
      </cell>
    </row>

    <!-- needComment -->
    <row>
      <cell anchor='east' weightx="1" fill="both" columns="2">
        <JCheckBox id='needComment'
                   onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
      </cell>
    </row>
  </Table>

  <Table id='editI18nTable'>
    <row>
      <cell anchor="west">
        <JLabel id='label1Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label1'
                    onKeyReleased='getBean().setLabel1(label1.getText())'/>
      </cell>
      <cell anchor="west">
        <JLabel id='label2Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label2'
                    onKeyReleased='getBean().setLabel2(label2.getText())'/>
      </cell>
    </row>
    <row>
      <cell anchor="west">
        <JLabel id='label3Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label3'
                    onKeyReleased='getBean().setLabel3(label3.getText())'/>
      </cell>
      <cell anchor="west">
        <JLabel id='label4Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label4'
                    onKeyReleased='getBean().setLabel4(label4.getText())'/>
      </cell>
    </row>
    <row>
      <cell anchor="west">
        <JLabel id='label5Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label5'
                    onKeyReleased='getBean().setLabel5(label5.getText())'/>
      </cell>
      <cell anchor="west">
        <JLabel id='label6Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label6'
                    onKeyReleased='getBean().setLabel6(label6.getText())'/>
      </cell>
    </row>
    <row>
      <cell anchor="west">
        <JLabel id='label7Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label7'
                    onKeyReleased='getBean().setLabel7(label7.getText())'/>
      </cell>
      <cell anchor="west">
        <JLabel id='label8Label'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='label8'
                    onKeyReleased='getBean().setLabel8(label8.getText())'/>
      </cell>
    </row>
  </Table>
</fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI>
