/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

NumberEditor {
  bean:{tableEditBean};
  autoPopup:{config.isAutoPopupNumberEditor()};
  showPopupButton:{config.isShowNumberEditorButton()};
  showReset:true;
}

JToolBar {
  borderPainted:false;
  floatable:false;
  opaque:false;
}

#speciesLabel {
  labelFor:{species};
}

#species {
  property:"species";
  bean:{tableEditBean};
  showReset:true;
  selectedItem:{tableEditBean.getSpecies()};
}

#lengthLabel {
  labelFor:{length};
}

#length {
  property:"length";
  model:{tableEditBean.getLength()};
  useFloat:true;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.DECIMAL1_PATTERN};
}

#lengthSourceInformation {
  disabledIcon: {iconDataObserve};
  icon: {iconDataCalcule};
  enabled: {tableEditBean.isLengthSource()};
  toolTipText:{getLengthDataTip(tableEditBean.isLengthSource())};
}

#weightLabel {
  text:"observe.common.weight.ind";
  labelFor:{weight};
}

#weight {
  property:"weight";
  model:{tableEditBean.getWeight()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.DECIMAL2_PATTERN};
  _validatorLabel:{t("observe.common.weight.ind")};
}

#weightSourceInformation {
  disabledIcon: {iconDataObserve};
  icon: {iconDataCalcule};
  enabled: {tableEditBean.isWeightSource()};
  toolTipText:{getWeightDataTip(tableEditBean.isWeightSource())};
}

#comment {
  minimumSize:{new Dimension(10,80)};
}

#comment2 {
  text:{getStringValue(bean.getComment())};
}
