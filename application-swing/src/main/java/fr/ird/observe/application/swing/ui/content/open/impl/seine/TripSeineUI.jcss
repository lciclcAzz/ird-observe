/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanComboBox {
  bean:{bean};
}

NumberEditor {
  bean:{bean};
  useFloat:false;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
}

#generalTab {
  title: {t("observe.content.tripSeine.tab.general")};
}

#mapTab {
  title: {t("observe.content.tripSeine.tab.map")};
}

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

#observerLabel {
  text:"observe.common.observer";
  labelFor:{observer};
}

#observer {
  property:{TripSeineDto.PROPERTY_OBSERVER};
  selectedItem:{bean.getObserver()};
}

#captainLabel {
  text:"observe.common.captain";
  labelFor:{captain};
}

#captain {
  property:{TripSeineDto.PROPERTY_CAPTAIN};
  selectedItem:{bean.getCaptain()};
}

#dataEntryOperatorLabel {
  text:"observe.common.dataEntryOperator";
  labelFor:{dataEntryOperator};
}

#dataEntryOperator {
  property:{TripSeineDto.PROPERTY_DATA_ENTRY_OPERATOR};
  selectedItem:{bean.getDataEntryOperator()};
}

#vesselLabel {
  text:"observe.common.vessel";
  labelFor:{vessel};
}

#vessel {
  property:{TripSeineDto.PROPERTY_VESSEL};
  selectedItem:{bean.getVessel()};
}

#oceanLabel {
  text:"observe.common.ocean";
  labelFor:{ocean};
}

#ocean {
  property:{TripSeineDto.PROPERTY_OCEAN};
  selectedItem:{bean.getOcean()};
  enabled:{canEditOcean(bean.getRoute())};
}

#departureHarbourLabel {
  text:"observe.common.departureHarbour";
  labelFor:{departureHarbour};
}

#departureHarbour {
  property:{TripSeineDto.PROPERTY_DEPARTURE_HARBOUR};
  selectedItem:{bean.getDepartureHarbour()};
}

#landingHarbourLabel {
  text:"observe.common.landingHarbour";
  labelFor:{landingHarbour};
}

#landingHarbour {
  property:{TripSeineDto.PROPERTY_LANDING_HARBOUR};
  selectedItem:{bean.getLandingHarbour()};
}

#ersIdLabel{
  text:"observe.common.ersId";
  labelFor:{ersId};
}

#resetErsId{
  toolTipText:"observe.content.action.reset.ersId.tip";
  _resetPropertyName:{TripSeineDto.PROPERTY_ERS_ID};
}

#ersId {
  _propertyName:{TripSeineDto.PROPERTY_ERS_ID};
  text:{getStringValue(bean.getErsId())};
}

#startDateLabel {
  text:"observe.common.startDate";
  labelFor:{startDate};
}

#startDate {
  _propertyName:{TripSeineDto.PROPERTY_START_DATE};
  date:{bean.getStartDate()};
}

#endDateLabel {
  text:"observe.common.endDate";
  labelFor:{endDate};
}

#endDate {
  _propertyName:{TripSeineDto.PROPERTY_END_DATE};
  date:{bean.getEndDate()};
}

#formsUrlLabel {
  text:"observe.common.formsUrl";
  labelFor:{formsUrl};
}

#resetFormsUrl {
  _resetPropertyName:{TripSeineDto.PROPERTY_FORMS_URL};
  toolTipText:"observe.content.action.reset.formsUrl.tip";
}

#formsUrl {
  _propertyName:{TripSeineDto.PROPERTY_FORMS_URL};
  text:{getStringValue(bean.getFormsUrl())};
}

#openLinkFormulairesUrl {
  actionIcon:"openLink";
  opaque:false;
  enabled:{!isEmpty(bean.getFormsUrl())};
  toolTipText:"observe.content.action.openLink.formsUrl.tip";
  _notBlocking:true;
}

#reportsUrlLabel {
  text:"observe.common.reportsUrl";
  labelFor:{reportsUrl};
}

#resetReportsUrl {
  _resetPropertyName:{TripSeineDto.PROPERTY_REPORTS_URL};
  toolTipText:"observe.content.action.reset.reportsUrl.tip";
}

#reportsUrl {
  _propertyName:{TripSeineDto.PROPERTY_REPORTS_URL};
  text:{getStringValue(bean.getReportsUrl())};
}

#openLinkRapportsUrl {
  actionIcon:"openLink";
  opaque:false;
  enabled:{!isEmpty(bean.getReportsUrl())};
  toolTipText:"observe.content.action.openLink.reportsUrl.tip";
  _notBlocking:true;
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment"))};
  minimumSize:{new Dimension(10,50)};
}

#comment2 {
  text:{getStringValue(bean.getComment())};
}

#reopen {
  _toolTipText:{t("observe.content.action.reopen.maree.tip")};
}

#close {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenRoute()};
  _toolTipText:{t("observe.action.close.maree.tip")};
}

#closeAndCreate {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenRoute()};
  _text:{t("observe.content.action.closeAndCreate.maree")};
  _toolTipText:{t("observe.content.action.closeAndCreate.maree.tip")};
}

#delete {
  _toolTipText:{t("observe.action.delete.maree.tip")};
}
