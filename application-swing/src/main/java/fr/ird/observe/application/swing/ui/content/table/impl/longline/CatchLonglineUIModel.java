package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderModelBuilder;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class CatchLonglineUIModel extends ContentTableUIModel<SetLonglineCatchDto, CatchLonglineDto> {

    public static final String PROPERTY_CARACTERISTICS_TAB_VALID = "caracteristicsTabValid";

    public static final String PROPERTY_DEPREDATED_TAB_VALID = "depredatedTabValid";

    public static final String PROPERTY_FOOD_AND_SEXUAL_TAB_VALID = "foodAndSexualTabValid";

    public static final String PROPERTY_BRANCHLINE_TAB_VALID = "branchlineTabValid";

    public static final String PROPERTY_BRANCHLINE_TIME_SINCE_CONTACT = "branchlineTimeSinceContact";

    public static final String PROPERTY_SHOW_INDIVIDUAL_TABS = "showIndividualTabs";

    public static final Set<String> CARACTERISTIC_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(CatchLonglineDto.PROPERTY_SECTION,
                                               CatchLonglineDto.PROPERTY_BASKET,
                                               CatchLonglineDto.PROPERTY_BRANCHLINE,
                                               CatchLonglineDto.PROPERTY_SPECIES_CATCH,
                                               CatchLonglineDto.PROPERTY_PHOTO_REFERENCES,
                                               CatchLonglineDto.PROPERTY_COUNT,
                                               CatchLonglineDto.PROPERTY_TOTAL_WEIGHT,
                                               CatchLonglineDto.PROPERTY_CATCH_HEALTHNESS,
                                               CatchLonglineDto.PROPERTY_HOOK_POSITION,
                                               CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE,
                                               CatchLonglineDto.PROPERTY_DISCARD_HEALTHNESS,
                                               CatchLonglineDto.PROPERTY_HOOK_WHEN_DISCARDED).build();

    public static final Set<String> DEPREDATED_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(CatchLonglineDto.PROPERTY_DEPREDATED,
                                               CatchLonglineDto.PROPERTY_BEAT_DIAMETER,
                                               CatchLonglineDto.PROPERTY_PREDATOR).build();

    public static final Set<String> FOOD_AND_SEXUAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(CatchLonglineDto.PROPERTY_STOMAC_FULLNESS,
                                               CatchLonglineDto.PROPERTY_SEX,
                                               CatchLonglineDto.PROPERTY_MATURITY_STATUS,
                                               CatchLonglineDto.PROPERTY_GONADE_WEIGHT).build();

    public static final Set<String> BRANCHLINE_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(BranchlineDto.PROPERTY_DEPTH_RECORDER,
                                               BranchlineDto.PROPERTY_HOOK_LOST,
                                               BranchlineDto.PROPERTY_TRACE_CUT_OFF,
                                               BranchlineDto.PROPERTY_TIMER,
                                               BranchlineDto.PROPERTY_TIME_SINCE_CONTACT,
                                               BranchlineDto.PROPERTY_TIMER_TIME_ON_BOARD,
                                               BranchlineDto.PROPERTY_BAIT_HAULING_STATUS).build();


    private static final long serialVersionUID = 1L;

    protected final SizeMeasuresTableModel sizeMeasuresTableModel;

    protected final WeightMeasuresTableModel weightMeasuresTableModel;

    /**
     * flag to see the individual tabs (only on individual mode + not in create mode).
     */
    protected boolean showIndividualTabs;

    protected boolean caracteristicsTabValid;

    protected boolean depredatedTabValid;

    protected boolean foodAndSexualTabValid;

    protected boolean branchlineTabValid;

    protected Date branchlineTimeSinceContact;

    public CatchLonglineUIModel(CatchLonglineUI ui) {
        super(SetLonglineCatchDto.class,
              CatchLonglineDto.class,
              new String[]{
                      SetLonglineCatchDto.PROPERTY_CATCH_LONGLINE
              },
              new String[]{CatchLonglineDto.PROPERTY_SPECIES_CATCH,
                           CatchLonglineDto.PROPERTY_ACQUISITION_MODE,
                           CatchLonglineDto.PROPERTY_COUNT,
                           CatchLonglineDto.PROPERTY_CATCH_HEALTHNESS,
                           CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE,
                           CatchLonglineDto.PROPERTY_DISCARD_HEALTHNESS,
                           CatchLonglineDto.PROPERTY_DEPREDATED,
                           CatchLonglineDto.PROPERTY_NUMBER,
                           CatchLonglineDto.PROPERTY_HOME_ID,
                           CatchLonglineDto.PROPERTY_HOOK_POSITION,
                           CatchLonglineDto.PROPERTY_HOOK_WHEN_DISCARDED,
                           CatchLonglineDto.PROPERTY_MATURITY_STATUS,
                           CatchLonglineDto.PROPERTY_PHOTO_REFERENCES,
                           CatchLonglineDto.PROPERTY_SEX,
                           CatchLonglineDto.PROPERTY_PREDATOR,
                           CatchLonglineDto.PROPERTY_STOMAC_FULLNESS,
                           CatchLonglineDto.PROPERTY_TOTAL_WEIGHT,
                           CatchLonglineDto.PROPERTY_BEAT_DIAMETER,
                           CatchLonglineDto.PROPERTY_GONADE_WEIGHT,
                           CatchLonglineDto.PROPERTY_SECTION,
                           CatchLonglineDto.PROPERTY_BASKET,
                           CatchLonglineDto.PROPERTY_BRANCHLINE,
                           CatchLonglineDto.PROPERTY_COMMENT
              });

        this.sizeMeasuresTableModel = new SizeMeasuresTableModel();
        this.weightMeasuresTableModel = new WeightMeasuresTableModel();

        List<ContentTableMeta<CatchLonglineDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_SECTION, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_BASKET, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_BRANCHLINE, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_SPECIES_CATCH, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_ACQUISITION_MODE, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_COUNT, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_CATCH_HEALTHNESS, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_DISCARD_HEALTHNESS, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_DEPREDATED, false),
                ContentTableModel.newTableMeta(CatchLonglineDto.class, CatchLonglineDto.PROPERTY_COMMENT, false));

        initModel(ui, metas);

    }

    public SizeMeasuresTableModel getSizeMeasuresTableModel() {
        return sizeMeasuresTableModel;
    }

    public WeightMeasuresTableModel getWeightMeasuresTableModel() {
        return weightMeasuresTableModel;
    }

    public boolean isShowIndividualTabs() {
        return showIndividualTabs;
    }

    public void setShowIndividualTabs(boolean showIndividualTabs) {
        this.showIndividualTabs = showIndividualTabs;
        firePropertyChange(PROPERTY_SHOW_INDIVIDUAL_TABS, null, showIndividualTabs);
    }

    public boolean isCaracteristicsTabValid() {
        return caracteristicsTabValid;
    }

    public void setCaracteristicsTabValid(boolean caracteristicsTabValid) {
        this.caracteristicsTabValid = caracteristicsTabValid;
        firePropertyChange(PROPERTY_CARACTERISTICS_TAB_VALID, null, caracteristicsTabValid);
    }

    public boolean isDepredatedTabValid() {
        return depredatedTabValid;
    }

    public void setDepredatedTabValid(boolean depredatedTabValid) {
        this.depredatedTabValid = depredatedTabValid;
        firePropertyChange(PROPERTY_DEPREDATED_TAB_VALID, null, depredatedTabValid);
    }

    public boolean isFoodAndSexualTabValid() {
        return foodAndSexualTabValid;
    }

    public void setFoodAndSexualTabValid(boolean foodAndSexualTabValid) {
        this.foodAndSexualTabValid = foodAndSexualTabValid;
        firePropertyChange(PROPERTY_FOOD_AND_SEXUAL_TAB_VALID, null, foodAndSexualTabValid);
    }

    public boolean isBranchlineTabValid() {
        return branchlineTabValid;
    }

    public void setBranchlineTabValid(boolean branchlineTabValid) {
        this.branchlineTabValid = branchlineTabValid;
        firePropertyChange(PROPERTY_BRANCHLINE_TAB_VALID, null, branchlineTabValid);
    }

    public Date getBranchlineTimeSinceContact() {
        return branchlineTimeSinceContact;
    }

    public void setBranchlineTimeSinceContact(Date branchlineTimeSinceContact) {
//        Date oldValue = getBranchlineTimeSinceContact();
        this.branchlineTimeSinceContact = branchlineTimeSinceContact;
        firePropertyChange(PROPERTY_BRANCHLINE_TIME_SINCE_CONTACT, null, branchlineTimeSinceContact);

    }

    @Override
    protected ContentTableModel<SetLonglineCatchDto, CatchLonglineDto> createTableModel(ObserveContentTableUI<SetLonglineCatchDto, CatchLonglineDto> ui, List<ContentTableMeta<CatchLonglineDto>> contentTableMetas) {
        return new CatchLonglineTableModel(ui, contentTableMetas);
    }

    @Override
    protected BinderModelBuilder<CatchLonglineDto, CatchLonglineDto> prepareChildLoador(String binderName) {

        BinderModelBuilder<CatchLonglineDto, CatchLonglineDto> builder = super.prepareChildLoador(binderName);

        builder.addCollectionStrategy(Binder.CollectionStrategy.duplicate, CatchLonglineDto.PROPERTY_PREDATOR);

        return builder;

    }

}
