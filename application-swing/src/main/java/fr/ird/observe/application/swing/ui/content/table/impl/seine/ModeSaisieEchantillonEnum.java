/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Pour définir le mode saisie d'un échantillon.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.8
 */
public enum ModeSaisieEchantillonEnum {
    /**
     * Le mode de saisie par défaut.
     *
     * L'observateur entre un effectif et une classe de taille, il ne peut pas
     * saisir le poids (celui sera déduit par calcul).
     */
    byEffectif(n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.seine.ModeSaisieEchantillonEnum.byEffectif")),

    /**
     * Le mode de saisie par individu.
     *
     * L'observateur saisie les poids individuel, il ne peut pas saisir
     * d'effectif, l'effectif est de 1.
     */
    byIndividu(n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.seine.ModeSaisieEchantillonEnum.byIndividu"));

    private final String i18nKey;

    ModeSaisieEchantillonEnum(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public static ModeSaisieEchantillonEnum valueOf(int ordinal)
            throws IllegalArgumentException {
        for (ModeSaisieEchantillonEnum o : values()) {
            if (o.ordinal() == ordinal) {
                return o;
            }
        }
        throw new IllegalArgumentException(
                "could not find a " + ModeSaisieEchantillonEnum.class.getSimpleName() +
                " value for ordinal " + ordinal);
    }

    public String getI18nKey() {
        return i18nKey;
    }

    @Override
    public String toString() {
        return t(i18nKey);
    }
}
