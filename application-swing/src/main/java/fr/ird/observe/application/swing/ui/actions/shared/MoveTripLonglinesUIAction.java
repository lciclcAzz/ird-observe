package fr.ird.observe.application.swing.ui.actions.shared;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveTripLonglinesUIAction extends MoveTripsUIAction<TripLonglineDto> {

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(MoveTripLonglinesUIAction.class);

    public static final String ACTION_NAME = "moveTripLonglines";

    public MoveTripLonglinesUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME);
    }

    @Override
    protected void checkUIClass(ContentUI<?> ui) throws IllegalStateException {
        if (!(ui instanceof TripLonglinesUI)) {
            throw new IllegalStateException("Can not come here!");
        }
    }

    @Override
    protected GearType getGearType(ContentUI<?> ui) {
        return GearType.longline;
    }

    @Override
    protected List<Integer> getPositions(List<String> tripIds, String programId) {
        TripLonglineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripLonglineService();
        return service.moveTripLonglinesToProgram(tripIds, programId);
    }

    @Override
    protected void updateModelData(ContentUI<?> ui) {
        TripLonglinesUI tripLonglinesUI = (TripLonglinesUI) ui;
        TripLonglinesUIModel model = tripLonglinesUI.getModel();
        List<DataReference<TripLonglineDto>> data = new ArrayList<>(model.getData());
        data.removeAll(model.getSelectedDatas());
        model.setData(data);
    }
}
