package fr.ird.observe.application.swing.backup;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingApplicationDataSourcesManager;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BackupsManager implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BackupsManager.class);

    private static final Pattern AUTOMATIC_BACKUP_FILENAME_PATTERN = Pattern.compile("obstuna-local-([0-9-]+)\\.sql\\.gz");

    private final Path backupsPath;
    private final Path storePath;
    private final List<BackupStorage> backups;
    private final BlockingDeque<BackupStorage> backupsToCheck = new LinkedBlockingDeque<>();
    private boolean run = true;

    private final Thread checkBackups = new Thread("Check-Backups") {
        @Override
        public void run() {

            while (run) {

                while (!backupsToCheck.isEmpty()) {
                    log.info("waiting for backup to check");
                    try {
                        BackupStorage backupStorage = backupsToCheck.takeFirst();
                        addBackup(backupStorage);
                    } catch (InterruptedException e) {
                        log.error(e);
                    }
                }
            }

        }

        private void addBackup(BackupStorage backupStorage) {
            log.info("Will check backup " + backupStorage.getFile());
            ObserveSwingApplicationDataSourcesManager dataSourcesManager = ObserveSwingApplicationContext.get().getDataSourcesManager();

            try {
                byte[] dump = Files.readAllBytes(backupStorage.getFile().toPath());
                DataSourceCreateConfigurationDto createDto = new DataSourceCreateConfigurationDto();
                createDto.setImportDatabase(dump);

                ObserveSwingDataSource dataSource = dataSourcesManager.newTemporaryH2Datasource("check-" + backupStorage.getName());
                try {
                    dataSource.create(createDto);
                } finally {
                    dataSource.destroy();
                }

                backupStorage.setVerified(true);
                log.info("Add sane backup from " + backupStorage.getFile());
                backups.add(backupStorage);
                storeBackups();
            } catch (Exception e) {
                log.error("Could not check backup " + backupStorage.getFile(), e);
            }

        }

    };

    public BackupsManager(Path backupsPath, Path storePath) {
        this.storePath = storePath;
        this.backupsPath = backupsPath;
        this.checkBackups.start();

        try {
            List<BackupStorage> backups;
            if (Files.exists(storePath)) {
                try (BufferedReader reader = Files.newBufferedReader(storePath)) {
                    Type typeOfT = new TypeToken<List<BackupStorage>>() {
                    }.getType();
                    backups = new Gson().fromJson(reader, typeOfT);
                    log.info(String.format("Loaded %d backups from %s", backups.size(), storePath));
                }
            } else {
                backups = new LinkedList<>();
            }

            Iterator<BackupStorage> iterator = backups.iterator();
            while (iterator.hasNext()) {
                BackupStorage backupStorage = iterator.next();
                if (!backupStorage.exists()) {
                    iterator.remove();
                    continue;
                }
                if (!backupStorage.isVerified()) {
                    iterator.remove();
                    addAutomaticBackup(backupStorage.getFile().toPath());
                }
            }
            this.backups = backups;
        } catch (IOException e) {
            throw new ObserveSwingTechnicalException("Can't init backups manager", e);
        }
    }

    private List<BackupStorage> discoverAutomaticBackups(Path backupsPath) throws IOException {
        List<BackupStorage> backups = new ArrayList<>();
        Files.walkFileTree(backupsPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Matcher matcher = AUTOMATIC_BACKUP_FILENAME_PATTERN.matcher(file.toFile().getName());
                if (matcher.matches()) {
                    BackupStorage backupStorage = new BackupStorage();
                    backupStorage.setDate(new Date(Files.getLastModifiedTime(file).toMillis()));
                    backupStorage.setFile(file.toFile());
                    backupStorage.setAutomatic(true);
                    backupStorage.setVerified(false);
                    backupStorage.setName(matcher.group(1));
                    backups.add(backupStorage);
                }
                return FileVisitResult.CONTINUE;
            }
        });
        log.info(String.format("Discovered %d automatic backups from %s", backups.size(), backupsPath));
        return backups;
    }

    public List<BackupStorage> getAutomaticBackups() {
        List<BackupStorage> result = backups.stream().filter(BackupStorage::isAutomatic).collect(Collectors.toList());
        result.sort(Comparator.comparing(BackupStorage::getDate).reversed());
        return result;
    }

    public List<BackupStorage> getUserBackups() {
        List<BackupStorage> result = backups.stream().filter(BackupStorage::isUser).collect(Collectors.toList());
        result.sort(Comparator.comparing(BackupStorage::getDate).reversed());
        return result;
    }

    public void addAutomaticBackup(Path backupPath) {
        Matcher matcher = AUTOMATIC_BACKUP_FILENAME_PATTERN.matcher(backupPath.toFile().getName());
        if (!matcher.matches()) {
            throw new IllegalStateException("backup does not match the pattern.");
        }
        BackupStorage backupStorage = new BackupStorage();
        backupStorage.setDate(new Date());
        backupStorage.setFile(backupPath.toFile());
        backupStorage.setAutomatic(true);
        backupStorage.setVerified(false);
        backupStorage.setName(matcher.group(1));
        backupsToCheck.add(backupStorage);
    }

    public void addUserBackup(Path backupPath) {
        BackupStorage backupStorage = new BackupStorage();
        backupStorage.setDate(new Date());
        backupStorage.setFile(backupPath.toFile());
        backupStorage.setAutomatic(false);
        backupStorage.setVerified(false);
        String name = StringUtils.removeEnd(backupPath.toFile().getName(), ".sql.gz");
        backupStorage.setName(name);
        backupsToCheck.add(backupStorage);
    }

    public void sanityBackups() {
        log.info("Sanity backups at " + backupsPath);
    }

    @Override
    public void close() throws IOException {

        run = false;

        try {
            log.debug("Waiting for " + checkBackups + " to end");
            checkBackups.join();
            log.debug("Closed " + checkBackups);
        } catch (InterruptedException e) {
            throw new ObserveSwingTechnicalException(e);
        }

    }

    private void storeBackups() throws IOException {
        log.info(String.format("Store %d backups to %s", backups.size(), storePath));
        try (BufferedWriter writer = Files.newBufferedWriter(storePath)) {
            new Gson().toJson(backups, writer);
        }
    }

}
