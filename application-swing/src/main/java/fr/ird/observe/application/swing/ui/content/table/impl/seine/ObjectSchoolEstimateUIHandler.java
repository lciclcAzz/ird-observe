/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateHelper;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.seine.ObjectSchoolEstimateService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ObjectSchoolEstimateUIHandler extends ContentTableUIHandler<FloatingObjectSchoolEstimateDto, ObjectSchoolEstimateDto> {


    /** Logger */
    private static final Log log = LogFactory.getLog(ObjectSchoolEstimateUIHandler.class);

    public ObjectSchoolEstimateUIHandler(ObjectSchoolEstimateUI ui) {
        super(ui, DataContextType.SetSeine);
    }

    @Override
    public ObjectSchoolEstimateUI getUi() {
        return (ObjectSchoolEstimateUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, ObjectSchoolEstimateDto bean, boolean create) {
        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            getUi().getSpecies().requestFocus();
        }
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedFloatingObjectId();
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(table,
                                            n("observe.content.schoolEstimate.table.speciesThon"),
                                            n("observe.content.schoolEstimate.table.speciesThon.tip"),
                                            n("observe.content.schoolEstimate.table.weight"),
                                            n("observe.content.schoolEstimate.table.weight.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected void doPersist(FloatingObjectSchoolEstimateDto bean) {

        SaveResultDto saveResult = getObjectSchoolEstimateService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<FloatingObjectSchoolEstimateDto> form = getObjectSchoolEstimateService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        FloatingObjectSchoolEstimateHelper.copyFloatingObjectSchoolEstimateDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case ObjectSchoolEstimateDto.PROPERTY_SPECIES: {
                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineObjectSchoolEstimateId();

                ReferentialService referentialService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newReferentialService();
                Form<SpeciesListDto> speciesListDtoForm = referentialService.loadForm(SpeciesListDto.class, speciesListId);
                SpeciesListDto speciesListDto = speciesListDtoForm.getObject();

                Set<String> speciesIds = speciesListDto.getSpeciesIds();
                result = ReferentialReferences.filterContains(result, speciesIds);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected ObjectSchoolEstimateService getObjectSchoolEstimateService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newObjectSchoolEstimateService();
    }
}
