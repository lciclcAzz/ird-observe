/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage.tabs;

import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.ObserveDbUserHelper;
import fr.ird.observe.services.dto.constants.ObserveDbRole;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Le modèle la sécurité à appliquer sur une base postgres
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SecurityModel implements Serializable {

    public static final String PROPERTY_ADMINISTRATEUR = "administrateur";

    public static final String PROPERTY_ROLE = "role";

    public static final String PROPERTY_ASSIGNED = "assigned";

    private static final long serialVersionUID = 1L;

    protected ObserveDbUserDto administrateur;

    protected final Set<ObserveDbUserDto> users;

    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public SecurityModel() {
        users = Sets.newHashSet();
    }

    public ObserveDbUserDto getAdministrateur() {
        return administrateur;
    }

    public Set<ObserveDbUserDto> getUsersByRole(ObserveDbRole role) {
        return users.stream()
                    .filter(ObserveDbUserHelper.newRolePredicate(role))
                    .collect(Collectors.toSet());
    }

    public List<String> getUserNamesByRole(ObserveDbRole role) {
        return users.stream()
                    .filter(ObserveDbUserHelper.newRolePredicate(role))
                    .map(ObserveDbUserHelper.NAME_FUNCTION)
                    .collect(Collectors.toList());
    }

    public List<String> getDataUserNames() {
        return getUserNamesByRole(ObserveDbRole.USER);
    }

    public List<String> getReferentialUserNames() {
        return getUserNamesByRole(ObserveDbRole.REFERENTIAL);
    }

    public List<String> getTechnicalUserNames() {
        return getUserNamesByRole(ObserveDbRole.TECHNICAL);
    }

    public List<String> getUnusedUserNames() {
        return getUserNamesByRole(ObserveDbRole.UNUSED);
    }

    public Set<ObserveDbUserDto> getUsers() {
        return Sets.newHashSet(users);
    }

    public Set<ObserveDbUserDto> getUsersWithoutAdministrator() {
        Predicate<ObserveDbUserDto> predicate = ObserveDbUserHelper.newRolePredicate(ObserveDbRole.ADMINISTRATOR);
        return users.stream().filter(u -> !predicate.test(u)).collect(Collectors.toSet());
    }

    public boolean isRole(ObserveDbRole r, ObserveDbUserDto user) {
        return r.equals(user.getRole());
    }

    public void init(Collection<ObserveDbUserDto> users) {
        this.users.clear();
        this.users.addAll(users);

        Optional<ObserveDbUserDto> optionalAdministrator = users.stream().filter(ObserveDbUserHelper.newRolePredicate(ObserveDbRole.ADMINISTRATOR)).findFirst();
        if (optionalAdministrator.isPresent()) {
            this.administrateur = optionalAdministrator.get();
        } else {
            throw new IllegalStateException("No administrator found");
        }
        firePropertyChange(PROPERTY_ROLE, null, getUsers());
        firePropertyChange(PROPERTY_ADMINISTRATEUR, null, getAdministrateur());
        firePropertyChange(PROPERTY_ASSIGNED, null, getAssigned());
    }

    public void setRole(ObserveDbUserDto user, ObserveDbRole role, boolean fire) {
        user.setRole(role);
        if (fire) {
            if (role != null) {
                firePropertyChange(role.name(), null, getUsersByRole(role));
            }
            firePropertyChange(PROPERTY_ASSIGNED, null, getAssigned());
        }
    }

    public void setRole(Iterable<ObserveDbUserDto> users, ObserveDbRole role) {
        for (ObserveDbUserDto user : users) {
            setRole(user, role, false);
        }
        firePropertyChange(role.name(), null, getUsersByRole(role));
        firePropertyChange(PROPERTY_ASSIGNED, null, getAssigned());
    }

    public int getAssigned() {
        int countNotAssigned = (int) users.stream().filter(ObserveDbUserHelper.newRolePredicate(null)).count();
        return users.size() - countNotAssigned;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }
}
