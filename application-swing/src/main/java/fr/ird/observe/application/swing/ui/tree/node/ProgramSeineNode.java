package fr.ird.observe.application.swing.ui.tree.node;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.ObserveDataProvider;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeBridge;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.loadors.ProgramSeineNodeChildLoador;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.ReferentialService;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
public class ProgramSeineNode extends ReferentialReferenceNodeSupport<ProgramDto> {

    private static final long serialVersionUID = 1L;

    public ProgramSeineNode(ReferentialReference<ProgramDto> entity) {
        super(ProgramDto.class,
              entity,
              ((GearType) entity.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE)).name(),
              ObserveTreeHelper.getChildLoador(ProgramSeineNodeChildLoador.class));
    }

    @Override
    protected ReferentialReference<ProgramDto> fetchEntity() {
        ReferentialService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newReferentialService();
        return service.loadReference(ProgramDto.class, getId());
    }

    public void populateChilds(ImmutableList<DataReference<TripSeineDto>> trips, ObserveTreeBridge bridge, ObserveDataProvider provider) {
        ((ProgramSeineNodeChildLoador) childLoador).setTrips(trips);
        try {
            populateChilds(bridge, provider);
        } finally {
            ((ProgramSeineNodeChildLoador) childLoador).setTrips(null);
        }
    }
}
