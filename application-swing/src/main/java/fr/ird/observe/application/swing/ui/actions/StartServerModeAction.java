package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveMainUIHandler;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.tools.Server;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.SQLException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class StartServerModeAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(StartServerModeAction.class);

    private final ObserveMainUI ui;

    public StartServerModeAction(ObserveMainUI ui) {

        super(t("observe.action.start.server.mode"), SwingUtil.getUIManagerActionIcon("db-start-server"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.start.server.mode.tip"));
        putValue(MNEMONIC_KEY, (int) 'S');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        run();

    }

    public void run() {

        if (log.isInfoEnabled()) {
            log.info("Will start server mode...");
        }
        ObserveSwingApplicationConfig config = ui.getConfig();
        File dbDirectory = new File(config.getLocalDBDirectory(), "obstuna");
        String h2Login = config.getH2Login();
        String h2Password = config.getH2Password();
        Integer port = config.getH2ServerPort();

        ui.setMode(ObserveUIMode.SERVER);

        try {

            Server server = Server.createTcpServer("-tcp",
                                                   "-tcpAllowOthers",
                                                   "-ifExists",
                                                   "-baseDir", dbDirectory.getAbsolutePath(),
                                                   "-tcpDaemon",
                                                   "-tcpPort",
                                                   String.valueOf(port));

            String url = String.format(ObserveMainUIHandler.H2_SERVER_URL_PATTERN,
                                       server.getURL(),
                                       dbDirectory.getAbsolutePath());

            server.start();
            if (log.isInfoEnabled()) {
                log.info("server starts at " + url);
            }

            String text = t("observe.message.server.info", dbDirectory);
            ui.getServerModeInfo().setText(text);

            ui.getServerModeURL().setText(url);
            ui.getServerModeLogin().setText(h2Login);
            ui.getServerModePassword().setText(h2Password);

            // On mémorise l'instance du server dans le contexte applicatif afin de pouvoir la récupérer plus tard,
            // par exemple lorsque l'on souhaitera arrêter le server.
            ObserveSwingApplicationContext.get().setH2Server(server);

        } catch (SQLException e) {

            if (log.isErrorEnabled()) {
                log.error("Could not start h2 server ", e);
            }
            ErrorDialogUI.showError(e);
        }

    }

}
