package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.swing.JTables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.JTable;

/**
 * Abstract action to select a cell in a table.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public abstract class AbstractSelectTableAction<M extends EditableTableModelSupport> extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractSelectTableAction.class);

    private final M model;

    private final JTable table;

    public AbstractSelectTableAction(M model, JTable table) {
        this.model = model;
        this.table = table;
    }

    protected void doSelectCell(int rowIndex, int columnIndex) {

        if (log.isDebugEnabled()) {
            log.debug("Will select cell at " + getCellCoordinate(rowIndex, columnIndex));
        }
        JTables.doSelectCell(table, rowIndex, columnIndex);
    }

    protected int getSelectedRow() {
        return table.getSelectedRow();
    }

    protected int getSelectedColumn() {
        return table.getSelectedColumn();
    }

    protected int getRowCount() {
        return table.getRowCount();
    }

    protected int getColumnCount() {
        return table.getColumnCount();
    }

    protected boolean isCellEditable(int rowIndex, int columnIndex) {
        return rowIndex > -1 && columnIndex > -1 &&
                table.isCellEditable(rowIndex, columnIndex);
    }

    protected boolean isCreateNewRow(int rowIndex) {

        boolean canCreateNewRow = model.isCanAddRow();

        if (canCreateNewRow) {

            if (rowIndex == -1 && model.isEmpty()) {

                // model is empty, we surely can add a new row
                canCreateNewRow = true;

            } else {

                // ask model if we can add a new row
                canCreateNewRow = model.isCanCreateNewRow(rowIndex);

            }

        }

        return canCreateNewRow;

    }

    protected String getCellCoordinate(int rowIndex, int columnIndex) {
        return " [" + rowIndex + ", " + columnIndex + "]";
    }

    protected void addNewRow() {
        model.addNewRow();
    }

}
