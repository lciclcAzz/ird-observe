/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeCallbackResults;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeContext;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeEngine;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeResult;
import jaxx.runtime.swing.model.JaxxDefaultListModel;

import javax.swing.DefaultListSelectionModel;
import java.util.List;

/**
 * Le modèle de l'opération de synchronization de réferentiel.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SynchronizeModel extends AdminActionModel {

    private final JaxxDefaultListModel<ObsoleteReferentialReference> obsoleteReferences = new JaxxDefaultListModel<>();
    private final DefaultListSelectionModel obsoleteReferencesSelectionModel = new DefaultListSelectionModel();
    /** data source we want to synchronize. */
    protected ObserveSwingDataSource source;
    /** data source which contains central referentiel. */
    protected ObserveSwingDataSource centralSource;
    /** Le resultat de la synchronisation des référentiels. */
    private UnidirectionalReferentialSynchronizeResult referentialSynchronizeResult;
    private UnidirectionalReferentialSynchronizeEngine engine;
    private UnidirectionalReferentialSynchronizeContext referentialSynchronizeContext;
    private UnidirectionalReferentialSynchronizeCallbackResults referentialSynchronizeCallbackResults;

    public SynchronizeModel() {
        super(AdminStep.SYNCHRONIZE);
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public void setCentralSource(ObserveSwingDataSource centralSource) {
        this.centralSource = centralSource;
    }

    public UnidirectionalReferentialSynchronizeResult getReferentialSynchronizeResult() {
        return referentialSynchronizeResult;
    }

    public void setReferentialSynchronizeResult(UnidirectionalReferentialSynchronizeResult referentialSynchronizeResult) {
        this.referentialSynchronizeResult = referentialSynchronizeResult;
    }

    public UnidirectionalReferentialSynchronizeEngine getEngine() {
        return engine;
    }

    public void setEngine(UnidirectionalReferentialSynchronizeEngine engine) {
        this.engine = engine;
    }

    public UnidirectionalReferentialSynchronizeContext getReferentialSynchronizeContext() {
        return referentialSynchronizeContext;
    }

    public void setReferentialSynchronizeContext(UnidirectionalReferentialSynchronizeContext referentialSynchronizeContext) {
        this.referentialSynchronizeContext = referentialSynchronizeContext;
    }

    public UnidirectionalReferentialSynchronizeCallbackResults getReferentialSynchronizeCallbackResults() {
        return referentialSynchronizeCallbackResults;
    }

    public void setReferentialSynchronizeCallbackResults(UnidirectionalReferentialSynchronizeCallbackResults referentialSynchronizeCallbackResults) {
        this.referentialSynchronizeCallbackResults = referentialSynchronizeCallbackResults;
    }

    public JaxxDefaultListModel<ObsoleteReferentialReference> getObsoleteReferences() {
        return obsoleteReferences;
    }

    public void setObsoleteReferences(List<ObsoleteReferentialReference> obsoleteReferences) {
        this.obsoleteReferences.setAllElements(obsoleteReferences);
    }

    public DefaultListSelectionModel getObsoleteReferencesSelectionModel() {
        return obsoleteReferencesSelectionModel;
    }

    @Override
    public void destroy() {
        super.destroy();
        obsoleteReferencesSelectionModel.clearSelection();
        obsoleteReferences.clear();
        referentialSynchronizeCallbackResults = null;
        referentialSynchronizeResult = null;
        centralSource = null;
        source = null;
        engine = null;
        referentialSynchronizeContext = null;
    }
}
