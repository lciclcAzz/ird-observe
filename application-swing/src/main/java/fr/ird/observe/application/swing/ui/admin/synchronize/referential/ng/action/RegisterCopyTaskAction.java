package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.AddReferentialSynchronizeTask;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class RegisterCopyTaskAction extends RegisterTasksActionSupport {

    private static final long serialVersionUID = 1L;

    private final RegisterAddTaskAction addAction;
    private final RegisterUpdateTaskAction updateAction;

    public RegisterCopyTaskAction(ReferentialSynchroUI ui, boolean left) {
        super(ui, ReferentialSynchronizeResources.COPY, left, false);
        addAction = new RegisterAddTaskAction(ui, left);
        updateAction = new RegisterUpdateTaskAction(ui, left);

    }

    @Override
    protected void createTasks() {
        addAction.updateNodes();
        updateAction.updateNodes();
        addAction.createTasks();
        updateAction.createTasks();
    }

    @Override
    protected <R extends ReferentialDto> AddReferentialSynchronizeTask<R> createTask(boolean left, ReferentialReference<R> reference, ReferentialReference<R> replaceReference) {
        return new AddReferentialSynchronizeTask<>(left, reference);
    }
}
