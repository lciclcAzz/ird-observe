/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.open.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIHandler;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.RouteHelper;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.RouteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.util.Date;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
class RouteUIHandler extends ContentOpenableUIHandler<RouteDto> {

//    private static final String UPDATE_TRIP_NODE = "updateTripNode";

    /** Logger */
    private static final Log log = LogFactory.getLog(RouteUIHandler.class);

    RouteUIHandler(RouteUI ui) {
        super(ui,
              DataContextType.TripSeine,
              DataContextType.Route,
              n("observe.content.route.message.not.open"));
    }

    @Override
    public RouteUI getUi() {
        return (RouteUI) super.getUi();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String routeId = getSelectedId();

        if (routeId == null) {

            // mode creation
            return ContentMode.CREATE;
        }

        // route deja existante        
        if (getOpenDataManager().isOpenRoute(routeId)) {

            // la route est ouverte
            return ContentMode.UPDATE;
        }

        RouteUI ui = getUi();

        // route non ouverte
        if (!dataContext.isSelectedOpen(TripSeineDto.class)) {

            addMessage(ui, NuitonValidatorScope.INFO,
                       getTypeI18nKey(TripSeineDto.class),
                       t("observe.content.tripSeine.message.not.open"));

        } else {

            // la maree courante est ouverte
            addMessage(ui, NuitonValidatorScope.INFO,
                       getTypeI18nKey(RouteDto.class),
                       t(closeMessage));
        }
        return ContentMode.READ;
    }

    @Override
    public void openUI() {

        super.openUI();

        String tripId = getSelectedParentId();
        String routeId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "tripId  = " + tripId);
            log.info(prefix + "routeId = " + routeId);
        }

        ContentMode mode = computeContentMode();

        if (log.isInfoEnabled()) {
            log.info(prefix + "content mode = " + mode);
        }

        RouteDto editBean = getBean();

        boolean create = routeId == null;

        Form<RouteDto> form;
        if (create) {

            // create mode
            form = getRouteService().preCreate(tripId);

        } else {

            // update mode
            form = getRouteService().loadForm(routeId);

        }

        setContentMode(mode);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        RouteHelper.copyRouteDto(form.getObject(), editBean);

        finalizeOpenUI(mode, create);
    }

    @Override
    public void startEditUI(String... binding) {

        boolean create = getModel().getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(getModel().getMode());

        RouteUI ui = getUi();

        ui.getValidator().setContext(contextName);

        if (create) {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(RouteDto.class),
                       t("observe.content.route.message.creating"));
        } else {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(RouteDto.class),
                       t("observe.content.route.message.updating"));

            if (getModel().isHistoricalData()) {

                addInfoMessage(t("observe.message.historical.data"));
            }
        }

        super.startEditUI(RouteUI.BINDING_DATE_DATE,
                          RouteUI.BINDING_START_LOG_VALUE_MODEL,
                          RouteUI.BINDING_END_LOG_VALUE_ENABLED,
                          RouteUI.BINDING_COMMENT2_TEXT,
                          RouteUI.BINDING_CLOSE_ENABLED,
                          RouteUI.BINDING_CLOSE_AND_CREATE_ENABLED);

        // le formulaire est en état modifié uniquement si on est en création
        getModel().setModified(create);
    }

    @Override
    protected boolean doSave(RouteDto bean) throws Exception {

        boolean notPersisted = bean.isNotPersisted();

        String tripId = getSelectedParentId();

        if (log.isInfoEnabled()) {
            log.info("will save route " + bean.getId());
        }

        // on sauvegarde toujours en debut de jour
        Date date = DateUtil.getDay(bean.getDate());
        bean.setDate(date);
        bean.setOpen(true);

        TripChildSaveResultDto saveResult = getRouteService().save(tripId, bean);
        saveResult.toDto(bean);

        // on doit toujours recharger le nœud de la marée puisque le nombre de routes a changé
        setUpdateMareeNodeTag(true);

        obtainChildPosition(bean);

        // ouverture de la route
        if (notPersisted) {
            getOpenDataManager().openRoute(getSelectedParentId(), bean.getId());
        }

        return true;
    }

    @Override
    protected int getOpenablePosition(String parentId, RouteDto bean) {
        return getRouteService().getRoutePositionInTripSeine(parentId, bean.getId());
    }

    @Override
    protected void afterSave(boolean refresh) {
        super.afterSave(refresh);
        repaintTripNode();
    }

    @Override
    protected void afterDelete() {
        super.afterDelete();
        repaintTripNode();
    }

    @Override
    protected boolean doDelete(RouteDto bean) {

        if (askToDelete(bean)) {
            return false;
        }
        if (log.isInfoEnabled()) {
            log.info("Will delete Route " + bean.getId());
        }

        String tripId = getSelectedParentId();
        getRouteService().delete(tripId, bean.getId());
        getOpenDataManager().closeRoute(getSelectedId());

        if (log.isInfoEnabled()) {
            log.info("Delete done for Route " + bean.getId());
        }

        // on doit toujours recharger le nœud de la marée puisque le nombre de routes a changé
        setUpdateMareeNodeTag(true);

        return true;
    }

    @Override
    protected boolean doOpenData() {
        boolean result = getOpenDataManager().canOpenRoute(getSelectedParentId());
        if (result) {
            getOpenDataManager().openRoute(getSelectedParentId(), getSelectedId());
        }
        return result;
    }

    @Override
    public boolean doCloseData() {

        RouteDto route = getBean();

        // on doit vérifier qu'il existe une activité de fin
        // de veille (type activity vessel == 16)

        boolean mustAddFinVeille = !route.isActivityFindDeVeilleFound();

        boolean createActivityFinDeVeille = false;
        boolean closeActivityFinDeVeille = false;
        boolean gotoActivityFinDeVeille = false;

        ObserveTreeHelper treeHelper = getTreeHelper(getUi());
        ObserveNode routeNode = treeHelper.getSelectedNode();

        if (mustAddFinVeille) {

            // on indique à l'observer qu'il doit créer une activité de type 16

            int reponse = UIHelper.askUser(
                    getUi(),
                    t("observe.title.need.confirm"),
                    t("observe.message.need.fin.veille.activity"),
                    JOptionPane.WARNING_MESSAGE,
                    new Object[]{
                            t("observe.choice.not.create.fin.veille.activity.and.continue"),
                            t("observe.choice.create.fin.veille.activity.and.continue"),
                            t("observe.choice.create.fin.veille.activity"),
                            t("observe.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + reponse);
            }


            switch (reponse) {
                case JOptionPane.CLOSED_OPTION:
                case 3:

                    // abandon objectOperation
                    return false;
                case 0:

                    // rien a faire
                    // on veut juste cloturer la route
                    break;
                case 1:

                    // creation de l'activity de fin de veille
                    // fermeture de l'activity de fin de veille
                    // fermeture de la route
                    createActivityFinDeVeille = true;
                    closeActivityFinDeVeille = true;

                    break;
                case 2:

                    // creation de l'activity de fin de veille
                    // selection de cette activity de fin de veille
                    createActivityFinDeVeille = true;
                    gotoActivityFinDeVeille = true;
                    break;
            }
        }

        if (createActivityFinDeVeille) {

            // stop l'édition de la route
            stopEditUI();

            // creation de l'action de fin de veille
            addActivityFinDeVeille(closeActivityFinDeVeille);

            if (gotoActivityFinDeVeille) {

                // on selection l'activity de fin de veille et on y reste
                // donc on ne continue pas la fermeture de la route
                return false;
            }

            // on retourne sur la route
            // que l'on va refermer
            treeHelper.selectNode(routeNode);
        }

        // fermeture de la route
        getOpenDataManager().closeRoute(getSelectedId());
        return true;
    }

    private ActivitySeineUI addActivityFinDeVeille(boolean close) {

        ObserveTreeHelper treeHelper = getTreeHelper(getUi());

        // on créee l'activity de fin de veille
        ObserveNode parentNode = treeHelper.getSelectedNode();
        parentNode = treeHelper.findNode(parentNode, ObserveI18nDecoratorHelper.getTypePluralI18nKey(ActivitySeineDto.class));
        if (log.isDebugEnabled()) {
            log.debug("PARENT NODE = " + parentNode);
        }
        treeHelper.addUnsavedNode(parentNode, ActivitySeineDto.class);

        // on recupère l'écran d'édition
        ActivitySeineUI selectedUI = (ActivitySeineUI) ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

        // on recupère l'activity de fin de veille
        ReferentialReference<VesselActivitySeineDto> vesselActivitySeine = null;

        for (ReferentialReference<VesselActivitySeineDto> refVesselActivity : selectedUI.getVesselActivitySeine().getData()) {

            if (ActivitySeineDto.ACTIVITY_FIN_DE_VEILLE.equals(refVesselActivity.getPropertyValue(VesselActivitySeineDto.PROPERTY_CODE))) {
                vesselActivitySeine = refVesselActivity;
                break;
            }
        }

        // on la positionne sur le bean d'édition
        selectedUI.getBean().setVesselActivitySeine(vesselActivitySeine);

        // on initialise la fin de veille a la dernière minute du jour
        selectedUI.getBean().setTime(DateUtil.getEndOfDay(DateUtil.createDate(0, 0, 0)));

        if (close) {

            // on sauvegarde l'activity
            selectedUI.save(false);

            // on ferme l'activity
            selectedUI.closeData();

            // on ferme l'écean
            selectedUI.stopEdit();
        }
        return selectedUI;
    }

    @Override
    protected boolean obtainCanReopen(boolean create) {

        // on peut reouvrir une route si :
        // - pas de route ouverte
        // - la maree courante est ouverte
        return !create && getOpenDataManager().canOpenRoute(getSelectedParentId());
    }

//    private void repaintTripNode() {
//        Boolean updateTripNode = getUi().getContextValue(Boolean.class, UPDATE_TRIP_NODE);
//
//        getUi().removeContextValue(Boolean.class, UPDATE_TRIP_NODE);
//
//        if (updateTripNode == null || !updateTripNode) {
//            return;
//        }
//
//
//        ObserveTreeHelper treeHelper = getTreeHelper(getUi());
//        ObserveNode tripNode = treeHelper.getSelectedNode().getParent().getParent();
//        if (log.isInfoEnabled()) {
//            log.info("Refresh trip node : " + tripNode);
//        }
//        treeHelper.reloadNode(tripNode, false);
//    }

//    protected void setUpdateMareeNodeTag(boolean wasUpdated) {
//
//        if (wasUpdated) {
//
//            // la date de fin a ete modifiee, il faut : redessiner le noeud de la maree le repositionner
//            getUi().setContextValue(Boolean.TRUE, UPDATE_TRIP_NODE);
//
//        } else {
//
//            getUi().removeContextValue(Boolean.class, UPDATE_TRIP_NODE);
//
//        }
//
//    }

    private RouteService getRouteService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();
    }
}
