package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.ReferentialSynchronizeTaskSupport;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

/**
 * Created on 03/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ReferentialSynchronizeTaskListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        ReferentialSynchronizeTaskSupport task = (ReferentialSynchronizeTaskSupport) value;
        super.getListCellRendererComponent(list, "<html><body>" + task.getLabel(), index, isSelected, cellHasFocus);
        setIcon(task.getIcon());
        return this;

    }

}
