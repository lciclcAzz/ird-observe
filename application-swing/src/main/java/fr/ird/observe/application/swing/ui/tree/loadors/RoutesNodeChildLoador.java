/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.node.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.node.RouteSeineNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.seine.RouteService;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Le chargeur des noeuds de routes d'une marée.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class RoutesNodeChildLoador extends AbstractDataReferenceChildLoador<RouteDto> {

    private static final long serialVersionUID = 1L;

    public RoutesNodeChildLoador() {
        super(RouteDto.class);
    }

    @Override
    public List<DataReference<RouteDto>> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) throws Exception {
        RouteService routeService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();
        DataReferenceSet<RouteDto> routeByTripSeine = routeService.getRouteByTripSeine(parentId);
        return new ArrayList<>(routeByTripSeine.getReferences());
    }


    @Override
    public ObserveNode createNode(DataReference<RouteDto> data, NavDataProvider dataProvider) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        ObserveNode result = new RouteSeineNode(data);
        ObserveNode child = createPluralizeStringNode(ActivitySeineDto.class, ActivitySeinesNodeChildLoador.class);
        result.add(child);
        return result;
    }


}
