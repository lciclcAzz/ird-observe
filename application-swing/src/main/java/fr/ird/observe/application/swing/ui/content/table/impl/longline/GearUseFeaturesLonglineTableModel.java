package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineHelper;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeaturesLonglineTableModel extends ContentTableModel<TripLonglineGearUseDto, GearUseFeaturesLonglineDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GearUseFeaturesLonglineTableModel.class);

    private static final long serialVersionUID = 1L;

    private transient GearUseFeaturesLonglineUIHandler gearUseFeaturesLonglineUIHandler;

    public GearUseFeaturesLonglineTableModel(ObserveContentTableUI<TripLonglineGearUseDto, GearUseFeaturesLonglineDto> context,
                                             List<ContentTableMeta<GearUseFeaturesLonglineDto>> contentTableMetas) {
        super(context, contentTableMetas);
    }

    public void setGearUseFeaturesLonglineUIHandler(GearUseFeaturesLonglineUIHandler gearUseFeaturesLonglineUIHandler) {
        this.gearUseFeaturesLonglineUIHandler = gearUseFeaturesLonglineUIHandler;
    }

    @Override
    public void addNewEntry() {

        int editingRow = getSelectedRow();

        if (editingRow > -1) {

            // store sizes and weights for the selected row
            // before creating a new one ?
            GearUseFeaturesLonglineUIModel model = getModel();
            model.getMeasurementsTableModel().storeInCacheForRow(editingRow);

        }

        super.addNewEntry();
    }

    @Override
    public void updateRowFromEditBean() {

        GearUseFeaturesLonglineUIModel model = getModel();

        int editingRow = getSelectedRow();
        GearUseFeaturesLonglineDto rowBean = getRowBean();
        GearUseFeaturesMeasurementLonglinesTableModel measurementsTableModel = model.getMeasurementsTableModel();

        List<GearUseFeaturesMeasurementLonglineDto> measurements;

        if (rowBean.getId() == null && CollectionUtils.isEmpty(measurementsTableModel.getCacheForRow(editingRow))) {

            // new gear usage, add default measurements

            ReferentialReference<GearDto> gear = rowBean.getGear();
            measurements = gearUseFeaturesLonglineUIHandler.getDefaultGearUseFeaturesMeasurementLongline(gear.getId());
            if (log.isInfoEnabled()) {
                log.info("Create mode, use default measurements: " + measurements.size());
            }
            measurementsTableModel.removeCacheForRow(editingRow);
            measurementsTableModel.initCacheForRow(editingRow, measurements);

            measurementsTableModel.setData(measurements);
            measurementsTableModel.setModified(false);

        } else {

            // store current measurements for the selected row
            measurementsTableModel.storeInCacheForRow(editingRow);
            measurements = measurementsTableModel.getData();
        }

        rowBean.setGearUseFeaturesMeasurement(Sets.newLinkedHashSet(measurements));

        super.updateRowFromEditBean();

    }

    @Override
    protected void removeRow(int row) {
        super.removeRow(row);

        // remove sizes and weights for the deleted row
        // also update rows to row - 1 (when after the deleted row)
        GearUseFeaturesLonglineUIModel model = getModel();
        model.getMeasurementsTableModel().removeCacheForRow(row);

    }

    @Override
    protected void setChilds(TripLonglineGearUseDto parent, List<GearUseFeaturesLonglineDto> childs) {
        parent.getGearUseFeaturesLongline().clear();
        parent.addAllGearUseFeaturesLongline(childs);
    }

    @Override
    public void resetEditBean() {

        int row = getSelectedRow();
        if (log.isInfoEnabled()) {
            log.info("Reset edit bean at row: " + row);
        }
        GearUseFeaturesLonglineUIModel model = getModel();
        model.getMeasurementsTableModel().resetCacheForRow(row);

        super.resetEditBean();

    }

    @Override
    protected Collection<GearUseFeaturesLonglineDto> getChilds(TripLonglineGearUseDto bean) {
        return bean.getGearUseFeaturesLongline();
    }

    @Override
    protected void load(GearUseFeaturesLonglineDto source, GearUseFeaturesLonglineDto target) {
        GearUseFeaturesLonglineHelper.copyGearUseFeaturesLonglineDto(source, target);
    }

    @Override
    protected void resetRow(int row) {
        super.resetRow(row);

        GearUseFeaturesLonglineUIModel model = getModel();
        model.getMeasurementsTableModel().resetCacheForRow(row);
    }

    @Override
    protected GearUseFeaturesLonglineUIModel getModel() {
        return (GearUseFeaturesLonglineUIModel) super.getModel();
    }
}
