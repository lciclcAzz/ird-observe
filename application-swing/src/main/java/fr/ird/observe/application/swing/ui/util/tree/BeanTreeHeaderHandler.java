package fr.ird.observe.application.swing.ui.util.tree;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * @author Samuel Maisonneuve- maisonneuve@codelutin.com
 */
public class BeanTreeHeaderHandler implements UIHandler<BeanTreeHeader> {

    private static final Log log = LogFactory.getLog(BeanTreeHeaderHandler.class);
    private BeanTreeHeader ui;

    public BeanTreeHeader getUi() {
        return ui;
    }

    private JTree getTree() {
        return getUi().getTree();
    }

    @Override
    public void beforeInit(BeanTreeHeader beanTreeHeader) {
        this.ui = beanTreeHeader;
    }

    @Override
    public void afterInit(BeanTreeHeader beanTreeHeader) {
    }

    public void collapseAll() {
        // Let's unselect the nodes before collapsing them
        unselectAll();

        JTree tree = getTree();
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.collapseRow(i);
        }
    }

    public void expandAll() {
        JTree tree = getTree();
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.expandRow(i);
        }
    }

    public void selectAll() {
        // To be selected, nodes need to be expanded
        expandAll();

        JTree tree = getTree();
        TreeSelectionModel selectionModel = tree.getSelectionModel();

        for (int i = 0, l = tree.getRowCount(); i < l; i++) {

            TreePath path = tree.getPathForRow(i);
            if (!selectionModel.isPathSelected(path)) {
                tree.setSelectionPath(path);
            }
        }
    }

    public void unselectAll() {
        JTree tree = getTree();
        TreeSelectionModel selectionModel = tree.getSelectionModel();

        for (int i = 0, l = tree.getRowCount(); i < l; i++) {

            TreePath path = tree.getPathForRow(i);
            if (selectionModel.isPathSelected(path)) {
                tree.setSelectionPath(path);
            }
        }
    }
}
