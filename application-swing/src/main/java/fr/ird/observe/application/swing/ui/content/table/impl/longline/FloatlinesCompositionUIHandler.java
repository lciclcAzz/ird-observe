package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 */
public class FloatlinesCompositionUIHandler extends ContentTableUIHandler<SetLonglineGlobalCompositionDto, FloatlinesCompositionDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(FloatlinesCompositionUIHandler.class);

    public FloatlinesCompositionUIHandler(FloatlinesCompositionUI ui) {
        super(ui, DataContextType.SetLongline);
    }

    @Override
    public FloatlinesCompositionUI getUi() {
        return (FloatlinesCompositionUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, FloatlinesCompositionDto bean, boolean create) {

        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            getUi().getLineType().requestFocus();
        }

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.floatlinesComposition.table.lineType"),
                n("observe.content.floatlinesComposition.table.lineType.tip"),
                n("observe.content.floatlinesComposition.table.length"),
                n("observe.content.floatlinesComposition.table.length.tip"),
                n("observe.content.floatlinesComposition.table.proportion"),
                n("observe.content.floatlinesComposition.table.proportion.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, LineTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));

        // when model change in table, let's recompute the proportion sum
        table.getModel().addTableModelListener(e -> {

            int proportionSum = getBean().getFloatlinesCompositionProportionSum();
            getBean().setFloatlinesCompositionProportionSum(proportionSum);
        });

    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    public void afterSave(boolean refresh) {
        super.afterSave(refresh);
    }

    @Override
    protected void doPersist(SetLonglineGlobalCompositionDto bean) {
        // fait par le doSave de LonglineGlobalCompositionUIHandler
    }

    @Override
    protected void loadEditBean(String beanId) {
        // fait par le loadEditBean de LonglineGlobalCompositionUIHandler
    }
}
