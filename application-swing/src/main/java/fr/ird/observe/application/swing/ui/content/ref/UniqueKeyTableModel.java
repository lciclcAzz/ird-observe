/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.ref;

import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;

import javax.swing.table.AbstractTableModel;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Un modèle de tableau pour afficher les clef metier des objets du
 * référentiel.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class UniqueKeyTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    protected final String[] columns;

    protected final List<Object[]> datas;

    protected transient DecoratorService decoratorService;

    public UniqueKeyTableModel(String[] columns, List<Object[]> datas) {
        this.columns = columns;
        this.datas = datas;
    }

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    @Override
    public int getRowCount() {
        return datas.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return datas.get(rowIndex)[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // tableau non editable
        return false;
    }

    @Override
    public String getColumnName(int column) {
        String property = columns[column];
        return t(ObserveI18nDecoratorHelper.getPropertyI18nKey(property));
    }
}
