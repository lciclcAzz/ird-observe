package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.List;
import java.util.Set;

/**
 * Created on 3/24/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUseFeaturesSeineUIModel extends ContentTableUIModel<TripSeineGearUseDto, GearUseFeaturesSeineDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(GearUseFeaturesSeineDto.PROPERTY_GEAR,
                                               GearUseFeaturesSeineDto.PROPERTY_NUMBER,
                                               GearUseFeaturesSeineDto.PROPERTY_USED_IN_TRIP,
                                               GearUseFeaturesSeineDto.PROPERTY_COMMENT).build();

    protected boolean generalTabValid;

    private final GearUseFeaturesMeasurementSeinesTableModel measurementsTableModel;

    public GearUseFeaturesSeineUIModel(GearUseFeaturesSeineUI ui) {

        super(TripSeineGearUseDto.class,
                GearUseFeaturesSeineDto.class,
                new String[]{
                        TripSeineGearUseDto.PROPERTY_ID,
                        TripSeineGearUseDto.PROPERTY_GEAR_USE_FEATURES_SEINE,
                        TripSeineGearUseDto.PROPERTY_LAST_UPDATE_DATE,
                },
                new String[]{
                      GearUseFeaturesSeineDto.PROPERTY_ID,
                      GearUseFeaturesSeineDto.PROPERTY_NUMBER,
                      GearUseFeaturesSeineDto.PROPERTY_USED_IN_TRIP,
                      GearUseFeaturesSeineDto.PROPERTY_GEAR_USE_FEATURES_MEASUREMENT,
                      GearUseFeaturesSeineDto.PROPERTY_COMMENT
              }
        );

        this.measurementsTableModel = new GearUseFeaturesMeasurementSeinesTableModel();

        List<ContentTableMeta<GearUseFeaturesSeineDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(GearUseFeaturesSeineDto.class, GearUseFeaturesSeineDto.PROPERTY_GEAR, false),
                ContentTableModel.newTableMeta(GearUseFeaturesSeineDto.class, GearUseFeaturesSeineDto.PROPERTY_NUMBER, false),
                ContentTableModel.newTableMeta(GearUseFeaturesSeineDto.class, GearUseFeaturesSeineDto.PROPERTY_USED_IN_TRIP, false),
                ContentTableModel.newTableMeta(GearUseFeaturesSeineDto.class, GearUseFeaturesSeineDto.PROPERTY_COMMENT, false));

        initModel(ui, metas);

    }

    @Override
    protected GearUseFeaturesSeineTableModel createTableModel(
            ObserveContentTableUI<TripSeineGearUseDto, GearUseFeaturesSeineDto> ui,
            List<ContentTableMeta<GearUseFeaturesSeineDto>> contentTableMetas) {
        return new GearUseFeaturesSeineTableModel(ui, contentTableMetas);
    }

    public GearUseFeaturesMeasurementSeinesTableModel getMeasurementsTableModel() {
        return measurementsTableModel;
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, null, isGeneralTabValid());
    }

}
