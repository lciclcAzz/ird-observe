package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterHelper;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.longline.ActivityLongLineEncounterService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class EncounterUIHandler extends ContentTableUIHandler<ActivityLonglineEncounterDto, EncounterDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(EncounterUIHandler.class);

    public EncounterUIHandler(EncounterUI ui) {
        super(ui, DataContextType.ActivityLongline);
    }

    @Override
    public EncounterUI getUi() {
        return (EncounterUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, EncounterDto bean, boolean create) {
        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            getUi().getEncounterType().requestFocus();
        }
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(table,
                                            n("observe.content.encounter.table.encouterType"),
                                            n("observe.content.encounter.table.encouterType.tip"),
                                            n("observe.content.encounter.table.species"),
                                            n("observe.content.encounter.table.species.tip"),
                                            n("observe.content.encounter.table.distance"),
                                            n("observe.content.encounter.table.distance.tip"),
                                            n("observe.content.encounter.table.count"),
                                            n("observe.content.encounter.table.count.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, EncounterTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedActivityId();
    }

    @Override
    protected void doPersist(ActivityLonglineEncounterDto bean) {

        SaveResultDto saveResult = getActivityLongLineEncouterService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<ActivityLonglineEncounterDto> form = getActivityLongLineEncouterService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        ActivityLonglineEncounterHelper.copyActivityLonglineEncounterDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case EncounterDto.PROPERTY_SPECIES: {

                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListLonglineEncounterId();

                ReferentialService referentialService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newReferentialService();

                Form<SpeciesListDto> speciesListDtoForm = referentialService.loadForm(SpeciesListDto.class, speciesListId);
                SpeciesListDto speciesListDto = speciesListDtoForm.getObject();

                Set<String> speciesIds = speciesListDto.getSpeciesIds();

                result = ReferentialReferences.filterContains(result, speciesIds);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected ActivityLongLineEncounterService getActivityLongLineEncouterService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLongLineEncounterService();
    }
}
