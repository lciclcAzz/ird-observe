<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe :: Services ToPIA validation
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!DOCTYPE validators PUBLIC
  "-//Apache Struts//XWork Validator 1.0.3//EN"
  "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="vesselActivityLongline">

    <!-- pas de vesselActivityLongline selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.vesselActivity</message>
    </field-validator>

    <!-- vesselActivityLongline desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ vesselActivityLongline.enabled ]]>
      </param>
      <message>validator.service.activity.desactivated.vesselActivity</message>
    </field-validator>

  </field>

  <field name="date">

    <!-- pas de date selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.date</message>
    </field-validator>

  </field>

  <field name="time">

    <!-- pas d'heure d'observation selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.time</message>
    </field-validator>

  </field>

  <field name="quadrant">

    <!-- quadrant obligatoire (http://forge.codelutin.com/issues/840) -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.quadrant</message>
    </field-validator>

    <!-- coherence quadrant par rapport a l'ocean de la maree -->
    <field-validator type="quadrant" short-circuit="true">
      <param name="ocean">tripLonglineEntity.ocean</param>
      <message>
        validator.service.activity.invalid.quadrant##${tripLonglineEntity.ocean.getProperty("label")}
      </message>
    </field-validator>

  </field>

  <field name="seaSurfaceTemperature">

    <!-- temperature surface non saisie ||  12.0 <= temperature surface  <= 35.0 -->
    <field-validator type="fieldexpressionwithparams" short-circuit="true">
      <param name="doubleParams">min:12.0|max:35.0</param>
      <param name="expression">
        <![CDATA[ seaSurfaceTemperature == null || (doubles.min <= seaSurfaceTemperature && seaSurfaceTemperature <= doubles.max)]]>
      </param>
      <message>
        validator.service.activity.bound.seaSurfaceTemperature##${doubles.min}##${doubles.max}
      </message>
    </field-validator>

  </field>

  <field name="latitude">

    <!-- latitude obligatoire  -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.latitude</message>
    </field-validator>

    <!-- 0 <= latitude <= 90 -->
    <field-validator type="fieldexpressionwithparams" short-circuit="true">
      <param name="doubleParams">min:0.0|max:90.0</param>
      <param name="expression"><![CDATA[
                (doubles.min <= @java.lang.Math@abs(latitude) && @java.lang.Math@abs(latitude) <= doubles.max)
             ]]>
      </param>
      <message>
        validator.service.activity.bound.latitude##${doubles.min}##${doubles.max}
      </message>
    </field-validator>

  </field>

  <field name="longitude">

    <!-- longitude obligatoire -->
    <field-validator type="required" short-circuit="true">
      <message>validator.service.activity.required.longitude</message>
    </field-validator>

    <!-- 0 <= longitude <= 180 -->
    <field-validator type="fieldexpressionwithparams" short-circuit="true">
      <param name="doubleParams">min:0.0|max:180.0</param>
      <param name="expression"><![CDATA[
                (doubles.min <= @java.lang.Math@abs(longitude) && @java.lang.Math@abs(longitude) <= doubles.max)
             ]]>
      </param>
      <message>
        validator.service.activity.bound.longitude##${doubles.min}##${doubles.max}
      </message>
    </field-validator>

  </field>

  <field name="comment">
    <!-- comentaire de moins de 1024 caractères -->
    <field-validator type="stringlength">
      <param name="maxLength">1024</param>
      <message>validator.service.activity.comment.tobig</message>
    </field-validator>

    <!-- comentaire requis pour le type d'activity vessel selectionne -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ vesselActivityLongline == null || !vesselActivityLongline.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>
        validator.service.activity.required.comment.for.vesselActivity
      </message>
    </field-validator>

  </field>

  <field name="fpaZone">

    <!-- fpaZone desactive -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ fpaZone == null || fpaZone.enabled ]]>
      </param>
      <message>validator.service.activityLongline.desactivated.fpaZone</message>
    </field-validator>

  </field>

</validators>
