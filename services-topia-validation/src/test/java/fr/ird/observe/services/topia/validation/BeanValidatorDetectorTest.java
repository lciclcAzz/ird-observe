/*
 * #%L
 * ObServe :: Services ToPIA validation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.validation;

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.BaitsComposition;
import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.BranchlinesComposition;
import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.Encounter;
import fr.ird.observe.entities.longline.FloatlinesComposition;
import fr.ird.observe.entities.longline.GearUseFeaturesLongline;
import fr.ird.observe.entities.longline.HooksComposition;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SensorUsed;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.Tdr;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.FpaZone;
import fr.ird.observe.entities.referentiel.Gear;
import fr.ird.observe.entities.referentiel.GearCaracteristic;
import fr.ird.observe.entities.referentiel.GearCaracteristicType;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Organism;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.SpeciesGroup;
import fr.ird.observe.entities.referentiel.SpeciesList;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.VesselSizeCategory;
import fr.ird.observe.entities.referentiel.VesselType;
import fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitSettingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitType;
import fr.ird.observe.entities.referentiel.longline.CatchFateLongline;
import fr.ird.observe.entities.referentiel.longline.EncounterType;
import fr.ird.observe.entities.referentiel.longline.Healthness;
import fr.ird.observe.entities.referentiel.longline.HookPosition;
import fr.ird.observe.entities.referentiel.longline.HookSize;
import fr.ird.observe.entities.referentiel.longline.HookType;
import fr.ird.observe.entities.referentiel.longline.ItemHorizontalPosition;
import fr.ird.observe.entities.referentiel.longline.ItemVerticalPosition;
import fr.ird.observe.entities.referentiel.longline.LightsticksColor;
import fr.ird.observe.entities.referentiel.longline.LightsticksType;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.entities.referentiel.longline.MaturityStatus;
import fr.ird.observe.entities.referentiel.longline.MitigationType;
import fr.ird.observe.entities.referentiel.longline.SensorBrand;
import fr.ird.observe.entities.referentiel.longline.SensorDataFormat;
import fr.ird.observe.entities.referentiel.longline.SensorType;
import fr.ird.observe.entities.referentiel.longline.SettingShape;
import fr.ird.observe.entities.referentiel.longline.SizeMeasureType;
import fr.ird.observe.entities.referentiel.longline.StomacFullness;
import fr.ird.observe.entities.referentiel.longline.TripType;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLongline;
import fr.ird.observe.entities.referentiel.longline.WeightMeasureType;
import fr.ird.observe.entities.referentiel.seine.DetectionMode;
import fr.ird.observe.entities.referentiel.seine.ObjectFate;
import fr.ird.observe.entities.referentiel.seine.ObjectOperation;
import fr.ird.observe.entities.referentiel.seine.ObjectType;
import fr.ird.observe.entities.referentiel.seine.ObservedSystem;
import fr.ird.observe.entities.referentiel.seine.ReasonForDiscard;
import fr.ird.observe.entities.referentiel.seine.ReasonForNoFishing;
import fr.ird.observe.entities.referentiel.seine.ReasonForNullSet;
import fr.ird.observe.entities.referentiel.seine.SpeciesFate;
import fr.ird.observe.entities.referentiel.seine.SpeciesStatus;
import fr.ird.observe.entities.referentiel.seine.SurroundingActivity;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyOperation;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyType;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;
import fr.ird.observe.entities.referentiel.seine.Wind;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.entities.seine.ObjectObservedSpecies;
import fr.ird.observe.entities.seine.ObjectSchoolEstimate;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.entities.seine.TransmittingBuoy;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.SortedSet;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/** @author Tony Chemit - dev@tchemit.fr */
public class BeanValidatorDetectorTest extends AbstractValidatorDetectorTest {

    SortedSet<NuitonValidator<?>> validators;

    public BeanValidatorDetectorTest() {
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    static Class<?>[] ALL_TYPES;

    @BeforeClass
    public static void setUpClass() {
        ALL_TYPES = ObserveEntityEnum.getContractClasses();
    }

    @Override
    protected File getRootDirectory(File basedir) {
        return new File(basedir,
                        "src" + File.separator + "main" + File.separator + "resources");
    }

    @Test
    public void detectAll() {

        SortedSet<NuitonValidator<?>> validators = detectValidators(ALL_TYPES);
        assertFalse(validators.isEmpty());
        assertEquals(108, validators.size());

    }

    @Test
    public void testDetectService() {
        String contextName = ValidateService.SERVICE_VALIDATION_CONTEXT;

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName(validators,
                                              contextName,
                                              ActivityLongline.class,
                                              ActivitySeine.class,
                                              BaitHaulingStatus.class,
                                              BaitSettingStatus.class,
                                              BaitType.class,
                                              BaitsComposition.class,
                                              Branchline.class,
                                              BranchlinesComposition.class,
                                              CatchFateLongline.class,
                                              CatchLongline.class,
                                              Country.class,
                                              DetectionMode.class,
                                              Encounter.class,
                                              EncounterType.class,
                                              FloatingObject.class,
                                              FloatlinesComposition.class,
                                              FpaZone.class,
                                              Gear.class,
                                              GearCaracteristic.class,
                                              GearCaracteristicType.class,
                                              GearUseFeaturesLongline.class,
                                              GearUseFeaturesSeine.class,
                                              Harbour.class,
                                              Healthness.class,
                                              HookPosition.class,
                                              HookSize.class,
                                              HookType.class,
                                              HooksComposition.class,
                                              ItemHorizontalPosition.class,
                                              ItemVerticalPosition.class,
                                              LengthWeightParameter.class,
                                              LightsticksColor.class,
                                              LightsticksType.class,
                                              LineType.class,
                                              MaturityStatus.class,
                                              MitigationType.class,
                                              NonTargetCatch.class,
                                              NonTargetLength.class,
                                              NonTargetSample.class,
                                              ObjectFate.class,
                                              ObjectObservedSpecies.class,
                                              ObjectOperation.class,
                                              ObjectSchoolEstimate.class,
                                              ObjectType.class,
                                              ObservedSystem.class,
                                              Ocean.class,
                                              Organism.class,
                                              Person.class,
                                              Program.class,
                                              ReasonForDiscard.class,
                                              ReasonForNoFishing.class,
                                              ReasonForNullSet.class,
                                              Route.class,
                                              SchoolEstimate.class,
                                              SensorBrand.class,
                                              SensorDataFormat.class,
                                              SensorType.class,
                                              SensorUsed.class,
                                              SetLongline.class,
                                              SetSeine.class,
                                              SettingShape.class,
                                              Sex.class,
                                              SizeMeasureType.class,
                                              Species.class,
                                              SpeciesFate.class,
                                              SpeciesGroup.class,
                                              SpeciesList.class,
                                              SpeciesStatus.class,
                                              StomacFullness.class,
                                              SurroundingActivity.class,
                                              TargetLength.class,
                                              TargetSample.class,
                                              Tdr.class,
                                              TransmittingBuoy.class,
                                              TransmittingBuoyOperation.class,
                                              TransmittingBuoyType.class,
                                              TripLongline.class,
                                              TripSeine.class,
                                              TripType.class,
                                              Vessel.class,
                                              VesselActivityLongline.class,
                                              VesselActivitySeine.class,
                                              VesselSizeCategory.class,
                                              VesselType.class,
                                              WeightCategory.class,
                                              WeightMeasureType.class,
                                              Wind.class);
    }

    @Test
    public void testDetectServiceExt() {
        String contextName = ValidateService.SERVICE_VALIDATION_CONTEXT;

        validators = detectValidators(Pattern.compile(contextName + "-.*"), ALL_TYPES);

        assertValidatorSetWithMultiContextName(validators,
                                               contextName + "-encounter", ActivityLongline.class,
                                               contextName + "-sensorUsed", ActivityLongline.class,
                                               contextName + "-observedSystem", ActivitySeine.class,
                                               contextName + "-table", Basket.class,
                                               contextName + "-catchLongline", Branchline.class,
                                               contextName + "-table", Branchline.class,
                                               contextName + "-objectSchoolEstimate", FloatingObject.class,
                                               contextName + "-transmittingBuoyOperation", FloatingObject.class,
                                               contextName + "-table", Section.class,
                                               contextName + "-baitsComposition", SetLongline.class,
                                               contextName + "-branchlinesComposition", SetLongline.class,
                                               contextName + "-floatlinesComposition", SetLongline.class,
                                               contextName + "-globalComposition", SetLongline.class,
                                               contextName + "-hooksComposition", SetLongline.class,
                                               contextName + "-nonTargetCatch", SetSeine.class,
                                               contextName + "-schoolEstimate", SetSeine.class,
                                               contextName + "-targetCatch", SetSeine.class,
                                               contextName + "-targetDiscarded", SetSeine.class,
                                               contextName + "-targetCatch", TargetCatch.class,
                                               contextName + "-targetDiscarded", TargetCatch.class,
                                               contextName + "-gearUseFeatures", TripSeine.class
        );

    }
}
