/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration;

import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.LengthWeightParameterDecorator;
import fr.ird.observe.application.swing.decoration.decorators.NonTargetCatchDecorator;
import fr.ird.observe.application.swing.decoration.decorators.NonTargetLengthDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ObjectObservedSpeciesDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ObserveDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.SpeciesDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TargetCatchDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TripLonglineDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TripSeineDecorator;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ObserveModelInitializer;
import fr.ird.observe.services.dto.ObserveModelInitializerRunner;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.RouteStubDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.util.GPSPoint;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;


/**
 * Le service de décoration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class DecoratorService extends DecoratorProvider {

    public static final String HAULING_IDENTIFIER = "haulingIdentifier";
    public static final String TRIP_CONTEXT = "Trip";

    static {
        n("observe.common.label");
    }

    /** la locale du referentiel. */
    private ReferentialLocale referentialLocale;

    public DecoratorService(ReferentialLocale referentialLocale) {
        this.referentialLocale = referentialLocale;
        loadDecorators();
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public void setReferentialLocale(ReferentialLocale referentialLocale) {
        this.referentialLocale = referentialLocale;
    }

    @Override
    protected void loadDecorators() {
        if (referentialLocale == null) {
            // on n'enregistre pas les décorateur tant que la locale n'est pas
            // positionnée
            return;
        }

        ObserveModelInitializerRunner.init(modelInitializer);
    }

    public <T extends ReferentialDto> ReferentialReferenceDecorator<T> getReferentialReferenceDecorator(Class<T> referenceType) {
        return (ReferentialReferenceDecorator) getDecoratorByType(ReferentialReference.class, referenceType.getSimpleName());
    }

    public <T extends ReferentialDto> void sort(Class<T> type, List<ReferentialReference<T>> data) {
        new ReferentialReferenceComparator<>(type).sort(data);
    }

    private class ReferentialReferenceComparator<T extends ReferentialDto> implements Comparator<ReferentialReference<T>> {

        private final ReferentialReferenceDecorator<T> decorator;

        private final Map<ReferentialReference<T>, String> cache = new HashMap<>();

        private ReferentialReferenceComparator(Class<T> type) {
            decorator = getReferentialReferenceDecorator(type);
        }

        private String get(ReferentialReference<T> id) {
            String value = cache.get(id);
            if (value == null) {
                value = decorator.toString(id);
                cache.put(id, value);
            }
            return value;
        }

        @Override
        public int compare(ReferentialReference<T> o1, ReferentialReference<T> o2) {
            return get(o1).compareTo(get(o2));
        }

        public void sort(List<ReferentialReference<T>> data) {
            Collections.sort(data, this);
            cache.clear();
        }
    }

    public <T extends DataDto> DataReferenceDecorator<T> getDataReferenceDecorator(Class<T> referenceType) {
        return (DataReferenceDecorator) getDecoratorByType(DataReference.class, referenceType.getSimpleName());
    }

    public <T extends DataDto> DataReferenceDecorator<T> getDataReferenceDecorator(Class<T> referenceType, String context) {
        return (DataReferenceDecorator) getDecoratorByType(DataReference.class, referenceType.getSimpleName() + context);
    }

    public <D extends IdDto> Decorator<D> getReferenceDecorator(Class<D> referenceType) {
        Decorator<D> decorator;

        if (DataDto.class.isAssignableFrom(referenceType)) {
            decorator = getDataReferenceDecorator((Class) referenceType);
        } else {
            decorator = getReferentialReferenceDecorator((Class) referenceType);
        }

        return decorator;
    }

    public DataReferenceDecorator<?> getTripReferenceDecorator(DataReference<?> tripDto) {
        DataReferenceDecorator<?> decorator;
        if (tripDto.getType().isAssignableFrom(TripSeineDto.class)) {
            decorator = getDataReferenceDecorator(TripSeineDto.class);
        } else {
            decorator = getDataReferenceDecorator(TripLonglineDto.class);
        }
        return decorator;
    }

    private final ObserveModelInitializer modelInitializer = new ObserveModelInitializer() {

        private String haulingIdentifier;
        private String settingIdentifier;
        private String libelle;
        private Locale locale;

        @Override
        public void start() {

            locale = referentialLocale.getLocale();
            libelle = referentialLocale.getLibelle();
            settingIdentifier = l(locale, "observe.common.settingIdentifier");
            haulingIdentifier = l(locale, "observe.common.haulingIdentifier");

        }

        @Override
        public void end() {
            // Trip commun decorator
            registerDecorator(TRIP_CONTEXT,
                              new DataReferenceDecorator<>("${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s"));

            // gps decorators
            registerObserveDecorator("activity-gps",
                                     ActivitySeineDto.class,
                                     t("observe.common.gps.activity"));
            registerObserveDecorator("gpsPoint-gps",
                                     GPSPoint.class,
                                     t("observe.common.gps.gpsPoint"));

            registerObserveDecorator(GPSPoint.class,
                                     "${time}$td/%1$tm/%1$tY %1$tH:%1$tM##${latitude}$s##${longitude}$s",
                                     " ");

            registerObserveDecorator(SectionTemplate.class, "${id}$s##${floatlineLengths}$s", " ");

        }

        @Override
        public void initCommentableDto() {
            // rien à faire
        }

        @Override
        public void initDataDto() {
            // rien à faire
        }

        @Override
        public void initDataFileDto() {
            // rien à faire
        }

        @Override
        public void initIdDto() {
            // rien à faire
        }

        @Override
        public void initObserveDbUserDto() {

        }

        @Override
        public void initOpenableDto() {
            // rien à faire
        }

        @Override
        public void initTripMapDto() {
            // rien à faire
        }

        @Override
        public void initTripMapPointDto() {
            // rien à faire
        }

        @Override
        public void initActivityLonglineDto() {
            registerDataAndDataReferenceDecorator(ActivityLonglineDto.class,
                                                  "${timeStamp}$td/%1$tm/%1$tY %1$tH:%1$tM##${vesselActivityLongline/label}$s",
                                                  "${timeStamp}$td/%1$tm/%1$tY %1$tH:%1$tM##${vesselActivityLongline}$s",
                                                  " - ");
        }

        @Override
        public void initActivityLonglineEncounterDto() {
            // rien à faire
        }

        @Override
        public void initActivityLonglineSensorUsedDto() {
            // rien à faire
        }

        @Override
        public void initActivityLonglineStubDto() {
            // rien à faire
        }

        @Override
        public void initBaitsCompositionDto() {
            registerDataAndDataReferenceDecorator(BaitsCompositionDto.class, "${baitType/label}$s##${proportion}$s",
                                                  "${baitType}$s##${proportion}$s");
        }

        @Override
        public void initBasketDto() {
            registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, BasketDto.class, "${haulingIdentifier}$s");
            registerDataAndDataReferenceDecorator(BasketDto.class,
                                                  " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");

        }

        @Override
        public void initBranchlineDto() {
            registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, BranchlineDto.class, "${haulingIdentifier}$s");
            registerDataAndDataReferenceDecorator(BranchlineDto.class,
                                                  " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");

        }

        @Override
        public void initBranchlinesCompositionDto() {
            registerDataAndDataReferenceDecorator(BranchlinesCompositionDto.class, "${length}$s##${proportion}$s");
        }

        @Override
        public void initCatchLonglineDto() {
            registerDataAndDataReferenceDecorator(CatchLonglineDto.class, "${homeId}$s");
        }

        @Override
        public void initEncounterDto() {
            registerDataAndDataReferenceDecorator(EncounterDto.class,
                                                  "${encounterType/label}$s##${species/label}$s",
                                                  "${encounterType}$s##${species}$s",
                                                  " - ");
        }

        @Override
        public void initFloatlinesCompositionDto() {
            registerDataAndDataReferenceDecorator(FloatlinesCompositionDto.class, "${lineType/label}$s##${proportion}$s",
                                                  "${lineType}$s##${proportion}$s");
        }

        @Override
        public void initGearUseFeaturesLonglineDto() {
            registerDataAndDataReferenceDecorator(GearUseFeaturesLonglineDto.class, "${gear/label}$s##${number}$s",
                                                  "${gear}$s##${number}$s");
        }

        @Override
        public void initGearUseFeaturesMeasurementLonglineDto() {
            registerDataAndDataReferenceDecorator(GearUseFeaturesMeasurementLonglineDto.class, "${id}$s");
        }

        @Override
        public void initHooksCompositionDto() {
            registerDataAndDataReferenceDecorator(HooksCompositionDto.class, "${hookType/label}$s##${proportion}$s",
                                                  "${hookType}$s##${proportion}$s");
        }

        @Override
        public void initSectionDto() {
            registerDataAndDataReferenceDecorator(SectionDto.class,
                                                  " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");
            registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, SectionDto.class, "${haulingIdentifier}$s");
        }

        @Override
        public void initSensorUsedDto() {
            //FIXME how to decorate ?
            registerDataAndDataReferenceDecorator(SensorUsedDto.class, "${sensorType/label}$s", "${sensorType}$s");
        }

        @Override
        public void initSetLonglineDto() {
            registerDataAndDataReferenceDecorator(SetLonglineDto.class, t("observe.type.setLongline"));
        }

        @Override
        public void initSetLonglineCatchDto() {
            // rien à faire
        }

        @Override
        public void initSetLonglineDetailCompositionDto() {
            // rien à faire
        }

        @Override
        public void initSetLonglineGlobalCompositionDto() {
            // rien à faire
        }

        @Override
        public void initSetLonglineStubDto() {
            // rien à faire
        }

        @Override
        public void initSetLonglineTdrDto() {
            // rien à faire
        }

        @Override
        public void initSizeMeasureDto() {
            registerDataAndDataReferenceDecorator(SizeMeasureDto.class, "${sizeType/label}$s##${size}$s",
                                                  "${sizeType}$s##${size}$s");
        }

        @Override
        public void initTdrDto() {
            registerDataAndDataReferenceDecorator(TdrDto.class, "${homeId}$s");
        }

        @Override
        public void initTripLonglineDto() {
            registerDecorator(new TripLonglineDecorator());
            registerDataReferenceDecorator(TripLonglineDto.class,
                                           "${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s");
        }

        @Override
        public void initTripLonglineActivityDto() {

        }

        @Override
        public void initTripLonglineGearUseDto() {
            registerDataAndDataReferenceDecorator(TripLonglineGearUseDto.class, t("observe.type.tripLonglineGearUse"));
        }

        @Override
        public void initWeightMeasureDto() {
            registerDataAndDataReferenceDecorator(WeightMeasureDto.class, "${weightMeasureType/label}$s##${weight}$s",
                                                  "${weightMeasureType}$s##${weight}$s");
        }

        @Override
        public void initCountryDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(CountryDto.class, libelle);
        }

        @Override
        public void initFpaZoneDto() {
            //FIXME Use startDate - endDate
            registerDefaultReferentialAndReferentialReferenceDecorator(FpaZoneDto.class, libelle);
        }

        @Override
        public void initGearDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(GearDto.class, libelle);
        }

        @Override
        public void initGearCaracteristicDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(GearCaracteristicDto.class, libelle);
        }

        @Override
        public void initGearCaracteristicTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(GearCaracteristicTypeDto.class, libelle);
        }

        @Override
        public void initHarbourDto() {
            registerReferentialAndReferentialReferenceDecorator(HarbourDto.class, "${code}$s##${name}$s##${locode}$s");
        }

        @Override
        public void initI18nReferentialDto() {
            // rien à faire
        }

        @Override
        public void initLengthWeightParameterDto() {
            registerDecorator(new LengthWeightParameterDecorator());
            registerReferentialReferenceDecorator(LengthWeightParameterDto.class,
                                                  "${sex}$s##${ocean}$s##${species}$s##" + t("observe.common.lengthWeightFormula") + " ${lengthWeightFormula}$s");
        }

        @Override
        public void initOceanDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(OceanDto.class, libelle);
        }

        @Override
        public void initOrganismDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(OrganismDto.class, libelle);
        }

        @Override
        public void initPersonDto() {
            registerReferentialAndReferentialReferenceDecorator(PersonDto.class, "${lastName}$s##${firstName}$s");
        }

        @Override
        public void initProgramDto() {
            registerReferentialAndReferentialReferenceDecorator(ProgramDto.class,
                                                                "[${gearTypePrefix}$s] " + t("observe.common.program") + " ${" + libelle + "}$s",
                                                                "[${gearTypePrefix}$s] " + t("observe.common.program") + " ${label}$s");

        }

        @Override
        public void initReferentialDto() {
            // rien à faire
        }

        @Override
        public void initSexDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SexDto.class, libelle);
        }

        @Override
        public void initSizeMeasureTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SizeMeasureTypeDto.class, libelle);
        }

        @Override
        public void initSpeciesDto() {
            registerDecorator(new SpeciesDecorator());
            registerReferentialReferenceDecorator(SpeciesDto.class, "${faoCode}$s##${scientificLabel}$s");
        }

        @Override
        public void initSpeciesGroupDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesGroupDto.class, libelle);
        }

        @Override
        public void initSpeciesListDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesListDto.class, libelle);
        }

        @Override
        public void initVesselDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(VesselDto.class, libelle);
        }

        @Override
        public void initVesselSizeCategoryDto() {
            registerReferentialAndReferentialReferenceDecorator(VesselSizeCategoryDto.class, "${code}$s##${gaugeLabel}$s##${capacityLabel}$s");
        }

        @Override
        public void initVesselTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(VesselTypeDto.class, libelle);
        }

        @Override
        public void initWeightMeasureTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(WeightMeasureTypeDto.class, libelle);
        }

        @Override
        public void initBaitHaulingStatusDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(BaitHaulingStatusDto.class, libelle);
        }

        @Override
        public void initBaitSettingStatusDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(BaitSettingStatusDto.class, libelle);
        }

        @Override
        public void initBaitTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(BaitTypeDto.class, libelle);
        }

        @Override
        public void initCatchFateLonglineDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(CatchFateLonglineDto.class, libelle);
        }

        @Override
        public void initEncounterTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(EncounterTypeDto.class, libelle);
        }

        @Override
        public void initHealthnessDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(HealthnessDto.class, libelle);
        }

        @Override
        public void initHookPositionDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(HookPositionDto.class, libelle);
        }

        @Override
        public void initHookSizeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(HookSizeDto.class, libelle);
        }

        @Override
        public void initHookTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(HookTypeDto.class, libelle);
        }

        @Override
        public void initItemHorizontalPositionDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ItemHorizontalPositionDto.class, libelle);
        }

        @Override
        public void initItemVerticalPositionDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ItemVerticalPositionDto.class, libelle);
        }

        @Override
        public void initLightsticksColorDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(LightsticksColorDto.class, libelle);
        }

        @Override
        public void initLightsticksTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(LightsticksTypeDto.class, libelle);
        }

        @Override
        public void initLineTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(LineTypeDto.class, libelle);
        }

        @Override
        public void initMaturityStatusDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(MaturityStatusDto.class, libelle);
        }

        @Override
        public void initMitigationTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(MitigationTypeDto.class, libelle);
        }

        @Override
        public void initSensorBrandDto() {
            registerReferentialAndReferentialReferenceDecorator(SensorBrandDto.class, "${code}$s##${brandName}$s");
        }

        @Override
        public void initSensorDataFormatDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SensorDataFormatDto.class, libelle);
        }

        @Override
        public void initSensorTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SensorTypeDto.class, libelle);
        }

        @Override
        public void initSettingShapeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SettingShapeDto.class, libelle);
        }

        @Override
        public void initStomacFullnessDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(StomacFullnessDto.class, libelle);
        }

        @Override
        public void initTripTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(TripTypeDto.class, libelle);
        }

        @Override
        public void initVesselActivityLonglineDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(VesselActivityLonglineDto.class, libelle);
        }

        @Override
        public void initDetectionModeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(DetectionModeDto.class, libelle);
        }

        @Override
        public void initObjectFateDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ObjectFateDto.class, libelle);
        }

        @Override
        public void initObjectOperationDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ObjectOperationDto.class, libelle);
        }

        @Override
        public void initObjectTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ObjectTypeDto.class, libelle);
        }

        @Override
        public void initObservedSystemDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ObservedSystemDto.class, libelle);
        }

        @Override
        public void initReasonForDiscardDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForDiscardDto.class, libelle);
        }

        @Override
        public void initReasonForNoFishingDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForNoFishingDto.class, libelle);
        }

        @Override
        public void initReasonForNullSetDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForNullSetDto.class, libelle);
        }

        @Override
        public void initSpeciesFateDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesFateDto.class, libelle);
        }

        @Override
        public void initSpeciesStatusDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesStatusDto.class, libelle);
        }

        @Override
        public void initSurroundingActivityDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(SurroundingActivityDto.class, libelle);
        }

        @Override
        public void initTransmittingBuoyOperationDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(TransmittingBuoyOperationDto.class, libelle);
        }

        @Override
        public void initTransmittingBuoyTypeDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(TransmittingBuoyTypeDto.class, libelle);
        }

        @Override
        public void initVesselActivitySeineDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(VesselActivitySeineDto.class, libelle);
        }

        @Override
        public void initWeightCategoryDto() {
            registerDefaultReferentialAndReferentialReferenceDecorator(WeightCategoryDto.class, libelle);
        }

        @Override
        public void initWindDto() {
            registerReferentialAndReferentialReferenceDecorator(WindDto.class,
                                                                "${code}$s##${" + libelle + "}$s##${speedRange}$s",
                                                                "${code}$s##${label}$s##${speedRange}$s");
        }

        @Override
        public void initSaveResultDto() {
            // rien à faire
        }

        @Override
        public void initTripChildSaveResultDto() {
            // rien à faire
        }

        @Override
        public void initActivitySeineDto() {
            registerDataAndDataReferenceDecorator(ActivitySeineDto.class,
                                                  "${time}$tH:%1$tM##${vesselActivitySeine/label}$s",
                                                  "${time}$tH:%1$tM##${vesselActivitySeine}$s",
                                                  " - ");
        }

        @Override
        public void initActivitySeineStubDto() {
            registerDataAndDataReferenceDecorator(ActivitySeineStubDto.class, "${time}$tH:%1$tM");
        }

        @Override
        public void initFloatingObjectDto() {
            registerDataAndDataReferenceDecorator(FloatingObjectDto.class,
                                                  "DCP ${objectType/label}$s", "DCP ${objectType}$s", " - ");
        }

        @Override
        public void initFloatingObjectObservedSpeciesDto() {
            registerDataAndDataReferenceDecorator(FloatingObjectObservedSpeciesDto.class, t("observe.type.floatingObjectObservedSpecies"));
        }

        @Override
        public void initFloatingObjectSchoolEstimateDto() {
            registerDataAndDataReferenceDecorator(FloatingObjectSchoolEstimateDto.class, t("observe.type.floatingObjectSchoolEstimate"));
        }

        @Override
        public void initFloatingObjectTransmittingBuoyDto() {
            registerDataAndDataReferenceDecorator(FloatingObjectTransmittingBuoyDto.class, t("observe.type.floatingObjectTransmittingBuoy"));
        }

        @Override
        public void initGearUseFeaturesMeasurementSeineDto() {
            registerDataAndDataReferenceDecorator(GearUseFeaturesMeasurementSeineDto.class, "${id}$s");
        }

        @Override
        public void initGearUseFeaturesSeineDto() {
            registerDataAndDataReferenceDecorator(GearUseFeaturesSeineDto.class, "${gear/label}$s##${number}$s",
                                                  "${gear}$s##${number}$s");
        }

        @Override
        public void initNonTargetCatchDto() {
            registerDecorator(new NonTargetCatchDecorator());
            registerDataReferenceDecorator(NonTargetCatchDto.class, "${species}$s##${speciesFate}$s");
        }

        @Override
        public void initNonTargetLengthDto() {

            registerDecorator(new NonTargetLengthDecorator());
            registerDataReferenceDecorator(NonTargetLengthDto.class, "${species}$s##${length}$f##${gender}$s");
        }

        @Override
        public void initNonTargetSampleDto() {
            registerDataAndDataReferenceDecorator(NonTargetSampleDto.class, t("observe.type.nonTargetSample"));
        }

        @Override
        public void initObjectObservedSpeciesDto() {
            registerDecorator(new ObjectObservedSpeciesDecorator());
            registerDataReferenceDecorator(ObjectObservedSpeciesDto.class, "${species}$s##${count}$d");
        }

        @Override
        public void initObjectSchoolEstimateDto() {
            registerDataAndDataReferenceDecorator(ObjectSchoolEstimateDto.class,
                                                  "${species/scientificLabel}$s##${totalWeight}$d",
                                                  "${species}$s##${totalWeight}$d");
        }

        @Override
        public void initRouteDto() {
            registerDataAndDataReferenceDecorator(RouteDto.class, "${date}$td/%1$tm/%1$tY");
        }

        @Override
        public void initRouteStubDto() {
            registerDataAndDataReferenceDecorator(RouteStubDto.class, "${date}$td/%1$tm/%1$tY");
        }

        @Override
        public void initSchoolEstimateDto() {
            registerDataAndDataReferenceDecorator(SchoolEstimateDto.class,
                                                  "${species/scientificLabel}$s##${totalWeight}$d##${meanWeight}$d",
                                                  "${species}$s##${totalWeight}$d##${meanWeight}$d");
        }

        @Override
        public void initSetSeineDto() {
            registerDataAndDataReferenceDecorator(SetSeineDto.class, t("observe.type.setSeine"));
        }

        @Override
        public void initSetSeineNonTargetCatchDto() {
            registerDataAndDataReferenceDecorator(SetSeineNonTargetCatchDto.class, t("observe.type.setSeineNonTargetCatch"));
        }

        @Override
        public void initSetSeineSchoolEstimateDto() {
            registerDataAndDataReferenceDecorator(SetSeineSchoolEstimateDto.class, t("observe.type.setSeineSchoolEstimate"));
        }

        @Override
        public void initSetSeineTargetCatchDto() {
            registerDataAndDataReferenceDecorator(SetSeineTargetCatchDto.class, t("observe.type.setSeineTargetCatch"));
        }

        @Override
        public void initTargetCatchDto() {
            registerDecorator(new TargetCatchDecorator());
        }

        @Override
        public void initTargetLengthDto() {
            registerDataAndDataReferenceDecorator(TargetLengthDto.class,
                                                  "${species/scientificLabel}$s##${length}$f##${count}$d",
                                                  "${species}$s##${length}$f##${count}$d");
        }

        @Override
        public void initTargetSampleDto() {
            registerDataAndDataReferenceDecorator(TargetSampleDto.class, t("observe.type.targetSample"));
        }

        @Override
        public void initTransmittingBuoyDto() {
            registerDataAndDataReferenceDecorator(TransmittingBuoyDto.class,
                                                  "${transmittingBuoyType/label}$s##${transmittingBuoyOperation/label}$s##${code}$s",
                                                  "${transmittingBuoyType}$s##${transmittingBuoyOperation}$s##${code}$s");
        }

        @Override
        public void initTripSeineDto() {
            registerDecorator(new TripSeineDecorator());
            registerDataReferenceDecorator(TripSeineDto.class,
                                           "${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s");
        }

        @Override
        public void initTripSeineGearUseDto() {
            registerDataAndDataReferenceDecorator(TripSeineGearUseDto.class, t("observe.type.tripSeineGearUse"));
        }

        private <T extends ReferentialDto> void registerDefaultReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String libelle) {
            registerReferentialReferenceDecorator(referenceType, "${code}$s##${label}$s");
            registerObserveDecorator(referenceType, "${code}$s##${" + libelle + "}$s", " ");
        }

        private <T extends ReferentialDto> void registerReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String expression) {
            registerReferentialReferenceDecorator(referenceType, expression);
            registerObserveDecorator(referenceType, expression);
        }

        private <T extends ReferentialDto> void registerReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression) {
            registerReferentialReferenceDecorator(referenceType, referenceExpression);
            registerObserveDecorator(referenceType, expression);
        }

        private <T extends ReferentialDto> void registerReferentialReferenceDecorator(Class<T> referenceType, String referenceExpression) {
            registerDecorator(referenceType.getSimpleName(), new ReferentialReferenceDecorator<>(referenceExpression));
        }

        private <T extends IdDto> void registerDataAndDataReferenceDecorator(String context, Class<T> referenceType, String expression) {
            registerDataReferenceDecorator(context, referenceType, expression);
            registerObserveDecorator(context, referenceType, expression, " ");
        }

        private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression) {
            registerDataReferenceDecorator(referenceType, expression);
            registerObserveDecorator(referenceType, expression, " ");
        }

        private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression) {
            registerDataReferenceDecorator(referenceType, referenceExpression);
            registerObserveDecorator(referenceType, expression, " ");
        }

        private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression, String separator) {
            registerDataReferenceDecorator(referenceType, referenceExpression);
            registerObserveDecorator(referenceType, expression, separator);
        }

        private <T extends IdDto> void registerDataReferenceDecorator(Class<T> referenceType, String referenceExpression) {
            registerDataReferenceDecorator(null, referenceType, referenceExpression);
        }

        private <T extends IdDto> void registerDataReferenceDecorator(String context, Class<T> referenceType, String referenceExpression) {
            registerDecorator(referenceType.getSimpleName() + (context == null ? "" : context), new DataReferenceDecorator<>(referenceExpression));
        }

        private <T> void registerObserveDecorator(Class<T> referenceType, String expression) {
            registerDecorator(new ObserveDecorator<>(referenceType, expression));
        }

        private <T> void registerObserveDecorator(Class<T> referenceType, String expression, String separator) {
            registerDecorator(new ObserveDecorator<>(referenceType, expression, separator));
        }

        private <T> void registerObserveDecorator(String context, Class<T> referenceType, String expression) {
            registerDecorator(context, new ObserveDecorator<>(referenceType, expression));
        }

        private <T> void registerObserveDecorator(String context, Class<T> referenceType, String expression, String separator) {
            registerDecorator(context, new ObserveDecorator<>(referenceType, expression, separator));
        }
    };
}
