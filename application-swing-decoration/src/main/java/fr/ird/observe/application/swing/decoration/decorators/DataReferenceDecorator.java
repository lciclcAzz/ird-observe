package fr.ird.observe.application.swing.decoration.decorators;

/*-
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import org.apache.commons.jxpath.JXPathContext;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class DataReferenceDecorator<D extends DataDto> extends ObserveDecorator<DataReference<D>> implements Cloneable {

    private static final long serialVersionUID = 1L;

    public DataReferenceDecorator(String expression) {
        super((Class) DataReference.class, expression);
    }

    @Override
    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext, String token) {

        // assume all values are comparable
        Comparable<Comparable<?>> value;

        Object contextBean = jxcontext.getContextBean();

        if (contextBean instanceof AbstractReference) {

            String[] tokens = token.split("/");

            value = getValueFromReference(tokens, (AbstractReference) contextBean, 0);

            if (value == null) {
                value = (Comparable<Comparable<?>>) getDefaultNullValue(tokens[0]);
            }

        } else {
            value = super.getTokenValue(jxcontext, token);
        }

        return value;

    }

    @Override
    public String toString(Object bean) {
        String result = super.toString(bean);
        return result == null ? null : result.trim();
    }
}
