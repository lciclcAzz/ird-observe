/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.AbstractReference;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.MultiJXPathDecorator;

import java.io.Serializable;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract decorator for any complex decorator.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
public class ObserveDecorator<E> extends MultiJXPathDecorator<E> implements Cloneable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveDecorator.class);

    public static final String DEFAULT_SEPARATOR = "##";

    public static final String DEFAULT_SEPARATOR_REPLACEMENT = " - ";

    public ObserveDecorator(Class<E> internalClass, String expression) {
        super(internalClass, expression, DEFAULT_SEPARATOR,
              DEFAULT_SEPARATOR_REPLACEMENT);
    }

    public ObserveDecorator(Class<E> internalClass, String expression, String separator) {
        super(internalClass, expression, DEFAULT_SEPARATOR, separator);
    }

    @Override
    public final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext,
                                                      String token) {
        // assume all values are comparable
        Comparable<Comparable<?>> value;

        try {

            String[] tokens = token.split("/");

            Object value0 = jxcontext.getValue(tokens[0]);

            if (value0 instanceof AbstractReference) {

                value = getValueFromReference(tokens, (AbstractReference<?>) value0, 1);

            } else {

                value = (Comparable<Comparable<?>>) jxcontext.getValue(token);
            }

            if (value == null) {
                value = (Comparable<Comparable<?>>) getDefaultNullValue(token);
            }

        } catch (Exception e) {
            value = (Comparable<Comparable<?>>) getDefaultUndefinedValue(token);
        }

        return value;
    }

    protected Comparable<Comparable<?>> getValueFromReference(String[] tokens, AbstractReference<?> referenceBean, int startIndex) {

        for (int i = startIndex, max = tokens.length - 1; i < max; i++) {

            if (referenceBean.getPropertyNames().contains(tokens[i])) {

                Serializable propertyValue = referenceBean.getPropertyValue(tokens[i]);

                if (propertyValue == null || !(propertyValue instanceof AbstractReference)) {

                    return (Comparable<Comparable<?>>) getDefaultUndefinedValue(StringUtils.join(tokens, "/"));

                }

                referenceBean = (AbstractReference<?>) propertyValue;
            }
        }

        Comparable<Comparable<?>> value = null;

        String lastToken = tokens[tokens.length - 1];
        if (referenceBean.getPropertyNames().contains(lastToken)) {
            value = (Comparable<Comparable<?>>) referenceBean.getPropertyValue(lastToken);
        }

        return value;
    }

    protected Comparable<?> getDefaultUndefinedValue(String token) {
        if (log.isDebugEnabled()) {
            log.debug("No defined value for token [" + token + "]");
        }
        return t("observe.common.none");
    }

    protected Comparable<?> getDefaultNullValue(String token) {
        if (log.isDebugEnabled()) {
            log.debug("Null value for token [" + token + "]");
        }
        return t("observe.common.none");
    }


}
