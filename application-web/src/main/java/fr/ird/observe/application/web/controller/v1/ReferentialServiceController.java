package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.ReferenceMap;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.DataNotFoundException;
import fr.ird.observe.services.service.ReferenceSetsRequest;
import fr.ird.observe.services.service.ReferentialService;

import java.util.Collection;
import java.util.Date;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialServiceController extends ObserveAuthenticatedServiceControllerSupport<ReferentialService> implements ReferentialService {

    public ReferentialServiceController() {
        super(ReferentialService.class);
    }

    @Override
    public <R extends ReferentialDto> ReferentialReferenceSet<R> getReferenceSet(Class<R> type, Date lastUpdateDate) {
        return service.getReferenceSet(type, lastUpdateDate);
    }

    @Override
    public ImmutableSet<ReferentialReferenceSet<?>> getReferentialReferenceSets(ReferenceSetsRequest request) {
        return service.getReferentialReferenceSets(request);
    }

    @Override
    public SpeciesDto loadSpecies(String id) {
        return service.loadSpecies(id);
    }

    @Override
    public <R extends ReferentialDto> Form<R> loadForm(Class<R> type, String id) throws DataNotFoundException {
        return service.loadForm(type, id);
    }

    @Override
    public <R extends ReferentialDto> ReferentialReference<R> loadReference(Class<R> type, String id) throws DataNotFoundException {
        return service.loadReference(type, id);
    }

    @Override
    public <R extends ReferentialDto> Form<R> preCreate(Class<R> type) {
        return service.preCreate(type);
    }

    @Override
    public <R extends ReferentialDto> SaveResultDto save(R bean) {
        return service.save(bean);
    }

    @Override
    public <R extends ReferentialDto> void delete(Class<R> type, String id) throws DataNotFoundException {
        service.delete(type, id);
    }

    @Override
    public <R extends ReferentialDto> void delete(Class<R> type, Collection<String> ids) throws DataNotFoundException {
        service.delete(type, ids);
    }

    @Override
    public <E extends ReferentialDto> void replaceReference(Class<E> beanType, String idToReplace, String replaceId) {
        service.replaceReference(beanType, idToReplace, replaceId);
    }

    @Override
    public <R extends ReferentialDto> ReferenceMap findAllUsages(R bean) throws DataNotFoundException {
        return service.findAllUsages(bean);
    }

    @Override
    public <R extends ReferentialDto> boolean exists(Class<R> type, String id) {
        return service.exists(type, id);
    }
}
