package fr.ird.observe.application.web.converter;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 07/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDtoConverter extends AbstractConverter  {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveDtoConverter.class);

    protected final Gson gson;

    public ObserveDtoConverter(Gson gson) {
        this.gson = gson;
    }


    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
        T observeDto;

        if (ObserveDto.class.isAssignableFrom(type)) {

            String gsonContent = value.toString();

            observeDto = gson.fromJson(gsonContent, type);

            if (log.isInfoEnabled()) {
                log.info("convert observeDto: " + observeDto);
            }

        } else {
            throw conversionException(type, value);
        }

        return observeDto;

    }

    @Override
    protected Class<?> getDefaultType() {
        return ObserveDto.class;
    }
}
