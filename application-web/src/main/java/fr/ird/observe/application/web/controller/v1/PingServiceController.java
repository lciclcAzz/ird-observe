package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfig;
import fr.ird.observe.application.web.controller.ObserveWebMotionController;
import fr.ird.observe.services.service.PingService;
import org.nuiton.version.Version;

/**
 * Created on 06/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class PingServiceController extends ObserveWebMotionController implements PingService {

    public Version ping() {

        ObserveWebApplicationConfig configuration = getApplicationConfiguration();

        return configuration.getModelVersion();
    }

}
