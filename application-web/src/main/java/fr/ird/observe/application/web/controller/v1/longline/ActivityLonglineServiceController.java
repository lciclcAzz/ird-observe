package fr.ird.observe.application.web.controller.v1.longline;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ActivityLonglineServiceController extends ObserveAuthenticatedServiceControllerSupport<ActivityLonglineService> implements ActivityLonglineService {

    public ActivityLonglineServiceController() {
        super(ActivityLonglineService.class);
    }

    @Override
    public DataReferenceSet<ActivityLonglineDto> getActivityLonglineByTripLongline(String tripLonglineId) {
        return service.getActivityLonglineByTripLongline(tripLonglineId);
    }

    @Override
    public int getActivityLonglinePositionInTripLongline(String tripLonglineId, String activityLonglineId) {
        return service.getActivityLonglinePositionInTripLongline(tripLonglineId, activityLonglineId);
    }

    @Override
    public Form<ActivityLonglineDto> loadForm(String activityLonglineId) {
        return service.loadForm(activityLonglineId);
    }

    @Override
    public ActivityLonglineDto loadDto(String activityLonglineId) {
        return service.loadDto(activityLonglineId);
    }

    @Override
    public DataReference<ActivityLonglineDto> loadReferenceToRead(String activityLonglineId) {
        return service.loadReferenceToRead(activityLonglineId);
    }

    @Override
    public boolean exists(String activityLonglineId) {
        return service.exists(activityLonglineId);
    }

    @Override
    public Form<ActivityLonglineDto> preCreate(String tripLonglineId) {
        return service.preCreate(tripLonglineId);
    }

    @Override
    public TripChildSaveResultDto save(String tripLonglineId, ActivityLonglineDto dto) {
        return service.save(tripLonglineId, dto);
    }

    @Override
    public boolean delete(String tripLonglineId, String activityLonglineId) {
        return service.delete(tripLonglineId, activityLonglineId);
    }

    @Override
    public int moveActivityLonglineToTripLongline(String activityLonglineId, String tripLonglineId) {
        return service.moveActivityLonglineToTripLongline(activityLonglineId, tripLonglineId);
    }

    @Override
    public List<Integer> moveActivityLonglinesToTripLongline(List<String> activityLonglineIds, String tripLonglineId) {
        return service.moveActivityLonglinesToTripLongline(activityLonglineIds, tripLonglineId);
    }
}
