package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.service.seine.SetSeineService;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SetSeineServiceController extends ObserveAuthenticatedServiceControllerSupport<SetSeineService> implements SetSeineService {

    public SetSeineServiceController() {
        super(SetSeineService.class);
    }

    @Override
    public Form<SetSeineDto> loadForm(String setSeineId) {
        return service.loadForm(setSeineId);
    }

    @Override
    public SetSeineDto loadDto(String setSeineId) {
        return service.loadDto(setSeineId);
    }

    @Override
    public DataReference<SetSeineDto> loadReferenceToRead(String setSeineId) {
        return service.loadReferenceToRead(setSeineId);
    }

    @Override
    public boolean exists(String setSeineId) {
        return service.exists(setSeineId);
    }

    @Override
    public Form<SetSeineDto> preCreate(String routeId, String activitySeineId) {
        return service.preCreate(routeId, activitySeineId);
    }

    @Override
    public SaveResultDto save(String activitySeineId, SetSeineDto dto) {
        return service.save(activitySeineId, dto);
    }

    @Override
    public void delete(String activitySeineId, String setSeineId) {
        service.delete(activitySeineId, setSeineId);
    }

}
