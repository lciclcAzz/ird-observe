package fr.ird.observe.application.web.injector;

/*-
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * Cet injecteur a été mis en place pour permettre la conversion des immutableSet arrivant dans une requete.
 * Les immutableSet provoquent une erreur lors du traitement par le framework webmotion qui traite ces objets comme des collections,
 * essayant de les instancier avant de pousser des valeurs dedans... les ImmutableSet ne sont pas instanciables.
 *
 * Nous nous contentons ici de pousser les valeurs de la requête directement dans l'immutableSet
 * car celles-ci nous arrivent non sérialisées (cf. fr.ird.observe.services.rest.ObserveServiceFactoryRest.RemoteInvocationHandler.addParameters())
 *
 * Cet injecteur ne fonctionnera pas si les données arrivent sérialisées.
 *
 * @author Samuel Maisonneuve - maisonneuve@codelutin.com
 */
public class ImmutableSetInjector implements ExecutorParametersInjectorHandler.Injector {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImmutableSetInjector.class);

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {

        ImmutableSet result = null;

        if (ImmutableSet.class.isAssignableFrom(type)) {
            // Type des objets contenu dans le set
            // Type[] actualTypeArguments = ((ParameterizedType) generic).getActualTypeArguments();
            // Type subType = actualTypeArguments[0];

            ImmutableSet.Builder builder = ImmutableSet.builder();

            Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);

            Objects.requireNonNull(parameterTree, "Le paramètre " + name + " n'as pas été trouvé, recompiler (parameter)!");

            String[] values = (String[]) parameterTree.getValue();
            for (String value: values) {
                builder.add(value);
            }

            result = builder.build();

            if (log.isInfoEnabled()) {
                log.info("Inject ImmutableSet: " + result);
            }

        }

        return result;

    }
}
