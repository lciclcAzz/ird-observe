package fr.ird.observe.application.web.request;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.ObserveWebApplicationContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.security.AdminApiKeyNotFoundException;
import fr.ird.observe.services.security.AuthenticationTokenNotFoundException;
import fr.ird.observe.services.security.InvalidAdminKeyApiException;
import org.debux.webmotion.server.call.HttpContext;

import java.util.Locale;

/**
 * Created on 4/25/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebRequestContext {

    private static final String REQUEST_OBSERVE_WEB_REQUEST_CONTEXT = ObserveWebRequestContext.class.getName();

    public static ObserveWebRequestContext getRequestContext(HttpContext httpContext) {

        return (ObserveWebRequestContext)
                httpContext.getRequest().getAttribute(REQUEST_OBSERVE_WEB_REQUEST_CONTEXT);
    }

    public static void setRequestContext(HttpContext httpContext,
                                         ObserveWebRequestContext serviceContext) {
        httpContext.getRequest().setAttribute(REQUEST_OBSERVE_WEB_REQUEST_CONTEXT, serviceContext);
    }

    private final ObserveWebApplicationContext applicationContext;

    private final Locale applicationLocale;

    private final ReferentialLocale referentialLocale;

    private final ObserveSpeciesListConfiguration speciesListConfiguration;

    private final String optionalAdminApiKey;

    private final String optionalAuthenticationToken;

    public ObserveWebRequestContext(ObserveWebApplicationContext applicationContext,
                                    Locale applicationLocale,
                                    ReferentialLocale referentialLocale,
                                    ObserveSpeciesListConfiguration speciesListConfiguration,
                                    String adminApiKey,
                                    String authenticationToken) {
        this.applicationContext = applicationContext;
        this.applicationLocale = applicationLocale;
        this.referentialLocale = referentialLocale;
        this.speciesListConfiguration = speciesListConfiguration;
        this.optionalAdminApiKey = adminApiKey;
        this.optionalAuthenticationToken = authenticationToken;
    }

    public ObserveWebApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public <S extends ObserveService> S newService(Class<S> serviceType, ObserveDataSourceConfiguration dataSourceConfiguration) {

        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                applicationLocale,
                referentialLocale,
                applicationContext.getApplicationConfiguration().getTemporaryDirectory(),
                speciesListConfiguration,
                dataSourceConfiguration);
        return applicationContext.newService(observeServiceInitializer, serviceType);

    }

    public <S extends ObserveService> S newService(Class<S> serviceType, ObserveDataSourceConfigurationAndConnection configurationAndConnection) {

        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                applicationLocale,
                referentialLocale,
                applicationContext.getApplicationConfiguration().getTemporaryDirectory(),
                speciesListConfiguration,
                configurationAndConnection);
        return applicationContext.newService(observeServiceInitializer, serviceType);

    }

    public void checkAdminApiKeyIsValid() {
        checkAdminApiKeyIsPresent();
        String configurationAdminKey = applicationContext.getApplicationConfiguration().getAdminApiKey();
        if (!configurationAdminKey.equals(optionalAdminApiKey)) {
            throw new InvalidAdminKeyApiException(optionalAdminApiKey);
        }
    }

    public String getAuthenticationToken() {
        checkAuthenticationTokenIsPresent();
        return optionalAuthenticationToken;
    }

    private void checkAdminApiKeyIsPresent() {
        if (optionalAdminApiKey == null) {
            throw new AdminApiKeyNotFoundException();
        }
    }

    private void checkAuthenticationTokenIsPresent() {
        if (optionalAuthenticationToken == null) {
            throw new AuthenticationTokenNotFoundException();
        }
    }

}
