package fr.ird.observe.application.web;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfig;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabaseException;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabaseRoleException;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabasesException;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabasesHelper;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUserException;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUserPermissionException;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUsersException;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsers;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsersHelper;
import fr.ird.observe.application.web.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.runner.ObserveServiceMainFactory;
import fr.ird.observe.services.dto.gson.ObserveDtoGsonSupplier;
import org.debux.webmotion.server.call.HttpContext;
import org.nuiton.version.Version;

import javax.servlet.ServletContext;
import java.io.Closeable;
import java.io.IOException;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebApplicationContext implements Closeable {

    public static final String APPLICATION_CONTEXT_PARAMETER = ObserveWebApplicationContext.class.getName();

    public static final String MISSING_APPLICATION_CONTEXT =
            ObserveWebApplicationContext.class.getSimpleName() + " not found. You probably forgot to" +
            " register " + ObserveWebApplicationListener.class.getName() + " in your web.xml";

    protected ObserveDtoGsonSupplier gsonSupplier;

    protected ObserveWebApplicationConfig applicationConfiguration;

    protected ObserveServiceFactory mainServiceFactory;

    protected ObserveWebDatabases databases;

    protected ObserveWebUsers users;

    protected ObserveWebSecurityApplicationContext securityApplicationContext;

    public static ObserveWebApplicationContext getApplicationContext(HttpContext context) {

        ServletContext servletContext = context.getServletContext();
        ObserveWebApplicationContext result =
                (ObserveWebApplicationContext) servletContext
                        .getAttribute(ObserveWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        Preconditions.checkState(result != null, ObserveWebApplicationContext.MISSING_APPLICATION_CONTEXT);

        return result;

    }

    public void init(String contextPath) throws InvalidObserveWebDatabaseException, InvalidObserveWebDatabasesException, InvalidObserveWebDatabaseRoleException, InvalidObserveWebUserPermissionException, InvalidObserveWebUsersException, InvalidObserveWebUserException {

        // init configuration
        applicationConfiguration = ObserveWebApplicationConfig.fromContextPath(contextPath);

        // init databases
        ObserveWebDatabasesHelper databasesHelper = new ObserveWebDatabasesHelper();
        databases = databasesHelper.load(applicationConfiguration.getDatabasesConfigurationFile());

        // init users
        ObserveWebUsersHelper usersHelper = new ObserveWebUsersHelper();
        users = usersHelper.load(databases, applicationConfiguration.getUsersConfigurationFile());

        // init security application context
        securityApplicationContext = new ObserveWebSecurityApplicationContext(applicationConfiguration);

        Version modelVersion = applicationConfiguration.getModelVersion();

        securityApplicationContext.init(databases, users, modelVersion);

        // init service factory
        mainServiceFactory = ObserveServiceMainFactory.get();

        // init gson supplier
        boolean devMode = applicationConfiguration.isDevMode();
        gsonSupplier = new ObserveDtoGsonSupplier(devMode);

    }

    public void reloadConfiguration() throws InvalidObserveWebUserPermissionException, InvalidObserveWebUsersException, InvalidObserveWebUserException, InvalidObserveWebDatabaseException, InvalidObserveWebDatabasesException, InvalidObserveWebDatabaseRoleException {

        // init databases
        ObserveWebDatabasesHelper databasesHelper = new ObserveWebDatabasesHelper();
        databases = databasesHelper.load(applicationConfiguration.getDatabasesConfigurationFile());

        // init users
        ObserveWebUsersHelper usersHelper = new ObserveWebUsersHelper();
        users = usersHelper.load(databases, applicationConfiguration.getUsersConfigurationFile());

        Version modelVersion = applicationConfiguration.getModelVersion();

        securityApplicationContext.init(databases, users, modelVersion);

    }

    @Override
    public void close() throws IOException {

        // Supprimer le cache des sessions
        securityApplicationContext.close();

        // Fermer l'usine de services
        mainServiceFactory.close();

    }

    public ObserveDtoGsonSupplier getGsonSupplier() {
        return gsonSupplier;
    }

    public ObserveWebApplicationConfig getApplicationConfiguration() {
        return applicationConfiguration;
    }

    public ObserveWebSecurityApplicationContext getSecurityApplicationContext() {
        return securityApplicationContext;
    }

    public ObserveWebDatabases getDatabases() {
        return databases;
    }

    public ObserveWebUsers getUsers() {
        return users;
    }

    public <S extends ObserveService> S newService(ObserveServiceInitializer observeServiceInitializer, Class<S> serviceType) {
        return mainServiceFactory.newService(observeServiceInitializer, serviceType);
    }

}
