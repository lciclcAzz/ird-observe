package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadReferentialPermission;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ReferentialSynchronizeDiffService extends ObserveService {

    @ReadReferentialPermission
    <R extends ReferentialDto> ReferentialReferenceSet<R> getEnabledReferentialReferenceSet(Class<R> referentialName);

    @PostRequest
    @ReadReferentialPermission
    <R extends ReferentialDto> ReferentialReferenceSet<R> getReferentialReferenceSet(Class<R> referentialName, ImmutableSet<String> ids);

    @PostRequest
    @ReadReferentialPermission
    <R extends ReferentialDto> ReferentialMultimap<R> getReferentials(Class<R> referentialName, ImmutableSet<String> ids);

    /**
     * Récupération sur la source à synchroniser des versions de ses référentiels.
     *
     * @return les versions de tous les référentiels de la base à synchroniser.
     */
    @ReadReferentialPermission
    ReferentialDataSourceStates getSourceReferentialStates();

}
