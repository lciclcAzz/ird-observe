package fr.ird.observe.services.service.actions.validate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ObserveDto;

/**
 * Le résultat d'une validation de données.
 *
 * Created on 02/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidateDataResult implements ObserveDto {

    protected final ValidateDataRequest validateDataRequest;

    protected final ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> resultByType;

    public ValidateDataResult(ValidateDataRequest validateDataRequest, ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> resultByType) {
        this.validateDataRequest = validateDataRequest;
        this.resultByType = resultByType;
    }

    public ValidateDataRequest getValidateDataRequest() {
        return validateDataRequest;
    }

    public ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> getResultByType() {
        return resultByType;
    }

}
