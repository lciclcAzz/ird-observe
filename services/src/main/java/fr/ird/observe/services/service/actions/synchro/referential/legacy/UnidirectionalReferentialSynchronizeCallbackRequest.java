package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.util.Collection;

/**
 * Représente une demande du call back utilisateur pour un type de référentiel donné.
 *
 * Created on 12/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeCallbackRequest<R extends ReferentialDto> {

    private final Class<R> referentialName;
    private final ImmutableSet<ReferentialReference<R>> referentialsToReplace;
    private final ImmutableSet<ReferentialReference<R>> availableReferentials;

    public Class<R> getReferentialName() {
        return referentialName;
    }

    public ImmutableSet<ReferentialReference<R>> getReferentialsToReplace() {
        return referentialsToReplace;
    }

    public ImmutableSet<ReferentialReference<R>> getAvailableReferentials() {
        return availableReferentials;
    }

    UnidirectionalReferentialSynchronizeCallbackRequest(Class<R> referentialName,
                                                                Collection<ReferentialReference<R>> referentialsToReplace,
                                                                Collection<ReferentialReference<R>> availableReferentials) {
        this.referentialName = referentialName;
        this.referentialsToReplace = ImmutableSet.copyOf(referentialsToReplace);
        this.availableReferentials = ImmutableSet.copyOf(availableReferentials);
    }

}
