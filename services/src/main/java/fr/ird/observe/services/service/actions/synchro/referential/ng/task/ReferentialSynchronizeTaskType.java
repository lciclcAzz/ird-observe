package fr.ird.observe.services.service.actions.synchro.referential.ng.task;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public enum ReferentialSynchronizeTaskType {

    ADD(false),
    UPDATE(false),
    REVERT(false),
    DELETE(true),
    DESACTIVATE(true);

    private final boolean withReplace;

    ReferentialSynchronizeTaskType(boolean withReplace) {
        this.withReplace = withReplace;
    }

    public boolean withReplace() {
        return withReplace;
    }
}
