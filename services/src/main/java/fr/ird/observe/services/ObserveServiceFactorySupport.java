package fr.ird.observe.services;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.reflections.Reflections;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ObserveServiceFactorySupport implements ObserveServiceFactory {

    protected ObserveServiceFactory mainServiceFactory;

    @Override
    public ObserveServiceFactory getMainServiceFactory() {
        return mainServiceFactory;
    }

    @Override
    public void setMainServiceFactory(ObserveServiceFactory mainServiceFactory) {
        this.mainServiceFactory = mainServiceFactory;
    }

    protected <S extends ObserveService> Class<S> getServiceClassType(LoadingCache<Class<?>, Class<?>> serviceTypeCache, Class<S> serviceType) {
        try {
            return (Class<S>) serviceTypeCache.get(serviceType);
        } catch (ExecutionException e) {
            throw new IllegalStateException("Could not get service implementation class for " + serviceType.getName(), e);
        }
    }

    protected static LoadingCache<Class<?>, Class<?>> newServiceImplementationTypesCache(String suffix) {

        return CacheBuilder.newBuilder().build(new CacheLoader<Class<?>, Class<?>>() {

            Set<Class<? extends ObserveService>> serviceImpls = new Reflections("fr.ird.observe.services").getSubTypesOf(ObserveService.class);

            @Override
            public Class<?> load(Class<?> key) throws Exception {

                Optional<Class<? extends ObserveService>> optionalImpl =
                        serviceImpls.stream()
                                    .filter(type -> type.getSimpleName().equals(key.getSimpleName() + suffix))
                                    .findFirst();

                if (optionalImpl.isPresent()) {
                    return optionalImpl.get();
                }
                throw new IllegalStateException("Could not get class: " + key);

            }
        });

    }

}
