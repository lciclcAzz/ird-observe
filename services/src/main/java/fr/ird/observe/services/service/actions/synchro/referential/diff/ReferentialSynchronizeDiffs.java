package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Optional;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialSynchronizeDiffs {

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final ReferentialSynchronizeDiff.Builder leftBuilder;
        private final ReferentialSynchronizeDiff.Builder rightBuilder;

        private Builder() {
            leftBuilder = ReferentialSynchronizeDiff.builder();
            rightBuilder = ReferentialSynchronizeDiff.builder();
        }

        <R extends ReferentialDto> Builder addLeftAddedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            leftBuilder.addAddedReferential(referentialName, referentialDto);
            return this;
        }

        <R extends ReferentialDto> Builder addLeftUpdatedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            leftBuilder.addUpdatedReferential(referentialName, referentialDto);
            return this;
        }

        <R extends ReferentialDto> Builder addRightAddedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            rightBuilder.addAddedReferential(referentialName, referentialDto);
            return this;
        }

        <R extends ReferentialDto> Builder addRightUpdatedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            rightBuilder.addUpdatedReferential(referentialName, referentialDto);
            return this;
        }

        public ReferentialSynchronizeDiffs build() {
            ReferentialSynchronizeDiff leftDiff = leftBuilder.build();
            ReferentialSynchronizeDiff rightDiff = rightBuilder.build();
            return new ReferentialSynchronizeDiffs(
                    leftDiff,
                    rightDiff,
                    ImmutableSet.<Class<? extends ReferentialDto>>builder()
                            .addAll(leftDiff.getReferentialNames())
                            .addAll(rightDiff.getReferentialNames())
                            .build()
            );
        }
    }

    private final ReferentialSynchronizeDiff leftDiff;
    private final ReferentialSynchronizeDiff rightDiff;
    /**
     * Tous les types de référentiels décrits pour cette source.
     */
    private final ImmutableSet<Class<? extends ReferentialDto>> referentialNames;

    public ReferentialSynchronizeDiff getLeftDiff() {
        return leftDiff;
    }

    public ReferentialSynchronizeDiff getRightDiff() {
        return rightDiff;
    }

    public ImmutableSet<Class<? extends ReferentialDto>> getReferentialNames() {
        return referentialNames;
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getLeftAddedReferentials(Class<R> referentialName) {
        return leftDiff.getAddedReferentials(referentialName);
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getLeftUpdatedReferentials(Class<R> referentialName) {
        return leftDiff.getUpdatedReferentials(referentialName);
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getRightAddedReferentials(Class<R> referentialName) {
        return rightDiff.getAddedReferentials(referentialName);
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getRightUpdatedReferentials(Class<R> referentialName) {
        return rightDiff.getUpdatedReferentials(referentialName);
    }

    private ReferentialSynchronizeDiffs(ReferentialSynchronizeDiff leftDiff, ReferentialSynchronizeDiff rightDiff, ImmutableSet<Class<? extends ReferentialDto>> referentialNames) {
        this.leftDiff = leftDiff;
        this.rightDiff = rightDiff;
        this.referentialNames = referentialNames;
    }

}
