package fr.ird.observe.services.service.actions.validate;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;
import org.nuiton.validator.NuitonValidatorScope;

import java.util.Objects;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidatorDto {

    private final Class<? extends IdDto> type;
    private final NuitonValidatorScope scope;
    private final String context;
    private final Set<String> fields;

    public ValidatorDto(Class<? extends IdDto> type, NuitonValidatorScope scope, String context, Set<String> fields) {
        this.type = type;
        this.scope = scope;
        this.context = context;
        this.fields = fields;
    }

    public Class<? extends IdDto> getType() {
        return type;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getContext() {
        return context;
    }

    public Set<String> getFields() {
        return fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidatorDto that = (ValidatorDto) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(scope, that.scope) &&
                Objects.equals(context, that.context);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, scope, context);
    }
}
