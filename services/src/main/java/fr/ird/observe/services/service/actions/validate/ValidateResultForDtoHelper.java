package fr.ird.observe.services.service.actions.validate;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.nuiton.validator.NuitonValidatorScope;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidateResultForDtoHelper {

    public static EnumMap<NuitonValidatorScope, Integer> getScopesCount(ValidateResultForDto<?> validateResultForDto) {

        EnumMap<NuitonValidatorScope, Integer> result = Maps.newEnumMap(NuitonValidatorScope.class);

        for (NuitonValidatorScope s : NuitonValidatorScope.values()) {
            result.put(s, 0);
        }

        ImmutableSet<ValidationMessage> messages = validateResultForDto.getMessages();

        for (ValidationMessage validationMessage : messages) {

            NuitonValidatorScope scope = validationMessage.getScope();

            result.put(scope, result.get(scope) + 1);
        }

        for (NuitonValidatorScope s : NuitonValidatorScope.values()) {

            if (result.get(s) == 0) {

                result.remove(s);

            }

        }

        return result;
    }

    public static EnumSet<NuitonValidatorScope> getScopes(ValidateResultForDto<?> validateResultForDto) {

        return Sets.newEnumSet(Iterables.transform(validateResultForDto.getMessages(), ValidationMessage::getScope), NuitonValidatorScope.class);
    }

    public static List<ValidationMessage> scopeMessageFilter(final NuitonValidatorScope scope, ValidateResultForDto<?> validateResultForDto) {

        return Lists.newLinkedList(validateResultForDto.getMessages().stream().filter(input -> scope.equals(input.getScope())).collect(Collectors.toList()));
    }

}
