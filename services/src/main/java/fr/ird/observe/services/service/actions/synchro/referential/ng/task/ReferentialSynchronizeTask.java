package fr.ird.observe.services.service.actions.synchro.referential.ng.task;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Optional;

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ReferentialSynchronizeTask<R extends ReferentialDto> {

    private final Class<R> referentialType;
    private final String referentialId;
    private final String replaceReferentialId;

    public ReferentialSynchronizeTask(Class<R> referentialName, String referentialId, String replaceReferenceId) {
        this.referentialType = referentialName;
        this.referentialId = referentialId;
        this.replaceReferentialId = replaceReferenceId;
    }

    public Class<R> getReferentialType() {
        return referentialType;
    }

    public String getReferentialId() {
        return referentialId;
    }

    public Optional<String> getOptionalReplaceReferentialId() {
        return Optional.ofNullable(replaceReferentialId);
    }

}
