package fr.ird.observe.services.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.ObserveDto;

import java.io.Serializable;

/**
 * Pour retourner le résultat de la consolidation d'une marée de type seine.
 *
 * Un tel objet n'est créé que si des modifications ont été effectuée sur la marée.
 *
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ConsolidateTripSeineDataResult implements Serializable, ObserveDto {

    private static final long serialVersionUID = 1L;

    /**
     * L'identifiant de la marée.
     */
    protected final String tripSeineId;

    /**
     * Le libéllé de la marée.
     */
    protected final String tripSeineLabel;

    /**
     * Les résultats pour les activités modifiées lors de l'opération de consolidation.
     */
    protected final ImmutableSet<ConsolidateActivitySeineDataResult> consolidateActivitySeineDataResults;

    public ConsolidateTripSeineDataResult(String tripSeineId, String tripSeineLabel, ImmutableSet<ConsolidateActivitySeineDataResult> consolidateActivitySeineDataResults) {
        this.tripSeineId = tripSeineId;
        this.tripSeineLabel=tripSeineLabel;
        this.consolidateActivitySeineDataResults = consolidateActivitySeineDataResults;
    }

    public String getTripSeineId() {
        return tripSeineId;
    }

    public String getTripSeineLabel() {
        return tripSeineLabel;
    }

    public ImmutableSet<ConsolidateActivitySeineDataResult> getConsolidateActivitySeineDataResults() {
        return consolidateActivitySeineDataResults;
    }
}
