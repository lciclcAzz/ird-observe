package fr.ird.observe.services.service;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;

/**
 * Pour signifier qu'on a demandé une resources non existente.
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final Class<? extends IdDto> type;
    private final String id;

    public DataNotFoundException(Class<? extends IdDto> type, String id) {
        this.type = type;
        this.id = id;
    }

    public Class<? extends IdDto> getType() {
        return type;
    }

    public String getId() {
        return id;
    }
}
