package fr.ird.observe.services.service.seine;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface RouteService extends ObserveService {

    @ReadDataPermission
    DataReferenceSet<RouteDto> getRouteByTripSeine(String tripSeineId);

    @ReadDataPermission
    int getRoutePositionInTripSeine(String tripSeineId, String routeId);

    @ReadDataPermission
    Form<RouteDto> loadForm(String routeId);

    @ReadDataPermission
    RouteDto loadDto(String routeId);

    @ReadDataPermission
    DataReference<RouteDto> loadReferenceToRead(String routeId);

    @ReadDataPermission
    boolean exists(String routeId);

    @WriteDataPermission
    Form<RouteDto> preCreate(String tripSeineId);

    @Write
    @WriteDataPermission
    @PostRequest
    TripChildSaveResultDto save(String tripSeineId, RouteDto dto);

    @Write
    @WriteDataPermission
    @DeleteRequest
    boolean delete(String tripSeineId, String routeId);

    @Write
    @WriteDataPermission
    @PostRequest
    int moveRouteToTripSeine(String routeId, String tripSeineId);

    @Write
    @WriteDataPermission
    @PostRequest
    List<Integer> moveRoutesToTripSeine(List<String> routeIds, String tripSeineId);
}
