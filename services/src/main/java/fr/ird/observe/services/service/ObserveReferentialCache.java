package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinition;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Un cache de référentiels.
 *
 * Created on 10/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveReferentialCache implements Closeable, Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveReferentialCache.class);

    private final Map<Class<? extends ReferentialDto>, ReferentialReferenceSet<?>> cache;

    public ObserveReferentialCache() {
        this.cache = new LinkedHashMap<>();
    }

    public <D extends ReferentialDto> ReferentialReferenceSet<D> getReferentialReferenceSet(ReferentialService referentialService, Class<D> type) {

        ReferentialReferenceSet<D> referenceSetDto = (ReferentialReferenceSet<D>) cache.get(type);

        Date lastUpdate = null;
        if (referenceSetDto != null) {
            lastUpdate = referenceSetDto.getLastUpdate();
        }

        ReferentialReferenceSet<D> lastReferenceSetDto = referentialService.getReferenceSet(type, lastUpdate);
        if (lastReferenceSetDto != null) {

            cache.put(type, lastReferenceSetDto);
            referenceSetDto = lastReferenceSetDto;

        }

        return referenceSetDto;

    }

    public ImmutableMap<Class<?>, ReferentialReferenceSet<?>> loadReferenceSets(ReferentialService referentialService, String referentialRequestName) {

        ImmutableMap.Builder<Class<?>, ReferentialReferenceSet<?>> result = ImmutableMap.builder();

        if (referentialRequestName != null) {

            if (log.isInfoEnabled()) {
                log.info("Loading referantialReferenceSetRequest: " + referentialRequestName);
            }
            ReferenceSetRequestDefinition requestDefinition = ReferenceSetRequestDefinitions.get(referentialRequestName);

            ImmutableMap<Class<?>, Date> lastUpdateDates = getLastUpdateDates(referentialRequestName);

            ReferenceSetsRequest request = new ReferenceSetsRequest();
            request.setRequestName(referentialRequestName);
            request.setLastUpdateDates(lastUpdateDates);

            ImmutableSet<ReferentialReferenceSet<?>> referenceSetResult = referentialService.getReferentialReferenceSets(request);

            for (ReferentialReferenceSet<?> referentialReferenceSet : referenceSetResult) {

                cache.put(referentialReferenceSet.getType(), referentialReferenceSet);

            }

            for (ReferenceSetDefinition<? extends ReferentialDto> referentialReferenceSetDefinition : requestDefinition.getReferentialReferenceSetDefinitions()) {

                Class<? extends ReferentialDto> type = referentialReferenceSetDefinition.getType();
                ReferentialReferenceSet<?> referentialReferenceSet = cache.get(type);
                result.put(type, referentialReferenceSet);

            }
        }

        return result.build();

    }

    /**
     * Pour récupérer les dates de dernières mises à jour des ensembles de références utiliées par la requète.
     *
     * @param requestName le nom de la requète
     * @return le dictionnaire des dates de dernières mises à jour pour chaque ensemble de référentiels
     */
    public ImmutableMap<Class<?>, Date> getLastUpdateDates(String requestName) {

        ReferenceSetRequestDefinition requestDefinition = ReferenceSetRequestDefinitions.get(requestName);

        ImmutableMap.Builder<Class<?>, Date> builder = ImmutableMap.builder();

        for (ReferenceSetDefinition<? extends ReferentialDto> referentialSetDefinition : requestDefinition.getReferentialReferenceSetDefinitions()) {

            Class<? extends ReferentialDto> type = referentialSetDefinition.getType();
            ReferentialReferenceSet<?> referenceSetDto = cache.get(type);
            if (referenceSetDto != null) {
                Date lastUpdate = referenceSetDto.getLastUpdate();
                builder.put(type, lastUpdate);
            }

        }

        return builder.build();

    }

    @Override
    public void close() {
        cache.clear();
    }

}
