package fr.ird.observe.services.service.actions.validate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ObserveDto;

/**
 * Les résultats de la validation d'un dto.
 *
 * On a une référence sur l'objet validé ainsi que les messages de validation
 * qui lui sont associés.
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidateResultForDto<D extends IdDto> implements ObserveDto {

    protected final AbstractReference<D> dto;

    protected final ImmutableSet<ValidationMessage> messages;

    public ValidateResultForDto(AbstractReference<D> dto, ImmutableSet<ValidationMessage> messages) {
        this.dto = dto;
        this.messages = messages;
    }

    public AbstractReference<D> getDto() {
        return dto;
    }

    public ImmutableSet<ValidationMessage> getMessages() {
        return messages;
    }

}
