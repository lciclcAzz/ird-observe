/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.service.actions.report;


import fr.ird.observe.services.dto.ObserveModelType;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportOperation;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.SortedProperties;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe responsable de la construction d'un report à partir de sa définition
 * lue dans un fichier de propriétés.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ReportBuilder {

    public static final Pattern REPORT_DEFINITION_PATTERN = Pattern.compile("report.(\\w+).name");

    public static final String REQUEST_PREFIX = "request.";

    public static final String OPERATION_PREFIX = "operations.";

    public static final String VARIABLE_PREFIX = "variable.";

    public static final String REPEAT_VARIABLE_PREFIX = "repeatVariable.";

    /** Logger */
    private static final Log log = LogFactory.getLog(ReportBuilder.class);

    /** les propriétés chargées par le builder pour construire les reports */
    protected Properties properties;

    /** la liste des lastName sde reports connus par le système */
    protected List<String> reportNames;

    /** les operations connues par le système */
    protected static Map<String, Class<?>> operations;

    public List<String> getReportNames() {
        return reportNames;
    }

    public List<Report> load(URL definition) throws IOException {
        properties = new SortedProperties();
        InputStream in = definition.openStream();
        try {
            properties.load(in);
        } finally {
            in.close();
        }

        reportNames = detectReportNames();
        if (log.isInfoEnabled()) {
            log.info("Detected report names : " + reportNames);
        }

        List<Report> reports = new ArrayList<>();
        for (String reportName : reportNames) {
            Report report = build(reportName);
            reports.add(report);
        }

        return reports;
    }


    protected List<String> detectReportNames() {
        List<String> reportNames = new ArrayList<>();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            Matcher matcher = REPORT_DEFINITION_PATTERN.matcher(key);
            if (!matcher.matches()) {

                // pas de match
                continue;
            }
            String reportName = matcher.group(1);
            if (log.isInfoEnabled()) {
                log.info("Discover a new report : " + reportName);
            }
            reportNames.add(reportName);
        }
        return reportNames;
    }

    protected Report build(String reportName) {

        Map<String, String> dico = detectReportProperties(reportName);

        if (log.isDebugEnabled()) {
            log.debug("Will build report [" + reportName + "] with " + dico.size() + " properties (" + dico + ").");
        }

        ObserveModelType modelType = ObserveModelType.valueOf(getValue(dico, "modelType").trim().toUpperCase());
        String name = getValue(dico, "name").trim();
        String description = getValue(dico, "description").trim();
        String rows = getValue(dico, "rows");
        String columns = getValue(dico, "columns");
        String[] rowHeaders = rows == null ? null : rows.split(",");
        if (rowHeaders != null) {
            for (int i = 0; i < rowHeaders.length; i++) {
                String rowHeader = rowHeaders[i];
                rowHeaders[i] = rowHeader.trim();
            }
        }
        String[] columnHeaders = columns == null ? null : columns.split(",");
        if (columnHeaders != null) {
            for (int i = 0; i < columnHeaders.length; i++) {
                String columnHeader = columnHeaders[i];
                columnHeaders[i] = columnHeader.trim();
            }
        }
        ReportRequest[] requests = getRequests(reportName, dico);
        ReportOperation[] operationss = getOperations(reportName, dico);
        ReportVariable[] variables = getVariables(reportName, dico);
        ReportVariable[] repeatVariables = getRepeatVariables(reportName, dico);

        // a la fin il ne devrait plus y avoir de propriétés pour le report
        if (!dico.isEmpty()) {
            if (log.isWarnEnabled()) {
                log.warn("Il reste des propriétés non utilisées pour le report [" + reportName + "] : " + dico);
            }
        }

        return new Report(modelType,
                          reportName,
                          name,
                          description,
                          rowHeaders,
                          columnHeaders,
                          operationss,
                          variables,
                          repeatVariables,
                          requests
        );
    }

    protected Map<String, String> detectReportProperties(String reportName) {

        Map<String, String> dico = new TreeMap<>();

        // pour chaque report, on récupère ses informations
        String reportKeyPrefix = "report." + reportName + ".";
        int reportKeyPrefixLength = reportKeyPrefix.length();

        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if (!key.startsWith(reportKeyPrefix)) {
                // pas de match
                continue;
            }
            String realKey = key.substring(reportKeyPrefixLength);
            dico.put(realKey, (String) properties.get(key));
        }

        return dico;
    }

    protected ReportRequest[] getRequests(String reportName, Map<String, String> dico) {

        Map<Integer, String> requestDico = new TreeMap<>();
        Map<Integer, String> requestRepeatDico = new TreeMap<>();
        Iterator<Map.Entry<String, String>> itr = dico.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            if (!key.startsWith(REQUEST_PREFIX)) {

                continue;
            }
            String REQUEST_REPEAT_SUFFIX = ".repeat";
            if (key.endsWith(REQUEST_REPEAT_SUFFIX)) {

                // definition d'un repeat
                String request = entry.getValue();
                String requestId = key.substring(REQUEST_PREFIX.length());
                requestId = requestId.substring(0, requestId.length() - REQUEST_REPEAT_SUFFIX.length());
                Integer id = Integer.valueOf(requestId);
                if (log.isInfoEnabled()) {
                    log.info("Detects a request repeat [" + reportName + ":" + id + "] = " + request);
                }
                requestRepeatDico.put(id, request);
                itr.remove();
                continue;
            }
            String request = entry.getValue();
            String requestId = key.substring(REQUEST_PREFIX.length());
            Integer id = Integer.valueOf(requestId);
            if (log.isDebugEnabled()) {
                log.debug("Detects a request [" + reportName + ":" + id + "] = " + request);
            }
            requestDico.put(id, request);
            itr.remove();

        }

        // On trie les request
        List<Integer> ids = new ArrayList<>(requestDico.keySet());
        Collections.sort(ids);

        // on construit (dans le bon ordre la liste des requetes)
        List<ReportRequest> result = new ArrayList<>();
        for (Integer id : ids) {

            String requestDef = requestDico.get(id);
            String requestRepeatDef = requestRepeatDico.get(id);

            ReportRequest def = getRequest(requestDef, requestRepeatDef);
            if (log.isInfoEnabled()) {
                log.info("Detects a request : " + def);
            }
            result.add(def);

        }
        return result.toArray(new ReportRequest[result.size()]);
    }

    protected ReportRequest getRequest(String requestDef,
                                       String requestRepeatDef) {

        String[] parts = requestDef.split("\\|");
        if (parts.length != 3) {
            throw new IllegalArgumentException("La définition de la requete doit etre de type 'X,Y|layout|hql' mais est : " + requestDef);
        }
        String[] coords = parts[0].split(",");
        String layout = parts[1];
        String hql = parts[2];

        if (coords.length != 2) {
            throw new IllegalArgumentException("La définition des coordonées doit etre de type 'X,Y' mais est : " + parts[0]);
        }

        int x = Integer.valueOf(coords[0]);
        int y = Integer.valueOf(coords[1]);
        ReportRequest.RequestLayout realLayout = ReportRequest.RequestLayout.valueOf(layout);

        ReportRequest.RequestRepeat repeat = null;
        if (requestRepeatDef != null) {

            // il y a un repeat
            String[] repeatParts = requestRepeatDef.split("\\|");
            if (repeatParts.length != 2) {
                throw new IllegalArgumentException("La définition d'un repéteur de requete doit etre de type 'repeatName|layout' mais est : " + requestRepeatDef);
            }
            String repeatName = repeatParts[0].trim();
            String repeatLayout = repeatParts[1].trim();
            ReportRequest.RequestLayout realRepeatLayout =
                    ReportRequest.RequestLayout.valueOf(repeatLayout);
            repeat = new ReportRequest.RequestRepeat(repeatName, realRepeatLayout);

        }
        return new ReportRequest(realLayout, x, y, hql, repeat);
    }

    protected ReportOperation[] getOperations(String reportName, Map<String, String> dico) {

        Map<Integer, String> requestDico = new TreeMap<>();
        Iterator<Map.Entry<String, String>> itr = dico.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            if (!key.startsWith(OPERATION_PREFIX)) {

                continue;
            }
            String operations = entry.getValue();
            String requestId = key.substring(OPERATION_PREFIX.length());
            Integer id = Integer.valueOf(requestId);
            if (log.isDebugEnabled()) {
                log.debug("Detects a operations [" + reportName + ":" + id + "] = " + operations);
            }
            requestDico.put(id, operations);
            itr.remove();
        }

        // On trie les request
        List<Integer> ids = new ArrayList<>(requestDico.keySet());
        Collections.sort(ids);

        // on construit (dans le bon ordre la liste des requetes)
        List<ReportOperation> result = new ArrayList<>();
        for (Integer id : ids) {

            String operationsDef = requestDico.get(id);

            ReportOperation def = ReportOperation.valueOf(operationsDef);
            if (log.isInfoEnabled()) {
                log.info("Detects a operations : " + def);
            }
            result.add(def);
        }
        return result.toArray(new ReportOperation[result.size()]);
    }

    protected ReportVariable[] getVariables(String reportName, Map<String, String> dico) {
        List<ReportVariable> result = new ArrayList<>();
        Iterator<Map.Entry<String, String>> itr = dico.entrySet().iterator();
        List<String> ids = new ArrayList<>();
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            if (!key.startsWith(VARIABLE_PREFIX)) {

                continue;
            }
            String operations = entry.getValue();
            String id = key.substring(VARIABLE_PREFIX.length());

            // on interdit la surcharge d'une variable déjà trouvée pour le report
            if (ids.contains(id)) {
                throw new IllegalArgumentException("La variable " + id + " est déjà définie pour le report " + reportName);
            }

            // on interdit l'utilisation de la variable tripId
            if (ReportRequest.TRIP_ID_VARIABLE.equals(id)) {
                throw new IllegalArgumentException("La variable tripId n'est pas utilisable (c'est uen variable réservée) pour le report " + reportName);
            }
            ids.add(id);
            String[] parts = operations.split("\\|");

            if (parts.length != 2) {
                throw new IllegalArgumentException("La définition d'une variable doit etre de la forme 'type|hql' mais est : " + operations);
            }
            String typeStr = parts[0].trim();
            String request = parts[1].trim();
            Class<?> type;
            try {
                type = Class.forName(typeStr);
            } catch (ClassNotFoundException eee) {
                throw new IllegalArgumentException("Le type " + typeStr + " n'est pas connu", eee);
            }
            ReportVariable variable = new ReportVariable(id, type, request);
            if (log.isInfoEnabled()) {
                log.info("Detects a variable : [" + reportName + ":" + variable.getName() + "] = " + variable.getRequest() + " (type = " + variable.getType().getName() + ")");
            }
            result.add(variable);
            itr.remove();
        }
        return result.toArray(new ReportVariable[result.size()]);
    }

    protected ReportVariable[] getRepeatVariables(String reportName, Map<String, String> dico) {
        List<ReportVariable> result = new ArrayList<>();
        Iterator<Map.Entry<String, String>> itr = dico.entrySet().iterator();
        List<String> ids = new ArrayList<>();
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            if (!key.startsWith(REPEAT_VARIABLE_PREFIX)) {

                continue;
            }
            String operations = entry.getValue();
            String id = key.substring(REPEAT_VARIABLE_PREFIX.length());

            // on interdit la surcharge d'une variable déjà trouvée pour le report
            if (ids.contains(id)) {
                throw new IllegalArgumentException("La variable de répétition " + id + " est déjà définie pour le report " + reportName);
            }

            // on interdit l'utilisation de la variable tripId
            if (ReportRequest.TRIP_ID_VARIABLE.equals(id)) {
                throw new IllegalArgumentException("La variable de répétition tripId n'est pas utilisable (c'est uen variable réservée) pour le report " + reportName);
            }
            ids.add(id);
            String[] parts = operations.split("\\|");

            if (parts.length != 2) {
                throw new IllegalArgumentException("La définition d'une variable de répétition doit etre de la forme 'type|hql' mais est : " + operations);
            }
            String typeStr = parts[0].trim();
            String request = parts[1].trim();
            Class<?> type = null;
            try {
                type = Class.forName(typeStr);
            } catch (ClassNotFoundException eee) {
                throw new IllegalArgumentException("Le type " + type + " n'est pas connu", eee);
            }
            ReportVariable variable = new ReportVariable(id, type, request);
            if (log.isInfoEnabled()) {
                log.info("Detects a variable : [" + reportName + ":" + variable.getName() + "] = " + variable.getRequest() + " (type = " + variable.getType().getName() + ")");
            }
            result.add(variable);
            itr.remove();
        }
        return result.toArray(new ReportVariable[result.size()]);
    }

    protected String getValue(Map<String, String> dico, String key) {

        String value = dico.get(key);
        if (value != null) {
            dico.remove(key);
        }
        return value;
    }

    public void clear() {
        if (reportNames != null) {
            reportNames.clear();
            reportNames = null;
        }
        if (properties != null) {
            properties.clear();
            properties = null;
        }
    }
}
