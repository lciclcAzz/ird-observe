package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.services.service.seine.TripSeineGearUseService;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntities;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class TripSeineGearUseServiceTopiaTest extends AbstractServiceTopiaTest {

    protected TripSeineGearUseService service;

    protected ReferentialService referentialService;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(TripSeineGearUseService.class);

        referentialService = topiaTestMethodResource.newService(ReferentialService.class);

    }

    @Test
    public void loadToEditTest() {

        TripSeine tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_1);

        Form<TripSeineGearUseDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);
        TripSeineGearUseDto tripSeineGearUseDto = form.getObject();

        Assert.assertEquals(tripSeine.getTopiaId(), tripSeineGearUseDto.getId());
        Assert.assertEquals(tripSeine.sizeGearUseFeaturesSeine(), tripSeineGearUseDto.sizeGearUseFeaturesSeine());

        GearUseFeaturesSeine featuresSeine = tripSeine.getGearUseFeaturesSeineByTopiaId(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID);
        GearUseFeaturesSeineDto featuresSeineDto = tripSeineGearUseDto.getGearUseFeaturesSeine()
                                                                      .stream()
                                                                      .filter(IdHelper.newIdPredicate(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID))
                                                                      .findFirst()
                                                                      .get();

        Assert.assertEquals(featuresSeine.getTopiaId(), featuresSeineDto.getId());
        assertEntityEqualsReferenceDto(featuresSeine.getGear(), featuresSeineDto.getGear());
        Assert.assertEquals(featuresSeine.getNumber(), featuresSeineDto.getNumber());
        Assert.assertEquals(featuresSeine.getUsedInTrip(), featuresSeineDto.getUsedInTrip());
        Assert.assertEquals(featuresSeine.getComment(), featuresSeineDto.getComment());

        Assert.assertEquals(featuresSeine.sizeGearUseFeaturesMeasurement(), featuresSeineDto.sizeGearUseFeaturesMeasurement());

        for (GearUseFeaturesMeasurementSeineDto measurementSeineDto : featuresSeineDto.getGearUseFeaturesMeasurement()) {
            GearUseFeaturesMeasurementSeine measurementSeine = featuresSeine.getGearUseFeaturesMeasurement()
                                                                            .stream()
                                                                            .filter(TopiaEntities.entityHasId(measurementSeineDto.getId())::apply)
                                                                            .findFirst()
                                                                            .get();

            assertEntityEqualsReferenceDto(measurementSeine.getGearCaracteristic(), measurementSeineDto.getGearCaracteristic());
            Assert.assertEquals(measurementSeine.getMeasurementValue(), measurementSeineDto.getMeasurementValue());
        }

        //FIXME Topia test
//        Assert.assertNotNull(formDto.getLabels());
//
//        Set<Class<?>> types = ReferenceSetDtos.getTypes(formDto.getLabels());
//        Assert.assertTrue(types.contains(GearDto.class));
//        Assert.assertTrue(types.contains(GearCaracteristicDto.class));
//        Assert.assertEquals(2, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.sizeReference() > 0);
//
//        }
    }

    @Test
    @CopyDatabaseConfiguration
    public void saveUpdateTest() {
        Form<TripSeineGearUseDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        TripSeineGearUseDto tripSeineGearUseDto = form.getObject();

        GearUseFeaturesSeineDto gearUseFeaturesSeineDto = tripSeineGearUseDto.getGearUseFeaturesSeine()
                                                                             .stream()
                                                                             .filter(IdHelper.newIdPredicate(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID))
                                                                             .findFirst()
                                                                             .get();

        gearUseFeaturesSeineDto.setNumber(12);
        gearUseFeaturesSeineDto.setComment("Un Commentaire");
        for (GearUseFeaturesMeasurementSeineDto gearUseFeaturesMeasurementSeineDto : gearUseFeaturesSeineDto.getGearUseFeaturesMeasurement()) {
            if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.20".equals(gearUseFeaturesMeasurementSeineDto.getGearCaracteristic().getId())) {
                gearUseFeaturesMeasurementSeineDto.setMeasurementValue("4");
            } else if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.21".equals(gearUseFeaturesMeasurementSeineDto.getGearCaracteristic().getId())) {
                gearUseFeaturesMeasurementSeineDto.setMeasurementValue("false");
            } else if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.22".equals(gearUseFeaturesMeasurementSeineDto.getGearCaracteristic().getId())) {
                gearUseFeaturesMeasurementSeineDto.setMeasurementValue("true");
            }
        }

        service.save(tripSeineGearUseDto);

        GearUseFeaturesSeine gearUseFeaturesSeine = topiaTestMethodResource.findById(GearUseFeaturesSeine.class, ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID);

        Assert.assertEquals(gearUseFeaturesSeineDto.getId(), gearUseFeaturesSeine.getTopiaId());
        assertReferenceDtoEqualsEntity(gearUseFeaturesSeineDto.getGear(), gearUseFeaturesSeine.getGear());
        Assert.assertEquals(gearUseFeaturesSeineDto.getNumber(), gearUseFeaturesSeine.getNumber());
        Assert.assertEquals(gearUseFeaturesSeineDto.getUsedInTrip(), gearUseFeaturesSeine.getUsedInTrip());
        Assert.assertEquals(gearUseFeaturesSeineDto.getComment(), gearUseFeaturesSeine.getComment());

        Assert.assertEquals(gearUseFeaturesSeineDto.sizeGearUseFeaturesMeasurement(), gearUseFeaturesSeine.sizeGearUseFeaturesMeasurement());

        for (GearUseFeaturesMeasurementSeineDto measurementSeineDto : gearUseFeaturesSeineDto.getGearUseFeaturesMeasurement()) {
            GearUseFeaturesMeasurementSeine measurementSeine = gearUseFeaturesSeine.getGearUseFeaturesMeasurement()
                                                                                   .stream()
                                                                                   .filter(TopiaEntities.entityHasId(measurementSeineDto.getId())::apply)
                                                                                   .findFirst()
                                                                                   .get();

            assertReferenceDtoEqualsEntity(measurementSeineDto.getGearCaracteristic(), measurementSeine.getGearCaracteristic());
            Assert.assertEquals(measurementSeineDto.getMeasurementValue(), measurementSeine.getMeasurementValue());
        }

    }


}
