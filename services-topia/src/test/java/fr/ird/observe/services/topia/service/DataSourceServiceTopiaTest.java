package fr.ird.observe.services.topia.service;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.topia.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.ObserveTestResources;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.List;

/**
 * Created on 23/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 *         FIXME IT tests
 */
@Ignore
@DatabaseNameConfiguration(DatabaseName.dataSourceTest)
public class DataSourceServiceTopiaTest extends AbstractServiceTopiaTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSourceServiceTopiaTest.class);

    protected DataSourceService service;

    @Before
    public void setUp() {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();

        service = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);

    }

    @CopyDatabaseConfiguration
    @Test(expected = DatabaseNotFoundException.class)
    public void testOpenNotExistingDatabase() throws IOException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();
        service.open(dataSourceConfiguration);

    }

    @Test
    public void testCanConnect() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, InterruptedException, BabModelVersionException {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();

        try {
            service.checkCanConnect(dataSourceConfiguration);
            Assert.fail();
        } catch (DatabaseNotFoundException e) {
            Assert.assertTrue(true);
        }

        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setCanCreateEmptyDatabase(true);
        service.create(dataSourceConfiguration, dataSourceCreateConfiguration);

        // Base déjà en cours d'utilisation
        try {
            service.checkCanConnect(dataSourceConfiguration);
            Assert.fail();
        } catch (DatabaseConnexionNotAuthorizedException e) {
            Assert.assertTrue(true);
        }

        closeDatabase(dataSourceConfiguration);

        service.checkCanConnect(dataSourceConfiguration);

        char[] password = dataSourceConfiguration.getPassword();

        try {
            dataSourceConfiguration.setPassword('b');
            service.checkCanConnect(dataSourceConfiguration);
            Assert.fail();
        } catch (DatabaseConnexionNotAuthorizedException e) {
            Assert.assertTrue(true);
        }

        dataSourceConfiguration.setPassword(password);
        ObserveDataSourceConnection dataSourceConnection = service.open(dataSourceConfiguration);

        // La base n'est pas encore lockée
        ReferentialService referentialService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, ReferentialService.class);
        referentialService.getReferenceSet(GearDto.class, new Date());

        // Base déjà en cours d'utilisation
        try {
            service.checkCanConnect(dataSourceConfiguration);
            Assert.fail();
        } catch (DatabaseConnexionNotAuthorizedException e) {
            Assert.assertTrue(true);
        }

        closeDatabase(dataSourceConfiguration);

        service.checkCanConnect(dataSourceConfiguration);

    }

    @Test
    public void testOpen() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, InterruptedException, BabModelVersionException {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();
        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setCanCreateEmptyDatabase(true);
        service.create(dataSourceConfiguration, dataSourceCreateConfiguration);

        // Une fois la base crée, on est déjà connecté dessus, la base est lockée
        // On la ferme
        closeDatabase(dataSourceConfiguration);


        service.open(dataSourceConfiguration);
        assertSchemaCreated(topiaTestMethodResource.getTopiaApplicationContext());

    }

    @CopyDatabaseConfiguration
    @Test
    public void testCreateEmptyDataSource() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();
        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setCanCreateEmptyDatabase(true);

        service.create(dataSourceConfiguration, dataSourceCreateConfiguration);
        assertSchemaCreated(topiaTestMethodResource.getTopiaApplicationContext());

    }

    @CopyDatabaseConfiguration
    @Test
    public void testCreateDataSourceFromImportDatabase() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        String scriptPath = TOPIA_TEST_CLASS_RESOURCE.getScriptPath("referentiel", ObserveTestConfiguration.getFirstModelVersion());

        byte[] dumpContent = ObserveTestResources.getResourceContent(scriptPath);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();
        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setImportDatabase(dumpContent);

        service.create(dataSourceConfiguration, dataSourceCreateConfiguration);
        assertSchemaCreated(topiaTestMethodResource.getTopiaApplicationContext());

    }

    @CopyDatabaseConfiguration
    @Test
    public void testCreateDataSourceFromImportReferential() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        // On crée un base qui fait office de source de référentiel
        String scriptPath = TOPIA_TEST_CLASS_RESOURCE.getScriptPath("referentiel", ObserveTestConfiguration.getFirstModelVersion());
        createDataSourceFromScript(scriptPath);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();

        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setImportReferentialDataSourceConfiguration(dataSourceConfiguration);

        ObserveDataSourceConfigurationTopiaH2 createdDatabaseDataSourceConfiguration = topiaTestMethodResource.createDataSourceConfigurationH2(getClass(), "resultDatabase");

        DataSourceService dataSourceService = TOPIA_TEST_CLASS_RESOURCE.newService(createdDatabaseDataSourceConfiguration, DataSourceService.class);

        dataSourceService.create(createdDatabaseDataSourceConfiguration, dataSourceCreateConfiguration);

        ObserveTopiaApplicationContext createdDatabaseTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(createdDatabaseDataSourceConfiguration);
        assertSchemaCreated(createdDatabaseTopiaApplicationContext);

    }

    @CopyDatabaseConfiguration
    @Test
    public void testCreateDataSourceFromImportReferentialAndData() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        // On crée un base qui fait office de source de référentiel et de source de données (uniquement des données seine)
        String scriptPath = TOPIA_TEST_CLASS_RESOURCE.getScriptPath("dataForTestSeine", ObserveTestConfiguration.getFirstModelVersion());
        createDataSourceFromScript(scriptPath);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();

        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setImportReferentialDataSourceConfiguration(dataSourceConfiguration);
        dataSourceCreateConfiguration.setImportDataConfiguration(dataSourceConfiguration, ImmutableSet.of(ObserveFixtures.TRIP_SEINE_ID_1));

        ObserveDataSourceConfigurationTopiaH2 createdDatabaseDataSourceConfiguration = topiaTestMethodResource.createDataSourceConfigurationH2(getClass(), "resultDatabase");

        DataSourceService dataSourceService = TOPIA_TEST_CLASS_RESOURCE.newService(createdDatabaseDataSourceConfiguration, DataSourceService.class);

        dataSourceService.create(createdDatabaseDataSourceConfiguration, dataSourceCreateConfiguration);

        ObserveTopiaApplicationContext createdDatabaseTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(createdDatabaseDataSourceConfiguration);

        assertSchemaCreated(createdDatabaseTopiaApplicationContext);
        assertEntitiesExist(createdDatabaseTopiaApplicationContext, ObserveFixtures.TRIP_SEINE_ID_1);
    }

    protected void closeDatabase(ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration) throws InterruptedException {

        service.close();

        File lockFile = dataSourceConfiguration.getLockFile();
        while (lockFile.exists()) {
            if (log.isInfoEnabled()) {
                log.info("Database lock " + lockFile + " still exist... Wait ");
            }
            Thread.sleep(100);
        }
    }

    public void createDataSourceFromScript(String scriptPath) throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        DataSourceService dataSourceService = topiaTestMethodResource.newService(DataSourceService.class);

        DataSourceCreateConfigurationDto createConfiguration = new DataSourceCreateConfigurationDto();
        byte[] databaseToImportContent = ObserveTestResources.getResourceContent(scriptPath);
        createConfiguration.setImportDatabase(databaseToImportContent);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.getDataSourceConfiguration();
        dataSourceService.create(dataSourceConfiguration, createConfiguration);

    }

    public void assertSchemaCreated(TopiaApplicationContext topiaApplicationContext) {

        try (TopiaPersistenceContext topiaPersistenceContext = topiaApplicationContext.newPersistenceContext()) {
            assertSchemaCreated(topiaApplicationContext, topiaPersistenceContext, false);
        }

    }

    public void assertSchemaCreated(TopiaApplicationContext topiaApplicationContext, TopiaPersistenceContext persistenceContext, boolean noData) {

        // on verifie que le schema a bien ete cree
        for (Class<TopiaEntity> c : topiaApplicationContext.getContractClasses()) {
            if (Modifier.isAbstract(topiaApplicationContext.getImplementationClass(c).getModifiers())) {
                continue;
            }
            if (log.isDebugEnabled()) {
                log.debug("check table exists for entity: " + c.getName());
            }
            List<?> r = persistenceContext.getDao(c).findAll();
            Assert.assertNotNull(r);
            if (noData) {
                Assert.assertEquals(0, r.size());
            }
        }
    }

    public void assertEntitiesExist(TopiaApplicationContext topiaApplicationContext, String... ids) {

        try (TopiaPersistenceContext persistenceContext = topiaApplicationContext.newPersistenceContext()) {

            for (String id : ids) {
                try {
                    persistenceContext.findByTopiaId(id);
                    if (log.isInfoEnabled()) {
                        log.info("Entity " + id + " found.");
                    }
                } catch (TopiaException e) {
                    Assert.fail("Could not find entity with id: " + id);
                }
            }
        }
    }


}
