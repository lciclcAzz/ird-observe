/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;


import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Test du report {@code accessoryCatch}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public class ReportAccessoryCatchTest extends AbstractReportServiceTopiaTest {

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Liste des captures accessoires selon le type de banc, filtrées par groupe",
                "Afficher les captures accessoires par groupe d'espèce\n" +
                "Les poids sont exprimés en tonnes."
        );

        assertReportDimension(
                report,
                -1,
                3,
                new String[]{"Espèce",
                             "Banc libre",
                             "Banc objet",
                }
        );

        assertReportNbRequests(report, 3);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.row,
                0,
                0
        );
        assertReportRequestDimension(
                requests[1],
                ReportRequest.RequestLayout.row,
                1,
                0
        );
        assertReportRequestDimension(
                requests[2],
                ReportRequest.RequestLayout.row,
                2,
                0
        );
    }

    @Override
    protected String getReportId() {
        return "accessoryCatch";
    }

    @Override
    protected void prepareVariables() {
        setVariableValue("speciesGroup", "fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075");
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 3, 54, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "[FAO]MAE [sc]Aetobatus narinari [fr]Aigle de mer léopard", "-", "-");
        assertResultRow(result, row++, "[FAO]PTH [sc]Alopias pelagicus [fr]Renard pélagique", "-", "-");
        assertResultRow(result, row++, "[FAO]BTH [sc]Alopias superciliosus [fr]Renard à gros yeux", "-", "-");
        assertResultRow(result, row++, "[FAO]ALV [sc]Alopias vulpinus [fr]Renard", "-", "-");
        assertResultRow(result, row++, "[FAO]WSH [sc]Carcharodon carcharias [fr]Grand requin blanc", "-", "-");
        assertResultRow(result, row++, "[FAO]CCE [sc]Carcharhinus leucas [fr]Requin-bouledogue", "-", "-");
        assertResultRow(result, row++, "[FAO]FAL [sc]Carcharhinus falciformis [fr]Requin soyeux", "-", "+ Effectif : 17 Poids total (t) : 0.1753");
        assertResultRow(result, row++, "[FAO]OCS [sc]Carcharhinus longimanus [fr]Requin océanique", "+ Effectif : 1 Poids total (t) : 0.0447", "-");
        assertResultRow(result, row++, "[FAO]DUS [sc]Carcharhinus obscurus [fr]Requin sombre", "-", "-");
        assertResultRow(result, row++, "[FAO]PLS [sc]Dasyatys (Pteroplatytrygon) violacea [fr]Pastenague violette", "-", "-");
        assertResultRow(result, row++, "[FAO]SHL [sc]Etmopterus spp [fr]Genre Etmopterus", "-", "-");
        assertResultRow(result, row++, "[FAO]THR [sc]Alopias spp [fr]Renards de mer nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]RSK [sc]Carcharhinidae spp [fr]Famille Carcharhinidae", "-", "+ Effectif : 11 Poids total (t) : 0.55");
        assertResultRow(result, row++, "[FAO]STT [sc]Dasyatidae [fr]Famille Dasyatidae", "+ Effectif : 7 Poids total (t) : 0.015", "-");
        assertResultRow(result, row++, "[FAO]MSK [sc]Lamnidae [fr]Famille Lamnidae", "-", "-");
        assertResultRow(result, row++, "[FAO]2FOD [sc]Odontaspididae [fr]Odontaspididae", "-", "-");
        assertResultRow(result, row++, "[FAO]- [sc]Rhincodontidae [fr]Famille Rhincodontidae", "-", "-");
        assertResultRow(result, row++, "[FAO]SPY [sc]Sphyrnidae [fr]Famille Sphyrnidae", "-", "+ Effectif : 1");
        assertResultRow(result, row++, "[FAO]TIG [sc]Galeocerdo cuvier [fr]Requin tigre commun", "-", "-");
        assertResultRow(result, row++, "[FAO]ISB [sc]Isistius brasiliensis [fr]Squalelet féroce", "-", "-");
        assertResultRow(result, row++, "[FAO]SMA [sc]Isurus oxyrinchus [fr]Taupe bleu", "-", "-");
        assertResultRow(result, row++, "[FAO]MAK [sc]Isurus spp [fr]Taupes", "-", "-");
        assertResultRow(result, row++, "[FAO]MAN [sc]Mobulidae  [fr]Mantes, diables de mer nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]MYL [sc]Myliobatis aquila [fr]Aigle commun", "-", "-");
        assertResultRow(result, row++, "[FAO]RMB [sc]Manta birostris [fr]Mante atlantique", "+ Effectif : 9 Poids total (t) : 1.1824", "-");
        assertResultRow(result, row++, "[FAO]RMT [sc]Mobula tarapacana [fr]Diable géant de Guinée", "-", "-");
        assertResultRow(result, row++, "[FAO]MNT [sc]Manta spp [fr]Raies manta", "-", "-");
        assertResultRow(result, row++, "[FAO]RMM [sc]Mobula mobular [fr]Mante mobula", "-", "-");
        assertResultRow(result, row++, "[FAO]LMP [sc]Megachasma pelagios [fr]Requin grande gueule", "-", "-");
        assertResultRow(result, row++, "[FAO]RMJ [sc]Mobula japanica [fr]Mante aiguillat", "-", "-");
        assertResultRow(result, row++, "[FAO]CVX [sc]Carcharhiniformes [fr]Ordre Carcharhiniformes", "-", "-");
        assertResultRow(result, row++, "[FAO]HDQ [sc]Heterodontiformes [fr]Ordre Heterodontiformes", "-", "-");
        assertResultRow(result, row++, "[FAO]HXW [sc]Hexanchiformes [fr]Ordre Hexanchiformes", "-", "-");
        assertResultRow(result, row++, "[FAO]LMZ [sc]Lamniformes [fr]Ordre Lamniformes", "-", "-");
        assertResultRow(result, row++, "[FAO]OCX [sc]Orectolobiformes [fr]Ordre Orectolobiformes", "-", "-");
        assertResultRow(result, row++, "[FAO]PWS [sc]Pristiophorus spp [fr]Requins-scies nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]SHX [sc]Squaliformes [fr]Ordre Squaliformes", "-", "-");
        assertResultRow(result, row++, "[FAO]ASK [sc]Squatinidae [fr]Anges de mer nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]BSH [sc]Prionace glauca [fr]Peau bleue", "-", "-");
        assertResultRow(result, row++, "[FAO]POR [sc]Lamna nasus [fr]Requin taupe commun", "-", "-");
        assertResultRow(result, row++, "[FAO]SRX [sc]Raie non identifiée [fr]Raie non identifiée", "-", "-");
        assertResultRow(result, row++, "[FAO]2REX [sc]Requin non identifié [fr]Requin non identifié", "-", "-");
        assertResultRow(result, row++, "[FAO]- [sc]Rhinopteridae [fr]Famille Rhinopteridae", "-", "-");
        assertResultRow(result, row++, "[FAO]NZX [sc]Rhinoptera spp [fr]Mourines", "-", "-");
        assertResultRow(result, row++, "[FAO]RMA [sc]Manta alfredi [fr]Manta Alfredi", "-", "-");
        assertResultRow(result, row++, "[FAO]RMC [sc]Mobula coilloti  [fr]Mobula coilloti ", "-", "-");
        assertResultRow(result, row++, "[FAO]RMO [sc]Mobula thurstoni [fr]Mobula", "-", "-");
        assertResultRow(result, row++, "[FAO]RMV [sc]Mobula spp [fr]Mobula nca", "-", "-");
        assertResultRow(result, row++, "[FAO]BSK [sc]Cetorhinus maximus [fr]Requin pèlerin", "-", "-");
        assertResultRow(result, row++, "[FAO]RHN [sc]Rhincodon typus [fr]Requin baleine", "-", "-");
        assertResultRow(result, row++, "[FAO]SPL [sc]Sphyrna lewini [fr]Requin marteau halicorne", "-", "-");
        assertResultRow(result, row++, "[FAO]SPK [sc]Sphyrna mokarran [fr]Grand requin marteau", "-", "-");
        assertResultRow(result, row++, "[FAO]SPZ [sc]Sphyrna zygaena [fr]Requin marteau commun", "-", "-");
        assertResultRow(result, row, "[FAO]TOD [sc]Torpedinidae [fr]Torpilles, raies électriq. nca ", "-", "-");


    }
}
