/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;


import fr.ird.observe.services.dto.actions.report.DataMatrix;

/**
 * Test du report {@code discardedAccessoire}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public class ReportAccessoryCatch2Test extends ReportAccessoryCatchTest {

    @Override
    protected void prepareVariables() {
        setVariableValue("speciesGroup", "fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.12092280503502995");
    }


    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 3, 151, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "[FAO]LUK [sc]Selene dorsalis [fr]Selene dorsalis", "-", "-");
        assertResultRow(result, row++, "[FAO]ABU [sc]Abudefduf saxatilis  [fr]Sergent-major ", "-", "-");
        assertResultRow(result, row++, "[FAO]ALI [sc]Alepisaurus spp [fr]Lanciers nca", "-", "-");
        assertResultRow(result, row++, "[FAO]ALS [sc]Carcharhinus albimarginatus [fr]Requin pointe blanche", "-", "-");
        assertResultRow(result, row++, "[FAO]ALX [sc]Alepisaurus ferox [fr]Lancier longnez", "-", "-");
        assertResultRow(result, row++, "[FAO]AMB [sc]Seriola dumerili [fr]Purlish amberjack", "-", "-");
        assertResultRow(result, row++, "[FAO]AML [sc]Carcharhinus amblyrhynchos [fr]Grey reef shark", "-", "-");
        assertResultRow(result, row++, "[FAO]DDD [sc]Abudefduf vaigiensis [fr]Sergent-major", "-", "-");
        assertResultRow(result, row++, "[FAO]AVR [sc]Aprion virescens [fr]Job gris", "-", "-");
        assertResultRow(result, row++, "[FAO]- [sc]Balistes capriscus [fr]Baliste (Atlantique)", "-", "-");
        assertResultRow(result, row++, "[FAO]AJS [sc]Abalistes stellatus [fr]Baliste étoilé", "-", "+ Effectif : 20 Poids total (t) : 0.01");
        assertResultRow(result, row++, "[FAO]TRG [sc]Balistes carolinensis [fr]Baliste cabri ", "-", "-");
        assertResultRow(result, row++, "[FAO]BVP [sc]Balistes punctatus [fr]Baliste à taches bleues", "-", "-");
        assertResultRow(result, row++, "[FAO]BAR [sc]Sphyraena spp [fr]Barracudas nca", "-", "-");
        assertResultRow(result, row++, "[FAO]ALN [sc]Aluterus scriptus [fr]Bourse écriture", "+ Effectif : 1 Poids total (t) : 0.00050", "-");
        assertResultRow(result, row++, "[FAO]ALM [sc]Aluterus monoceros [fr]Bourse loulou", "-", "+ Effectif : 16 Poids total (t) : 0.01060");
        assertResultRow(result, row++, "[FAO]CNT [sc]Canthidermis maculata [fr]Baliste rude", "+ Effectif : 2 Poids total (t) : 0.00134", "+ Effectif : 2230 Poids total (t) : 1.49359");
        assertResultRow(result, row++, "[FAO]BAF [sc]Ablennes hians [fr]Orphie plate ", "-", "-");
        assertResultRow(result, row++, "[FAO]BLR [sc]Carcharhinus melanopterus [fr]Requin pointes noires", "-", "-");
        assertResultRow(result, row++, "[FAO]BON [sc]Sarda sarda [fr]Bonite à dos rayé", "-", "+ Effectif : 11");
        assertResultRow(result, row++, "[FAO]BPQ [sc]Brama japonica [fr]Castagnole du Pacific", "-", "-");
        assertResultRow(result, row++, "[FAO]BRA [sc]Brama spp. [fr]", "-", "-");
        assertResultRow(result, row++, "[FAO]BRZ [sc]Bramidae [fr]Famille Bramidae", "-", "-");
        assertResultRow(result, row++, "[FAO]BRO [sc]Carcharhinus brachyurus [fr]Requin cuivre", "-", "-");
        assertResultRow(result, row++, "[FAO]CAQ [sc]Catharacta lonnbergi [fr]Labbe brun", "-", "-");
        assertResultRow(result, row++, "[FAO]CBG [sc]Cubiceps gracilis [fr]Libine", "-", "-");
        assertResultRow(result, row++, "[FAO]DVH [sc]Cyclichthys orbicularis [fr]Cyclichthys orbicularis", "-", "-");
        assertResultRow(result, row++, "[FAO]CCL [sc]Carcharhinus limbatus [fr]Requin bordé", "-", "-");
        assertResultRow(result, row++, "[FAO]CCP [sc]Carcharhinus plumbeus [fr]Requin gris", "-", "-");
        assertResultRow(result, row++, "[FAO]MSD [sc]Decapterus macarellus [fr]Comète maquereau ", "+ Effectif : 47 Poids total (t) : 0.0024", "+ Effectif : 6 Poids total (t) : 0.0027");
        assertResultRow(result, row++, "[FAO]CLP [sc]Clupeidae  [fr]Famille Clupeidae", "-", "-");
        assertResultRow(result, row++, "[FAO]NXU [sc]Caranx lugubris [fr]Carangue noire", "-", "-");
        assertResultRow(result, row++, "[FAO]CFW [sc]Coryphaena equiselis [fr]Coryphène dauphin", "+ Effectif : 2 Poids total (t) : 0.008", "-");
        assertResultRow(result, row++, "[FAO]DOL [sc]Coryphaena hippurus [fr]Coryphène commun", "+ Effectif : 5 Poids total (t) : 0.03824", "+ Effectif : 107 Poids total (t) : 0.57632");
        assertResultRow(result, row++, "[FAO]NGT [sc]Carangoides orthogrammus [fr]Carangue des îles ", "-", "-");
        assertResultRow(result, row++, "[FAO]CXS [sc]Caranx sexfasciatus [fr]Carangue vorace ", "-", "+ Effectif : 22 Poids total (t) : 0.01100");
        assertResultRow(result, row++, "[FAO]CRU [sc]Crustacea [fr]Crustacés nca", "-", "-");
        assertResultRow(result, row++, "[FAO]RUB [sc]Caranx crysos [fr]Carangue coubali", "-", "-");
        assertResultRow(result, row++, "[FAO]3CUH [sc]Uraspis helvola [fr]Carangue coton helvola", "-", "-");
        assertResultRow(result, row++, "[FAO]CUP [sc]Cubiceps spp [fr]Cubiceps nca", "-", "-");
        assertResultRow(result, row++, "[FAO]USE [sc]Uraspis secunda [fr]Carangue coton", "-", "-");
        assertResultRow(result, row++, "[FAO]3CUX [sc]Uraspis spp [fr]Carangue coton", "-", "-");
        assertResultRow(result, row++, "[FAO]CWZ [sc]Carcharhinus spp [fr]Requins Carcharhinus nca", "-", "-");
        assertResultRow(result, row++, "[FAO]CWZ [sc]Carcharhinus spp [fr]Requins Carcharhinus nca", "-", "-");
        assertResultRow(result, row++, "[FAO]DIY [sc]Diodon hystrix [fr]Porc-épic boubou ", "-", "-");
        assertResultRow(result, row++, "[FAO]DIO [sc]Diodontidae [fr]Famille Diodontidae", "+ Effectif : 1 Poids total (t) : 0.00050", "-");
        assertResultRow(result, row++, "[FAO]3DEY [sc]Diodon eydouxii [fr]Porc-épic", "-", "-");
        assertResultRow(result, row++, "[FAO]EAG [sc]Myliobatidae [fr]Aigles de mer nca", "-", "-");
        assertResultRow(result, row++, "[FAO]EHN [sc]Echeneis naucrates  [fr]Rémora commun ", "+ Effectif : 7 Poids total (t) : 0.00350", "-");
        assertResultRow(result, row++, "[FAO]RRU [sc]Elagatis bipinnulata [fr]Commère saumon", "-", "+ Effectif : 211 Poids total (t) : 0.45108");
        assertResultRow(result, row++, "[FAO]HTL [sc]Phtheirichthys lineatus  [fr]Rémora ", "-", "-");
        assertResultRow(result, row++, "[FAO]TRI [sc]Balistidae [fr]Famille Balistidae", "-", "-");
        assertResultRow(result, row++, "[FAO]BEN [sc]Belonidae [fr]Famille Belonidae", "-", "-");
        assertResultRow(result, row++, "[FAO]FBT [sc]Natator depressus [fr]Tortue plate", "-", "-");
        assertResultRow(result, row++, "[FAO]DOX [sc]Coryphaenidae [fr]Famille Coryphaenidae", "-", "-");
        assertResultRow(result, row++, "[FAO]CGX [sc]Carangidae [fr]Famille Carangidae", "+ Effectif : 1 Poids total (t) : 0.00050", "+ Effectif : 49 Poids total (t) : 0.02450");
        assertResultRow(result, row++, "[FAO]ECN [sc]Echeneidae [fr]Famille Echeneidae", "+ Effectif : 2 Poids total (t) : 0.00100", "-");
        assertResultRow(result, row++, "[FAO]SPA [sc]Ephippidae [fr]Famille Ephippidae", "-", "-");
        assertResultRow(result, row++, "[FAO]EXQ [sc]Euleptorhamphus velox [fr]Demi-bec volant ", "-", "-");
        assertResultRow(result, row++, "[FAO]FLY [sc]Exocoetidae [fr]Famille Exocoetidae", "-", "-");
        assertResultRow(result, row++, "[FAO]FIT [sc]Fistularia spp [fr]Cornettes", "-", "-");
        assertResultRow(result, row++, "[FAO]FFX [sc]Monacanthidae [fr]Poisson-lime", "-", "-");
        assertResultRow(result, row++, "[FAO]KYP [sc]Kyphosus spp [fr]Kyphosus calicagères nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]3MOP [sc]Molidae [fr]Famille Molidae", "-", "-");
        assertResultRow(result, row++, "[FAO]DSF [sc]Pomacentridae [fr]Famille Pomacentridae", "-", "-");
        assertResultRow(result, row++, "[FAO]MAX [sc]Scombridae [fr]Famille Scombridae", "-", "-");
        assertResultRow(result, row++, "[FAO]BSX [sc]Serranidae [fr]Famille Serranidae", "-", "-");
        assertResultRow(result, row++, "[FAO]PUX [sc]Tetraodontidae [fr]Famille Tetraodontidae", "-", "-");
        assertResultRow(result, row++, "[FAO]GEP [sc]Gempylidae [fr]Escoliers, rouvets nca", "-", "-");
        assertResultRow(result, row++, "[FAO]GES [sc]Gempylus serpens [fr]Escolier serpent", "-", "-");
        assertResultRow(result, row++, "[FAO]JDP [sc]Dasyatis pastinaca [fr]Pastenague commune", "-", "-");
        assertResultRow(result, row++, "[FAO]KYC [sc]Kyphosus cinerascens [fr]Calicagère bleue ", "-", "-");
        assertResultRow(result, row++, "[FAO]KYS [sc]Kyphosus sectatrix [fr]Calicagère blanche ", "-", "-");
        assertResultRow(result, row++, "[FAO]KYV [sc]Kyphosus vaigiensis [fr]Caligagère", "-", "+ Effectif : 5 Poids total (t) : 0.0025");
        assertResultRow(result, row++, "[FAO]LAG [sc]Lampris guttatus [fr]Opah", "-", "-");
        assertResultRow(result, row++, "[FAO]LAM [sc]Lampris spp [fr]Saumon des dieux", "-", "-");
        assertResultRow(result, row++, "[FAO]LAP [sc]Lampris spp [fr]Opah", "-", "-");
        assertResultRow(result, row++, "[FAO]3LDI [sc]Lactoria diaphana [fr]Lactoria diaphana", "-", "-");
        assertResultRow(result, row++, "[FAO]LEC [sc]Lepidocybium flavobrunneum [fr]Escolier noir", "-", "-");
        assertResultRow(result, row++, "[FAO]LGH [sc]Lagocephalus lagocephalus [fr]Compère océanique ", "-", "-");
        assertResultRow(result, row++, "[FAO]LMA [sc]Isurus paucus [fr]Petite taupe, Mako", "-", "-");
        assertResultRow(result, row++, "[FAO]LOB [sc]Lobotes surinamensis [fr]Croupia roche ", "-", "+ Effectif : 28 Poids total (t) : 0.06754");
        assertResultRow(result, row++, "[FAO]LOP [sc]Lophotus capellei [fr]Roi des harengs", "-", "-");
        assertResultRow(result, row++, "[FAO]LVM [sc]Luvarus imperialis [fr]Luvar ", "-", "-");
        assertResultRow(result, row++, "[FAO]MRW [sc]Masturus lanceolatus [fr]Poisson-lune à queue pointue", "-", "-");
        assertResultRow(result, row++, "[FAO]MAW [sc]Scomberomorus tritor [fr]Thazard blanc", "-", "-");
        assertResultRow(result, row++, "[FAO]MAZ [sc]Scomber spp [fr]Maquereaux scomber nca", "-", "-");
        assertResultRow(result, row++, "[FAO]MOX [sc]Mola mola [fr]Poisson lune", "-", "-");
        assertResultRow(result, row++, "[FAO]MOP [sc]Mola spp [fr]Poissons lunes nca", "-", "-");
        assertResultRow(result, row++, "[FAO]MZZ [sc]Osteichthyes [fr]Poissons marins nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]NAU [sc]Naucrates ductor [fr]Poisson pilote", "+ Effectif : 1 Poids total (t) : 0.00050", "-");
        assertResultRow(result, row++, "[FAO]NEN [sc]Nesiarchus nasutus [fr]Escolier long nez", "-", "-");
        assertResultRow(result, row++, "[FAO]NXI [sc]Caranx ignobilis [fr]Carangue têtue", "-", "-");
        assertResultRow(result, row++, "[FAO]OMZ [sc]Ommastrephidae [fr]Encornets Ommastrephidae nca", "-", "-");
        assertResultRow(result, row++, "[FAO]OOT [sc]Oreosoma atlanticum  [fr]Oreosoma atlanticum ", "-", "-");
        assertResultRow(result, row++, "[FAO]OSF [sc]Stegostoma fasciatum [fr]Requin zèbre", "-", "-");
        assertResultRow(result, row++, "[FAO]PFM [sc]Pristipomoides filamentosus [fr]Colas fil", "-", "-");
        assertResultRow(result, row++, "[FAO]BAT [sc]Platax spp [fr]Poules d'eau ", "-", "-");
        assertResultRow(result, row++, "[FAO]BAO [sc]Platax teira [fr]Poule d'eau", "-", "-");
        assertResultRow(result, row++, "[FAO]POA [sc]Brama brama [fr]Grande castagnole", "-", "-");
        assertResultRow(result, row++, "[FAO]PRP [sc]Promethichthys prometheus [fr]Escolier clair", "-", "-");
        assertResultRow(result, row++, "[FAO]PSC [sc]Psenes cyanophrys [fr]Psenes cyanophrys", "-", "-");
        assertResultRow(result, row++, "[FAO]PSK [sc]Pseudocarcharias kamoharai [fr]Requin crocodile", "-", "-");
        assertResultRow(result, row++, "[FAO]RAJ [sc]Rajidae [fr]Rajidés nca", "-", "-");
        assertResultRow(result, row++, "[FAO]RZV [sc]Ranzania laevis [fr]Ranzania", "-", "-");
        assertResultRow(result, row++, "[FAO]3RAU [sc]Remora australis [fr]Rémora", "-", "-");
        assertResultRow(result, row++, "[FAO]RDB [sc]Dasyatis benneti [fr]Bennett s stingray", "-", "-");
        assertResultRow(result, row++, "[FAO]RDX [sc]Dasyatis ushiei [fr]Cow stingray", "-", "-");
        assertResultRow(result, row++, "[FAO]RDZ [sc]Dasyatis zugei [fr]Pale-edged stingray", "-", "-");
        assertResultRow(result, row++, "[FAO]RRL [sc]Remorina albescens [fr]Rémora blanche", "-", "-");
        assertResultRow(result, row++, "[FAO]REO [sc]Remora remora [fr]Rémora", "+ Effectif : 2 Poids total (t) : 0.001", "-");
        assertResultRow(result, row++, "[FAO]REZ [sc]Remora osteochir [fr]Rémora des marlins ", "-", "-");
        assertResultRow(result, row++, "[FAO]REY [sc]Remora brachyptera [fr]Rémora des espadons ", "-", "-");
        assertResultRow(result, row++, "[FAO]RMH [sc]Mobula hypostoma [fr]Mante diable", "-", "-");
        assertResultRow(result, row++, "[FAO]OIL [sc]Ruvettus pretiosus [fr]Rouvet", "-", "-");
        assertResultRow(result, row++, "[FAO]RXX [sc]Rexea spp. [fr]Rexea spp.", "-", "-");
        assertResultRow(result, row++, "[FAO]SBF [sc]Thunnus maccoyii [fr]Thon rouge du Sud", "-", "-");
        assertResultRow(result, row++, "[FAO]SDX [sc]Decapterus spp [fr]Comètes nca ", "-", "-");
        assertResultRow(result, row++, "[FAO]YTL [sc]Seriola rivoliana [fr]Sériole limon ", "-", "-");
        assertResultRow(result, row++, "[FAO]MAS [sc]Scomber japonicus [fr]Maquereau espagnol", "-", "-");
        assertResultRow(result, row++, "[FAO]SKH [sc]Selachimorpha [fr]Requins divers nca", "-", "-");
        assertResultRow(result, row++, "[FAO]SKX [sc]Elasmobranchii [fr]Requins, raies, etc. nca", "-", "-");
        assertResultRow(result, row++, "[FAO]SLP [sc]Hydrurga leptonyx [fr]Léopard de mer", "-", "-");
        assertResultRow(result, row++, "[FAO]SNK [sc]Thyrsites atun [fr]Escolier", "-", "-");
        assertResultRow(result, row++, "[FAO]GBA [sc]Sphyraena barracuda [fr]Barracuda", "-", "+ Effectif : 26 Poids total (t) : 0.11222");
        assertResultRow(result, row++, "[FAO]BAZ [sc]Sphyraenidae [fr]Famille Sphyraenidae", "-", "-");
        assertResultRow(result, row++, "[FAO]SPN [sc]Sphyrna spp [fr]Requins marteau nca", "-", "-");
        assertResultRow(result, row++, "[FAO]SQC [sc]Loligo spp [fr]Calmars Loligo nca", "-", "-");
        assertResultRow(result, row++, "[FAO]MAC [sc]Scomber scombrus [fr]Maquereau commun", "-", "-");
        assertResultRow(result, row++, "[FAO]SXH [sc]Scombrolabrax heterolepis [fr]Escolier aile longue", "-", "-");
        assertResultRow(result, row++, "[FAO]TAL [sc]Taractichthys longipinnis [fr]Big-scale pomfret", "-", "-");
        assertResultRow(result, row++, "[FAO]BTS [sc]Tylosurus crocodilus [fr]Aiguille crocodile ", "-", "+ Effectif : 1 Poids total (t) : 0.00050");
        assertResultRow(result, row++, "[FAO]TCR [sc]Taractes rubescens [fr]Keeltail pomfret", "-", "-");
        assertResultRow(result, row++, "[FAO]THM [sc]Thyrsitoides merleyi [fr]Escolier gracile", "-", "-");
        assertResultRow(result, row++, "[FAO]TJZ [sc]Trachipterus jacksonensis [fr]", "-", "-");
        assertResultRow(result, row++, "[FAO]TRE [sc]Caranx spp [fr]Chinchards, carangues nca", "-", "-");
        assertResultRow(result, row++, "[FAO]TRQ [sc]Trachipterus trachipterus [fr]Mediterranean dealfish", "-", "-");
        assertResultRow(result, row++, "[FAO]TRW [sc]Trachipterus ishikawae [fr]", "-", "-");
        assertResultRow(result, row++, "[FAO]TST [sc]Taractichtys steindachneri [fr]Castagnole fauchoire", "-", "-");
        assertResultRow(result, row++, "[FAO]TUS [sc]Thunnus spp [fr]Thons Thunnus nca", "-", "-");
        assertResultRow(result, row++, "[FAO]UBP [sc]Cubiceps capensis [fr]Dérivant", "-", "-");
        assertResultRow(result, row++, "[FAO]URU [sc]Uraspis uraspis [fr]Carangue paia ", "-", "-");
        assertResultRow(result, row++, "[FAO]VLF* [sc]Very long fish nei [fr]Très longs poissons nca", "-", "-");
        assertResultRow(result, row++, "[FAO]VVL* [sc]Very very long fish nei [fr]Très très longs poissons nca", "-", "-");
        assertResultRow(result, row++, "[FAO]WAH [sc]Acanthocybium solandri [fr]Thazard bâtard", "-", "+ Effectif : 90 Poids total (t) : 0.6800");
        assertResultRow(result, row++, "[FAO]WHH [sc]Tetrapturus spp [fr]Makaires Tetrapturus nca", "-", "-");
        assertResultRow(result, row++, "[FAO]9XXX [sc]Indéterminé [fr]Indéterminé", "-", "-");
        assertResultRow(result, row++, "[FAO]XXX* [sc] [fr]", "-", "-");
        assertResultRow(result, row++, "[FAO]YMO [sc]Sthenoteuthis oualaniensis [fr]Encornet bande violette", "-", "-");
        assertResultRow(result, row++, "[FAO]YTC [sc]Seriola lalandi [fr]Sériole chicard", "-", "-");
        assertResultRow(result, row, "[FAO]ZAO [sc]Zanclus cornutus [fr]Zanclus cornutus", "-", "-");
    }

}
