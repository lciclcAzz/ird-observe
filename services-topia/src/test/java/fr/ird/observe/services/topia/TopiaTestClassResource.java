package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.ObserveTestResources;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.spi.DatabaseClassifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.Description;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Locale;

/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TopiaTestClassResource extends TestClassResourceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaTestClassResource.class);

    private final ObserveServiceFactoryTopia serviceFactory;

    private final ObserveSpeciesListConfiguration speciesListConfiguration;

    private final DataSourcesForTestManager dataSourcesForTestManager;

    public TopiaTestClassResource() {
        this(DatabaseClassifier.DEFAULT);
    }

    public TopiaTestClassResource(DatabaseClassifier classifier) {
        super(classifier);
        this.speciesListConfiguration = ObserveSpeciesListConfiguration.newDefaultConfiguration();
        this.serviceFactory = new ObserveServiceFactoryTopia() {

            @Override
            protected ObserveServiceContextTopia createServiceContext(ObserveServiceInitializer observeServiceInitializer) {
                return new ObserveServiceContextTopiaTaiste(observeServiceInitializer, mainServiceFactory, this);
            }
        };
        serviceFactory.setMainServiceFactory(serviceFactory);

        dataSourcesForTestManager = new DataSourcesForTestManager();
    }

    public DataSourcesForTestManager getDataSourcesForTestManager() {
        return dataSourcesForTestManager;
    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfiguration(Version dbVersion, String dbName, File targetPath, String login, char[] password) throws DataSourceCreateWithNoReferentialImportException, IOException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        ObserveDataSourceConfigurationTopiaH2 sharedDatabaseConfiguration = dataSourcesForTestManager.createSharedDataSourceConfigurationH2(dbVersion, dbName, login, password);

        File sharedDatabaseFile = sharedDatabaseConfiguration.getDatabaseFile();

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;

        boolean sharedDatabaseExist = sharedDatabaseFile.exists();
        if (!sharedDatabaseExist) {

            if (log.isInfoEnabled()) {
                log.info("Create shared database: " + dbVersion.toString() + "/" + dbName + " to " + sharedDatabaseFile);
            }

            try (DataSourceService dataSourceService = newService(sharedDatabaseConfiguration, DataSourceService.class)) {
                DataSourceCreateConfigurationDto createConfiguration = new DataSourceCreateConfigurationDto();

                String scriptPath = "/db/" + dbVersion.toString() + "/" + dbName + ".sql.gz";

                byte[] databaseToImportContent = ObserveTestResources.getResourceContent(scriptPath);

                createConfiguration.setImportDatabase(databaseToImportContent);

                dataSourceService.create(sharedDatabaseConfiguration, createConfiguration);
            }

        }

        if (targetPath == null) {

            dataSourceConfiguration = sharedDatabaseConfiguration;
        } else {

            // Use a copy

            dataSourceConfiguration = dataSourcesForTestManager.createDataSourceConfigurationH2(targetPath, dbVersion, dbName, login, password);
            File databaseFileTarget = dataSourceConfiguration.getDatabaseFile();

            if (log.isInfoEnabled()) {
                log.info("Copy database: " + dbVersion.toString() + "/" + dbName + " to " + databaseFileTarget);
            }
            Files.createDirectories(databaseFileTarget.toPath().getParent());
            Files.copy(sharedDatabaseFile.toPath(), databaseFileTarget.toPath());

        }

        dataSourceConfiguration.setModelVersion(ObserveTestConfiguration.getModelVersion());

        return dataSourceConfiguration;

    }

    public <S extends ObserveService> S newService(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                Locale.FRANCE,
                ReferentialLocale.FR,
                temporaryDirectoryRoot.toFile(),
                speciesListConfiguration,
                dataSourceConfiguration
        );
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                Locale.FRANCE,
                ReferentialLocale.FR,
                temporaryDirectoryRoot.toFile(),
                speciesListConfiguration,
                dataSourceConnection
        );
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    void closeServiceFactory() {
        serviceFactory.close();
    }

    protected void after(Description description) throws IOException {

        super.after(description);

        closeServiceFactory();

    }

}
