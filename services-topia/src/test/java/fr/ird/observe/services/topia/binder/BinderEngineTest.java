package fr.ird.observe.services.topia.binder;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineStubDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDto;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineActivityDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.RouteStubDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.List;

/**
 * Created on 26/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class BinderEngineTest extends AbstractServiceTopiaTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BinderEngineTest.class);

    private BinderEngine binderEngine;

    @Before
    public void setUp() {

        binderEngine = BinderEngine.get();

    }

    @Test
    public void testTransformEntityToReferentialDto() throws Exception {

        transformReferentialEntityToDto(CountryDto.class);
        transformReferentialEntityToDto(FpaZoneDto.class);
        transformReferentialEntityToDto(GearDto.class);
        transformReferentialEntityToDto(GearCaracteristicDto.class);
        transformReferentialEntityToDto(GearCaracteristicTypeDto.class);
        transformReferentialEntityToDto(HarbourDto.class);
        transformReferentialEntityToDto(LengthWeightParameterDto.class);
        transformReferentialEntityToDto(OceanDto.class);
        transformReferentialEntityToDto(OrganismDto.class);
        transformReferentialEntityToDto(PersonDto.class);
        transformReferentialEntityToDto(ProgramDto.class);
        transformReferentialEntityToDto(SexDto.class);
        transformReferentialEntityToDto(SpeciesDto.class);
        transformReferentialEntityToDto(SpeciesGroupDto.class);
        transformReferentialEntityToDto(SpeciesListDto.class);
        transformReferentialEntityToDto(VesselDto.class);
        transformReferentialEntityToDto(VesselSizeCategoryDto.class);
        transformReferentialEntityToDto(VesselTypeDto.class);
        transformReferentialEntityToDto(BaitHaulingStatusDto.class);
        transformReferentialEntityToDto(BaitSettingStatusDto.class);
        transformReferentialEntityToDto(BaitTypeDto.class);
        transformReferentialEntityToDto(CatchFateLonglineDto.class);
        transformReferentialEntityToDto(EncounterTypeDto.class);
        transformReferentialEntityToDto(HealthnessDto.class);
        transformReferentialEntityToDto(HookPositionDto.class);
        transformReferentialEntityToDto(HookSizeDto.class);
        transformReferentialEntityToDto(HookTypeDto.class);
        transformReferentialEntityToDto(ItemHorizontalPositionDto.class);
        transformReferentialEntityToDto(ItemVerticalPositionDto.class);
        transformReferentialEntityToDto(LightsticksColorDto.class);
        transformReferentialEntityToDto(LightsticksTypeDto.class);
        transformReferentialEntityToDto(LineTypeDto.class);
        transformReferentialEntityToDto(MaturityStatusDto.class);
        transformReferentialEntityToDto(MitigationTypeDto.class);
        transformReferentialEntityToDto(SensorBrandDto.class);
        transformReferentialEntityToDto(SensorDataFormatDto.class);
        transformReferentialEntityToDto(SensorTypeDto.class);
        transformReferentialEntityToDto(SettingShapeDto.class);
        transformReferentialEntityToDto(SizeMeasureTypeDto.class);
        transformReferentialEntityToDto(StomacFullnessDto.class);
        transformReferentialEntityToDto(TripTypeDto.class);
        transformReferentialEntityToDto(VesselActivityLonglineDto.class);
        transformReferentialEntityToDto(WeightMeasureTypeDto.class);
        transformReferentialEntityToDto(DetectionModeDto.class);
        transformReferentialEntityToDto(ObjectFateDto.class);
        transformReferentialEntityToDto(ObjectOperationDto.class);
        transformReferentialEntityToDto(ObjectTypeDto.class);
        transformReferentialEntityToDto(ObservedSystemDto.class);
        transformReferentialEntityToDto(ReasonForDiscardDto.class);
        transformReferentialEntityToDto(ReasonForNoFishingDto.class);
        transformReferentialEntityToDto(ReasonForNullSetDto.class);
        transformReferentialEntityToDto(SpeciesFateDto.class);
        transformReferentialEntityToDto(SpeciesStatusDto.class);
        transformReferentialEntityToDto(SurroundingActivityDto.class);
        transformReferentialEntityToDto(TransmittingBuoyOperationDto.class);
        transformReferentialEntityToDto(TransmittingBuoyTypeDto.class);
        transformReferentialEntityToDto(VesselActivitySeineDto.class);
        transformReferentialEntityToDto(WeightCategoryDto.class);
        transformReferentialEntityToDto(WindDto.class);

    }

    @Test
    public void testTransformEntityToDataDto() throws Exception {

        transformDataEntityToDto(ActivityLonglineDto.class);
        transformDataEntityToDto(ActivityLonglineEncounterDto.class);
        transformDataEntityToDto(ActivityLonglineSensorUsedDto.class);
        transformDataEntityToDto(ActivityLonglineStubDto.class);
        transformDataEntityToDto(BaitsCompositionDto.class);
        transformDataEntityToDto(BasketDto.class);
        transformDataEntityToDto(BranchlineDto.class);
        transformDataEntityToDto(BranchlinesCompositionDto.class);
        transformDataEntityToDto(CatchLonglineDto.class);
        transformDataEntityToDto(EncounterDto.class);
        transformDataEntityToDto(FloatlinesCompositionDto.class);
        transformDataEntityToDto(GearUseFeaturesLonglineDto.class);
        transformDataEntityToDto(GearUseFeaturesMeasurementLonglineDto.class);
        transformDataEntityToDto(HooksCompositionDto.class);
        transformDataEntityToDto(SectionDto.class);
        transformDataEntityToDto(SensorUsedDto.class);
        transformDataEntityToDto(SetLonglineDto.class);
        transformDataEntityToDto(SetLonglineCatchDto.class);
        transformDataEntityToDto(SetLonglineDetailCompositionDto.class);
        transformDataEntityToDto(SetLonglineGlobalCompositionDto.class);
        transformDataEntityToDto(SetLonglineStubDto.class);
        transformDataEntityToDto(SetLonglineTdrDto.class);
        transformDataEntityToDto(SizeMeasureDto.class);
        transformDataEntityToDto(TdrDto.class);
        transformDataEntityToDto(TripLonglineDto.class);
        transformDataEntityToDto(TripLonglineActivityDto.class);
        transformDataEntityToDto(TripLonglineGearUseDto.class);
        transformDataEntityToDto(WeightMeasureDto.class);
        transformDataEntityToDto(ActivitySeineDto.class);
        transformDataEntityToDto(ActivitySeineStubDto.class);
        transformDataEntityToDto(FloatingObjectDto.class);
        transformDataEntityToDto(FloatingObjectObservedSpeciesDto.class);
        transformDataEntityToDto(FloatingObjectSchoolEstimateDto.class);
        transformDataEntityToDto(FloatingObjectTransmittingBuoyDto.class);
        transformDataEntityToDto(GearUseFeaturesMeasurementSeineDto.class);
        transformDataEntityToDto(GearUseFeaturesSeineDto.class);
        transformDataEntityToDto(NonTargetCatchDto.class);
        transformDataEntityToDto(NonTargetLengthDto.class);
        transformDataEntityToDto(NonTargetSampleDto.class);
        transformDataEntityToDto(ObjectObservedSpeciesDto.class);
        transformDataEntityToDto(ObjectSchoolEstimateDto.class);
        transformDataEntityToDto(RouteDto.class);
        transformDataEntityToDto(RouteStubDto.class);
        transformDataEntityToDto(SchoolEstimateDto.class);
        transformDataEntityToDto(SetSeineDto.class);
        transformDataEntityToDto(SetSeineNonTargetCatchDto.class);
        transformDataEntityToDto(SetSeineSchoolEstimateDto.class);
        transformDataEntityToDto(SetSeineTargetCatchDto.class);
        transformDataEntityToDto(TargetCatchDto.class);
        transformDataEntityToDto(TargetLengthDto.class);
        transformDataEntityToDto(TargetSampleDto.class);
        transformDataEntityToDto(TransmittingBuoyDto.class);
        transformDataEntityToDto(TripSeineDto.class);
        transformDataEntityToDto(TripSeineGearUseDto.class);


    }

    protected <D extends DataDto, E extends ObserveDataEntity> void transformDataEntityToDto(Class<D> dtoType) {

        Class<E> entityType = binderEngine.getDataEntityType(dtoType);

        try (ObserveTopiaPersistenceContext persistenceContext = topiaTestMethodResource.getTopiaApplicationContext().newPersistenceContext()) {

            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            List<E> entities = dao.findAll();

            if (!entities.isEmpty()) {
                E entity = entities.get(0);

                if (log.isInfoEnabled()) {
                    log.info("transform " + entity.getTopiaId() + " to " + dtoType.getName());
                }
                D dto = binderEngine.transformEntityToDataDto(ReferentialLocale.FR, dtoType, entity);

                Assert.assertNotNull(dto);
                Assert.assertEquals(entity.getTopiaId(), dto.getId());
//            Assert.assertEquals(entity.getTopiaCreateDate(), dto.getCreateDate());
//            Assert.assertEquals(entity.getTopiaVersion(), dto.getVersion());

                E entity2 = dao.forTopiaIdEquals(entity.getTopiaId()).findUnique();
                binderEngine.copyDataDtoToEntity(ReferentialLocale.FR, dto, entity2);

                Assert.assertNotNull(entity2);
                Assert.assertEquals(entity.getTopiaId(), entity.getTopiaId());
//                Assert.assertEquals(entity.getTopiaCreateDate(), entity.getTopiaCreateDate());
//                Assert.assertEquals(entity.getTopiaVersion(), entity.getTopiaVersion());

            }

        }

    }

    protected <D extends ReferentialDto, E extends ObserveReferentialEntity> void transformReferentialEntityToDto(Class<D> dtoType) {

        Class<E> entityType = binderEngine.getReferentialEntityType(dtoType);

        try (ObserveTopiaPersistenceContext persistenceContext = topiaTestMethodResource.getTopiaApplicationContext().newPersistenceContext()) {

            TopiaDao<E> dao = persistenceContext.getDao(entityType);
            List<E> entities = dao.findAll();

            E entity = entities.get(0);

            if (log.isInfoEnabled()) {
                log.info("transform " + entity.getTopiaId() + " to " + dtoType.getName());
            }
            D dto = binderEngine.transformEntityToReferentialDto(ReferentialLocale.FR, entity);

            Assert.assertNotNull(dto);
            Assert.assertEquals(entity.getTopiaId(), dto.getId());
            Assert.assertEquals(entity.getTopiaCreateDate(), dto.getCreateDate());
            Assert.assertEquals(entity.getTopiaVersion(), dto.getVersion());
            Assert.assertEquals(entity.getLastUpdateDate(), dto.getLastUpdateDate());
            Assert.assertEquals(entity.isNeedComment(), dto.isNeedComment());
            Assert.assertEquals(entity.isEnabled(), dto.isEnabled());
            Assert.assertEquals(entity.getUri(), dto.getUri());
            Assert.assertEquals(entity.getCode(), dto.getCode());

            E entity2 = binderEngine.transformReferentialDtoToEntity(ReferentialLocale.FR, dto);

            Assert.assertNotNull(entity2);
            Assert.assertEquals(entity.getTopiaId(), entity.getTopiaId());
            Assert.assertEquals(entity.getTopiaCreateDate(), entity.getTopiaCreateDate());
            Assert.assertEquals(entity.getTopiaVersion(), entity.getTopiaVersion());
            Assert.assertEquals(entity.getLastUpdateDate(), entity.getLastUpdateDate());
            Assert.assertEquals(entity.isNeedComment(), entity.isNeedComment());
            Assert.assertEquals(entity.isEnabled(), entity.isEnabled());
            Assert.assertEquals(entity.getUri(), entity.getUri());
            Assert.assertEquals(entity.getCode(), entity.getCode());

        }

    }

}
