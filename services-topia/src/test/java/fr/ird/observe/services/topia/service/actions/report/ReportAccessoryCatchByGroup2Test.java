/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;


import fr.ird.observe.services.dto.actions.report.DataMatrix;

/**
 * Pour tester le report {@code discardedAccessoireByGroup}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public class ReportAccessoryCatchByGroup2Test extends ReportAccessoryCatchByGroupTest {

    @Override
    protected void prepareVariables() {
        setVariableValue("speciesGroup", "fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.12092280503502995");
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 11, 21, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "[FAO]AJS [sc]Abalistes stellatus [fr]Baliste étoilé", "0", "20", "0", "0", "0", "20", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]ALN [sc]Aluterus scriptus [fr]Bourse écriture", "1", "0", "0", "0", "0", "1", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]ALM [sc]Aluterus monoceros [fr]Bourse loulou", "0", "16", "0", "0", "2", "14", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]CNT [sc]Canthidermis maculata [fr]Baliste rude", "2", "2230", "0", "0", "185", "1981", "0", "0", "66", "0.0");
        assertResultRow(result, row++, "[FAO]BON [sc]Sarda sarda [fr]Bonite à dos rayé", "0", "11", "0", "0", "0", "11", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]MSD [sc]Decapterus macarellus [fr]Comète maquereau ", "47", "6", "0", "0", "0", "6", "0", "0", "47", "0.0");
        assertResultRow(result, row++, "[FAO]CFW [sc]Coryphaena equiselis [fr]Coryphène dauphin", "2", "0", "0", "0", "0", "2", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]DOL [sc]Coryphaena hippurus [fr]Coryphène commun", "5", "107", "0", "0", "1", "59", "0", "37", "15", "0.0");
        assertResultRow(result, row++, "[FAO]CXS [sc]Caranx sexfasciatus [fr]Carangue vorace ", "0", "22", "0", "0", "1", "18", "0", "0", "3", "0.0");
        assertResultRow(result, row++, "[FAO]DIO [sc]Diodontidae [fr]Famille Diodontidae", "1", "0", "0", "0", "1", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]EHN [sc]Echeneis naucrates  [fr]Rémora commun ", "7", "0", "0", "0", "6", "1", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]RRU [sc]Elagatis bipinnulata [fr]Commère saumon", "0", "211", "0", "0", "25", "177", "0", "0", "9", "0.0");
        assertResultRow(result, row++, "[FAO]CGX [sc]Carangidae [fr]Famille Carangidae", "1", "49", "0", "0", "7", "43", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]ECN [sc]Echeneidae [fr]Famille Echeneidae", "2", "0", "0", "0", "2", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]KYV [sc]Kyphosus vaigiensis [fr]Caligagère", "0", "5", "0", "0", "0", "5", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]LOB [sc]Lobotes surinamensis [fr]Croupia roche ", "0", "28", "0", "0", "1", "26", "0", "0", "1", "0.0");
        assertResultRow(result, row++, "[FAO]NAU [sc]Naucrates ductor [fr]Poisson pilote", "1", "0", "0", "0", "1", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]REO [sc]Remora remora [fr]Rémora", "2", "0", "0", "0", "2", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]GBA [sc]Sphyraena barracuda [fr]Barracuda", "0", "26", "0", "0", "3", "23", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]BTS [sc]Tylosurus crocodilus [fr]Aiguille crocodile ", "0", "1", "0", "0", "1", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row, "[FAO]WAH [sc]Acanthocybium solandri [fr]Thazard bâtard", "0", "90", "0", "0", "3", "85", "0", "0", "2", "0.0");

    }
}
