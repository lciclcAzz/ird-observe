package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.service.seine.TargetSampleService;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class TargetSampleServiceTopiaTest extends AbstractServiceTopiaTest {

    protected TargetSampleService service;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(TargetSampleService.class);

    }

    @Test
    public void canUseTargetSampleTest() {

        Assert.assertTrue(service.canUseTargetSample(ObserveFixtures.SET_SEINE_ID, false));
        Assert.assertFalse(service.canUseTargetSample(ObserveFixtures.SET_SEINE_ID, true));

    }

    @Test
    public void loadForEditTest() {

        Form<TargetSampleDto> form = service.loadForm(ObserveFixtures.SET_SEINE_ID, false);

        Assert.assertNotNull(form);

        TargetSampleDto targetSampleDto = form.getObject();

        Assert.assertNotNull(targetSampleDto);
        Assert.assertFalse(targetSampleDto.getDiscarded());
        Assert.assertTrue(targetSampleDto.isTargetLengthEmpty());

    }

}
