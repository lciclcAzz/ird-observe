package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;
import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.topia.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.test.DatabaseName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTable;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Created on 29/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReplicateTestSupport extends AbstractServiceTopiaTest {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ReplicateTestSupport.class);

    protected File scriptFile;

    protected ReplicateTablesRequest.Builder createReplicateTablesRequest(DatabaseName databaseName) throws DataSourceCreateWithNoReferentialImportException, IOException, IncompatibleDataSourceCreateConfigurationException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        File targetDatabaseDirectory = new File(topiaTestMethodResource.getTestDirectory(), "targetReplication");
        ObserveDataSourceConfigurationTopiaH2 targetTopiaConfiguration = TOPIA_TEST_CLASS_RESOURCE.createDataSourceConfiguration(topiaTestMethodResource.getDbVersion(), databaseName.name(), targetDatabaseDirectory, topiaTestMethodResource.getLogin(), topiaTestMethodResource.getPassword());
        ObserveTopiaApplicationContext targetTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(targetTopiaConfiguration);

        scriptFile = new File(topiaTestMethodResource.getTestDirectory(), "script.sql");

        Writer writer = Files.newWriter(scriptFile, StandardCharsets.UTF_8);

        return topiaTestMethodResource.getTopiaApplicationContext()
                                      .getSqlBatchService()
                                      .replicateTablesRequestBuilder()
                                      .to(targetTopiaApplicationContext)
                                      .to(writer);
    }

    protected static void assertReplicateTripResults(ReplicateTablesRequest replicationRequest, ImmutableMap<String, Long> expectedResults) {

        TopiaSqlTables tables = replicationRequest.getTables();
        try (ObserveTopiaPersistenceContext persistenceContext = (ObserveTopiaPersistenceContext) replicationRequest.getTargetTopiaApplicationContext().newPersistenceContext()) {

            for (TopiaSqlTable table : tables) {

                String fullyTableName = table.getFullyTableName();

                if (!expectedResults.containsKey(fullyTableName.toLowerCase())) {

                    Long count = persistenceContext.countTable(fullyTableName);
                    System.out.println(fullyTableName + " , " + count);

                }

            }

            for (Map.Entry<String, Long> entry : expectedResults.entrySet()) {
                String fullyTableName = entry.getKey();
                Long expectedCount = entry.getValue();
                Long count = persistenceContext.countTable(fullyTableName);

                System.out.println("Found: " + fullyTableName + " , " + count);
                Assert.assertEquals("Should have found " + expectedCount + " on " + fullyTableName + ", but was " + count, expectedCount, count);

            }

        }

    }

    protected void testReplicate0(ReplicateTablesRequest request, ImmutableMap<String, Long> expectedTablesCount) {

        TopiaSqlTables tables = request.getTables();
        log.info(tables);

        topiaTestMethodResource.getTopiaApplicationContext()
                               .getSqlBatchService()
                               .execute(request);


        assertReplicateTripResults(request, expectedTablesCount);

        if (log.isInfoEnabled()) {
            log.info(String.format("Replicate to script (length: %s) :\n%s", StringUtil.convertMemory(scriptFile.length()), scriptFile));
        }

    }

}
