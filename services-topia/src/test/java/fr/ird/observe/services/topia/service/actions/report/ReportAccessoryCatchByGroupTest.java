/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Pour tester le report {@code discardedAccessoireByGroup}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public class ReportAccessoryCatchByGroupTest extends AbstractReportServiceTopiaTest {

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Dénombrement des captures accessoires et devenir, filtrés par groupe",
                "Afficher les nombres de captures accessoires par groupe d'espèce selon le type de banc et le devenir"
        );

        assertReportDimension(
                report,
                -1,
                11,
                new String[]{"Espèce",
                             "Total BL",
                             "Total BO",
                             "Sorti vivant/échappé",
                             "Sorti mort",
                             "Rejeté vivant",
                             "Rejeté mort",
                             "Partiellement conservé",
                             "Cuve",
                             "Cuisine",
                             "Autre"
                }
        );

        assertReportNbRequests(report, 1);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.row,
                0,
                0
        );
    }

    @Override
    protected String getReportId() {
        return "accessoryCatchByGroup";
    }

    @Override
    protected void prepareVariables() {
        setVariableValue("speciesGroup", "fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075");
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 11, 6, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "[FAO]FAL [sc]Carcharhinus falciformis [fr]Requin soyeux", "0", "17", "0", "0", "10", "7", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]OCS [sc]Carcharhinus longimanus [fr]Requin océanique", "1", "0", "0", "0", "1", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]RSK [sc]Carcharhinidae spp [fr]Famille Carcharhinidae", "0", "11", "0", "0", "5", "6", "0", "0", "0", "0.0");
        assertResultRow(result, row++, "[FAO]STT [sc]Dasyatidae [fr]Famille Dasyatidae", "7", "0", "0", "0", "6", "0", "0", "0", "0", "1.0");
        assertResultRow(result, row++, "[FAO]SPY [sc]Sphyrnidae [fr]Famille Sphyrnidae", "0", "1", "0", "0", "1", "0", "0", "0", "0", "0.0");
        assertResultRow(result, row, "[FAO]RMB [sc]Manta birostris [fr]Mante atlantique", "9", "0", "0", "0", "9", "0", "0", "0", "0", "0.0");
    }
}
