package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;

/**
 * Pour générer une requète sql de désactivation d'un référentiel donné.
 * Created on 04/08/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class DesactivateSqlStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DesactivateSqlStatementGenerator.class);

    private static final String UPDATE_STATEMENT = "UPDATE %s.%s SET enabled = false, topiaVersion = topiaVersion + 1 WHERE topiaId ='%s';\n";

    private final String schemaName;
    private final String tableName;

    public DesactivateSqlStatementGenerator(TopiaMetadataEntity metadataEntity) {
        this.schemaName = metadataEntity.getDbSchemaName();
        this.tableName = metadataEntity.getDbTableName();
    }

    public String generateSql(String sourceId) {

        StringBuilder result = new StringBuilder();

        String sql = String.format(UPDATE_STATEMENT, schemaName, tableName, sourceId);
        result.append(sql);

        if (log.isDebugEnabled()) {
            log.debug("sql: " + sql);
        }

        return result.toString();

    }


}
