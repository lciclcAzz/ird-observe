package fr.ird.observe.services.topia.service;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.EntityMap;
import fr.ird.observe.entities.constants.ReferenceStatusPersist;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.ReferenceMap;
import fr.ird.observe.services.dto.constants.ReferenceStatus;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinition;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.DataNotFoundException;
import fr.ird.observe.services.service.ReferenceSetsRequest;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.ReplaceSqlStatementGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialServiceTopia extends ObserveServiceTopia implements ReferentialService {

    private static final Log log = LogFactory.getLog(ReferentialServiceTopia.class);

    @Override
    public <D extends ReferentialDto> ReferentialReferenceSet<D> getReferenceSet(Class<D> type, Date lastUpdateDate) {
        if (log.isTraceEnabled()) {
            log.trace("getReferenceSet(" + type.getName() + ", " + lastUpdateDate + ")");
        }

        Class<ObserveReferentialEntity> entityType = BINDER_ENGINE.getReferentialEntityType(type);

        //FIXME A revoir car on devrait toujours avoir une date de dernière mise à jour
        Optional<Date> lastUpdateOptional = getLastUpdate(entityType);

        ReferentialReferenceSet<D> result = null;

        if (lastUpdateOptional.isPresent() && (lastUpdateDate == null || lastUpdateOptional.get().after(lastUpdateDate))) {

            List<ObserveReferentialEntity> entities = loadEntities(entityType);

            result = toReferentialReferenceSet(type, entities, lastUpdateOptional.get());

        }

        return result;

    }

    @Override
    public ImmutableSet<ReferentialReferenceSet<?>> getReferentialReferenceSets(ReferenceSetsRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("getReferentialReferenceSets(" + request.getRequestName() + ")");
        }

        String requestName = request.getRequestName();

        ReferenceSetRequestDefinition requestDefinition = ReferenceSetRequestDefinitions.get(requestName);

        ImmutableSet.Builder<ReferentialReferenceSet<?>> resultBuilder = ImmutableSet.builder();

        ImmutableMap<Class<?>, Date> lastUpdateDates = request.getLastUpdateDates();
        Objects.requireNonNull(lastUpdateDates);

        for (ReferenceSetDefinition<? extends ReferentialDto> definition : requestDefinition.getReferentialReferenceSetDefinitions()) {

            Date lastUpdateDate = lastUpdateDates.get(definition.getType());

            ReferentialReferenceSet<? extends ReferentialDto> referenceSet = getReferenceSet(definition.getType(), lastUpdateDate);
            if (referenceSet != null) {
                resultBuilder.add(referenceSet);
            }

        }

        return resultBuilder.build();

    }

    @Override
    public SpeciesDto loadSpecies(String id) {
        if (log.isTraceEnabled()) {
            log.trace("loadSpecies(" + id + ")");
        }

        Species speciesEntity = loadEntity(SpeciesDto.class, id);
        ReferentialBinderSupport<Species, SpeciesDto> binder = getReferentialBinder(SpeciesDto.class);
        return binder.toDto(getReferentialLocale(), speciesEntity);

    }

    @Override
    public <D extends ReferentialDto> Form<D> loadForm(Class<D> type, String id) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + type.getName() + ", " + id + ")");
        }

        ObserveReferentialEntity entity = loadEntity(type, id);
        ReferenceSetRequestDefinitions request = ReferenceSetRequestDefinitions.get(type);
        Form<D> form = referentialEntityToForm(type, entity, request);

        D dto = form.getObject();
        dto.setVersion(entity.getTopiaVersion());
        dto.setCreateDate(entity.getTopiaCreateDate());
        dto.setLastUpdateDate(entity.getLastUpdateDate());
        dto.setStatus(ReferenceStatus.valueOf(entity.getStatus().name()));

        return form;

    }

    @Override
    public <D extends ReferentialDto> ReferentialReference<D> loadReference(Class<D> type, String id) throws DataNotFoundException {
        if (log.isTraceEnabled()) {
            log.trace("loadReference(" + type.getName() + ", " + id + ")");
        }

        ObserveReferentialEntity entity = loadEntity(type, id);
        ReferentialBinderSupport<ObserveReferentialEntity, D> referentialBinder = getReferentialBinder(type);
        return referentialBinder.toReferentialReference(getReferentialLocale(), entity);

    }

    @Override
    public <D extends ReferentialDto> Form<D> preCreate(Class<D> type) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + type.getName() + ")");
        }

        Class<ObserveReferentialEntity> entityType = getReferentialEntityType(type);
        ObserveReferentialEntity entity = newEntity(entityType);
        entity.setStatus(ReferenceStatusPersist.enabled);
        return referentialEntityToForm(type, entity, null);
    }

    public <D extends ReferentialDto> SaveResultDto save(D bean) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + bean.getId() + ")");
        }

        ObserveReferentialEntity entity = loadOrCreateEntityFromReferentialDto(bean);
        copyReferentialDtoToEntity(bean, entity);
        return saveEntity(entity);

    }

    @Override
    public <D extends ReferentialDto> void delete(Class<D> type, String id) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + type.getName() + ", " + id + ")");
        }

        Class<? extends ObserveReferentialEntity> entityType = getReferentialEntityType(type);
        deleteEntity(type, entityType, Collections.singleton(id));
    }

    @Override
    public <D extends ReferentialDto> void delete(Class<D> type, Collection<String> ids) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + type.getName() + ", [" + Joiner.on(", ").join(ids) + "])");
        }

        Class<? extends ObserveReferentialEntity> entityType = getReferentialEntityType(type);
        deleteEntity(type, entityType, ids);
    }

    @Override
    public <E extends ReferentialDto> void replaceReference(Class<E> beanType, String idToReplace, String replaceId) {
        if (log.isTraceEnabled()) {
            log.trace("replaceReference(" + beanType.getName() + ", " + idToReplace + "," + replaceId + ")");
        }
        Class<ObserveReferentialEntity> entityType = getReferentialEntityType(beanType);
        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entityType);

        TopiaMetadataModel metadataModel = serviceContext.getTopiaApplicationContext().getMetadataModel();

        ReplaceSqlStatementGenerator<E> generator = new ReplaceSqlStatementGenerator<E>(metadataModel, entityEnum.name());

        String sql = generator.generateSql(idToReplace, replaceId);

        if (log.isInfoEnabled()) {
            log.info("Replace sql code:\n" + sql);
        }
        getTopiaPersistenceContext().executeSqlScript(sql.getBytes());

    }

    @Override
    public <R extends ReferentialDto> ReferenceMap findAllUsages(R bean) throws DataNotFoundException {
        if (log.isTraceEnabled()) {
            log.trace("findAllUsages(" + bean.getId() + ")");
        }

        Class<R> referentialDtoType = (Class<R>) bean.getClass();
        Class<ObserveReferentialEntity> entityType = getReferentialEntityType(referentialDtoType);

        ObserveReferentialEntity entity = loadEntity(referentialDtoType, bean.getId());

        TopiaDao<ObserveReferentialEntity> dao = getTopiaPersistenceContext().getDao(entityType);
        EntityMap allUsages = new EntityMap(dao.findAllUsages(entity));

        ReferenceMap result = new ReferenceMap();

        for (Class<? extends TopiaEntity> type : allUsages.keySet()) {

            List<? extends TopiaEntity> entities = allUsages.get(type);

            if (ObserveReferentialEntity.class.isAssignableFrom(type)) {

                Class<ReferentialDto> dtoType = BINDER_ENGINE.getReferentialDtoType((Class) type);
                ReferentialReferenceSet<?> referenceSet = toReferentialReferenceSet(dtoType, (List) entities, null);
                result.put(dtoType, referenceSet.getReferences());
            } else {

                Class<DataDto> dtoType = BINDER_ENGINE.getDataDtoType((Class) type);
                DataReferenceSet<?> referenceSet = toDataReferenceSet(dtoType, (List) entities);
                result.put(dtoType, referenceSet.getReferences());
            }

        }

        return result;
    }

    @Override
    public <D extends ReferentialDto> boolean exists(Class<D> type, String id) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + type.getName() + ", " + id + ")");
        }

        Class<? extends ObserveReferentialEntity> entityType = getReferentialEntityType(type);
        return existsEntity(entityType, id);
    }

}
