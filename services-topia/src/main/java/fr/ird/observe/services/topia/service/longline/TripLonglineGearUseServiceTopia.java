package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.GearUseFeaturesLongline;
import fr.ird.observe.entities.longline.GearUseFeaturesMeasurementLongline;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.services.service.longline.TripLonglineGearUseService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripLonglineGearUseServiceTopia extends ObserveServiceTopia implements TripLonglineGearUseService {

    private static final Log log = LogFactory.getLog(TripLonglineGearUseServiceTopia.class);

    @Override
    public Form<TripLonglineGearUseDto> loadForm(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + tripLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineGearUseDto.class, tripLonglineId);

        return dataEntityToForm(
                TripLonglineGearUseDto.class,
                tripLongline,
                ReferenceSetRequestDefinitions.TRIP_LONGLINE_GEAR_USE_FORM);

    }

    @Override
    public SaveResultDto save(TripLonglineGearUseDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        TripLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        return saveEntity(entity);

    }

    protected SaveResultDto saveEntity(TripLongline entity) {
        SaveResultDto saveResultDto = super.saveEntity(entity);
        Date lastUpdateDate = saveResultDto.getLastUpdateDate();

        // propagate lastUpdateDate to every gear uses
        Set<GearUseFeaturesLongline> gearUseFeaturesLonglines = entity.getGearUseFeaturesLongline();
        for (GearUseFeaturesLongline gearUseFeaturesLongline : gearUseFeaturesLonglines) {
            gearUseFeaturesLongline.setLastUpdateDate(lastUpdateDate);
            for (GearUseFeaturesMeasurementLongline gearUseFeaturesMeasurementLongline : gearUseFeaturesLongline.getGearUseFeaturesMeasurement()) {
                gearUseFeaturesMeasurementLongline.setLastUpdateDate(lastUpdateDate);
            }
        }
        return saveResultDto;
    }

}
