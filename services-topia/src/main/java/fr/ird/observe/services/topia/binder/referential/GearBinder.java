package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Gear;
import fr.ird.observe.entities.referentiel.GearCaracteristic;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GearBinder extends ReferentialBinderSupport<Gear, GearDto> {

    public GearBinder() {
        super(Gear.class, GearDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, GearDto dto, Gear entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);
        entity.setGearCaracteristic(toEntitySet(dto.getGearCaracteristic(), GearCaracteristic.class));
    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Gear entity, GearDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);
        dto.setGearCaracteristic(toReferentialReferenceList(referentialLocale, entity.getGearCaracteristic(), GearCaracteristicDto.class));

    }

    @Override
    public ReferentialReference<GearDto> toReferentialReference(ReferentialLocale referentialLocale, Gear entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), getLabel(referentialLocale, entity));

    }

    @Override
    public ReferentialReference<GearDto> toReferentialReference(ReferentialLocale referentialLocale, GearDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), getLabel(referentialLocale, dto));

    }
}
