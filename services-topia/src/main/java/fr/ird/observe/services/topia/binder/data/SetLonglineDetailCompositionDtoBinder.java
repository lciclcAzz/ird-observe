package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SetLonglineDetailCompositionDtoBinder extends DataBinderSupport<SetLongline, SetLonglineDetailCompositionDto> {

    public SetLonglineDetailCompositionDtoBinder() {
        super(SetLongline.class, SetLonglineDetailCompositionDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SetLonglineDetailCompositionDto dto, SetLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSection(toEntitySet(referentialLocale, dto.getSection(), Section.class, entity.getSection()));
        entity.setSettingStartTimeStamp(dto.getSettingStartTimeStamp());
        entity.setMonitored(dto.getMonitored());
        entity.setTotalSectionsCount(dto.getTotalSectionsCount());
        entity.setBasketsPerSectionCount(dto.getBasketsPerSectionCount());
        entity.setBranchlinesPerBasketCount(dto.getBranchlinesPerBasketCount());
        entity.setHaulingDirectionSameAsSetting(dto.getHaulingDirectionSameAsSetting());
        entity.setHaulingBreaks(dto.getHaulingBreaks());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SetLongline entity, SetLonglineDetailCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSection(toLinkedHashSetData(referentialLocale, entity.getSection(), SectionDto.class));
        dto.setSettingStartTimeStamp(entity.getSettingStartTimeStamp());
        dto.setMonitored(entity.getMonitored());
        dto.setTotalSectionsCount(entity.getTotalSectionsCount());
        dto.setBasketsPerSectionCount(entity.getBasketsPerSectionCount());
        dto.setBranchlinesPerBasketCount(entity.getBranchlinesPerBasketCount());
        dto.setHaulingDirectionSameAsSetting(entity.getHaulingDirectionSameAsSetting());
        dto.setHaulingBreaks(entity.getHaulingBreaks());

    }

}
