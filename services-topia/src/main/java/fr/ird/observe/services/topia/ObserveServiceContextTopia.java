package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaSupport;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConnectionTopia;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.topia.entity.EntitiesSetFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

/**
 * Contexte d'un service ToPIA.
 *
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveServiceContextTopia {

    protected final ObserveServiceInitializer observeServiceInitializer;

    protected final ObserveServiceFactory mainServiceFactory;

    protected final ObserveServiceFactory serviceFactory;

    protected ObserveTopiaPersistenceContext topiaPersistenceContext;

    protected ObserveTopiaApplicationContext topiaApplicationContext;

    protected EntitiesSetFactory entitiesSetFactory;

    public ObserveServiceContextTopia(ObserveServiceInitializer observeServiceInitializer,
                                      ObserveServiceFactory mainServiceFactory,
                                      ObserveServiceFactory serviceFactory) {
        this.observeServiceInitializer = observeServiceInitializer;
        this.mainServiceFactory = mainServiceFactory;
        this.serviceFactory = serviceFactory;
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    public void setTopiaApplicationContext(ObserveTopiaApplicationContext topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
    }

    public Locale getApplicationLocale() {
        return observeServiceInitializer.getApplicationLocale();
    }

    public ReferentialLocale getReferentialLocale() {
        return observeServiceInitializer.getReferentialLocale();
    }

    public File getTemporaryDirectoryRoot() {
        return observeServiceInitializer.getTemporaryDirectoryRoot();
    }

    public boolean withDataSourceConnection() {
        return observeServiceInitializer.withDataSourceConnection();
    }

    public ObserveDataSourceConnectionTopia getDataSourceConnection() {
        return (ObserveDataSourceConnectionTopia) observeServiceInitializer.getDataSourceConnection();
    }

    public ObserveDataSourceConfigurationTopiaSupport getDataSourceConfiguration() {
        return (ObserveDataSourceConfigurationTopiaSupport) observeServiceInitializer.getDataSourceConfiguration();
    }

    public Date now() {
        return new Date();
    }

    public EntitiesSetFactory getEntitiesSetFactory() {
        if (entitiesSetFactory == null) {
            entitiesSetFactory = new EntitiesSetFactory(observeServiceInitializer.getSpeciesListConfiguration());
        }
        return entitiesSetFactory;
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) {
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        ObserveServiceInitializer newObserveServiceInitializer = ObserveServiceInitializer.create(observeServiceInitializer);
        newObserveServiceInitializer.setDataSourceConnection(dataSourceConnection);
        newObserveServiceInitializer.setDataSourceConfiguration(null);
        return mainServiceFactory.newService(newObserveServiceInitializer, serviceType);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        ObserveServiceInitializer newObserveServiceInitializer = ObserveServiceInitializer.create(observeServiceInitializer);
        newObserveServiceInitializer.setDataSourceConnection(null);
        newObserveServiceInitializer.setDataSourceConfiguration(dataSourceConfiguration);
        return mainServiceFactory.newService(newObserveServiceInitializer, serviceType);
    }

    public ObserveTopiaPersistenceContext getTopiaPersistenceContext() {
        return topiaPersistenceContext;
    }

    public void setTopiaPersistenceContext(ObserveTopiaPersistenceContext topiaPersistenceContext) {
        this.topiaPersistenceContext = topiaPersistenceContext;
    }

    public void closeTopiaPersistenceContext() {
        topiaPersistenceContext.close();
        topiaPersistenceContext = null;
    }

    public File createTemporaryDirectory(String prefix) {
        Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions.fromString("rwxr-x---");
        FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(posixFilePermissions);
        try {
            return Files.createTempDirectory(getTemporaryDirectoryRoot().toPath(), prefix, fileAttribute).toFile();
        } catch (IOException e) {
            throw new RuntimeException("Could not create temporary directory with preifx: " + prefix, e);
        }
    }

}
