package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.services.service.seine.TargetSampleService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TargetSampleServiceTopia extends ObserveServiceTopia implements TargetSampleService {

    private static final Log log = LogFactory.getLog(TargetSampleServiceTopia.class);

    @Override
    public boolean canUseTargetSample(String setSeineId, boolean discarded) {
        if (log.isTraceEnabled()) {
            log.trace("canUseTargetSample(" + setSeineId + ", " + discarded + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        return setSeine.canUseTargetSample(discarded);
    }

    protected TargetSample getTargetSample(SetSeine setSeine, boolean discarded) {

        TargetSample targetSample = setSeine.getTargetSample(discarded);

        if (targetSample == null) {
            targetSample = newEntity(TargetSample.class);
            targetSample.setDiscarded(discarded);
        }

        return targetSample;


    }

    @Override
    public Form<TargetSampleDto> loadForm(String setSeineId, boolean discarded) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ", " + discarded + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        TargetSample targetSample = getTargetSample(setSeine, discarded);

        return dataEntityToForm(
                TargetSampleDto.class,
                targetSample,
                ReferenceSetRequestDefinitions.TARGET_SAMPLE_FORM);
    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSampleSpecies(String setSeineId, boolean discarded) {
        if (log.isTraceEnabled()) {
            log.trace("getSampleSpecies(" + setSeineId + ", " + discarded + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        // on filtre la list des espéces cibles
        Set<Species> speciesSet = Sets.newLinkedHashSet();

        // on recupere la liste des espèces thon cible
        Collection<TargetCatch> targetCatches = setSeine.getTargetCatch();

        // on filtre sur les espèces montées sur le pont et rejetées
        if (targetCatches != null) {

            for (TargetCatch targetCatch : targetCatches) {

                // si echantillon rejeté : on ne conserve que les espèces rejectées montées sur le pont
                // sinon les espèce cible
                if (discarded && targetCatch.isDiscarded() && BooleanUtils.isTrue(targetCatch.getBroughtOnDeck())
                        || !discarded && !targetCatch.isDiscarded()) {

                    speciesSet.add(targetCatch.getWeightCategory().getSpecies());
                }

            }

        }

        ImmutableList.Builder<ReferentialReference<SpeciesDto>> speciesBuilder = ImmutableList.builder();

        ReferentialLocale referentialLocale = getReferentialLocale();

        ReferentialBinderSupport<ObserveReferentialEntity, SpeciesDto> binder = getReferentialBinder(SpeciesDto.class);

        for (Species species : speciesSet) {

            ReferentialReference<SpeciesDto> speciesRef = binder.toReferentialReference(referentialLocale, species);
            speciesBuilder.add(speciesRef);

        }

        return speciesBuilder.build();
    }

    @Override
    public SaveResultDto save(String setSeineId, TargetSampleDto dto) {

        if (log.isTraceEnabled()) {
            log.trace("save(" + setSeineId + ", " + dto.getId() + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        TargetSample entity = loadOrCreateEntityFromDataDto(dto);

        checkLastUpdateDate(entity, dto);

        copyDataDtoToEntity(dto, entity);

        for (TargetLength targetLength : entity.getTargetLength()) {
            targetLength.setTargetSample(entity);
        }

        if (dto.isNotPersisted()) {

            setSeine.addTargetSample(entity);

        }

        return saveEntity(setSeine, entity);

    }
}
