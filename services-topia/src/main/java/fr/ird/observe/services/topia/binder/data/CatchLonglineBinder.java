package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SizeMeasure;
import fr.ird.observe.entities.longline.WeightMeasure;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.longline.CatchFateLongline;
import fr.ird.observe.entities.referentiel.longline.Healthness;
import fr.ird.observe.entities.referentiel.longline.HookPosition;
import fr.ird.observe.entities.referentiel.longline.MaturityStatus;
import fr.ird.observe.entities.referentiel.longline.StomacFullness;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CatchLonglineBinder extends DataBinderSupport<CatchLongline, CatchLonglineDto> {

    public CatchLonglineBinder() {
        super(CatchLongline.class, CatchLonglineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, CatchLonglineDto dto, CatchLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setHomeId(dto.getHomeId());
        entity.setCount(dto.getCount());
        entity.setTotalWeight(dto.getTotalWeight());
        entity.setHookWhenDiscarded(dto.getHookWhenDiscarded());
        entity.setDepredated(dto.getDepredated());
        entity.setBeatDiameter(dto.getBeatDiameter());
        entity.setGonadeWeight(dto.getGonadeWeight());
        entity.setPhotoReferences(dto.getPhotoReferences());
        entity.setNumber(dto.getNumber());
        entity.setAcquisitionMode(dto.getAcquisitionMode());
        entity.setBasket(toEntity(dto.getBasket(), Basket.class));
        entity.setBranchline(toEntity(dto.getBranchline(), Branchline.class));
        entity.setSection(toEntity(dto.getSection(), Section.class));
        entity.setCatchFateLongline(toEntity(dto.getCatchFateLongline(), CatchFateLongline.class));
        entity.setDiscardHealthness(toEntity(dto.getDiscardHealthness(), Healthness.class));
        entity.setSpeciesCatch(toEntity(dto.getSpeciesCatch(), Species.class));
        entity.setMaturityStatus(toEntity(dto.getMaturityStatus(), MaturityStatus.class));
        entity.setStomacFullness(toEntity(dto.getStomacFullness(), StomacFullness.class));
        entity.setHookPosition(toEntity(dto.getHookPosition(), HookPosition.class));
        entity.setCatchHealthness(toEntity(dto.getCatchHealthness(), Healthness.class));
        entity.setSex(toEntity(dto.getSex(), Sex.class));
        entity.setSizeMeasure(toEntityCollection(referentialLocale, dto.getSizeMeasure(), SizeMeasure.class, entity.getSizeMeasure()));
        entity.setPredator(toEntitySet(dto.getPredator(), Species.class));
        entity.setWeightMeasure(toEntityCollection(referentialLocale, dto.getWeightMeasure(), WeightMeasure.class, entity.getWeightMeasure()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, CatchLongline entity, CatchLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setHomeId(entity.getHomeId());
        dto.setCount(entity.getCount());
        dto.setTotalWeight(entity.getTotalWeight());
        dto.setHookWhenDiscarded(entity.getHookWhenDiscarded());
        dto.setDepredated(entity.getDepredated());
        dto.setBeatDiameter(entity.getBeatDiameter());
        dto.setGonadeWeight(entity.getGonadeWeight());
        dto.setPhotoReferences(entity.getPhotoReferences());
        dto.setNumber(entity.getNumber());
        dto.setAcquisitionMode(entity.getAcquisitionMode());
        dto.setBasket(toDataReference(referentialLocale, entity.getBasket(), BasketDto.class));
        dto.setBranchline(toDataReference(referentialLocale, entity.getBranchline(), BranchlineDto.class));
        dto.setSection(toDataReference(referentialLocale, entity.getSection(), SectionDto.class));
        dto.setCatchFateLongline(toReferentialReference(referentialLocale, entity.getCatchFateLongline(), CatchFateLonglineDto.class));
        dto.setDiscardHealthness(toReferentialReference(referentialLocale, entity.getDiscardHealthness(), HealthnessDto.class));
        dto.setSpeciesCatch(toReferentialReference(referentialLocale, entity.getSpeciesCatch(), SpeciesDto.class));
        dto.setMaturityStatus(toReferentialReference(referentialLocale, entity.getMaturityStatus(), MaturityStatusDto.class));
        dto.setStomacFullness(toReferentialReference(referentialLocale, entity.getStomacFullness(), StomacFullnessDto.class));
        dto.setHookPosition(toReferentialReference(referentialLocale, entity.getHookPosition(), HookPositionDto.class));
        dto.setCatchHealthness(toReferentialReference(referentialLocale, entity.getCatchHealthness(), HealthnessDto.class));
        dto.setSex(toReferentialReference(referentialLocale, entity.getSex(), SexDto.class));
        dto.setPredator(toReferentialReferenceList(referentialLocale, entity.getPredator(), SpeciesDto.class));
        dto.setSizeMeasure(toLinkedHashSetData(referentialLocale, entity.getSizeMeasure(), SizeMeasureDto.class));
        dto.setWeightMeasure(toLinkedHashSetData(referentialLocale, entity.getWeightMeasure(), WeightMeasureDto.class));

    }

    @Override
    public DataReference<CatchLonglineDto> toDataReference(ReferentialLocale referentialLocale, CatchLongline entity) {

        return toDataReference(entity, entity.getHomeId());

    }

    @Override
    public DataReference<CatchLonglineDto> toDataReference(ReferentialLocale referentialLocale, CatchLonglineDto dto) {

        return toDataReference(dto, dto.getHomeId());

    }

}
