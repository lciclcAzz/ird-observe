package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.SensorUsed;
import fr.ird.observe.services.service.longline.ActivityLongLineSensorUsedService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.DataFileDto;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.DataFileNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ActivityLongLineSensorUsedServiceTopia extends ObserveServiceTopia implements ActivityLongLineSensorUsedService {

    private static final Log log = LogFactory.getLog(ActivityLongLineSensorUsedServiceTopia.class);

    @Override
    public Form<ActivityLonglineSensorUsedDto> loadForm(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + activityLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineSensorUsedDto.class, activityLonglineId);

        return dataEntityToForm(ActivityLonglineSensorUsedDto.class,
                                activityLongline,
                                ReferenceSetRequestDefinitions.ACTIVITY_LONGLINE_SENSOR_USED_FORM);

    }

    @Override
    public DataFileDto getDataFile(String sensorId) {
        if (log.isTraceEnabled()) {
            log.trace("getDataFile(" + sensorId + ")");
        }

        SensorUsed sensor = loadEntity(SensorUsedDto.class, sensorId);

        if (sensor.getData() == null) {
            throw new DataFileNotFoundException(SensorUsedDto.class, sensorId);
        }

        return newDataFileDto(sensor.getData(), sensor.getDataFilename());
    }

    @Override
    public SaveResultDto save(ActivityLonglineSensorUsedDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        ActivityLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);
        return saveEntity(entity);

    }
}
