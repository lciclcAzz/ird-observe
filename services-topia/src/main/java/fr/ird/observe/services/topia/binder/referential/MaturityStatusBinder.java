package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.longline.MaturityStatus;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MaturityStatusBinder extends ReferentialBinderSupport<MaturityStatus, MaturityStatusDto> {

    public MaturityStatusBinder() {
        super(MaturityStatus.class, MaturityStatusDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, MaturityStatusDto dto, MaturityStatus entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);
        entity.setLowerValue(dto.getLowerValue());
        entity.setUpperValue(dto.getUpperValue());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, MaturityStatus entity, MaturityStatusDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);
        dto.setLowerValue(entity.getLowerValue());
        dto.setUpperValue(entity.getUpperValue());

    }

    @Override
    public ReferentialReference<MaturityStatusDto> toReferentialReference(ReferentialLocale referentialLocale, MaturityStatus entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), getLabel(referentialLocale, entity));

    }

    @Override
    public ReferentialReference<MaturityStatusDto> toReferentialReference(ReferentialLocale referentialLocale, MaturityStatusDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), getLabel(referentialLocale, dto));

    }
}
