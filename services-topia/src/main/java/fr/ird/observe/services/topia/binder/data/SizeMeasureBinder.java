package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.SizeMeasure;
import fr.ird.observe.entities.referentiel.longline.SizeMeasureType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SizeMeasureBinder extends DataBinderSupport<SizeMeasure, SizeMeasureDto> {

    public SizeMeasureBinder() {
        super(SizeMeasure.class, SizeMeasureDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SizeMeasureDto dto, SizeMeasure entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSize(dto.getSize());
        entity.setSizeMeasureType(toEntity(dto.getSizeMeasureType(), SizeMeasureType.class));


    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SizeMeasure entity, SizeMeasureDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSize(entity.getSize());
        dto.setSizeMeasureType(toReferentialReference(referentialLocale, entity.getSizeMeasureType(), SizeMeasureTypeDto.class));

    }

    @Override
    public DataReference<SizeMeasureDto> toDataReference(ReferentialLocale referentialLocale, SizeMeasure entity) {
        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSizeMeasureType()),
                               entity.getSize());
    }

    @Override
    public DataReference<SizeMeasureDto> toDataReference(ReferentialLocale referentialLocale, SizeMeasureDto dto) {
        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSizeMeasureType()),
                               dto.getSize());
    }
}
