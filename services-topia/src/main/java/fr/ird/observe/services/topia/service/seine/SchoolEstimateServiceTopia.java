package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.services.service.seine.SchoolEstimateService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SchoolEstimateServiceTopia extends ObserveServiceTopia implements SchoolEstimateService {

    private static final Log log = LogFactory.getLog(SchoolEstimateServiceTopia.class);

    @Override
    public Form<SetSeineSchoolEstimateDto> loadForm(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineSchoolEstimateDto.class, setSeineId);

        return dataEntityToForm(
                SetSeineSchoolEstimateDto.class,
                setSeine,
                ReferenceSetRequestDefinitions.SET_SEINE_SCHOOL_ESTIMATE_FORM
        );
    }

    @Override
    public SaveResultDto save(SetSeineSchoolEstimateDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        SetSeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        for (SchoolEstimate schoolEstimate : entity.getSchoolEstimate()) {
            schoolEstimate.setSetSeine(entity);
        }

        return saveEntity(entity);

    }
}
