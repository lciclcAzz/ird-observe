package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Pour générer une requète sql d'ajout à partir d'un référentiel donné.
 *
 * Created on 29/06/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class InsertSqlStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InsertSqlStatementGenerator.class);

    private static final String INSERT_STATEMENT = "INSERT INTO %s.%s(%s) VALUES (%s);\n";
    private static final String NM_ASSOCIATION_INSERT_STATEMENT = "INSERT INTO %s.%s(%s, %s) VALUES ('%s', '%s');\n";

    private final Set<String> columnNames;
    private final String schemaName;
    private final String tableName;
    private final Binder<R, R> binder;
    private final String[] simplePropertyNames;
    private final String[] manyToOneAssociationNames;
    private final Set<ManyToManyAssociationStruct> manyToManyAssociations;
    private final Set<String> primitiveBooleanPropertyNames;
    private final Set<String> primitiveIntegerPropertyNames;
    private final Set<String> primitiveLongPropertyNames;
    private final Set<String> primitiveFloatPropertyNames;

    public InsertSqlStatementGenerator(TopiaMetadataEntity metadataEntity, Class<R> dtoType) {
        this.schemaName = metadataEntity.getDbSchemaName();
        this.tableName = metadataEntity.getDbTableName();
        Set<String> propertyNamesSet = metadataEntity.getProperties().keySet();
        this.simplePropertyNames = propertyNamesSet.toArray(new String[propertyNamesSet.size()]);
        this.primitiveBooleanPropertyNames = metadataEntity.getPrimitivePropertyNames("boolean");
        this.primitiveIntegerPropertyNames = metadataEntity.getPrimitivePropertyNames("int");
        this.primitiveLongPropertyNames = metadataEntity.getPrimitivePropertyNames("long");
        this.primitiveFloatPropertyNames = metadataEntity.getPrimitivePropertyNames("float");
        Set<String> manyToOneAssociationNamesSet = metadataEntity.getManyToOneAssociations().keySet();
        this.manyToOneAssociationNames = manyToOneAssociationNamesSet.toArray(new String[manyToOneAssociationNamesSet.size()]);

        Map<String, String> manyToManyAssociationsMap = metadataEntity.getManyToManyAssociations();
        this.manyToManyAssociations = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : manyToManyAssociationsMap.entrySet()) {
            String propertyName = entry.getKey();
            String dbColumnName = metadataEntity.getDbColumnName(propertyName);
            String tableName = metadataEntity.getBdManyToManyAssociationTableName(propertyName);
            String typeName = entry.getValue();
            ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(typeName);
            Class<? extends ReferentialDto> referentialype = BinderEngine.get().getReferentialDtoType(entityEnum);
            ManyToManyAssociationStruct manyToManyAssociation = new ManyToManyAssociationStruct(propertyName, dbColumnName, tableName, referentialype);
            manyToManyAssociations.add(manyToManyAssociation);
        }
        this.columnNames = computeColumnNames(metadataEntity, simplePropertyNames, manyToOneAssociationNames);
        this.binder = BinderFactory.newBinder(dtoType);
    }

    public String generateSql(R referentialDto) {

        List<String> parameters = new LinkedList<>();

        addStringParameter(referentialDto.getId(), parameters);
        addOtherTypeParameter(referentialDto.getVersion(), parameters);
        addTimestampParameter(referentialDto.getCreateDate(), parameters);

        Map<String, Object> simpleParameters = binder.obtainProperties(referentialDto, true, true, simplePropertyNames);
        for (String simplePropertyName : simplePropertyNames) {
            Object parameterValue = simpleParameters.get(simplePropertyName);

            if (primitiveBooleanPropertyNames.contains(simplePropertyName)) {
                addPrimitiveBooleanParameter((Boolean) parameterValue, parameters);
                continue;
            }
            if (primitiveIntegerPropertyNames.contains(simplePropertyName)) {
                addPrimitiveIntegerParameter((Integer) parameterValue, parameters);
                continue;
            }
            if (primitiveLongPropertyNames.contains(simplePropertyName)) {
                addPrimitiveLongParameter((Long) parameterValue, parameters);
                continue;
            }
            if (primitiveFloatPropertyNames.contains(simplePropertyName)) {
                addPrimitiveFloatParameter((Float) parameterValue, parameters);
                continue;
            }


            if (parameterValue == null) {
                addNullParameter(parameters);
                continue;
            }
            if (parameterValue instanceof String) {
                addStringParameter((String) parameterValue, parameters);
                continue;
            }
            if (parameterValue instanceof Date) {
                addDateParameter((Date) parameterValue, parameters);
                continue;
            }
            if (parameterValue instanceof Enum) {
                addEnumParameter((Enum) parameterValue, parameters);
                continue;
            }
            addOtherTypeParameter(parameterValue, parameters);
        }

        Map<String, Object> manyToOneParameters = binder.obtainProperties(referentialDto, true, true, manyToOneAssociationNames);
        for (String manyToOneAssociationName : manyToOneAssociationNames) {
            Object parameterValue = manyToOneParameters.get(manyToOneAssociationName);
            if (parameterValue == null) {
                addNullParameter(parameters);
                continue;
            }
            if (parameterValue instanceof ReferentialDto) {
                addReferentialDtoParameter((ReferentialDto) parameterValue, parameters);
                continue;
            }
            if (parameterValue instanceof ReferentialReference) {
                addReferentialReferenceParameter((ReferentialReference) parameterValue, parameters);
            }
        }

        String result = String.format(INSERT_STATEMENT,
                                      schemaName,
                                      tableName,
                                      String.join(",", columnNames),
                                      String.join(",", parameters));

        if (log.isDebugEnabled()) {
            log.debug("sql: " + result);
        }
        for (ManyToManyAssociationStruct manyToManyAssociation : manyToManyAssociations) {
            String nmAssociationSql = generateNmAssociationSql(referentialDto, manyToManyAssociation);
            result += nmAssociationSql;
        }

        return result;

    }

    private String generateNmAssociationSql(R referentialDto, ManyToManyAssociationStruct manyToManyAssociationStruct) {

        StringBuilder builder = new StringBuilder();

        Collection<ReferentialReference<?>> manyToManyAssociationValues = binder.obtainSourceProperty(referentialDto, manyToManyAssociationStruct.propertyName);
        if (CollectionUtils.isNotEmpty(manyToManyAssociationValues)) {

            String nmAssociationTableName = manyToManyAssociationStruct.tableName;
            String nmAssociationDbColumnName = manyToManyAssociationStruct.dbColumnName;
            String referentialDtoId = referentialDto.getId();

            for (ReferentialReference<?> nmAssociationValue : manyToManyAssociationValues) {

                addMnAssociation(nmAssociationTableName, nmAssociationDbColumnName, referentialDtoId, manyToManyAssociationStruct.type, nmAssociationValue.getId(), builder);

            }
        }

        return builder.toString();

    }

    protected <D extends ReferentialDto> void addMnAssociation(String nmAssociationTableName,
                                                               String nmAssociationDbColumnName,
                                                               String referentialDtoId,
                                                               Class<D> associationType,
                                                               String associationId,
                                                               StringBuilder builder) {


        String sql = String.format(NM_ASSOCIATION_INSERT_STATEMENT,
                                   schemaName,
                                   nmAssociationTableName,
                                   this.tableName,
                                   nmAssociationDbColumnName,
                                   referentialDtoId,
                                   associationId);
        if (log.isDebugEnabled()) {
            log.debug("sql: " + sql);
        }
        builder.append(sql);

    }

    private Set<String> computeColumnNames(TopiaMetadataEntity metadataEntity,
                                           String[] simplePropertyNames,
                                           String[] compositionPropertyNames) {
        Set<String> columnNames = new LinkedHashSet<>();
        columnNames.add(TopiaEntity.PROPERTY_TOPIA_ID);
        columnNames.add(TopiaEntity.PROPERTY_TOPIA_VERSION);
        columnNames.add(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);

        for (String propertyName : simplePropertyNames) {
            columnNames.add(metadataEntity.getDbColumnName(propertyName));
        }
        for (String propertyName : compositionPropertyNames) {
            columnNames.add(metadataEntity.getDbColumnName(propertyName));
        }
        return columnNames;
    }

    private void addNullParameter(List<String> parameters) {
        parameters.add("NULL");
    }

    private void addStringParameter(String parameter, List<String> parameters) {
        parameters.add("'" + parameter.replaceAll("'", "''") + "'");
    }

    private void addDateParameter(Date parameter, List<String> parameters) {
        parameters.add("'" + new Timestamp(parameter.getTime()) + "'");
    }

    private void addPrimitiveBooleanParameter(Boolean parameter, List<String> parameters) {
        parameters.add("" + (parameter != null && parameter));
    }

    private void addPrimitiveIntegerParameter(Integer parameter, List<String> parameters) {
        parameters.add("" + (parameter == null ? 0 : parameter));
    }

    private void addPrimitiveLongParameter(Long parameter, List<String> parameters) {
        parameters.add("" + (parameter == null ? 0 : parameter));
    }

    private void addPrimitiveFloatParameter(Float parameter, List<String> parameters) {
        parameters.add("" + (parameter == null ? 0f : parameter));
    }

    private void addTimestampParameter(Date parameter, List<String> parameters) {
        parameters.add("'" + new Timestamp(parameter.getTime()) + "'::timestamp");
    }

    private void addEnumParameter(Enum parameter, List<String> parameters) {
        parameters.add("" + parameter.ordinal());
    }

    private void addOtherTypeParameter(Object parameter, List<String> parameters) {
        parameters.add("" + parameter);
    }

    protected void addReferentialReferenceParameter(ReferentialReference parameter, List<String> parameters) {
        addStringParameter(parameter.getId(), parameters);
    }

    protected void addReferentialDtoParameter(ReferentialDto parameter, List<String> parameters) {
        addStringParameter(parameter.getId(), parameters);
    }

    /**
     * Pour décrire une association nm.
     */
    private static class ManyToManyAssociationStruct {

        /**
         * Le nom de la propriété dans l'objet.
         */
        private final String propertyName;
        /**
         * Le nom de la colonne de l'association dans la table d'association.
         */
        private final String dbColumnName;
        /**
         * Le nom de la table d'association.
         */
        private final String tableName;

        private final Class<? extends ReferentialDto> type;

        private ManyToManyAssociationStruct(String propertyName, String dbColumnName, String tableName, Class<? extends ReferentialDto> type) {
            this.propertyName = propertyName;
            this.dbColumnName = dbColumnName;
            this.tableName = tableName;
            this.type = type;
        }

    }
}
