package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BasketBinder extends DataBinderSupport<Basket, BasketDto> {

    public BasketBinder() {
        super(Basket.class, BasketDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, BasketDto dto, Basket entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSettingIdentifier(dto.getSettingIdentifier());
        entity.setHaulingIdentifier(dto.getHaulingIdentifier());
        entity.setFloatline1Length(dto.getFloatline1Length());
        entity.setFloatline2Length(dto.getFloatline2Length());
        entity.setBranchline(toEntitySet(referentialLocale, dto.getBranchline(), Branchline.class, entity.getBranchline()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Basket entity, BasketDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSettingIdentifier(entity.getSettingIdentifier());
        dto.setHaulingIdentifier(entity.getHaulingIdentifier());
        dto.setFloatline1Length(entity.getFloatline1Length());
        dto.setFloatline2Length(entity.getFloatline2Length());
        dto.setBranchline(toLinkedHashSetData(referentialLocale, entity.getBranchline(), BranchlineDto.class));

    }

    @Override
    public DataReference<BasketDto> toDataReference(ReferentialLocale referentialLocale, Basket entity) {

        return toDataReference(entity, entity.getSettingIdentifier(), entity.getHaulingIdentifier());

    }

    @Override
    public DataReference<BasketDto> toDataReference(ReferentialLocale referentialLocale, BasketDto dto) {

        return toDataReference(dto, dto.getSettingIdentifier(), dto.getHaulingIdentifier());

    }
}
