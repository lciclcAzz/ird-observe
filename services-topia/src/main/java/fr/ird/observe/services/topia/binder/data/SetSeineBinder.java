package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.seine.ReasonForNullSet;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SetSeineBinder extends DataBinderSupport<SetSeine, SetSeineDto> {

    public SetSeineBinder() {
        super(SetSeine.class, SetSeineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SetSeineDto dto, SetSeine entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setStartTime(dto.getStartTime());
        entity.setEndPursingTimeStamp(dto.getEndPursingTimeStamp());
        entity.setEndSetTimeStamp(dto.getEndSetTimeStamp());
        entity.setMaxGearDepth(dto.getMaxGearDepth());
        entity.setCurrentSpeed(dto.getCurrentSpeed());
        entity.setCurrentDirection(dto.getCurrentDirection());
        entity.setSchoolTopDepth(dto.getSchoolTopDepth());
        entity.setSchoolMeanDepth(dto.getSchoolMeanDepth());
        entity.setSchoolThickness(dto.getSchoolThickness());
        entity.setSonarUsed(dto.isSonarUsed());
        entity.setSupportVesselName(dto.getSupportVesselName());
        entity.setTargetDiscarded(dto.getTargetDiscarded());
        entity.setNonTargetDiscarded(dto.getNonTargetDiscarded());
        entity.setCurrentMeasureDepth(dto.getCurrentMeasureDepth());
        entity.setLastUpdateDate(dto.getLastUpdateDate());
        entity.setSchoolType(SCHOOL_TYPE_TO_ENTITY.apply(dto.getSchoolType()));
        entity.setReasonForNullSet(toEntity(dto.getReasonForNullSet(), ReasonForNullSet.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SetSeine entity, SetSeineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setStartTime(entity.getStartTime());
        dto.setEndPursingTimeStamp(entity.getEndPursingTimeStamp());
        dto.setEndSetTimeStamp(entity.getEndSetTimeStamp());
        dto.setMaxGearDepth(entity.getMaxGearDepth());
        dto.setCurrentSpeed(entity.getCurrentSpeed());
        dto.setCurrentDirection(entity.getCurrentDirection());
        dto.setSchoolTopDepth(entity.getSchoolTopDepth());
        dto.setSchoolMeanDepth(entity.getSchoolMeanDepth());
        dto.setSchoolThickness(entity.getSchoolThickness());
        dto.setSonarUsed(entity.isSonarUsed());
        dto.setSupportVesselName(entity.getSupportVesselName());
        dto.setTargetDiscarded(entity.getTargetDiscarded());
        dto.setNonTargetDiscarded(entity.getNonTargetDiscarded());
        dto.setCurrentMeasureDepth(entity.getCurrentMeasureDepth());

        dto.setSchoolType(SCHOOL_TYPE_TO_DTO.apply(entity.getSchoolType()));
        dto.setReasonForNullSet(toReferentialReference(referentialLocale, entity.getReasonForNullSet(), ReasonForNullSetDto.class));

    }

    @Override
    public DataReference<SetSeineDto> toDataReference(ReferentialLocale referentialLocale, SetSeine entity) {

        return toDataReference(entity, entity.getComment());

    }

    @Override
    public DataReference<SetSeineDto> toDataReference(ReferentialLocale referentialLocale, SetSeineDto dto) {

        return toDataReference(dto, dto.getComment());

    }

}
