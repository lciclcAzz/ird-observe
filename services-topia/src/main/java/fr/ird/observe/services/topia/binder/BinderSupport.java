package fr.ird.observe.services.topia.binder;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.constants.GearTypePersist;
import fr.ird.observe.entities.constants.ReferenceStatusPersist;
import fr.ird.observe.entities.constants.TripMapPointTypePersist;
import fr.ird.observe.entities.constants.seine.NonTargetCatchComputedValueSourcePersist;
import fr.ird.observe.entities.constants.seine.OwnershipPersist;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.constants.seine.TypeTransmittingBuoyOperationPersist;
import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.I18nReferentialEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.constants.ReferenceStatus;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.constants.TripMapPointType;
import fr.ird.observe.services.dto.constants.seine.NonTargetCatchComputedValueSource;
import fr.ird.observe.services.dto.constants.seine.Ownership;
import fr.ird.observe.services.dto.constants.seine.SchoolType;
import fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperation;
import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.I18nReferentialHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Function;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class BinderSupport<E extends TopiaEntity, D extends IdDto> {

    protected static final Function<TripMapPointType, TripMapPointTypePersist> TRIP_MAP_POINT_TO_ENTITY = input -> TripMapPointTypePersist.valueOf(input.name());

    protected static final Function<GearType, GearTypePersist> GEAR_TYPE_TO_ENTITY = input -> GearTypePersist.valueOf(input.name());

    protected static final Function<ReferenceStatus, ReferenceStatusPersist> REFERENCE_STATUS_TO_ENTITY = input -> ReferenceStatusPersist.valueOf(input.name());

    protected static final Function<Ownership, OwnershipPersist> OWNERSHIP_TO_ENTITY = input -> input == null ? null : OwnershipPersist.valueOf(input.name());

    protected static final Function<SchoolType, SchoolTypePersist> SCHOOL_TYPE_TO_ENTITY = input -> input == null ? null : SchoolTypePersist.valueOf(input.name());

    protected static final Function<TypeTransmittingBuoyOperation, TypeTransmittingBuoyOperationPersist> TYPE_TRANSMITTING_BUOY_OPERATION_TO_ENTITY = input -> input == null ? null : TypeTransmittingBuoyOperationPersist.valueOf(input.name());

    protected static final Function<NonTargetCatchComputedValueSource, NonTargetCatchComputedValueSourcePersist> NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_ENTITY = input -> input == null ? null : NonTargetCatchComputedValueSourcePersist.valueOf(input.name());

    protected static final Function<TripMapPointTypePersist, TripMapPointType> TRIP_MAP_POINT_TO_DTO = input -> TripMapPointType.valueOf(input.name());

    protected static final Function<GearTypePersist, GearType> GEAR_TYPE_TO_DTO = input -> input == null ? null : GearType.valueOf(input.name());

    protected static final Function<ReferenceStatusPersist, ReferenceStatus> REFERENCE_STATUS_TO_DTO = input -> input == null ? null : ReferenceStatus.valueOf(input.name());

    protected static final Function<OwnershipPersist, Ownership> OWNERSHIP_TO_DTO = input -> input == null ? null : Ownership.valueOf(input.name());

    protected static final Function<SchoolTypePersist, SchoolType> SCHOOL_TYPE_TO_DTO = input -> input == null ? null : SchoolType.valueOf(input.name());

//    protected static final Function<fr.ird.observe.entities.constants.seine.TypeTransmittingBuoyOperationPersist, fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperationPersist> TRANSMITTING_BUOY_OPERATION_TO_DTO = new Function<fr.ird.observe.entities.constants.seine.TypeTransmittingBuoyOperationPersist, fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperationPersist>() {
//
//        @Override
//        public fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperationPersist apply(fr.ird.observe.entities.constants.seine.TypeTransmittingBuoyOperationPersist input) {
//            return input == null ? null : fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperationPersist.valueOf(input.name());
//        }
//    };

    protected static final Function<NonTargetCatchComputedValueSourcePersist, NonTargetCatchComputedValueSource> NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_DTO = input -> input == null ? null : NonTargetCatchComputedValueSource.valueOf(input.name());

    protected final Class<E> entityType;

    protected final Class<D> dtoType;

    protected BinderSupport(Class<E> entityType, Class<D> dtoType) {
        this.entityType = entityType;
        this.dtoType = dtoType;
    }

    public abstract void copyToEntity(ReferentialLocale referentialLocale, D dto, E entity);

    public abstract void copyToDto(ReferentialLocale referentialLocale, E entity, D dto);

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL REFERENCE → ENTITY ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <DD extends ReferentialDto, EE extends ObserveReferentialEntity> EE toEntity(ReferentialReference<DD> reference, Class<EE> entityType) {

        EE entity = null;

        if (reference != null) {

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(reference.getType());

            entity = binder.toEntity(reference);

        }

        return entity;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL → ENTITY -------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <DD extends ReferentialDto, EE extends ObserveReferentialEntity> LinkedHashSet<EE> toEntitySet(Collection<ReferentialReference<DD>> references, Class<EE> entityType) {

        LinkedHashSet<EE> entityList = null;
        if (CollectionUtils.isNotEmpty(references)) {

            entityList = new LinkedHashSet<>(references.size());

            ReferentialReference<DD> firstReference = Iterables.get(references, 0, null);
            Class<DD> type = firstReference.getType();
            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(type);

            for (ReferentialReference<DD> reference : references) {

                EE entity = binder.toEntity(reference);
                entityList.add(entity);

            }

        }
        return entityList;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → REFERENTIAL REFERENCE ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <EE extends ObserveReferentialEntity, DD extends ReferentialDto> ReferentialReference<DD> toReferentialReference(ReferentialLocale referentialLocale, EE entity, Class<DD> dtoType) {

        ReferentialReference<DD> reference = null;
        if (entity != null) {

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(dtoType);
            reference = binder.toReferentialReference(referentialLocale, entity);

        }
        return reference;

    }

    protected <EE extends ObserveReferentialEntity, DD extends ReferentialDto> List<ReferentialReference<DD>> toReferentialReferenceList(ReferentialLocale referentialLocale, Collection<EE> entities, Class<DD> dtoType) {

        List<ReferentialReference<DD>> references = null;
        if (CollectionUtils.isNotEmpty(entities)) {

            references = new ArrayList<>(entities.size());

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(dtoType);

            for (EE entity : entities) {

                ReferentialReference<DD> reference = binder.toReferentialReference(referentialLocale, entity);
                references.add(reference);

            }

        }
        return references;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- LABELS ---------------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected String getLabel(ReferentialLocale locale, I18nReferentialEntity entity) {
        return entity == null ? "Non trouvé" : I18nReferenceEntities.getLabel(locale.ordinal(), entity);
    }

    protected String getLabel(ReferentialLocale locale, I18nReferentialDto dto) {
        return I18nReferentialHelper.decorate(locale.ordinal(), dto);
    }

    protected String getLabel(ReferentialLocale locale, ReferentialReference dto) {
        return (String) dto.getPropertyValue(ReferentialReference.PROPERTY_LABEL);
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- CREER DES ENTITY OU DTO ----------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected E newEntity() {
        try {
            return (E) ObserveEntityEnum.valueOf(entityType).getImplementation().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("What ever");
        }
    }

    protected D newDto() {
        try {
            return dtoType.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("What ever");
        }
    }

}
