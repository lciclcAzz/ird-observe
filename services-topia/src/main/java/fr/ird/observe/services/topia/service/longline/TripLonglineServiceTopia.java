package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.TripMapPoint;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.longline.TripLonglineTopiaDao;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.Species2;
import fr.ird.observe.entities.referentiel.SpeciesList;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceList;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.topia.entity.TripMapDtoFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripLonglineServiceTopia extends ObserveServiceTopia implements TripLonglineService {

    private static final Log log = LogFactory.getLog(TripLonglineServiceTopia.class);

    @Override
    public DataReferenceList<TripLonglineDto> getAllTripLongline() {
        if (log.isTraceEnabled()) {
            log.trace("getAllTripLongline()");
        }

        List<TripLongline> tripLonglines = loadEntities(TripLongline.class);

        tripLonglines.sort(Comparator.comparing(TripLongline::getStartDate));
        return toDataReferenceList(TripLonglineDto.class, tripLonglines);

    }

    @Override
    public DataReferenceList<TripLonglineDto> getTripLonglineByProgram(String programId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripLonglineByProgram(" + programId + ")");
        }

        List<TripLongline> tripLonglines = getDao().findAllStubByProgramId(programId, getReferentialLocale().ordinal());

        return toDataReferenceList(TripLonglineDto.class, tripLonglines);

    }


    @Override
    public TripLonglineDto loadDto(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + tripLonglineId + ")");
        }
        return loadEntityToDataDto(TripLonglineDto.class, tripLonglineId);

    }

    @Override
    public int getTripLonglinePositionInProgram(String programId, String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripLonglinePositionInProgram(" + programId + ", " + tripLonglineId + ")");
        }

        return getDao().findPositionByProgramId(programId, tripLonglineId);

    }

    @Override
    public TripMapDto getTripLonglineMap(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripLonglineMap(" + tripLonglineId + ")");
        }

        LinkedHashSet<TripMapPoint> points = getDao().extractTripMapActivityPoints(tripLonglineId);

        return TripMapDtoFactory.newTripMapDto(tripLonglineId, points);

    }

    @Override
    public DataReference<TripLonglineDto> loadReferenceToRead(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + tripLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        return toReference(tripLongline);

    }

    @Override
    public boolean exists(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + tripLonglineId + ")");
        }

        return existsEntity(TripLongline.class, tripLonglineId);

    }

    @Override
    public Form<TripLonglineDto> loadForm(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + tripLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        Form<TripLonglineDto> form = dataEntityToForm(TripLonglineDto.class,
                                                      tripLongline,
                                                      ReferenceSetRequestDefinitions.TRIP_LONGLINE_FORM);

        TripLonglineDto tripLonglineDto = form.getObject();

        if (tripLonglineDto.getEndDate() == null) {
            Date date = DateUtil.getEndOfDay(now());
            tripLonglineDto.setEndDate(date);
        }

        return form;

    }

    @Override
    public Form<TripLonglineDto> preCreate(String programId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + programId + ")");
        }

        TripLongline tripLongline = newEntity(TripLongline.class);

        Program program = loadEntity(ProgramDto.class, programId);

        Date date = DateUtil.getDay(now());

        tripLongline.setStartDate(date);

        tripLongline.setEndDate(date);

        tripLongline.setProgram(program);

        return dataEntityToForm(TripLonglineDto.class,
                                tripLongline,
                                ReferenceSetRequestDefinitions.TRIP_LONGLINE_FORM);

    }

    @Override
    public SaveResultDto save(TripLonglineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        TripLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        TripLonglineTopiaDao dao = getTopiaPersistenceContext().getTripLonglineDao();
        dao.updateEndDate(entity);

        return result;

    }

    @Override
    public void delete(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + tripLonglineId + ")");
        }

        deleteEntity(TripLonglineDto.class, TripLongline.class, Collections.singleton(tripLonglineId));
    }

    @Override
    public int moveTripLonglineToProgram(String tripLonglineId, String programId) {
        if (log.isTraceEnabled()) {
            log.trace("moveTripLonglineToProgram(" + tripLonglineId + ", " + programId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);
        Program program = loadEntity(ProgramDto.class, programId);

        tripLongline.setProgram(program);
        saveEntity(tripLongline);

        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();
        persistenceContext.flush();

        return getTripLonglinePositionInProgram(programId, tripLonglineId);

    }

    @Override
    public List<Integer> moveTripLonglinesToProgram(List<String> tripLonglineIds, String programId) {
        if (log.isTraceEnabled()) {
            log.trace("moveTripLonglinesToProgram([" + Joiner.on(", ").join(tripLonglineIds) + "], " + programId + ")");
        }

        Program program = loadEntity(ProgramDto.class, programId);

        List<Integer> result = new ArrayList<>();
        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();

        for (String tripLonglineId : tripLonglineIds) {

            TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);
            tripLongline.setProgram(program);

            saveEntity(tripLongline);
            persistenceContext.flush();

            result.add(getTripLonglinePositionInProgram(programId, tripLonglineId));
        }

        return result;

    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripLonglineId, String speciesListId) {
        if (log.isTraceEnabled()) {
            log.trace("getSpeciesByListAndTrip(" + tripLonglineId + ", " + speciesListId + ")");
        }

        // find Ocean
        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        Ocean ocean = tripLongline.getOcean();

        SpeciesList speciesList = loadEntity(SpeciesListDto.class, speciesListId);

        List<Species> species2 = Species2.filterByOcean(speciesList.getSpecies(), ocean);

        ReferentialBinderSupport<Species, SpeciesDto> binder = getReferentialBinder(SpeciesDto.class);

        ReferentialLocale referentialLocale = getReferentialLocale();

        ImmutableList.Builder<ReferentialReference<SpeciesDto>> references = ImmutableList.builder();

        for (Species species : species2) {

            ReferentialReference<SpeciesDto> reference = binder.toReferentialReference(referentialLocale, species);
            references.add(reference);

        }

        return references.build();
    }

    protected TripLonglineTopiaDao getDao() {
        return (TripLonglineTopiaDao) serviceContext.getTopiaPersistenceContext().getDao(TripLongline.class);
    }
}
