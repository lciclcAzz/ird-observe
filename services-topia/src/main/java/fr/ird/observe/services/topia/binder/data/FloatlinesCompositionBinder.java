package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.FloatlinesComposition;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FloatlinesCompositionBinder extends DataBinderSupport<FloatlinesComposition, FloatlinesCompositionDto> {

    public FloatlinesCompositionBinder() {
        super(FloatlinesComposition.class, FloatlinesCompositionDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, FloatlinesCompositionDto dto, FloatlinesComposition entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setLength(dto.getLength());
        entity.setProportion(dto.getProportion());
        entity.setLineType(toEntity(dto.getLineType(), LineType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, FloatlinesComposition entity, FloatlinesCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setLength(entity.getLength());
        dto.setProportion(entity.getProportion());
        dto.setLineType(toReferentialReference(referentialLocale, entity.getLineType(), LineTypeDto.class));

    }

    @Override
    public DataReference<FloatlinesCompositionDto> toDataReference(ReferentialLocale referentialLocale, FloatlinesComposition entity) {
        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getLineType()),
                               entity.getLength(),
                               entity.getProportion());
    }

    @Override
    public DataReference<FloatlinesCompositionDto> toDataReference(ReferentialLocale referentialLocale, FloatlinesCompositionDto dto) {
        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getLineType()),
                               dto.getLength(),
                               dto.getProportion());
    }
}
