package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.GearUseFeaturesMeasurementLongline;
import fr.ird.observe.entities.referentiel.GearCaracteristic;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GearUseFeaturesMeasurementLonglineBinder extends DataBinderSupport<GearUseFeaturesMeasurementLongline, GearUseFeaturesMeasurementLonglineDto> {

    public GearUseFeaturesMeasurementLonglineBinder() {
        super(GearUseFeaturesMeasurementLongline.class, GearUseFeaturesMeasurementLonglineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, GearUseFeaturesMeasurementLonglineDto dto, GearUseFeaturesMeasurementLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setMeasurementValue(dto.getMeasurementValue());
        entity.setGearCaracteristic(toEntity(dto.getGearCaracteristic(), GearCaracteristic.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, GearUseFeaturesMeasurementLongline entity, GearUseFeaturesMeasurementLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setMeasurementValue(entity.getMeasurementValue());
        dto.setGearCaracteristic(toReferentialReference(referentialLocale, entity.getGearCaracteristic(), GearCaracteristicDto.class));

    }

}
