package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.ird.observe.entities.TripMapPoint;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.TripMapPointDto;
import fr.ird.observe.services.dto.constants.TripMapPointType;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 09/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripMapDtoFactory {

    protected static final Function<TripMapPoint, TripMapPointDto> TRIP_MAP_POINT_TRIP_MAP_POINT_DTO_FUNCTION = tripMapPoint -> {
        TripMapPointDto result = new TripMapPointDto();
        result.setLatitude(tripMapPoint.getLatitude());
        result.setLongitude(tripMapPoint.getLongitude());
        result.setTime(tripMapPoint.getTime());
        result.setType(TripMapPointType.valueOf(tripMapPoint.getType().name()));
        return result;
    };

    public static TripMapDto newTripMapDto(String tripId, Set<TripMapPoint> points) {
        TripMapDto tripMapDto = new TripMapDto();
        tripMapDto.setId(tripId);
        LinkedHashSet<TripMapPointDto> pointDtos = Sets.newLinkedHashSet(Iterables.transform(points, TRIP_MAP_POINT_TRIP_MAP_POINT_DTO_FUNCTION::apply));
        tripMapDto.setPoints(pointDtos);
        return tripMapDto;
    }
}
