package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.services.service.seine.SetSeineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.service.DataNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SetSeineServiceTopia extends ObserveServiceTopia implements SetSeineService {

    private static final Log log = LogFactory.getLog(SetSeineServiceTopia.class);

    @Override
    public DataReference<SetSeineDto> loadReferenceToRead(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        ReferentialLocale referenceLocale = getReferentialLocale();

        DataBinderSupport<SetSeine, SetSeineDto> binder = getDataBinder(SetSeineDto.class);

        return binder.toDataReference(referenceLocale, setSeine);
    }

    @Override
    public SetSeineDto loadDto(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + setSeineId + ")");
        }

        return loadEntityToDataDto(SetSeineDto.class, setSeineId);
    }

    @Override
    public boolean exists(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + setSeineId + ")");
        }

        return existsEntity(SetSeine.class, setSeineId);
    }

    @Override
    public Form<SetSeineDto> loadForm(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        return dataEntityToForm(SetSeineDto.class,
                                setSeine,
                                ReferenceSetRequestDefinitions.SET_SEINE_FORM);
    }

    @Override
    public Form<SetSeineDto> preCreate(String routeId, String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + routeId + ", " + activitySeineId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        if (!route.containsActivitySeine(activitySeine)) {
            throw new DataNotFoundException(ActivitySeineDto.class, activitySeineId);
        }


        // on utilise l'heure de l'activité comme début de calée
        Date date = DateUtil.getTime(activitySeine.getTime(), false, false);

        SetSeine preCreated = newEntity(SetSeine.class);

        preCreated.setStartTime(date);

        // pour les dates de fin on utilise la date de la route
        Date routeDate = route.getDate();

        preCreated.setEndSetTimeStamp(date);
        preCreated.setEndPursingTimeStamp(date);

        preCreated.setEndSetDate(routeDate);
        preCreated.setEndPursingDate(routeDate);

        // recuperation du type de set a partir de l'activity
        SchoolTypePersist schoolType = activitySeine.getSchoolType();
        preCreated.setSchoolType(schoolType);

        return dataEntityToForm(SetSeineDto.class,
                                preCreated,
                                ReferenceSetRequestDefinitions.SET_SEINE_FORM);

    }

    @Override
    public SaveResultDto save(String activitySeineId, SetSeineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + activitySeineId + ", " + dto.getId() + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        SetSeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        if (dto.isNotPersisted()) {

            activitySeine.setSetSeine(entity);

        }

        return result;

    }

    @Override
    public void delete(String activitySeineId, String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + activitySeineId + ", " + setSeineId + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        if (!activitySeine.getSetSeine().equals(setSeine)) {
            throw new DataNotFoundException(SetSeineDto.class, setSeineId);
        }

        activitySeine.setSetSeine(null);

    }

}
