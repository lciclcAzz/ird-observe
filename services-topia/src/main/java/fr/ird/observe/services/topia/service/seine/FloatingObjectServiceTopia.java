package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.entities.seine.FloatingObjectTopiaDao;
import fr.ird.observe.services.service.seine.FloatingObjectService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.service.DataNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FloatingObjectServiceTopia extends ObserveServiceTopia implements FloatingObjectService {

    private static final Log log = LogFactory.getLog(FloatingObjectServiceTopia.class);

    @Override
    public DataReferenceSet<FloatingObjectDto> getFloatingObjectByActivitySeine(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getFloatingObjectByActivitySeine(" + activitySeineId + ")");
        }

        ReferentialLocale referenceLocale = getReferentialLocale();

        FloatingObjectTopiaDao dao = getTopiaPersistenceContext().getFloatingObjectDao();
        List<FloatingObject> dtos = dao.findAllStubByActivityId(activitySeineId, referenceLocale.ordinal());

        return toDataReferenceSet(FloatingObjectDto.class, dtos);

    }

    @Override
    public DataReference<FloatingObjectDto> loadReferenceToRead(String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + floatingObjectId + ")");
        }

        FloatingObject floatingObject = loadEntity(FloatingObjectDto.class, floatingObjectId);
        return toReference(floatingObject);

    }

    @Override
    public FloatingObjectDto loadDto(String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + floatingObjectId + ")");
        }

        return loadEntityToDataDto(FloatingObjectDto.class, floatingObjectId);
    }

    @Override
    public boolean exists(String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + floatingObjectId + ")");
        }

        return existsEntity(FloatingObject.class, floatingObjectId);
    }

    @Override
    public Form<FloatingObjectDto> loadForm(String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + floatingObjectId + ")");
        }

        FloatingObject floatingObject = loadEntity(FloatingObjectDto.class, floatingObjectId);

        return dataEntityToForm(FloatingObjectDto.class,
                                floatingObject,
                                ReferenceSetRequestDefinitions.FLOATING_OBJECT_FORM);
    }

    @Override
    public Form<FloatingObjectDto> preCreate(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + activitySeineId + ")");
        }

        FloatingObject floatingObject = newEntity(FloatingObject.class);

        return dataEntityToForm(FloatingObjectDto.class,
                                floatingObject,
                                ReferenceSetRequestDefinitions.FLOATING_OBJECT_FORM);
    }

    @Override
    public SaveResultDto save(String activitySeineId, FloatingObjectDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + activitySeineId + ", " + dto.getId() + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        FloatingObject entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        if (dto.isNotPersisted()) {
            activitySeine.addFloatingObject(entity);
        }

        return result;

    }

    @Override
    public void delete(String activitySeineId, String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + activitySeineId + ", " + floatingObjectId + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        FloatingObject floatingObject = loadEntity(FloatingObjectDto.class, floatingObjectId);

        if (!activitySeine.containsFloatingObject(floatingObject)) {
            throw new DataNotFoundException(FloatingObjectDto.class, floatingObjectId);
        }

        activitySeine.removeFloatingObject(floatingObject);

    }

}
