package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.SectionDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SectionBinder extends DataBinderSupport<Section, SectionDto> {

    public SectionBinder() {
        super(Section.class, SectionDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SectionDto dto, Section entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSettingIdentifier(dto.getSettingIdentifier());
        entity.setHaulingIdentifier(dto.getHaulingIdentifier());
        entity.setBasket(toEntitySet(referentialLocale, dto.getBasket(), Basket.class, entity.getBasket()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Section entity, SectionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSettingIdentifier(entity.getSettingIdentifier());
        dto.setHaulingIdentifier(entity.getHaulingIdentifier());
        dto.setBasket(toLinkedHashSetData(referentialLocale, entity.getBasket(), BasketDto.class));

    }

    @Override
    public DataReference<SectionDto> toDataReference(ReferentialLocale referentialLocale, Section entity) {

        //FIXME-tchemit Je ne sais pas pourquoi mais si on utilise ces deux variables inline
        //FIXME-tchemit alors les valeurs sont inversées!!! pb proxy hibernate
        Integer settingIdentifier = entity.getSettingIdentifier();
        Integer haulingIdentifier = entity.getHaulingIdentifier();
        return toDataReference(entity, settingIdentifier, haulingIdentifier);

    }

    @Override
    public DataReference<SectionDto> toDataReference(ReferentialLocale referentialLocale, SectionDto dto) {

        return toDataReference(dto, dto.getSettingIdentifier(), dto.getHaulingIdentifier());

    }
}
