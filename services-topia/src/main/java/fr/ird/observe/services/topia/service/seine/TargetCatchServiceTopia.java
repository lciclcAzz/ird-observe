package fr.ird.observe.services.topia.service.seine;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.entities.seine.TargetCatchs;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.services.service.seine.TargetCatchService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TargetCatchServiceTopia extends ObserveServiceTopia implements TargetCatchService {

    private static final Log log = LogFactory.getLog(TargetCatchServiceTopia.class);

    @Override
    public Form<SetSeineTargetCatchDto> loadForm(String setSeineId, boolean discarded) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ", " + discarded + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineTargetCatchDto.class, setSeineId);

        Form<SetSeineTargetCatchDto> form = dataEntityToForm(
                SetSeineTargetCatchDto.class,
                setSeine,
                ReferenceSetRequestDefinitions.SET_SEINE_TARGET_CATCH_FORM
        );

        SetSeineTargetCatchDto setSeineTargetCatchDto = form.getObject();

        setSeineTargetCatchDto.setDiscarded(discarded);

        // on filtre les captures pour ne garder que les conservées ou les rejetées
        Collection<TargetCatchDto> allTargetCatchDtos = setSeineTargetCatchDto.getTargetCatch();

        List<TargetCatchDto> targetCatchDtos = TargetCatchHelper.filterDiscarded(allTargetCatchDtos, discarded);

        setSeineTargetCatchDto.setTargetCatch(targetCatchDtos);

        // on cherche si il y a des échantillons sur les captures
        TargetSample targetSample = setSeine.getTargetSample(discarded);

        if (targetSample != null) {

            Set<String> speciesSampleIds = targetSample.getTargetLength()
                                                       .stream()
                                                       .map(TargetLength::getSpecies)
                                                       .map(Species::getTopiaId)
                                                       .collect(Collectors.toSet());

            for (TargetCatchDto targetCatchDto : setSeineTargetCatchDto.getTargetCatch()) {

                boolean hasSample = speciesSampleIds.contains(targetCatchDto.getSpecies().getId());

                targetCatchDto.setHasSample(hasSample);
            }

        }

        return form;
    }

    @Override
    public SaveResultDto save(SetSeineTargetCatchDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        // le type de capture à traiter
        boolean discarded = dto.isDiscarded();

        // on affecte le discarded a toute les capture à injecter
        dto.getTargetCatch().forEach(t -> t.setDiscarded(discarded));

        // On conserve les identifiants des espèces utilisables dans les échantillons
        Set<String> speciesIds = dto.getTargetCatch().stream()
                                    .map(TargetCatchHelper.SPECIES_FUNCTION)
                                    .map(ReferentialReferences.ID_FUNCTION)
                                    .collect(Collectors.toSet());

        SetSeine entity = loadEntity(SetSeineTargetCatchDto.class, dto.getId());

        checkLastUpdateDate(entity, dto);

        // on conserve la liste des autres captures qu'on va ensuite réinjecter après recopie du dto
        List<TargetCatch> otherTargetCatches = TargetCatchs.filterDiscarded(entity.getTargetCatch(), !discarded);

        copyDataDtoToEntity(dto, entity);

        // on réinjecte les autres captures
        entity.addAllTargetCatch(otherTargetCatches);

        // on supprime les échantillons qui ne correspondent plus a des captures
        TargetSample targetSample = entity.getTargetSample(discarded);

        if (targetSample != null) {

            // Récupération de tous les échantillons qui ne sont pas des espèces capturés
            List<TargetLength> targetLengthToDelete = targetSample.getTargetLength()
                                                                  .stream()
                                                                  .filter(targetLength -> !speciesIds.contains(targetLength.getSpecies().getTopiaId()))
                                                                  .collect(Collectors.toList());

            targetLengthToDelete.forEach(targetSample::removeTargetLength);

        }

        // on repositionne la calée sur les captures
        for (TargetCatch targetCatch : entity.getTargetCatch()) {
            targetCatch.setSetSeine(entity);
        }

        // Calcul du champs SetSeine.targetDiscarded
        boolean targetDiscarded;

        if (discarded) {

            // on est sur des captures
            // il suffit que les données entrantes contiennent une capture
            targetDiscarded = dto.sizeTargetCatch() > 0;
        } else {

            // on est sur des rejets
            // il suffit que les autres données contiennent une capture
            targetDiscarded = otherTargetCatches.size() > 0;
        }
        if (log.isInfoEnabled()) {
            log.info("SetSeine " + entity.getTopiaId() + ", targetDiscarded: " + targetDiscarded);
        }
        entity.setTargetDiscarded(targetDiscarded);

        return saveEntity(entity);

    }

}
