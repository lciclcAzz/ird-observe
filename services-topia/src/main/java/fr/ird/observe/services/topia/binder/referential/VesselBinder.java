package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.VesselSizeCategory;
import fr.ird.observe.entities.referentiel.VesselType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class VesselBinder extends ReferentialBinderSupport<Vessel, VesselDto> {

    public VesselBinder() {
        super(Vessel.class, VesselDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, VesselDto dto, Vessel entity) {


        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);
        entity.setVesselSizeCategory(toEntity(dto.getVesselSizeCategory(), VesselSizeCategory.class));
        entity.setVesselType(toEntity(dto.getVesselType(), VesselType.class));
        entity.setFlagCountry(toEntity(dto.getFlagCountry(), Country.class));
        entity.setFleetCountry(dto.getFleetCountry());
        entity.setKeelCode(dto.getKeelCode());
        entity.setChangeDate(dto.getChangeDate());
        entity.setYearService(dto.getYearService());
        entity.setLength(dto.getLength());
        entity.setCapacity(dto.getCapacity());
        entity.setPower(dto.getPower());
        entity.setSearchMaximum(dto.getSearchMaximum());
        entity.setComment(dto.getComment());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Vessel entity, VesselDto dto) {


        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);
        dto.setVesselSizeCategory(toReferentialReference(referentialLocale, entity.getVesselSizeCategory(), VesselSizeCategoryDto.class));
        dto.setVesselType(toReferentialReference(referentialLocale, entity.getVesselType(), VesselTypeDto.class));
        dto.setFlagCountry(toReferentialReference(referentialLocale, entity.getFlagCountry(), CountryDto.class));
        dto.setFleetCountry(entity.getFleetCountry());
        dto.setKeelCode(entity.getKeelCode());
        dto.setChangeDate(entity.getChangeDate());
        dto.setYearService(entity.getYearService());
        dto.setLength(entity.getLength());
        dto.setCapacity(entity.getCapacity());
        dto.setPower(entity.getPower());
        dto.setSearchMaximum(entity.getSearchMaximum());
        dto.setComment(entity.getComment());

    }

    @Override
    public ReferentialReference<VesselDto> toReferentialReference(ReferentialLocale referentialLocale, Vessel entity) {

        return toReferentialReference(entity,
                                      entity.getCode(),
                                      getLabel(referentialLocale, entity),
                                      entity.getVesselType().getTopiaId());

    }

    @Override
    public ReferentialReference<VesselDto> toReferentialReference(ReferentialLocale referentialLocale, VesselDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(),
                                      getLabel(referentialLocale, dto),
                                      dto.getVesselType().getId());

    }
}
