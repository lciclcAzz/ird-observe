package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.BaitsComposition;
import fr.ird.observe.entities.referentiel.longline.BaitSettingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BaitsCompositionBinder extends DataBinderSupport<BaitsComposition, BaitsCompositionDto> {

    public BaitsCompositionBinder() {
        super(BaitsComposition.class, BaitsCompositionDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, BaitsCompositionDto dto, BaitsComposition entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setProportion(dto.getProportion());
        entity.setIndividualSize(dto.getIndividualSize());
        entity.setIndividualWeight(dto.getIndividualWeight());
        entity.setBaitSettingStatus(toEntity(dto.getBaitSettingStatus(), BaitSettingStatus.class));
        entity.setBaitType(toEntity(dto.getBaitType(), BaitType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, BaitsComposition entity, BaitsCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setProportion(entity.getProportion());
        dto.setIndividualSize(entity.getIndividualSize());
        dto.setIndividualWeight(entity.getIndividualWeight());
        dto.setBaitSettingStatus(toReferentialReference(referentialLocale, entity.getBaitSettingStatus(), BaitSettingStatusDto.class));
        dto.setBaitType(toReferentialReference(referentialLocale, entity.getBaitType(), BaitTypeDto.class));

    }

    @Override
    public DataReference<BaitsCompositionDto> toDataReference(ReferentialLocale referentialLocale, BaitsComposition entity) {
        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getBaitType()),
                               getLabel(referentialLocale, entity.getBaitSettingStatus()),
                               entity.getIndividualSize(),
                               entity.getIndividualWeight(),
                               entity.getProportion());
    }

    @Override
    public DataReference<BaitsCompositionDto> toDataReference(ReferentialLocale referentialLocale, BaitsCompositionDto dto) {
        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getBaitType()),
                               getLabel(referentialLocale, dto.getBaitSettingStatus()),
                               dto.getIndividualSize(),
                               dto.getIndividualWeight(),
                               dto.getProportion());
    }

}
