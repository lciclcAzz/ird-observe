package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeineImpl;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivitySeineStubDtoBinder extends DataBinderSupport<ActivitySeine, ActivitySeineStubDto> {

    public ActivitySeineStubDtoBinder() {
        super(ActivitySeine.class, ActivitySeineStubDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ActivitySeineStubDto dto, ActivitySeine entity) {
    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ActivitySeine entity, ActivitySeineStubDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setTime(entity.getTime());
        dto.setLastUpdateDate(entity.getLastUpdateDate());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());

        boolean isFindeVeille = entity.getVesselActivitySeine() != null
                && ActivitySeineImpl.ACTIVITY_FIN_DE_VEILLE.equals(entity.getVesselActivitySeine().getCode());

        dto.setActivityFinDeVeille(isFindeVeille);

    }

}
