package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Pour générer une requète sql de remplacement à partir d'un référentiel donné.
 * Created on 04/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class ReplaceSqlStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReplaceSqlStatementGenerator.class);

    private static final String MANY_TO_ONE_ASSOCIATION_UPDATE_STATEMENT = "UPDATE %s.%s SET %s = '%s', topiaVersion = topiaVersion + 1 WHERE %s = '%s';\n";
    private static final String MANY_TO_MANY_ASSOCIATION_UPDATE_STATEMENT = "UPDATE %s.%s SET %s = '%s' WHERE %s = '%s';\n";

    /**
     * Informations pour remplacer dans une relation many-to-one.
     */
    private final Set<ReplacementStruct> manyToOneAssociationReplacements;
    /**
     * Informations pour remplacer dans une relation many-to-many.
     */
    private final Set<ReplacementStruct> manyToManyAssociationReplacements;

    public ReplaceSqlStatementGenerator(TopiaMetadataModel topiaMetadataModel, String referentialName) {
        this.manyToOneAssociationReplacements = computeManyToOneAssociationReplacements(referentialName, topiaMetadataModel);
        this.manyToManyAssociationReplacements = computeManyToManyAssociationReplacements(referentialName, topiaMetadataModel);
    }

    public String generateSql(String sourceId, String replacementId) {

        StringBuilder builder = new StringBuilder();

        for (ReplacementStruct replacementStruct : manyToOneAssociationReplacements) {

            String sql = generateSqlStatement(MANY_TO_ONE_ASSOCIATION_UPDATE_STATEMENT, sourceId, replacementId, replacementStruct);
            builder.append(sql);

        }

        for (ReplacementStruct replacementStruct : manyToManyAssociationReplacements) {

            String sql = generateSqlStatement(MANY_TO_MANY_ASSOCIATION_UPDATE_STATEMENT, sourceId, replacementId, replacementStruct);
            builder.append(sql);
        }

        return builder.toString();

    }

    private Set<ReplacementStruct> computeManyToOneAssociationReplacements(String referentialName, TopiaMetadataModel topiaMetadataModel) {

        Set<ReplacementStruct> result = new LinkedHashSet<>();

        for (TopiaMetadataEntity metadataEntity : topiaMetadataModel) {

            result.addAll(metadataEntity.getManyToOneAssociations().entrySet().stream()
                                        .filter(entry -> entry.getValue().equals(referentialName))
                                        .map(entry -> new ReplacementStruct(metadataEntity.getDbSchemaName(),
                                                                            metadataEntity.getDbTableName(),
                                                                            metadataEntity.getDbColumnName(entry.getKey())))
                                        .collect(Collectors.toList()));

        }

        return result;

    }

    private Set<ReplacementStruct> computeManyToManyAssociationReplacements(String referentialName, TopiaMetadataModel topiaMetadataModel) {

        Set<ReplacementStruct> result = new LinkedHashSet<>();

        for (TopiaMetadataEntity metadataEntity : topiaMetadataModel) {

            result.addAll(metadataEntity.getManyToManyAssociations().entrySet().stream()
                                        .filter(entry -> entry.getValue().equals(referentialName))
                                        .map(entry -> new ReplacementStruct(metadataEntity.getDbSchemaName(),
                                                                            metadataEntity.getBdManyToManyAssociationTableName(entry.getKey()),
                                                                            metadataEntity.getDbColumnName(entry.getKey())))
                                        .collect(Collectors.toList()));

        }

        return result;

    }

    private String generateSqlStatement(String sqlPattern, String sourceId, String replacementId, ReplacementStruct replacementStruct) {

        String sql = String.format(sqlPattern,
                                   replacementStruct.schemaName,
                                   replacementStruct.tableName,
                                   replacementStruct.columnName,
                                   replacementId,
                                   replacementStruct.columnName,
                                   sourceId);
        if (log.isDebugEnabled()) {
            log.debug("sql: " + sql);
        }
        return sql;

    }

    private static class ReplacementStruct {

        private final String schemaName;
        private final String tableName;
        private final String columnName;

        private ReplacementStruct(String schemaName, String tableName, String columnName) {
            this.schemaName = schemaName;
            this.tableName = tableName;
            this.columnName = columnName;
        }

    }

}
