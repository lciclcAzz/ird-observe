package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;

/**
 * Pour générer une requète sql de suppression à partir d'un référentiel donné.
 *
 * Created on 29/06/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DeleteSqlStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DeleteSqlStatementGenerator.class);

    private static final String DELETE_STATEMENT = "DELETE FROM %s.%s WHERE topiaId = '%s';\n";

    private final String schemaName;
    private final String tableName;

    public DeleteSqlStatementGenerator(TopiaMetadataEntity metadataEntity) {
        this.schemaName = metadataEntity.getDbSchemaName();
        this.tableName = metadataEntity.getDbTableName();
    }

    public String generateSql(String id) {

        String result = String.format(DELETE_STATEMENT, schemaName, tableName, id);

        if (log.isDebugEnabled()) {
            log.debug("sql: " + result);
        }

        return result;

    }

}
