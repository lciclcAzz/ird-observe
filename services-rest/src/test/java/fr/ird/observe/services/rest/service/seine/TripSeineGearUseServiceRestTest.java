package fr.ird.observe.services.rest.service.seine;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceList;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.services.rest.service.AbstractServiceRestTest;
import fr.ird.observe.services.service.seine.TripSeineGearUseService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripSeineGearUseServiceRestTest extends AbstractServiceRestTest {

    protected TripSeineGearUseService service;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        service = newService(TripSeineGearUseService.class);
    }

    @Ignore //FIXME
    @Test
    public void loadToReadTest() {


        DataReferenceList<TripSeineDto> allTripSeine = newService(TripSeineService.class).getAllTripSeine();

        Map<String, DataReference<TripSeineDto>> tripIds = DataReference.splitById(allTripSeine.getReferences());

        Assume.assumeTrue("Marée " + ObserveFixtures.TRIP_SEINE_ID_1 + " non trouvée dans cette base. Test annulé", tripIds.containsKey(ObserveFixtures.TRIP_SEINE_ID_1));
        Form<TripSeineGearUseDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);

        TripSeineGearUseDto tripSeineGearUseDto = form.getObject();

        Assert.assertEquals(ObserveFixtures.TRIP_SEINE_ID_1, tripSeineGearUseDto.getId());
        //FIXME
//        Assert.assertEquals(2, tripSeineGearUseDto.sizeGearUseFeaturesSeine());
//
//        GearUseFeaturesSeineDto feature1 = Iterables.find(tripSeineGearUseDto.getGearUseFeaturesSeine(), IdHelper.newIdPredicate(GEAR_USE_FEATURES_SEINE_ID_1));
//
//        Assert.assertEquals(GEAR_USE_FEATURES_SEINE_ID_1, feature1.getId());
//        Assert.assertEquals("fr.ird.observe.entities.referentiel.Gear#1239832686125#0.25", feature1.getGear().getId());
//        Assert.assertEquals("Radeau", feature1.getGear().getPropertyValue("label"));
//        Assert.assertEquals(new Integer(1), feature1.getNumber());
//        Assert.assertEquals(Boolean.TRUE, feature1.getUsedInTrip());
//        Assert.assertEquals(3, feature1.sizeGearUseFeaturesMeasurement());
//
//        GearUseFeaturesMeasurementSeineDto measurement1 = IdHelper.findById(
//                feature1.getGearUseFeaturesMeasurement(),
//                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230663#0.8598592739610341");
//        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.20", measurement1.getGearCaracteristic().getId());
//        Assert.assertEquals("3", measurement1.getMeasurementValue());
//
//
//        GearUseFeaturesMeasurementSeineDto measurement2 = IdHelper.findById(
//                feature1.getGearUseFeaturesMeasurement(),
//                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230664#0.3389960469962563");
//        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.21", measurement2.getGearCaracteristic().getId());
//        Assert.assertEquals("true", measurement2.getMeasurementValue());
//
//
//        GearUseFeaturesMeasurementSeineDto measurement3 = IdHelper.findById(
//                feature1.getGearUseFeaturesMeasurement(),
//                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230665#0.018683933154531762");
//        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.22", measurement3.getGearCaracteristic().getId());
//        Assert.assertEquals("false", measurement3.getMeasurementValue());
//
//        Assert.assertNotNull(formDto.getLabels());
//
//        Collection<Class> types = Collections2.transform(formDto.getLabels(), ReferenceSetDtos.getTypeFunction());
//        Assert.assertTrue(types.contains(GearDto.class));
//        Assert.assertTrue(types.contains(GearCaracteristicDto.class));
//        Assert.assertEquals(2, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertEquals(0, referenceSetDto.sizeReference());
//
//        }
    }

    @Ignore //FIXME
    @Test
    public void loadToEditTest() {

        DataReferenceList<TripSeineDto> allTripSeine = newService(TripSeineService.class).getAllTripSeine();

        Map<String, DataReference<TripSeineDto>> tripIds = DataReference.splitById(allTripSeine.getReferences());

        Assume.assumeTrue("Marée " + ObserveFixtures.TRIP_SEINE_ID_1 + " non trouvée dans cette base. Test annulé", tripIds.containsKey(ObserveFixtures.TRIP_SEINE_ID_1));
        Form<TripSeineGearUseDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);
        //FIXME
//        assertReadLabels(formDto, 2,
//                         GearDto.class,
//                         GearCaracteristicDto.class);

//        TripSeineGearUseDto tripSeineGearUseDto = formDto.getForm();
//
//        {
//            Assert.assertEquals(TRIP_SEINE_ID_1, tripSeineGearUseDto.getId());
//            Assert.assertEquals(2, tripSeineGearUseDto.sizeGearUseFeaturesSeine());
//
//            GearUseFeaturesSeineDto feature1 = Iterables.find(tripSeineGearUseDto.getGearUseFeaturesSeine(), IdHelper.newIdPredicate(GEAR_USE_FEATURES_SEINE_ID_1));
//
//            Assert.assertEquals(GEAR_USE_FEATURES_SEINE_ID_1, feature1.getId());
//            Assert.assertEquals("fr.ird.observe.entities.referentiel.Gear#1239832686125#0.25", feature1.getGear().getId());
//            Assert.assertEquals("Radeau", feature1.getGear().getPropertyValue("label"));
//            Assert.assertEquals(new Integer(1), feature1.getNumber());
//            Assert.assertEquals(Boolean.TRUE, feature1.getUsedInTrip());
//            Assert.assertEquals(3, feature1.sizeGearUseFeaturesMeasurement());
//
//            GearUseFeaturesMeasurementSeineDto measurement1 = IdHelper.findById(
//                    feature1.getGearUseFeaturesMeasurement(),
//                    "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230663#0.8598592739610341");
//            Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.20", measurement1.getGearCaracteristic().getId());
//            Assert.assertEquals("3", measurement1.getMeasurementValue());
//
//
//            GearUseFeaturesMeasurementSeineDto measurement2 = IdHelper.findById(
//                    feature1.getGearUseFeaturesMeasurement(),
//                    "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230664#0.3389960469962563");
//            Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.21", measurement2.getGearCaracteristic().getId());
//            Assert.assertEquals("true", measurement2.getMeasurementValue());
//
//
//            GearUseFeaturesMeasurementSeineDto measurement3 = IdHelper.findById(
//                    feature1.getGearUseFeaturesMeasurement(),
//                    "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230665#0.018683933154531762");
//            Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.22", measurement3.getGearCaracteristic().getId());
//            Assert.assertEquals("false", measurement3.getMeasurementValue());
//
//            Assert.assertNotNull(formDto.getLabels());
//
//            Collection<Class> types = Collections2.transform(formDto.getLabels(), ReferenceSetDtos.getTypeFunction());
//            Assert.assertTrue(types.contains(GearDto.class));
//            Assert.assertTrue(types.contains(GearCaracteristicDto.class));
//            Assert.assertEquals(2, formDto.sizeLabels());
//
//            for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//                Assert.assertTrue(referenceSetDto.sizeReference() > 0);
//
//            }
//
//        }
    }

    @Ignore
    @Test
    public void saveUpdateTest() {
        Form<TripSeineGearUseDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        TripSeineGearUseDto tripSeineGearUseDto = form.getObject();

        GearUseFeaturesSeineDto featuresSeineDto = tripSeineGearUseDto.getGearUseFeaturesSeine()
                                                                      .stream()
                                                                      .filter(IdHelper.newIdPredicate(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID_1))
                                                                      .findFirst()
                                                                      .get();

        featuresSeineDto.setNumber(12);
        featuresSeineDto.setComment("Un Commentaire");
        for (GearUseFeaturesMeasurementSeineDto measurementSeineDto : featuresSeineDto.getGearUseFeaturesMeasurement()) {
            if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.20".equals(measurementSeineDto.getGearCaracteristic().getId())) {
                measurementSeineDto.setMeasurementValue("4");
            } else if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.21".equals(measurementSeineDto.getGearCaracteristic().getId())) {
                measurementSeineDto.setMeasurementValue("false");
            } else if ("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.22".equals(measurementSeineDto.getGearCaracteristic().getId())) {
                measurementSeineDto.setMeasurementValue("true");
            }
        }

        service.save(tripSeineGearUseDto);

        Form<TripSeineGearUseDto> formAfterSave = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        tripSeineGearUseDto = formAfterSave.getObject();

        GearUseFeaturesSeineDto feature1 = tripSeineGearUseDto.getGearUseFeaturesSeine()
                                                              .stream()
                                                              .filter(IdHelper.newIdPredicate(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID_1))
                                                              .findFirst()
                                                              .get();


        Assert.assertEquals(ObserveFixtures.GEAR_USE_FEATURES_SEINE_ID_1, feature1.getId());
        Assert.assertEquals("fr.ird.observe.entities.referentiel.Gear#1239832686125#0.25", feature1.getGear().getId());
        Assert.assertEquals("Radeau", feature1.getGear().getPropertyValue("label"));
        Assert.assertEquals(new Integer(12), feature1.getNumber());
        Assert.assertEquals(Boolean.TRUE, feature1.getUsedInTrip());
        Assert.assertEquals(3, feature1.sizeGearUseFeaturesMeasurement());
        Assert.assertEquals("Un Commentaire", feature1.getComment());

        GearUseFeaturesMeasurementSeineDto measurement1 = IdHelper.findById(
                feature1.getGearUseFeaturesMeasurement(),
                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230663#0.8598592739610341");
        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.20", measurement1.getGearCaracteristic().getId());
        Assert.assertEquals("4", measurement1.getMeasurementValue());


        GearUseFeaturesMeasurementSeineDto measurement2 = IdHelper.findById(
                feature1.getGearUseFeaturesMeasurement(),
                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230664#0.3389960469962563");
        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.21", measurement2.getGearCaracteristic().getId());
        Assert.assertEquals("false", measurement2.getMeasurementValue());


        GearUseFeaturesMeasurementSeineDto measurement3 = IdHelper.findById(
                feature1.getGearUseFeaturesMeasurement(),
                "fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine#1440486230665#0.018683933154531762");
        Assert.assertEquals("fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.22", measurement3.getGearCaracteristic().getId());
        Assert.assertEquals("true", measurement3.getMeasurementValue());


    }

}
