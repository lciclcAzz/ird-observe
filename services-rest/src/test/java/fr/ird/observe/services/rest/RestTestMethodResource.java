package fr.ird.observe.services.rest;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.service.PingService;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestMethodResourceSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.runner.Description;
import org.nuiton.version.Version;

import java.util.Objects;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class RestTestMethodResource extends TestMethodResourceSupport<RestTestClassResource> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RestTestMethodResource.class);

    private ObserveDataSourceConfigurationRest dataSourceConfiguration;

    public RestTestMethodResource(RestTestClassResource restTestClassResource) {
        super(restTestClassResource);
    }

    public ObserveDataSourceConfigurationRest getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        Objects.requireNonNull(getUrl(), "Pas d'url spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");

        Class<?> testClass = description.getTestClass();
        String methodName = description.getMethodName();

        Version modelVersion = ObserveTestConfiguration.getModelVersion();

        dataSourceConfiguration = testClassResource.createDataSourceConfigurationRest(testClass, null, modelVersion, getUrl(), getLogin(), getPassword());

        checkServerIsAvailable(testClass, methodName);

    }

    private void checkServerIsAvailable(Class<?> testClass, String methodName) {

        boolean serverExist = true;

        PingService service = testClassResource.newService(dataSourceConfiguration, PingService.class);

        try {
            Version serverVersion = service.ping();
            Version modelVersion = ObserveTestConfiguration.getModelVersion();
            if (!serverVersion.equals(modelVersion)) {
                serverExist = false;
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error on check server ", e);
            }
            serverExist = false;
        }

        if (!serverExist) {
            if (log.isWarnEnabled()) {
                log.warn("Skip test [" + testClass.getName() + "#" + methodName + "], server " + dataSourceConfiguration.getServerUrl() + " is not available.");
            }
        }
        Assume.assumeTrue("Server " + dataSourceConfiguration.getServerUrl() + " not found", serverExist);
    }
}
