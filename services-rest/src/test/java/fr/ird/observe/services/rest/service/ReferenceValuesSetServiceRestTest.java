package fr.ird.observe.services.rest.service;

/*-
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.ReferenceSetsRequest;
import fr.ird.observe.services.service.ReferentialService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created on 13/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferenceValuesSetServiceRestTest extends AbstractServiceRestTest {

    protected ReferentialService service;

    @Before
    public void setUp() throws Exception {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration().clone();
        DataSourceService dataSourceService = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        ObserveDataSourceConnection connection = dataSourceService.open(dataSourceConfiguration);

        service = REST_TEST_CLASS_RESOURCE.newService(connection, ReferentialService.class);

    }

    @Test
    public void testGetTripSeineRequest() throws Exception {

        //FIXME
        String requestName = ReferenceSetRequestDefinitions.TRIP_SEINE_FORM.name();

        //FIXME
        ReferenceSetsRequest request = new ReferenceSetsRequest();
        request.setRequestName(requestName);
        request.setLastUpdateDates(ImmutableMap.of());

        ImmutableSet<ReferentialReferenceSet<?>> referenceSetResult = service.getReferentialReferenceSets(request);
        Assert.assertNotNull(referenceSetResult);
        Assert.assertEquals(5, referenceSetResult.size());

        //FIXME
//
//        ObserveReferenceSetRequestDefinition definition = ObserveReferenceSetRequestDefinitions.get(requestName);
//
//        ImmutableSet<ObserveReferenceSetRequestKeyDefinition> keys = definition.getKeys();
//        for (ObserveReferenceSetRequestKeyDefinition key : keys) {
//
//            String name = key.getName();
//            Assert.assertTrue(referenceSetsMap.containsKey(name));
//
//        }
//        Assert.assertEquals(keys.size(), referenceSetsMap.size());
//
//        ImmutableMap<String, Date> lastUpdateDates = referenceSetResult.getLastUpdateDates();
//
//        request.setRequestName(requestName);
//        request.setLastUpdateDates(lastUpdateDates);
//
//        // Re run the request, should receive no data
//
//        ObserveReferenceSetResult<ObserveReferentialReferenceValues> referenceSetResult2 = service.getReferentialReferenceSetDefinitions(request);
//        Assert.assertNotNull(referenceSetResult2);
//        Assert.assertEquals(requestName, referenceSetResult2.getRequestName());
//        ImmutableMap<String, ObserveReferenceSet<?, ObserveReferentialReferenceValues>> referenceSetsMap2 = referenceSetResult2.getReferenceSets();
//        Assert.assertNotNull(referenceSetsMap2);
//        Assert.assertTrue(referenceSetsMap2.isEmpty());

    }

}
