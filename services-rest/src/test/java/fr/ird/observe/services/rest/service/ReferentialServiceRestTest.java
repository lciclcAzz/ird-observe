package fr.ird.observe.services.rest.service;

/*-
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created on 08/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialServiceRestTest extends AbstractServiceRestTest {

    @Test
    public void testGetPrograms() throws IOException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, CloneNotSupportedException, BabModelVersionException {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration().clone();
        DataSourceService dataSourceService = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        ObserveDataSourceConnection connection = dataSourceService.open(dataSourceConfiguration);

        ReferentialService referentialService = REST_TEST_CLASS_RESOURCE.newService(connection, ReferentialService.class);

        ReferentialReferenceSet<ProgramDto> referentialReferenceSet = referentialService.getReferenceSet(ProgramDto.class, new Date());

        Assert.assertNotNull(referentialReferenceSet);

        ImmutableSet<ReferentialReference<ProgramDto>> reference = referentialReferenceSet.getReferences();

        Assert.assertNotNull(reference);
        Assert.assertEquals(28, reference.size());

        for (ReferentialReference<ProgramDto> referenceDto : reference) {

            List<String> propertyNames = referenceDto.getPropertyNames();
            Assert.assertNotNull(propertyNames);
            Assert.assertEquals(4, propertyNames.size());
            Assert.assertEquals(ProgramDto.PROPERTY_CODE, propertyNames.get(0));
            Assert.assertEquals(ProgramDto.PROPERTY_GEAR_TYPE, propertyNames.get(1));
            Assert.assertEquals(ProgramDto.PROPERTY_GEAR_TYPE_PREFIX, propertyNames.get(2));
            Assert.assertEquals(ProgramDto.PROPERTY_LABEL, propertyNames.get(3));

        }

    }

}
