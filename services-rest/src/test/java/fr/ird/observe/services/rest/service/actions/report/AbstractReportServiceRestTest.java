/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.rest.service.actions.report;


import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import fr.ird.observe.services.rest.service.AbstractServiceRestTest;
import fr.ird.observe.services.service.actions.report.ReportBuilder;
import fr.ird.observe.services.service.actions.report.ReportService;
import fr.ird.observe.test.ObserveFixtures;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Classe abstraite de test d'un report.
 *
 * On définit ici le mécanisme pour tester unitairement un report et son
 * résultat.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public abstract class AbstractReportServiceRestTest extends AbstractServiceRestTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(AbstractReportServiceRestTest.class);

    protected ReportService service;

    /** La liste de tous les reports connus. */
    protected static List<Report> reports;

    /** Le report à tester. */
    private Report report;

    protected abstract String getReportId();

    @Before
    public final void setUp() throws Exception {

        super.setUp();

        service = newService(ReportService.class);

        // recuperation du report à tester
        report = getReport(log, getReportId());

        if (log.isInfoEnabled()) {
            log.info("Will use report '" + report + "'");
        }

    }


    /**
     * L'unique test à lancer.
     *
     * On vérifie :
     * <ul>
     * <li>la syntaxe du report via {@link #testReportSyntax(Report)}</li>
     * <li>le résultat du report via {@link #testReportResult(DataMatrix)}</li>
     * </ul>
     *
     * @throws Exception pour toute erreur lors de l'execution du report
     */
    @Test
    public final void testReport() throws Exception {

        // test de la syntaxe du report
        testReportSyntax(report);

        // creation de l'executeur de report
        report = service.populateVariables(report, ObserveFixtures.TRIP_SEINE_ID_1);

        // preparation des variables
        prepareVariables();


        // on execute le report
        DataMatrix result = service.executeReport(report,ObserveFixtures. TRIP_SEINE_ID_1);

        if (log.isInfoEnabled()) {
            log.info("Result :\n" + result.getClipbordContent(true, true));
        }

        // on verifie le resultat
        testReportResult(result);
    }

    protected void prepareVariables() {

    }

    protected void setVariableValue(String variableName, String id) {

        for (ReportVariable variable : report.getVariables()) {

            if (variableName.equals(variable.getName())) {

                Object value = variable.getValues().stream()
                                       .filter(AbstractReference.newIdPredicate(id))
                                       .findFirst()
                                       .orElse(null);

                variable.setSelectedValue(value);

            }

        }

    }

    protected abstract void testReportSyntax(Report report);

    protected abstract void testReportResult(DataMatrix result);

    protected static Report getReport(Log log, String reportId) throws IOException {
        if (reports == null) {

            URL reportLocation =
                    AbstractReportServiceRestTest.class.getResource("/observe-reports.properties");

            if (log.isInfoEnabled()) {
                log.info("Loading reports from " + reportLocation);
            }

            ReportBuilder builder = new ReportBuilder();

            reports = builder.load(reportLocation);

            builder.clear();

            Assert.assertNotNull(reports);
            Assert.assertFalse(reports.isEmpty());
        }

        Report result = null;
        // recuperation du report à tester
        for (Report report : reports) {
            if (reportId.equals(report.getId())) {
                result = report;
                break;
            }
        }

        Assert.assertNotNull("Could not find report with id " + reportId, reports);
        return result;
    }

    protected void assertReportName(Report report,
                                    String name,
                                    String description) {
        Assert.assertEquals(getReportId(), report.getId());
        Assert.assertEquals(name, report.getName());
        Assert.assertEquals(description, report.getDescription());
    }

    protected void assertReportDimension(Report report,
                                         int rows,
                                         int columns,
                                         String[] columnsHeader,
                                         String[] rowsHeader) {
        Assert.assertEquals(rows, report.getRows());
        Assert.assertEquals(columns, report.getColumns());
        Assert.assertArrayEquals(columnsHeader, report.getColumnHeaders());
        Assert.assertArrayEquals(rowsHeader, report.getRowHeaders());
    }

    protected void assertReportNbRequests(Report report, int nbRequests) {
        ReportRequest[] requests = report.getRequests();
        Assert.assertNotNull(requests);
        Assert.assertEquals(nbRequests, requests.length);
    }

    protected void assertReportRequestDimension(ReportRequest request,
                                                ReportRequest.RequestLayout layout,
                                                int x,
                                                int y) {
        Assert.assertEquals(layout, request.getLayout());
//        Assert.assertEquals(new Point(width, height), request.getLocation());
        Assert.assertEquals(x, request.getX());
        Assert.assertEquals(y, request.getY());
    }

    protected void assertResultDimension(DataMatrix result,
                                         int width,
                                         int height,
                                         int x,
                                         int y) {
        Assert.assertEquals(width, result.getWidth());
        Assert.assertEquals(height, result.getHeight());
        Assert.assertEquals(x, result.getX());
        Assert.assertEquals(y, result.getY());
    }

    protected void assertResultRow(DataMatrix result, int rowId, Object... row) {
        Object[] actualRow = result.getData()[rowId];
        Assert.assertArrayEquals(row, actualRow);
    }
}
