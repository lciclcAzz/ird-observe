ActivityLongline,Activité
BaitsComposition,Composition des appâts
Basket,Basket
Branchline,Avançon
BranchlinesComposition,Composition des avançons
CatchLongline,Capture
Encounter,Rencontre
SetLongline,Opération de pêche
FloatlinesComposition,Composition de la ligne mère
HooksComposition,Composition des hameçons
Section,Section
SensorUsed,Capteur utilisé
SizeMeasure,Mesure de tailles
Tdr,Enregistreur de profondeur
TdrRecord,Enregistrement de profondeur
TripLongline,Marée
WeightMeasure;Mesure de poids