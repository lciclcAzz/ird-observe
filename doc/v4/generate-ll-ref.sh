#!/bin/sh

dir=observe-business/src/main/resources/db/4.0
outH2=${dir}/observe-ll-H2.sql
outPG=${dir}/observe-ll-PG.sql

rm -rf ${outH2}
rm -rf ${outPG}

while read line
  do
    class=$(echo ${line} | cut -d',' -f1 )
    upclass=${class^^}
    field=$(echo ${line} | cut -d',' -f2 )

    cat doc/v4/templateH2-table.sql | sed 's/XX/'${upclass}'/' | sed 's/XX/'${upclass}'/' >> ${outH2}
    cat doc/v4/templatePG-table.sql | sed 's/XX/'${upclass}'/' | sed 's/XX/'${upclass}'/' >> ${outPG}

  done < doc/v4/ref-longline.txt