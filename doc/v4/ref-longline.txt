BaitHaulingStatus,baitHaulingStatus,status de l'appât au virage
BaitSettingStatus,baitSettingStatus,status de l'appât au filage
BaitType,baitType,type d'appât
BranchlineType,branchlineType,type d'avançon
CatchFateLongline,catchFateLongline,devenir capture
EncounterType,encounterType,type de rencontre
FloatlineType,floatlineType,type de ligne mère
Healthness,healthness,état de santé
HookPosition,hookPosition,position du hameçon
HookType,hookType,type de hameçon
ItemLocationOnBranchline,itemVerticalPosition,localisation de l'élément sur l'avançon
ItemLocationOnLongline,itemHorizontalPosition,localisation de l'élément sur la ligne mère
LightsticksColor,lightsticksColor,couleur de baton lumineux
LightsticksType,lightsticksType,type de baton lumineux
LineType,lineType,type de ligne
MaturityStatus,maturityStatus,maturité
MitigationType,mitigationType,type de mesure d'atténuation
SensorBrand,sensorBrand,marque de capteur
SensorDataFormat,sensorDataFormat,format de données de capteur
SensorPosition,sensorPosition,position du capteur
SensorType,sensorType,type de capteur
SettingShape,settingShape,forme au filage
SizeMeasureType,sizeMeasureType,type de mesure de taille
StomacFullness,stomacFullness,niveau de remplissage de l'estomac
TripType,tripType,type de marée
VesselActivityLongline,vesselActivityLongline,type d'activité
WeightMeasureType,weightMeasureType,type de mesure de poids
