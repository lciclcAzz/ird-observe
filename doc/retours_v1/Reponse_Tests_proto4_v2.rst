========================================
Réponse aux tests de mars 2009 (proto 4)
========================================

:Author:   Tony Chemit <dev@tchemit.fr>
:Version:  2.0
:Status:   en cours
:Date:     18 avril 2009
:Abstract: Réponse à la synthèse des tests de mars 2009 sur ObServe et point d'avancement.

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2

Légende
-------

- Fait :  |done|
- Besoin de confirmation :  |question|
- A faire :  |todo|
- Ne sera pas réalisé dans cette version :  |undone|

Général
-------

- |done| les logos on été intégrés.
- |done| ajout d'un message lors du premier démarrage si la base est lockée.
- |done| positionnement toujours à gauche de la scrollbar horizontal de navigation.
- |undone| ajout d'un menu contextuel dans l'arbre de navigation.
- |done| ajouter une couleur (rouge sur les objets de l'arbre de navigation non encore saisis pour le moment uniquement pour les fils d'une calée)
- |done| |question| fermeture profonde des marées et routes (la fermeture d'une route ferme aussi son activite ouverte) idem pour les marées.
- |done| passer toutes les propriétés de type primitif (données numériques) par des types objet afifn de gérer la nullité sur les objets.
- |done| suppression par défaut de l'option générer une base vide dans l'assitant de création de base.
- |todo| faire une doc utilisateur et de dev.

Interfaces graphiques
---------------------

Libellés divers
===============

- |done| Cause coup nul.
- |done| correction faute orthographe dans le changeur de base.
- |todo| rendre plus complête les descriptions de l'assistant de base et des opérations base.
- |done| remplacement du libellé "Réinitialiser" par "annuler".
- |done| remplacement du libéllé "Accéder à..." en "Descendre vers..."
- |done| ajout des unités sur les libellés.


Arbre des objets
================

- |done| trier les routes par ordre croissant sur les jours d'obervsation.
- |done| trier les activités par ordre croissant sur les heures d'observation.
- |done| les objets de type route et activité sont repositionnés dans l'arbre
  après chaque modif ou création.
- |done| on laisse les objets fictifs "routes" et "activité" car ils contiennent
  maintenant des données et cela demanderait trop d'effort pour changer 
  le mode de fonctionnement actuel. 
- |undone| ajouter un menu contextuel pour l'écran d'édition reprennant exactement les opérations de l'écran.

Formulaire Marée
================
- |undone| mettre une croix de reset devant la date fin : non fait car cela n'est
  pas opportun : la date de fin est recalculée à chaque modification d'une
  route, et vaut toujours la date de la dernière route ou la date saisie (si elle
  est supérieure à la date de la dernière route).

Formulaire Route
================

- |done| positionnement du loch matin en dessous du jour d'observation.
- |undone| contrôle : la saisie d'une date précédant celle de la route la plus
  ancienne déclanche un warning ? Puisqu'on peut modifier librement la date de
  la route, on ne peut pas faire se genre de contrôle.
- |done| ajouter un reset sur les lochs.
- |done| ajout d'un contrôle pour vérifier qu'une activité 16 (fin de veille) 
  est la dernière activité d'une route à la fermeture d'une route sinon on
  propose la création de cette activité.
- |done| lors de la création au mise à jour d'une route, on met à jour la date
  de fin de marée automatiquement.

Formulaire Activité
===================

- |undone| contrôle : la saisie d'une heure qui précède celle de l'activité
  disposant de l'heure la plus avancée doit générer un warning. Puisque l'heure
  d'observation peut-être librement changée, on ne peut pas faire ce contrôle.
- |undone| on ne peut pas ajouter un warning tant qe l'observateur n'a pas 
  cliqué sur les heures (les controles portent sur les objets et pas sur les uis).
- |done| initialiser l'heure de début avec l'heure de fin de la dernière activité ou bien l'heure courant si pas d'activité.
- |done| ajouter un reset sur la température.
- |done| ajout des libellés sur les quadrants.
- |done| contrôle sur le quadrant par rapport à l'océan de la marée  (Indien : 1 ou 2, Atlantique : 1,2, 3 ou 4).
- |done| contrôle de la différence de température entre 2 activitè < 12 °.
- |done| contrôle : warning si pas de température saisie.
- |done| ajout des coordonnées sexagécimales (uniquement degre - minute, secondes toujours à 0).
- |done| contrôle : saisie des quadrant et coordonnées facultatif (warning).
- |done| PPX utilise le format DD de coordonnées donc pas de modification en base à faire.
- |done| les warning apparaissent aussi en mode création.
- |todo| contrôle : vérifier que la vitesse obtenue par calcul sur l'activité précédente est plausible (< 30 noeud).
- |done| contrôle : les horaires sont croissants : debut < fin coulissage < fin.
- |todo| contrôle : pas deux couples date/horaire identique.
- |todo| pour une activite de type 7 "fin de peche" on doit récupérer l'activité de
   type 6 "début de pêche" et vérifier qu'un délai de 45 minutes s'est écoulé.
- |undone| il n'est pas possible pour le moment de bloquer l'éditeur des heures.
- |done| Amélioration de la fermeture d'une activité (sur le noeud "Activités" le bouton clôturer est désormais présent).
- |done| controle : ajout warning tant q'une DCP n'a pas été ajouté.
- |done| Ajout d'une indication textuelle au dessus de l'activité bateau : "(Seule l'activité 6 – Début de pêche permet de saisir une calée)".

Formulaire Systèmes observés
============================

- |done| ajout des libellés.
- |done| deplacement de la distance en dessous de la liste des systèmes observés.

Formulaire Caléee
=================

- |done| suppression de la case à cocher "rejet faune" : la valeur est calculée
  |done| rapport aux données et n'est plus modifiable par l'utilisateur.
- |done| contrôle sur les horaires : h debut < h fin coulissage < h fin et | h fin - h fin coulissage | > 45 minutes.
- |done| contrôle sur la direction courant : 0/+360°.

Formulaire Estimation Banc
==========================

- |done| ajout d'une clef d'unicité sur l'espèce thon.
- |done| modification libellé "créer" en "Insérer l'estimation".

Formulaire Capture Thon
=======================

- |done| ajout d'une clef d'unicité sur l'espèce thon + categorie poids + identifiant cuve.
- |question| la liste des epèces de thon est filtrées sur celle appartenant à une catégorie poids donc il existe des espèces non listées.
- |done| modification libellé "Espèce de thon" en "Thon mis en cuve".
- |done| modification libellé "créer" en "Insérer cette capture / catégorie".

Formulaire Rejet Thon
=====================

- |question| la liste des epèces de thon est filtrées sur celle appartenant à une catégorie poids donc il existe des espèces non listées.
- |done| ajout au référentiel Rejet Thon de "4 - Poisson abimé".
- |done| suppression de la case à cocher "rejet thon" : la valeur est désormais calculée par rapport aux données.
- |done| la case à cocher monté sur le pont doit être sélectionnée par défaut.
- |done| A droite de la case à cocher "Monté sur le point" ajouter l'explication "Seules les espèces montées sur le pont pourront êtres échantillonées".
- |done| Il n'y a pas de lien avec la capture des thon sur les espèces disponibles.
- |done| modification libellé "créer" en "Insérer ce rejet / catégorie / raison".

Formulaire Echantillon Thon
===========================

- |done| modification libellé "Echantillon Thon" en "Echantillon Thon rejeté".
- |done| modification libellé "longueur " en "longueur (cm inf.)".
- |done| ajout du filtre sur les espèces précedemment rejeé ET monté à bord (actuellement le second test n'est pas fait).
- |done| modification libellé "créer" en "Insérer cet échantillon".

Formulaire Capture Faune
========================

- |done| modification libellé "Capture Faune" en "Faune accessoire conservée ou rejetée".
- |done| contrôle : 0 < poids estimé (en T) < 400 T.
- |done| contrôle : 0 < nombre  estimé < 10000.
- |done| contrôle : poids moyen, taille moyenne par rapport au tableau tableau faunaminmax donné (on pourra le transformer en csv)
  (si espèce non trouvée dans le fichier alors pas de contrôle ).
- |done| modification libellé "créer" en "Insérer cette espèce/poids/devenir".

Formulaire Echantillon Faune
============================

- |done| modification libellé "Echantillon Faune" en "Echantillon Faune accessoire".
- |done| modification libellé "créer" en "Insérer cette échantillon".
- |done| on  filtre les espèces sur celles capturée ou rejetés (actullement uniquement celle rejetées).
- |done| modification libellé "longueur " en "Taille (cm)".
- |done| l'écran est éditable uniquement si ces captures faunes on été saisies.
- |done| contrôle : longueur contraint dans le tableau faunaminmax.

Formulaire DCP
==============
- |question| ajouter dans réferentiel Type objet : "9 - Objet expérimental"
  il est avec un code 15 car y'a déjà une valeur pour le code 9, est-ce ok ?
- |done| modification libellé "Opération de l'objet" en "Opération sur l'objet".
- |done| la liste des opérations est remontée tout en haut.
- |done| modification du libellé de commentaire pour indiquer sur quel objet le
  commentaire est rattaché.

Formulaire Opération sur balise
===============================
- |done| contrôle : la saisie du code est facultatif (passage en warning).
- |undone| permettre l'utilisation d'un éditeur de texte simple ou éditeur
  numérique pour le code de balise selon la valeur enregistrée en base. Il n'est
  pas possible actuellement de faire ce genre de switch d'éditeur.
- |done| ajouter un reset sur le nombre de mise à l'eau.

Validation
----------

- |done| passer tous les contrôles n2 en n1
- |done| revoir les écrans avec tableaux pour faire la validation directement
  sur les objets et non pas sur des modeles d'ui
- |todo| mettre à jour la documentation des validateurs en cours

Import GPS
----------

- |done| réalisé (à tester)

Changement de source
--------------------

- |todo| faire la doc
- |todo| ajouter un résumé des opérations effectuées

Opérations sur base
-------------------

- |todo| faire la doc
- |todo| supprimer l'action de validation n2

Base Postgis
------------

- |done| créer un script sh pour lancer la création de la base pg.
- |todo| créer le script sql pour inserer le trigger de modification du champ theo_gem.


.. |done| image:: images/ok.png
.. |todo| image:: images/warning.png
.. |question| image:: images/question.png
.. |undone| image:: images/error.png
