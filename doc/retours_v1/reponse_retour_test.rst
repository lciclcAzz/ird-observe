=====================
Réponse sur les tests
=====================

.. contents::

Abstract
--------

Ce document répond point par point aux différentes remarques émises par les
membres de l'IRD par rapport aux différents tests qu'ils ont pu effectués sur
le second prototype d'ObServe-0.0.1 (janvier 2009).

Remarques générales
-------------------

Ergonomie
~~~~~~~~~

Je vais revoir les boutons d'action sur les différents écrans de saisie,
mon but serait d'en améliorer l'aspect et qu'ils prennent moins de place. (à faire)

Dans une version future (cela sera ma contribution pour l'IRD:)), pour les activités,
calées et DCP on aura plutôt des onglets sur l'écran de saisie plutôt que des neouds
dans l'arbre de navigation (mais c'est pas pour tout de suite!).

Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

En ce qui concerne les contrôles de saisie, j'en avais mis quelques uns mais
il ne s'agissait évidemment pas des contrôles définitifs et je vais les mettre
à jour en suivants vos remarques.

Modification du modèle
~~~~~~~~~~~~~~~~~~~~~~

- Dans le modèle de base, on a supprimé tous les champs *identifiant* sur les
  données observateurs, puisque les identifiant techniques les remplacent et
  donc il ne servent plus à rien.

- Un certain nombre de champs subsistent, mais je ne pense pas qu'ils soient
  encore nécessaire de les conserver (par exemple le numéro espece sur les
  captures); j'en fais la liste complête et on en rediscute à mon retour.
   

Niveau Maréee
-------------

Cinématique
~~~~~~~~~~~

Les quatres premiers champs :

- Observateur
- Senne
- Bateau
- Date de début

sont désormais modifiables après une première sauvegarde (sous couvert bien
sûr que la maréee soit ouverte).

Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

- Le champs Senne est désormais facultatif. On a rajouté un contrôle de saisie
  lorsque la senne n'a été renseignée (commentaire requis sur la marée).

- Un contrôle de saisie a été ajouté pour vérifier que la date de fin de marée
  est bien supérieure à sa date de début.

Ergonomie
~~~~~~~~~

Rendre le champs commentaire bien visible (à faire).

Libellés
~~~~~~~~

- Les libéllés des bulles d'aides sur les boutons (flèches vertes pour accéder
  ont été modifiés en "Accéder au référentiel XXX".

*Question :* Ces boutons avaient été ajoutés au début de prototypage mais étant
donné que l'utilisation d'ObServe ne permettra pas dans cette première version
de pouvoir modifier le référentiel, je serais assez d'avis à tout simplement
les supprimer. Qu'en pensez-vous ?

- Le libellé de la senne a été modifié pour voir apparaître la signification de
  ses propriétés numériques.

- Cela n'est peut-être pas évident d'ajouter une senne de type inconnu, car la
  senne ne possède pas de libellé en base. Je vous propose pour le moment de
  rester surla solution mise en place, à savoir un contrôle de saisie sur le
  commentaire de la marée si la senne n'a pas été renseignée.

- Le message de suppresion d'un objet a été modifié pour supprimer le "de type".

Fonctions manquantes
~~~~~~~~~~~~~~~~~~~~

Il n'a pas été prévu dans cette première version d'ObServe de pouvoir éditer le
référentiel sur la base Obstuna. La connection à ce serveur est actuellement
utilisé uniquement pour faire :

- la synchronisation de la base locale vers ce serveur.
- mettre à jour le référentiel de la base locale.

On pourrait imaginer effectivement par la suite de pouvoir éditer directement
le référentiel de la base centrale en utilisant ObServe en mode "connecté à
Obstuna" (et cela uniquement pour un administrateur d'Obstuna).


Niveau Route
------------

Cinématique
~~~~~~~~~~~

L'application a été conçue initialement pour optimiser les saisies sur
TabletPC, et donc éviter le nombre de clic souris et permettre de ne
pas à avoir un clavier. Je n'avais pas compris qu'elle pourrait aussi
être utilisée dans d'autres circonstances.

Je reviens vers vous après concertation avec Benjamin quand au meilleur
compromis possible envisageable. Il me semble à première abord que la seconde
solution soit assez simple à mettre en oeuvre et simplifie la saisie d'anciennes
données; mais la troisième solution semble aussi nécessaire...(à faire).

- Les champs loch du matin et jour d'observation sont désormais modifiables,
  même après une réouverture de route.

- Le calcul du jour d'observation a été modifié : si aucune route définie
  on utilise la date de début de marée, sinon on utilise le jour suivant
  celui de la dernière route.

Controle de saisie
~~~~~~~~~~~~~~~~~~

- Ajout d'un contrôle supplémentaire lochMatin == lochSoir == 0 bypassant les autres
  contrôles.

Cela va poser un petit problème à mon avis car j'avais ajouté en controle
de niveau 1 des controles de cohérences avec les loch des autres routes de la marée.
Le plus simple, à mon avis, est en niveau 1 de ne vérifier que la cohérence des lochs
sur la route en cours et en controle de niveau 2 la cohérence entre toutes les routes.
Qu'en pensez-vous ?

Niveau Activité
---------------

Cinématique
~~~~~~~~~~~

Le bouton "Créer une nouvelle activité" n'a pas été actuellement ajouté sur l'écran
d'une activité ouverte car cela est un peu compliqué à l'heure actuelle. Une autre
solution pourrait être lors de la fermeture d'une activité de retourner directement
sur le noeud *Activités*, ainsi un simple clic permettrait de créer une nouvelle
activité. Qu'en pensez-vous ?

Si vous êtes ok pour cette solution, on pourrait aussi l'appliquer sur les marées
et routes.

Libellés
~~~~~~~~

Les libellés NW (quadrant 4) NE (quadrant 1) SW (quadrant 3) SE (quadrant 2)
ont été ajoutés dans l'ui.

Remarques : J'avais bien pris compte du calcul des quadrants (c'était la bonne
disposition sur l'écran d'une activité en cours), mais j'avais oublié de
réappliquer cette disposition sur l'écran de création d'une activité...

Benjamin pensait utiliser un composant graphique un peu plus sympas (une image
de la terre découpée en 4) mais je n'ais eu le temps de le mettre en place. 

Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

- La saisie d'un quadrant a été rendue obligatoire. Il faut noter que lors de la saisie
  de la longitude ou latitude (hormis le cas ou long=lat=0), le quadrant est automatiquement
  mis à jour (peut-être il ne faudrait pas ? à vous de me dire...).

- Je regarde comment effectuer le contrôle de saisie des quadrants par rapport à l'océan
  de la marée en cours. (à faire).
    * si océan indien quadrant 1 ou 2.
    * si océan atlantique quadrant 1, 2, 3 ou 4.
    * si ocean pacifique pas de contrôle.
- Ajout d'un contrôle sur l'horaire de l'activité ?
    * horaire toujours strictement supérieure à celui d'une activité précédente.
A faire, il s'agit pour moi plus d'un controle de niveau 2 ?    

- Ajout d'un contrôle d'anomalie (pas d'erreur...) si aucun système observé sélectionné.

Widgets
~~~~~~~

- l'attribut vitesseVent a été supprimé de l'activité en base et sur les écrans de saisie.
       
- Réorganisation des widgets pour respecter l'order suivant :
  * Heure d’observation
  * Quadrant (et non cadran !)
  * Latitude
  * Longitude
  * Activité bateau
  * Activité environnante
  * Vitesse bateau
  * Température de surface
  * Vent Beaufort
  * Mode de détection
  * Systèmes observés (dans un autre noeud actuellement)
  * Distance système observé (dans un autre noeud actuellement, cela convient-t-il ?)
  * Raison non calée (accessible uniquement si une calée) (il s'agit de
    CauseNonCoupDeSenne en base)
  * commentaire (mais pas sur l'écran de création, ?).

J'ai préfére mettre dans un autre noeud (et plus tard un autre onglet) les
informations sur les systèmes observés et ceci afin d'alléger un peu l'écran
de saisie. Cela vous convient-il ?

Il n'est pas évident à l'heure actuelle de rajouter une ligne "intensité
inconnue" dans la liste des vents beaufort qui n'est ensuite pas enregistrée
en base, par contre on peut ajouter dans le référentiel une telle ligne
avec le champs NEDDCOMMENT activé, cela convient-il ?

TODO le reste :)

Niveau Calée
------------

Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

Ergonomie
~~~~~~~~~


Niveau Estimation de banc
-------------------------

Ajouter une ligne dans les espèces de thon : tout le banc avec needcomment=false (à faire)


Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

Il faut que le poids (en T) ou le poids moyen (en Kg) soit obligatoire mais pas les deux.

Niveau Capture de thons
-----------------------

C'est quoi la catégorie commerciale ? a rechercher

Un seul clic pour saisir est un peu compliqué, non ? a voir

Bug sur la saisie des capture de thons : changement d'une cuve et on se
retrouve avec des erreurs de validation sur les especes...

Niveau echantillonage de thon
-----------------------------

Pas testé

Niveau rejet de thon
--------------------

Le libellé de la colonne Monté sur le pont sera "M" avec une bulle d'info "Monté sur le pont"

Le référentiel RaisonRejet a été mise à jour :
- 1 espèce
- 2 taille
- 3 cuve pleine

Mettre en place la sauvegarde.

Niveau capture de Faune
-----------------------

L’entête « Taille moyenne (en kg) » a été corrigée en « Taille moyenne (en cm) ».

Contrôles de saisie
~~~~~~~~~~~~~~~~~~~

L'interface oblige la saisie des 4 valeurs. Or cette situation est
très rare. Il faut en fait tester la présence de l'une des 4 valeurs
pour accepter la saisie.


Niveau Echantillonage de faune associée
---------------------------------------

Limiter la liste des espèces qu'il est possible d'échantillonner aux espèces
qui ont été citées dans la partie "Capture de faune associée".


Ergonomie générale
------------------

- Trouver un moyen de rendre les champs non éditable plus lisible.

- rendre l'utilisation du pave numerique facultative :
  par exemple une option de configuration numericWidget

- ajouter sur le pave numerique un bouton de validation (et fermeture
  de la popup)

- Il n'est pas facile de naviguer dans les objets de l'arbre, pourriez-vous
  m'indiquer ce qui ne va pas ?

- Rendre la liste des programmes observateurs 'stables', je propose de
  toujours les trier alpabétiquement sur les libellés programmes.

- Toujours utiliser le format jj/mm/aaaa pour les dates et hh:mm
  pour les heures, c'est fait.

- Réduire la fenetre des messages à 20, 25%

Remarques générales
-------------------

- Pour les contrôles de saisies, je pensais qu'on en rediscuterait ensemble.
  Je relis le document fournit pour ceux de niveau 1, dans tous les cas ceux
  de niveaux 2 restent à définir.

- La gestion des DCP est possible uniquement sur certains types d'activité ?
  Par exemple actuellement c'est présent sur une activité de type 13 (pose
  ou modification d'une epave). Dans ce cas, il y a un bouton "Ajouter un
  objet flottant"; une fois le DCP ajouté, on a 3 nouveaux noeuds fils :

    * Opération de balise

    * Estimation Banc Objet

    * Espece faune observée.

Il me faudrait la liste des type d'activité sur lequel on peut gérer des DCP.

Remarques techniques
--------------------

Les base de données
~~~~~~~~~~~~~~~~~~~

Je vais voir avec Benjamin comment gérer au mieux les interactions des bases.

A l'heure actuelle, seule la génération d'une base embarquée (avec un
référentiel donné), ou l'importation d'une nouvelle base à partir d'un
fichier gzippé d'une base déjà préparée).

Je reviens vers vous quand on a une solution acceptable pour des non techniciens :)

Bug Xp
~~~~~~

Je ne pense pas que l'application puisse faire planté Xp comme ça :)
A revoir

Bug routes cloturees
~~~~~~~~~~~~~~~~~~~~

Pour le bug sur la validation des routes clotures cela est normal car
ce n'est pas encore en place :) et le message est juste une execption.

Dans tous les cas je ne pense pas que ce bouton va rester ici car cela
n'est pas cohérent de l'avoir sur l'écran de saisie, il sera dans un
menu qui permettra de sélectionner les routes et marées à valider en
niveau 2. 
  

Autres
------

Champs PostGIS
~~~~~~~~~~~~~~

Je m'attaquerais à ce point un peu plus tard dans l'avancement lorsque
la synchro, validation niveau 2 et import GPS seront OK.

Connexion SLL
~~~~~~~~~~~~~

Je me concerte avec Benjamin pour trouver comment faire :)

Contrôle de saisies
~~~~~~~~~~~~~~~~~~~

A revoir avec vous (voir un point précédent)

Génération des ToPIA id
~~~~~~~~~~~~~~~~~~~~~~~

Je ne connais pas l'algorithme utilisé et je me renseigne vers les
experts ToPIA et je reivens vers vous.

API
~~~

Un certain nombre de classes sont générées (par exemple toutes les
entités et la plupart des interfaces graphiques), pour ces classes
il est un peu dure de faire de la génération de javadoc en même
temps; par contre pour les autres classes, il devrait y avoir plus
de javadoc.

Icones
~~~~~~

JE met en place des icones temporaires et dès que vous avez les bons
icones, je les intègrerais au projet.










