package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 25/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MismatchMethodParameterNameException extends Exception {
    private static final long serialVersionUID = 1L;

    private final String sourceClassName;
    private final String sourceMethodName;
    private final int parameterIndex;
    private final String sourceParameterName;
    private final String targetClassName;
    private final String targetMethodName;
    private final String targetParameterName;

    public MismatchMethodParameterNameException(String sourceClassName,
                                                String sourceMethodName,
                                                String sourceParameterName,
                                                int parameterIndex,
                                                String targetClassName,
                                                String targetMethodName,
                                                String targetParameterName) {
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.parameterIndex = parameterIndex;
        this.sourceParameterName = sourceParameterName;
        this.targetClassName = targetClassName;
        this.targetMethodName = targetMethodName;
        this.targetParameterName = targetParameterName;
    }

    @Override
    public String getMessage() {
        return "Target class: " + targetClassName + ", method: " + targetMethodName + ", parameter at position " + parameterIndex + " name is wrong (was " + targetParameterName + "), but should be " + sourceParameterName + " (source class: " + sourceClassName + ", method: " + sourceMethodName + ")";
    }

}
