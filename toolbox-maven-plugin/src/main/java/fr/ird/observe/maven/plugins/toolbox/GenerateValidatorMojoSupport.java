package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Collection;

/**
 * Created on 31/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GenerateValidatorMojoSupport extends ToolboxMojoSupport implements ValidatorCacheRequest {

    @Parameter(defaultValue = "${project.basedir}/src/main/validators/validators.xml", required = true)
    private File validatorsFile;

    private Collection<ValidatorsCache.ValidatorInfo> validators;

    @Override
    protected void init() throws Exception {

        if (isSkip()) {
            return;
        }

        super.init();

        validators = ValidatorsCache.get().getValidators(this);

    }

    @Override
    protected boolean checkSkip() {
        if (isSkip()) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }
        if (validators.isEmpty()) {
            getLog().info("Skipping goal (no validator detected).");
            return false;
        }

        return super.checkSkip();
    }

    protected abstract Path createOutputFile() throws IOException;

    protected abstract boolean isSkip();

    public File getValidatorsFile() {
        return validatorsFile;
    }

    public Collection<ValidatorsCache.ValidatorInfo> getValidators() {
        return validators;
    }

    @Override
    public URLClassLoader getUrlClassLoader() throws MalformedURLException {
        return initClassLoader(getProject(), validatorsFile.getParentFile(), true, false, true, true, false);
    }

}
