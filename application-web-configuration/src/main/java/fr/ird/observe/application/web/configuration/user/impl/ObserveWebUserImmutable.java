package fr.ird.observe.application.web.configuration.user.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.observe.application.web.configuration.user.ObserveWebUser;

import java.util.Collection;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebUserImmutable implements ObserveWebUser<ObserveWebUserPermissionImmutable> {

    private final String login;

    private final String password;

    private final ImmutableMap<String, ObserveWebUserPermissionImmutable> permissions;

    public ObserveWebUserImmutable(String login, String password, Iterable<ObserveWebUserPermissionImmutable> permissions) {

        this.login = login;
        this.password = password;
        this.permissions = Maps.uniqueIndex(permissions, ObserveWebUserPermissionImmutable::getDatabase);

    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<ObserveWebUserPermissionImmutable> getPermissions() {
        return permissions.values();
    }

    @Override
    public Optional<ObserveWebUserPermissionImmutable> getPermissionByDatabaseName(String databaseName) {
        return Optional.ofNullable(permissions.get(databaseName));
    }

    public ObserveWebUserBean toBean() {
        ObserveWebUserBean observeWebUserBean = new ObserveWebUserBean();
        observeWebUserBean.setLogin(login);
        observeWebUserBean.setPassword(password);
        observeWebUserBean.setPermissions(Sets.newLinkedHashSet(Iterables.transform(getPermissions(), ObserveWebUserPermissionImmutable::toBean)));

        return observeWebUserBean;
    }

}
