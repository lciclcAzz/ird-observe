package fr.ird.observe.application.web.configuration.db;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseRoleBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabasesBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabasesImmutable;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebDatabasesHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveWebDatabasesHelper.class);

    public ObserveWebDatabasesImmutable load(File file) throws InvalidObserveWebDatabaseException, InvalidObserveWebDatabasesException, InvalidObserveWebDatabaseRoleException {

        ObserveWebDatabasesBean observeWebDatabasesBean = loadBean(file);
        validateObserveWebDatabasesBean(observeWebDatabasesBean);
        return observeWebDatabasesBean.toImmutable();

    }

    public ObserveWebDatabasesBean loadBean(File file) {

        if (log.isInfoEnabled()) {
            log.info("Loading databases from file: " + file);
        }
        ObserveWebDatabasesBean result;
        try (Reader fileReader = Files.newReader(file, Charsets.UTF_8)) {
            YamlReader reader = new YamlReader(fileReader, createConfig());
            result = reader.read(ObserveWebDatabasesBean.class);
            fileReader.close();

        } catch (Exception e) {
            throw new RuntimeException("Could not read databases from file: " + file, e);
        }

        return result;
    }

    public void validateObserveWebDatabasesBean(ObserveWebDatabasesBean observeWebDatabasesBean) throws InvalidObserveWebDatabaseException, InvalidObserveWebDatabaseRoleException, InvalidObserveWebDatabasesException {

        if (CollectionUtils.isEmpty(observeWebDatabasesBean.getDatabases())) {
            throw new InvalidObserveWebDatabaseException("No database defined");
        }

        // need extacly one default database
        ObserveWebDatabaseBean defaultDatabase = null;

        // unique database name
        Set<String> databaseNames = new LinkedHashSet<>();

        for (ObserveWebDatabaseBean observeWebDatabaseBean : observeWebDatabasesBean.getDatabases()) {

            validateObserveWebDatabaseBean(observeWebDatabaseBean);
            boolean added = databaseNames.add(observeWebDatabaseBean.getName());
            if (!added) {
                throw new InvalidObserveWebDatabasesException("Not unique database name found: " + observeWebDatabaseBean.getName());
            }
            if (observeWebDatabaseBean.isDefaultDatabase()) {

                if (defaultDatabase != null) {
                    throw new InvalidObserveWebDatabasesException("Found two default databases: " + defaultDatabase.getName() + " and " + observeWebDatabaseBean.getName());
                }
                defaultDatabase = observeWebDatabaseBean;
            }

        }

        if (defaultDatabase == null) {
            throw new InvalidObserveWebDatabasesException("No default databases defined");
        }

    }

    public void validateObserveWebDatabaseBean(ObserveWebDatabaseBean observeWebDatabaseBean) throws InvalidObserveWebDatabaseRoleException, InvalidObserveWebDatabaseException {

        String databaseName = observeWebDatabaseBean.getName();
        if (StringUtils.isBlank(databaseName)) {
            throw new InvalidObserveWebDatabaseException("Found a database with no name defined");
        }

        if (StringUtils.isBlank(observeWebDatabaseBean.getUrl())) {
            throw new InvalidObserveWebDatabaseException("Database " + databaseName + " has no url defined");
        }

        if (CollectionUtils.isEmpty(observeWebDatabaseBean.getRoles())) {
            throw new InvalidObserveWebDatabaseException("Database " + databaseName + " has no roles defined");
        }

        // unique login name
        Set<String> logins = new LinkedHashSet<>();

        for (ObserveWebDatabaseRoleBean observeWebDatabaseRoleBean : observeWebDatabaseBean.getRoles()) {

            validateObserveWebDatabaseRoleBean(observeWebDatabaseBean, observeWebDatabaseRoleBean);
            boolean added = logins.add(observeWebDatabaseRoleBean.getLogin());
            if (!added) {
                throw new InvalidObserveWebDatabaseException("Database " + databaseName + " contains a doublon login: " + observeWebDatabaseRoleBean.getLogin());
            }

        }

    }

    public void validateObserveWebDatabaseRoleBean(ObserveWebDatabaseBean observeWebDatabaseBean, ObserveWebDatabaseRoleBean observeWebDatabaseRoleBean) throws InvalidObserveWebDatabaseRoleException {

        String databaseName = observeWebDatabaseBean.getName();

        if (StringUtils.isBlank(observeWebDatabaseRoleBean.getLogin())) {
            throw new InvalidObserveWebDatabaseRoleException("In database " + databaseName + ", found a role with no login defined");
        }
        if (StringUtils.isBlank(observeWebDatabaseRoleBean.getPassword())) {
            throw new InvalidObserveWebDatabaseRoleException("In database " + databaseName + ", found a role " + observeWebDatabaseRoleBean.getLogin() + " with no password defined");
        }

    }

    public void store(ObserveWebDatabases databases, File file) {

        if (log.isInfoEnabled()) {
            log.info("Store databases to " + file);
        }

        try (BufferedWriter writer = Files.newWriter(file, Charsets.UTF_8)) {

            store(databases, writer);

        } catch (Exception e) {
            throw new RuntimeException("Could not write databases to file: " + file, e);
        }

    }

    public void store(ObserveWebDatabases databases, Writer writer) throws YamlException {

        YamlWriter yamlWriter = new YamlWriter(writer, createConfig());
        if (databases instanceof ObserveWebDatabasesImmutable) {

            ObserveWebDatabasesImmutable observeWebDatabasesImmutable = (ObserveWebDatabasesImmutable) databases;
            databases = observeWebDatabasesImmutable.toBean();

        }
        yamlWriter.write(databases);
        yamlWriter.close();

    }

    protected YamlConfig createConfig() {

        YamlConfig yamlConfig = new YamlConfig();
//        yamlConfig.setClassTag("db", ObserveWebDatabase.class);
//        yamlConfig.setClassTag("role", ObserveWebDatabaseRole.class);
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setWriteRootTags(false);
        yamlConfig.setPropertyElementType(ObserveWebDatabasesBean.class, "databases", ObserveWebDatabaseBean.class);
        yamlConfig.setPropertyElementType(ObserveWebDatabaseBean.class, "roles", ObserveWebDatabaseRoleBean.class);
        return yamlConfig;

    }
}
