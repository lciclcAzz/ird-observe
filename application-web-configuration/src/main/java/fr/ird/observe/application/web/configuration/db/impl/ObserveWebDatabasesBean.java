package fr.ird.observe.application.web.configuration.db.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;

import java.util.LinkedHashSet;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebDatabasesBean implements ObserveWebDatabases<ObserveWebDatabaseBean> {

    protected LinkedHashSet<ObserveWebDatabaseBean> databases;

    @Override
    public Optional<ObserveWebDatabaseBean> getDatabaseByName(String databaseName) {
        throw new UnsupportedOperationException("Can not call this method on bean version, use immutable one.");
    }

    @Override
    public LinkedHashSet<ObserveWebDatabaseBean> getDatabases() {
        return databases;
    }

    @Override
    public String getDefaultDatabaseName() {
        throw new UnsupportedOperationException("Can not call this method on bean version, use immutable one.");
    }

    @Override
    public ObserveWebDatabaseBean getDefaultDatabase() {
        throw new UnsupportedOperationException("Can not call this method on bean version, use immutable one.");
    }

    public ObserveWebDatabasesImmutable toImmutable() {

        return new ObserveWebDatabasesImmutable(Iterables.transform(databases, input -> input.toImmutable()));
    }

    public void setDatabases(LinkedHashSet<ObserveWebDatabaseBean> databases) {
        this.databases = databases;
    }

}
