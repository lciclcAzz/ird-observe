package fr.ird.observe.application.web.configuration;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import fr.ird.observe.util.ObserveUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * La configuration de l'application web.
 *
 * Created on 29/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebApplicationConfig extends GeneratedObserveWebApplicationConfig {

    /** Logger. */
    private static Log log = LogFactory.getLog(ObserveWebApplicationConfig.class);

    private static final String DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME = "observeweb.conf";

    public static ObserveWebApplicationConfig fromContextPath(String contextPath) {
        ObserveWebApplicationConfig config = new ObserveWebApplicationConfig();
        config.get().setDefaultOption(ObserveWebApplicationConfigOption.CONTEXT_PATH.getKey(), contextPath);
        config.init();
        return config;
    }

    private ObserveWebApplicationConfig() {
        this(DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME);
    }

    public ObserveWebApplicationConfig(String confFileName) {
        ApplicationConfig delegate = get();
        delegate.setEncoding(StandardCharsets.UTF_8.name());
        delegate.setConfigFileName(confFileName);
    }

    public void init(String... args) {

        if (log.isInfoEnabled()) {
            log.info("Starts to init ObserVeWeb configuration...");
        }

        try {
            get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new ObserveWebApplicationConfigInitException("could not parse configuration", e);
        }

        File baseDirectory = getBaseDirectory();

        if (isDevMode() && !baseDirectory.exists()) {
            // on utilise un répertoire temporaire comme basedir

            if (log.isInfoEnabled()) {
                log.info("Using a dev mode configuration.");
            }
            try {
                // Toujours s'assurer que le répertoire temporarie du système existe
                Path tmpdir = Paths.get(System.getProperty("java.io.tmpdir"));
                if (!Files.exists(tmpdir)) {
                    Files.createDirectories(tmpdir);
                }

                baseDirectory = Files.createTempDirectory("observeweb").toFile();
            } catch (IOException e) {
                throw new ObserveWebApplicationConfigInitException("could not create temporary basedir", e);
            }
            if (log.isInfoEnabled()) {
                log.info("Dev mode detected, use temporary basedir: " + baseDirectory);
            }
            setBaseDirectory(baseDirectory);
        }

        if (log.isInfoEnabled()) {
            log.info(getConfigurationDescription());
        }

        try {
            Files.createDirectories(baseDirectory.toPath());
        } catch (IOException e) {
            throw new ObserveWebApplicationConfigInitException("Impossible de créer le répertoire principal de l'application (" + baseDirectory + ")", e);
        }
        File temporaryDirectory = getTemporaryDirectory();
        try {
            Files.createDirectories(temporaryDirectory.toPath());
        } catch (IOException e) {
            throw new ObserveWebApplicationConfigInitException("Impossible de créer le répertoire temporaire de l'application (" + temporaryDirectory + ")", e);
        }

        File databasesConfigurationFile = getDatabasesConfigurationFile();
        if (!databasesConfigurationFile.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate default databases.yml");
            }

            try {
                CharSource charSource = Resources.asCharSource(getClass().getResource("/defaultDatabases.yml"), StandardCharsets.UTF_8);
                Files.write(databasesConfigurationFile.toPath(), charSource.readLines());
            } catch (IOException e) {
                throw new ObserveWebApplicationConfigInitException("Impossible de créer un fichier de configuration des bases", e);
            }
        }

        File usersConfigurationFile = getUsersConfigurationFile();
        if (!usersConfigurationFile.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate default users.yml");
            }
            try {
                CharSource charSource = Resources.asCharSource(getClass().getResource("/defaultUsers.yml"), StandardCharsets.UTF_8);
                Files.write(usersConfigurationFile.toPath(), charSource.readLines());
            } catch (IOException e) {
                throw new ObserveWebApplicationConfigInitException("Impossible de créer un fichier de configuration des utilisateurs", e);
            }

        }

        File log4jConfigurationFile = getLog4jConfigurationFile();
        if (!log4jConfigurationFile.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate default log4jConfigurationFile");
            }
            try {
                CharSource charSource = Resources.asCharSource(getClass().getResource("/observeweb-log4j.conf"), Charsets.UTF_8);
                Files.write(log4jConfigurationFile.toPath(), charSource.readLines());
            } catch (IOException e) {
                throw new ObserveWebApplicationConfigInitException("Impossible de créer un fichier de log4j", e);
            }
        }

        initLog();

        if (log.isInfoEnabled()) {
            log.info("Observe web configuration init done.");
        }

    }

    private void initLog() {

        File logFile = getLog4jConfigurationFile();

        if (!logFile.exists()) {
            throw new ObserveWebApplicationConfigInitException("Le fichier de configuration des logs (" + logFile + ") n'existe pas");
        }

        if (log.isInfoEnabled()) {
            log.info("Chargement du fichier de log : " + logFile);
        }

        Properties finalLogConfigurationProperties;
        try (BufferedReader inputStream = Files.newBufferedReader(logFile.toPath(), Charsets.UTF_8)) {

            Properties logConfigurationProperties = new Properties();
            logConfigurationProperties.load(inputStream);

            finalLogConfigurationProperties = ObserveUtil.loadProperties(logConfigurationProperties, get());

        } catch (Exception e) {
            throw new ObserveWebApplicationConfigInitException("Impossible de charger le fichier de configuration des logs", e);
        }

        LogManager.resetConfiguration();
        PropertyConfigurator.configure(finalLogConfigurationProperties);

        log = LogFactory.getLog(ObserveWebApplicationConfig.class);
        if (log.isInfoEnabled()) {
            log.info("Configuration des logs chargée depuis le fichier " + logFile);
        }

    }

}
