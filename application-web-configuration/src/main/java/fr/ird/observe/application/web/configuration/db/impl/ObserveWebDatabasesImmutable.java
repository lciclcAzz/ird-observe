package fr.ird.observe.application.web.configuration.db.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;

import java.util.Collection;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveWebDatabasesImmutable implements ObserveWebDatabases<ObserveWebDatabaseImmutable> {

    private final ImmutableMap<String, ObserveWebDatabaseImmutable> databases;

    private final ObserveWebDatabaseImmutable defaultDatabase;

    public ObserveWebDatabasesImmutable(Iterable<ObserveWebDatabaseImmutable> databases) {
        this.databases = Maps.uniqueIndex(databases, input -> input.getName());

        ObserveWebDatabaseImmutable defaultDb = null;
        for (ObserveWebDatabaseImmutable database : databases) {
            if (database.isDefaultDatabase()) {
                defaultDb = database;
                break;

            }
        }
        this.defaultDatabase = defaultDb;
    }

    @Override
    public Collection<ObserveWebDatabaseImmutable> getDatabases() {
        return databases.values();
    }

    @Override
    public String getDefaultDatabaseName() {
        return defaultDatabase.getName();
    }

    @Override
    public ObserveWebDatabaseImmutable getDefaultDatabase() {
        return defaultDatabase;
    }

    @Override
    public Optional<ObserveWebDatabaseImmutable> getDatabaseByName(String databaseName) {
        ObserveWebDatabaseImmutable database = databases.get(databaseName);
        return Optional.ofNullable(database);
    }

    public ObserveWebDatabasesBean toBean() {
        ObserveWebDatabasesBean observeWebDatabasesBean = new ObserveWebDatabasesBean();
        observeWebDatabasesBean.setDatabases(Sets.newLinkedHashSet(Iterables.transform(getDatabases(), input -> input.toBean())));
        return observeWebDatabasesBean;
    }

}
