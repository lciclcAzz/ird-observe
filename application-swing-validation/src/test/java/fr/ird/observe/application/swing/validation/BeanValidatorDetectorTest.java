package fr.ird.observe.application.swing.validation;

/*-
 * #%L
 * ObServe :: Application Swing Validation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.ValidatorTestHelper;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created on 26/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BeanValidatorDetectorTest extends AbstractValidatorDetectorTest {

    private static final String UI_CREATE_CONTEXT_NAME = "ui-create";

    private static final String UI_UPDATE_CONTEXT_NAME = "ui-update";

    SortedSet<NuitonValidator<?>> validators;

    public BeanValidatorDetectorTest() {
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    static Class<?>[] ALL_TYPES;

    @BeforeClass
    public static void setUpClass() {
        ALL_TYPES = new Class[]{
                ActivityLonglineDto.class,
                ActivityLonglineEncounterDto.class,
                ActivityLonglineSensorUsedDto.class,
                ActivitySeineDto.class,
                BaitsCompositionDto.class,
                BaitHaulingStatusDto.class,
                BaitSettingStatusDto.class,
                BaitTypeDto.class,
                BasketDto.class,
                BranchlineDto.class,
                BranchlinesCompositionDto.class,
                CatchFateLonglineDto.class,
                CatchLonglineDto.class,
                CountryDto.class,
                DetectionModeDto.class,
                EncounterDto.class,
                EncounterTypeDto.class,
                FloatingObjectDto.class,
                FloatingObjectSchoolEstimateDto.class,
                FloatingObjectTransmittingBuoyDto.class,
                FloatlinesCompositionDto.class,
                FpaZoneDto.class,
                GearCaracteristicDto.class,
                GearCaracteristicTypeDto.class,
                GearDto.class,
                GearUseFeaturesLonglineDto.class,
                GearUseFeaturesSeineDto.class,
                HarbourDto.class,
                HealthnessDto.class,
                HooksCompositionDto.class,
                HookPositionDto.class,
                HookSizeDto.class,
                HookTypeDto.class,
                ItemHorizontalPositionDto.class,
                ItemVerticalPositionDto.class,
                LengthWeightParameterDto.class,
                LightsticksColorDto.class,
                LightsticksTypeDto.class,
                LineTypeDto.class,
                MaturityStatusDto.class,
                MitigationTypeDto.class,
                NonTargetCatchDto.class,
                NonTargetLengthDto.class,
                NonTargetSampleDto.class,
                ObjectFateDto.class,
                ObjectObservedSpeciesDto.class,
                ObjectOperationDto.class,
                ObjectSchoolEstimateDto.class,
                ObjectTypeDto.class,
                ObservedSystemDto.class,
                OceanDto.class,
                OrganismDto.class,
                PersonDto.class,
                ProgramDto.class,
                ReasonForDiscardDto.class,
                ReasonForNoFishingDto.class,
                ReasonForNullSetDto.class,
                RouteDto.class,
                SchoolEstimateDto.class,
                SectionDto.class,
                SensorBrandDto.class,
                SensorDataFormatDto.class,
                SensorTypeDto.class,
                SensorUsedDto.class,
                SetLonglineDto.class,
                SetLonglineGlobalCompositionDto.class,
                SetSeineDto.class,
                SetSeineNonTargetCatchDto.class,
                SetSeineSchoolEstimateDto.class,
                SetSeineTargetCatchDto.class,
                SettingShapeDto.class,
                SexDto.class,
                SizeMeasureTypeDto.class,
                SpeciesDto.class,
                SpeciesFateDto.class,
                SpeciesGroupDto.class,
                SpeciesListDto.class,
                SpeciesStatusDto.class,
                StomacFullnessDto.class,
                SurroundingActivityDto.class,
                TargetCatchDto.class,
                TargetLengthDto.class,
                TargetSampleDto.class,
                TdrDto.class,
                TransmittingBuoyDto.class,
                TransmittingBuoyOperationDto.class,
                TransmittingBuoyTypeDto.class,
                TripLonglineDto.class,
                TripLonglineGearUseDto.class,
                TripSeineDto.class,
                TripSeineGearUseDto.class,
                TripTypeDto.class,
                VesselActivityLonglineDto.class,
                VesselActivitySeineDto.class,
                VesselDto.class,
                VesselSizeCategoryDto.class,
                VesselTypeDto.class,
                WeightCategoryDto.class,
                WeightMeasureTypeDto.class,
                WindDto.class
        };
    }

    @Override
    protected File getRootDirectory(File basedir) {
        return new File(basedir, "src" + File.separator + "main" + File.separator + "resources");
    }

    @Test
    public void detectAll() {

        SortedSet<NuitonValidator<?>> validators = detectValidators(ALL_TYPES);
        assertFalse(validators.isEmpty());
        assertEquals(175, validators.size());

    }

    @Test
    public void testDetectN1Create() {
        String contextName = UI_CREATE_CONTEXT_NAME;

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName(validators, contextName,
                                              ActivityLonglineDto.class,
                                              ActivitySeineDto.class,
                                              BaitHaulingStatusDto.class,
                                              BaitSettingStatusDto.class,
                                              BaitTypeDto.class,
                                              CatchFateLonglineDto.class,
                                              CountryDto.class,
                                              DetectionModeDto.class,
                                              EncounterTypeDto.class,
                                              FloatingObjectDto.class,
                                              FpaZoneDto.class,
                                              GearCaracteristicDto.class,
                                              GearCaracteristicTypeDto.class,
                                              GearDto.class,
                                              HarbourDto.class,
                                              HealthnessDto.class,
                                              HookPositionDto.class,
                                              HookSizeDto.class,
                                              HookTypeDto.class,
                                              ItemHorizontalPositionDto.class,
                                              ItemVerticalPositionDto.class,
                                              LengthWeightParameterDto.class,
                                              LightsticksColorDto.class,
                                              LightsticksTypeDto.class,
                                              LineTypeDto.class,
                                              MaturityStatusDto.class,
                                              MitigationTypeDto.class,
                                              ObjectFateDto.class,
                                              ObjectOperationDto.class,
                                              ObjectTypeDto.class,
                                              ObservedSystemDto.class,
                                              OceanDto.class,
                                              OrganismDto.class,
                                              PersonDto.class,
                                              ProgramDto.class,
                                              ReasonForDiscardDto.class,
                                              ReasonForNoFishingDto.class,
                                              ReasonForNullSetDto.class,
                                              RouteDto.class,
                                              SensorBrandDto.class,
                                              SensorDataFormatDto.class,
                                              SensorTypeDto.class,
                                              SetLonglineDto.class,
                                              SetSeineDto.class,
                                              SettingShapeDto.class,
                                              SexDto.class,
                                              SizeMeasureTypeDto.class,
                                              SpeciesDto.class,
                                              SpeciesFateDto.class,
                                              SpeciesGroupDto.class,
                                              SpeciesListDto.class,
                                              SpeciesStatusDto.class,
                                              StomacFullnessDto.class,
                                              SurroundingActivityDto.class,
                                              TransmittingBuoyOperationDto.class,
                                              TransmittingBuoyTypeDto.class,
                                              TripLonglineDto.class,
                                              TripSeineDto.class,
                                              TripTypeDto.class,
                                              VesselActivityLonglineDto.class,
                                              VesselActivitySeineDto.class,
                                              VesselDto.class,
                                              VesselSizeCategoryDto.class,
                                              VesselTypeDto.class,
                                              WeightCategoryDto.class,
                                              WeightMeasureTypeDto.class,
                                              WindDto.class);
    }

    public void assertValidatorSetWithSameContextName2(SortedSet<NuitonValidator<?>> result, String context, Class... contextThenClass) {
        Assert.assertNotNull(result);

        Iterator itr = result.iterator();
        int index = 0;
        NuitonValidatorScope[] scopes = NuitonValidatorScope.values();

        while (itr.hasNext()) {
            NuitonValidator next = (NuitonValidator) itr.next();
            ValidatorTestHelper.assertValidatorModel(next, context, contextThenClass[index++], scopes);
        }
        Assert.assertEquals((long) contextThenClass.length, (long) result.size());
    }

    @Test
    public void testDetectN1Update() {
        String contextName = UI_UPDATE_CONTEXT_NAME;

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName2(validators,
                                               contextName,
                                               ActivityLonglineDto.class,
                                               ActivityLonglineEncounterDto.class,
                                               ActivityLonglineSensorUsedDto.class,
                                               ActivitySeineDto.class,
                                               BaitHaulingStatusDto.class,
                                               BaitSettingStatusDto.class,
                                               BaitTypeDto.class,
                                               BaitsCompositionDto.class,
                                               BranchlineDto.class,
                                               BranchlinesCompositionDto.class,
                                               CatchFateLonglineDto.class,
                                               CatchLonglineDto.class,
                                               CountryDto.class,
                                               DetectionModeDto.class,
                                               EncounterDto.class,
                                               EncounterTypeDto.class,
                                               FloatingObjectDto.class,
                                               FloatingObjectSchoolEstimateDto.class,
                                               FloatingObjectTransmittingBuoyDto.class,
                                               FloatlinesCompositionDto.class,
                                               FpaZoneDto.class,
                                               GearCaracteristicDto.class,
                                               GearCaracteristicTypeDto.class,
                                               GearDto.class,
                                               GearUseFeaturesLonglineDto.class,
                                               GearUseFeaturesSeineDto.class,
                                               HarbourDto.class,
                                               HealthnessDto.class,
                                               HookPositionDto.class,
                                               HookSizeDto.class,
                                               HookTypeDto.class,
                                               HooksCompositionDto.class,
                                               ItemHorizontalPositionDto.class,
                                               ItemVerticalPositionDto.class,
                                               LengthWeightParameterDto.class,
                                               LightsticksColorDto.class,
                                               LightsticksTypeDto.class,
                                               LineTypeDto.class,
                                               MaturityStatusDto.class,
                                               MitigationTypeDto.class,
                                               NonTargetCatchDto.class,
                                               NonTargetLengthDto.class,
                                               NonTargetSampleDto.class,
                                               ObjectFateDto.class,
                                               ObjectObservedSpeciesDto.class,
                                               ObjectOperationDto.class,
                                               ObjectSchoolEstimateDto.class,
                                               ObjectTypeDto.class,
                                               ObservedSystemDto.class,
                                               OceanDto.class,
                                               OrganismDto.class,
                                               PersonDto.class,
                                               ProgramDto.class,
                                               ReasonForDiscardDto.class,
                                               ReasonForNoFishingDto.class,
                                               ReasonForNullSetDto.class,
                                               RouteDto.class,
                                               SchoolEstimateDto.class,
                                               SensorBrandDto.class,
                                               SensorDataFormatDto.class,
                                               SensorTypeDto.class,
                                               SensorUsedDto.class,
                                               SetLonglineDto.class,
                                               SetSeineDto.class,
                                               SetSeineNonTargetCatchDto.class,
                                               SetSeineSchoolEstimateDto.class,
                                               SettingShapeDto.class,
                                               SexDto.class,
                                               SizeMeasureTypeDto.class,
                                               SpeciesDto.class,
                                               SpeciesFateDto.class,
                                               SpeciesGroupDto.class,
                                               SpeciesListDto.class,
                                               SpeciesStatusDto.class,
                                               StomacFullnessDto.class,
                                               SurroundingActivityDto.class,
                                               TargetLengthDto.class,
                                               TargetSampleDto.class,
                                               TdrDto.class,
                                               TransmittingBuoyDto.class,
                                               TransmittingBuoyOperationDto.class,
                                               TransmittingBuoyTypeDto.class,
                                               TripLonglineDto.class,
                                               TripLonglineGearUseDto.class,
                                               TripSeineDto.class,
                                               TripSeineGearUseDto.class,
                                               TripTypeDto.class,
                                               VesselActivityLonglineDto.class,
                                               VesselActivitySeineDto.class,
                                               VesselDto.class,
                                               VesselSizeCategoryDto.class,
                                               VesselTypeDto.class,
                                               WeightCategoryDto.class,
                                               WeightMeasureTypeDto.class,
                                               WindDto.class);
    }

    @Test
    public void testDetectN1UpdateExt() {
        String contextName = UI_UPDATE_CONTEXT_NAME;

        validators = detectValidators(Pattern.compile(contextName + "-.*"), ALL_TYPES);

        assertValidatorSetWithMultiContextName(validators,
//                                               contextName + "-encounter", ActivityLonglineDto.class,
//                                               contextName + "-sensorUsed", ActivityLonglineDto.class,
                                               contextName + "-table", BasketDto.class,
                                               contextName + "-catchLongline", BranchlineDto.class,
                                               contextName + "-table", BranchlineDto.class,
//                                               contextName + "-objectSchoolEstimate", FloatingObjectDto.class,
//                                               contextName + "-transmittingBuoyOperation", FloatingObjectDto.class,
                                               contextName + "-table", SectionDto.class,
                                               contextName + "-baitsComposition", SetLonglineGlobalCompositionDto.class,
                                               contextName + "-branchlinesComposition", SetLonglineGlobalCompositionDto.class,
                                               contextName + "-floatlinesComposition", SetLonglineGlobalCompositionDto.class,
                                               contextName + "-globalComposition", SetLonglineGlobalCompositionDto.class,
                                               contextName + "-hooksComposition", SetLonglineGlobalCompositionDto.class,
//                                               contextName + "-nonTargetCatch", SetSeineDto.class,
//                                               contextName + "-schoolEstimate", SetSeineDto.class,
                                               contextName + "-targetCatch", SetSeineTargetCatchDto.class,
                                               contextName + "-targetDiscarded", SetSeineTargetCatchDto.class,
                                               contextName + "-targetCatch", TargetCatchDto.class,
                                               contextName + "-targetDiscarded", TargetCatchDto.class
//                                               contextName + "-gearUseFeatures", TripSeineDto.class
        );

    }

    public void assertValidatorSetWithMultiContextName(SortedSet<NuitonValidator<?>> result, Object... contextThenClass) {
        Assert.assertNotNull(result);
        Iterator itr = result.iterator();
        int index = 0;

        for (NuitonValidatorScope[] scopes = NuitonValidatorScope.values(); itr.hasNext(); ++index) {
            NuitonValidator next = (NuitonValidator) itr.next();
            ValidatorTestHelper.assertValidatorModel(next, (String) contextThenClass[2 * index], (Class) contextThenClass[2 * index + 1], scopes);
        }
        Assert.assertEquals((long) (contextThenClass.length / 2), (long) result.size());

    }
}
