# ObServe changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2017-02-21 11:59.

## Version [5.3.2](https://gitlab.com/ultreia.io/ird-observe/milestones/84)
&#10;&#10;*(from redmine: created on 2017-02-04)*

### Download (staging)
* [Application (observe-5.3.2.zip)](https://oss.sonatype.org/content/repositories/frirdobserve-1003/fr/ird/observe/observe/5.3.2/observe-5.3.2.zip)
* [Serveur (observe-5.3.2.war)](https://oss.sonatype.org/content/repositories/frirdobserve-1003/fr/ird/observe/observe/5.3.2/observe-5.3.2.war)

### Issues
  * [[Anomalie 704]](https://gitlab.com/ultreia.io/ird-observe/issues/704) **Ré ouverture de #8879** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 721]](https://gitlab.com/ultreia.io/ird-observe/issues/721) **Le code de vitesse du vent n&#39;apparaît pas dans sa liste déroulante, alors qu&#39;il est bien présent dans la base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 725]](https://gitlab.com/ultreia.io/ird-observe/issues/725) **La suppression d&#39;un type de sexe ne marche toujours pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 726]](https://gitlab.com/ultreia.io/ird-observe/issues/726) **Problème DTO sur espèce toujours présent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 727]](https://gitlab.com/ultreia.io/ird-observe/issues/727) **La consultation des références utilisées par une espèce ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 734]](https://gitlab.com/ultreia.io/ird-observe/issues/734) **Formulaire balise, ergonomie graphique et barre de séparation horizontale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 736]](https://gitlab.com/ultreia.io/ird-observe/issues/736) **Petits défauts d&#39;affichage dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 311]](https://gitlab.com/ultreia.io/ird-observe/issues/311) **[PS] Forcer l&#39;utilisateur à consulter l&#39;écran &quot;Systèmes observés&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 723]](https://gitlab.com/ultreia.io/ird-observe/issues/723) **Nom de l&#39;application dans sa barre de titre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.3.1](https://gitlab.com/ultreia.io/ird-observe/milestones/83)
&#10;&#10;*(from redmine: created on 2017-01-20)*

**Closed at 2017-01-20.**

### Download
* [Application (observe-5.3.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.3.1/observe-5.3.1.zip)
* [Serveur (observe-5.3.1.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.3.1/observe-5.3.1.war)

### Issues
  * [[Anomalie 720]](https://gitlab.com/ultreia.io/ird-observe/issues/720) **Les minutes dans le format DMD sont multipliés par 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.3](https://gitlab.com/ultreia.io/ird-observe/milestones/82)
&#10;&#10;*(from redmine: created on 2017-01-17)*

**Closed at 2017-01-19.**

### Download
* [Application (observe-5.3.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.3/observe-5.3.zip)
* [Serveur (observe-5.3.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.3/observe-5.3.war)

### Issues
  * [[Anomalie 702]](https://gitlab.com/ultreia.io/ird-observe/issues/702) **confusion base source/base de référence dans MAJ** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 707]](https://gitlab.com/ultreia.io/ird-observe/issues/707) **Le libellé observe.common.label a sauté** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 709]](https://gitlab.com/ultreia.io/ird-observe/issues/709) **Bascule distant-&gt; local lorsque la connexion serveur a été perdue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 713]](https://gitlab.com/ultreia.io/ird-observe/issues/713) **Petites mises au point sur le widget positions** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 714]](https://gitlab.com/ultreia.io/ird-observe/issues/714) **problème avec widget position** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 715]](https://gitlab.com/ultreia.io/ird-observe/issues/715) **Une exception apparait si on ouvre l&#39;assistant de changement de connexion et qu&#39;on annule** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 717]](https://gitlab.com/ultreia.io/ird-observe/issues/717) **Le serveur ouvre une quantité astronomique de connexions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 701]](https://gitlab.com/ultreia.io/ird-observe/issues/701) **Changement libellé dans popup &quot;Changer la source de données&quot;** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 716]](https://gitlab.com/ultreia.io/ird-observe/issues/716) **Templates de traduction mis à jour** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 719]](https://gitlab.com/ultreia.io/ird-observe/issues/719) **Les corrections orthographiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.2.1](https://gitlab.com/ultreia.io/ird-observe/milestones/81)
&#10;&#10;*(from redmine: created on 2017-01-06)*

**Closed at 2017-01-13.**

### Download
* [Application (observe-5.2.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.2.1/observe-5.2.1.zip)
* [Serveur (observe-5.2.1.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.2.1/observe-5.2.1.war)

### Issues
  * [[Anomalie 419]](https://gitlab.com/ultreia.io/ird-observe/issues/419) **[LL] libellés à mettre a jour dans l&#39;interface** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 657]](https://gitlab.com/ultreia.io/ird-observe/issues/657) **UI Assistant connexion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 691]](https://gitlab.com/ultreia.io/ird-observe/issues/691) **Sur autres entités qu&#39;espèce, la synchro de référentiel avancée plante toujours** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 692]](https://gitlab.com/ultreia.io/ird-observe/issues/692) **[BIS] Le transfert de 4 nouvelles marées de droite à gauche n&#39;aboutit pas** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 697]](https://gitlab.com/ultreia.io/ird-observe/issues/697) **Problème DTO sur espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 698]](https://gitlab.com/ultreia.io/ird-observe/issues/698) **Lors de la suppression d&#39;un programme, l&#39;UI propose des programmes qui ne sont pas du type de la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 699]](https://gitlab.com/ultreia.io/ird-observe/issues/699) **La suppression d&#39;un type de sexe ne marche pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 700]](https://gitlab.com/ultreia.io/ird-observe/issues/700) **La suppression d&#39;un équipement dans le référentiel ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 706]](https://gitlab.com/ultreia.io/ird-observe/issues/706) **Mode de connexion par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [6.0-RC-3](https://gitlab.com/ultreia.io/ird-observe/milestones/80)
&#10;&#10;*(from redmine: created on 2017-01-01)*

**Closed at 2016-01-01.**


### Issues
No issue.

## Version [6.0-RC-2](https://gitlab.com/ultreia.io/ird-observe/milestones/79)
&#10;&#10;*(from redmine: created on 2016-12-20)*

**Closed at 2016-12-23.**


### Issues
No issue.

## Version [6.0-RC-1](https://gitlab.com/ultreia.io/ird-observe/milestones/78)
&#10;&#10;*(from redmine: created on 2016-12-17)*

**Closed at 2016-12-17.**


### Issues
No issue.

## Version [5.1.4](https://gitlab.com/ultreia.io/ird-observe/milestones/77)
&#10;&#10;*(from redmine: created on 2016-12-16)*

**Closed at 2016-12-16.**

### Download
* [Application (observe-5.1.4.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.4/observe-5.1.4.zip)
* [Serveur (observe-5.1.4.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.4/observe-5.1.4.war)

### Issues
  * [[Anomalie 674]](https://gitlab.com/ultreia.io/ird-observe/issues/674) **Problèmes d&#39;affichage des formulaires depuis la 5.1.3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 675]](https://gitlab.com/ultreia.io/ird-observe/issues/675) **Exception si tentative de suppression d&#39;une espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 677]](https://gitlab.com/ultreia.io/ird-observe/issues/677) **Erreur sur suppression d&#39;une espèce (avec remplacement)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 679]](https://gitlab.com/ultreia.io/ird-observe/issues/679) **Plantage de la synchro avancée de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 680]](https://gitlab.com/ultreia.io/ird-observe/issues/680) **Mise en page de l&#39;écran synchro avancée de marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 684]](https://gitlab.com/ultreia.io/ird-observe/issues/684) **Le transfert de 4 nouvelles marées de droite à gauche n&#39;aboutit pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 685]](https://gitlab.com/ultreia.io/ird-observe/issues/685) **Gestion avancée des données - désactiver le bouton Suivant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 676]](https://gitlab.com/ultreia.io/ird-observe/issues/676) **Rendre le dialogue de désactivation plus explicite** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 678]](https://gitlab.com/ultreia.io/ird-observe/issues/678) **Synchro avancée de référentiel : griser les boutons des modes de fonctionnement non pertinents** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 681]](https://gitlab.com/ultreia.io/ird-observe/issues/681) **Droits sur synchro avancée de marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 683]](https://gitlab.com/ultreia.io/ird-observe/issues/683) **Nouvelle icône** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 686]](https://gitlab.com/ultreia.io/ird-observe/issues/686) **Use Topia 3.2 and Hibernate 5.1.3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 687]](https://gitlab.com/ultreia.io/ird-observe/issues/687) **Mise à jour des libraries** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.1.3](https://gitlab.com/ultreia.io/ird-observe/milestones/76)
&#10;&#10;*(from redmine: created on 2016-12-14)*

**Closed at 2016-12-14.**

### Download
* [Application (observe-5.1.3.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.3/observe-5.1.3.zip)
* [Serveur (observe-5.1.3.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.3/observe-5.1.3.war)

### Issues
  * [[Anomalie 665]](https://gitlab.com/ultreia.io/ird-observe/issues/665) **Problèmes de gestion mémoire lors d&#39;exports de marées en mode classique &amp; serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 672]](https://gitlab.com/ultreia.io/ird-observe/issues/672) **Mauvais chargement de l&#39;action de synchro référentiel dans certains cas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 673]](https://gitlab.com/ultreia.io/ird-observe/issues/673) **Si on ferme la base, il reste des références sur celle-ci, ce qui peut induire des fuites mémoires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 671]](https://gitlab.com/ultreia.io/ird-observe/issues/671) **Supprimer la configuration de la mémoire maximum à utiliser** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.1.2](https://gitlab.com/ultreia.io/ird-observe/milestones/75)
&#10;&#10;*(from redmine: created on 2016-12-10)*

**Closed at 2016-12-12.**

### Download
* [Application (observe-5.1.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.2/observe-5.1.2.zip)
* [Serveur (observe-5.1.2.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.2/observe-5.1.2.war)

### Issues
  * [[Anomalie 660]](https://gitlab.com/ultreia.io/ird-observe/issues/660) **L&#39;application ne gère pas bien le rappel du formulaire ouvert** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 662]](https://gitlab.com/ultreia.io/ird-observe/issues/662) **Problème d&#39;interprétation de coordonnée en DMD par le widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 663]](https://gitlab.com/ultreia.io/ird-observe/issues/663) **Classement des marées dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 664]](https://gitlab.com/ultreia.io/ird-observe/issues/664) **Impossible d&#39;exporter des marées en mode serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 666]](https://gitlab.com/ultreia.io/ird-observe/issues/666) **Mémorisation du dernier dossier utilisé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 667]](https://gitlab.com/ultreia.io/ird-observe/issues/667) **Problème de chargement du fichier des tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 669]](https://gitlab.com/ultreia.io/ird-observe/issues/669) **[persistence] Toujours utiliser la configuration hibernate customisé même pour les services de topia** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.1.1](https://gitlab.com/ultreia.io/ird-observe/milestones/74)
&#10;&#10;*(from redmine: created on 2016-12-08)*

**Closed at 2016-12-08.**

### Download
* [Application (observe-5.1.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.1/observe-5.1.1.zip)
* [Serveur (observe-5.1.1.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1.1/observe-5.1.1.war)

### Issues
  * [[Anomalie 650]](https://gitlab.com/ultreia.io/ird-observe/issues/650) **Mode serveur : URL de l&#39;interface web** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 654]](https://gitlab.com/ultreia.io/ird-observe/issues/654) **Redimensionnement de l&#39;assistant connexion** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 655]](https://gitlab.com/ultreia.io/ird-observe/issues/655) **Problèmes lors de la synchro du référentiel de bases locales** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 656]](https://gitlab.com/ultreia.io/ird-observe/issues/656) **[LL] autocomplétion des 3 champs de position sur la ligne** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)

## Version [5.1](https://gitlab.com/ultreia.io/ird-observe/milestones/73)
&#10;&#10;*(from redmine: created on 2016-12-04)*

**Closed at 2016-12-05.**

### Download
* [Application (observe-5.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1/observe-5.1.zip)
* [Serveur (observe-5.1.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.1/observe-5.1.war)

### Issues
  * [[Anomalie 530]](https://gitlab.com/ultreia.io/ird-observe/issues/530) **[PS] Des activités saisies ne sont pas retrouvées à la réouverture de la base** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 615]](https://gitlab.com/ultreia.io/ird-observe/issues/615) **[CARTE] Des exceptions apparaissent dans le terminal lorsqu&#39;on demande l&#39;affichage des cartes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 625]](https://gitlab.com/ultreia.io/ird-observe/issues/625) **La sauvegarde de marées central-&gt;local plante au-delà d&#39;une dizaine de marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 641]](https://gitlab.com/ultreia.io/ird-observe/issues/641) **Encore le widget positions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 643]](https://gitlab.com/ultreia.io/ird-observe/issues/643) **[LL] Problème de copie de marée avec des pièces-jointes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 644]](https://gitlab.com/ultreia.io/ird-observe/issues/644) **Validateur batch** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 243]](https://gitlab.com/ultreia.io/ird-observe/issues/243) **[ObserveLL] Trip - SensorUsed : laisser le bouton &quot;importer&quot; disponible même si un fichier a déjà été téléchargé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 270]](https://gitlab.com/ultreia.io/ird-observe/issues/270) **[LL] Piloter les fonctions/formulaires disponibles en fonction du type d&#39;activité choisi** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 295]](https://gitlab.com/ultreia.io/ird-observe/issues/295) **[LL] Formulaire Composition détaillée / onglet &quot;Détail Avançon&quot; : Quelques améliorations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 301]](https://gitlab.com/ultreia.io/ird-observe/issues/301) **[LL][PS] Formulaires Marée : quelques améliorations d&#39;ergonomie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 308]](https://gitlab.com/ultreia.io/ird-observe/issues/308) **[LL/PS] amélioration sur l&#39;écran activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 315]](https://gitlab.com/ultreia.io/ird-observe/issues/315) **Pour les rapports, proposer un choix sur le type de marées à utiliser** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 444]](https://gitlab.com/ultreia.io/ird-observe/issues/444) **[PS] Améliorer l&#39;ergonomie de l&#39;écran de saisie des mensurations de non target** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 518]](https://gitlab.com/ultreia.io/ird-observe/issues/518) **[LL] réorganisation du layout du formulaire Activité/Opération de pêche/Caractéristiques pour rapprocher les informations associées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 521]](https://gitlab.com/ultreia.io/ird-observe/issues/521) **[LL] taille hameçon doit être un champ non obligatoire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 522]](https://gitlab.com/ultreia.io/ird-observe/issues/522) **[LL] état appâts au filage doit être un champ non obligatoire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 524]](https://gitlab.com/ultreia.io/ird-observe/issues/524) **[LL] champ boolean &quot;rejeté avec hamecon&quot; doit accepter une valeur NULL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 525]](https://gitlab.com/ultreia.io/ird-observe/issues/525) **[LL] le champ &quot;devenir capture&quot; doit être obligatoire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 526]](https://gitlab.com/ultreia.io/ird-observe/issues/526) **[LL] Mode de saisie des captures &#39;groupé&#39; par défaut dans le cas d&#39;une marée auto-échantillonnage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 528]](https://gitlab.com/ultreia.io/ird-observe/issues/528) **[LL] défaut d&#39;affichage coord DMD** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 555]](https://gitlab.com/ultreia.io/ird-observe/issues/555) **[REFERENTIEL] Automatiser la réattribution de code lors de la désactivation ou la suppression d&#39;une ligne de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 573]](https://gitlab.com/ultreia.io/ird-observe/issues/573) **[UI][PS][LL] Widget des positions DMD par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 574]](https://gitlab.com/ultreia.io/ird-observe/issues/574) **[UI] Masque de saisie sur les formulaires captures (et surtout) rejets de thonidés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 635]](https://gitlab.com/ultreia.io/ird-observe/issues/635) **Petites améliorations ergonomiques dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.2](https://gitlab.com/ultreia.io/ird-observe/milestones/72)
&#10;&#10;*(from redmine: created on 2016-10-04)*

**Closed at 2017-01-06.**

### Download
* [Application (observe-5.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.2/observe-5.2.zip)
* [Serveur (observe-5.2.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.2/observe-5.2.war)

### Issues
  * [[Anomalie 658]](https://gitlab.com/ultreia.io/ird-observe/issues/658) **[LL] classement des captures** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 661]](https://gitlab.com/ultreia.io/ird-observe/issues/661) **Problèmes de fiabilité/rafraichissement sur le formulaire LL Captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 688]](https://gitlab.com/ultreia.io/ird-observe/issues/688) **L&#39;écran A propos ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 690]](https://gitlab.com/ultreia.io/ird-observe/issues/690) **Bug sur synchro avancée de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 693]](https://gitlab.com/ultreia.io/ird-observe/issues/693) **Il existe encore des validations qui ne passent pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 694]](https://gitlab.com/ultreia.io/ird-observe/issues/694) **La connexion serveur HTTPS ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 519]](https://gitlab.com/ultreia.io/ird-observe/issues/519) **[LL] changement libellé &quot;Instrumentée&quot; -&gt; &quot;Horloges utilisées&quot; sur formulaire Activité/Opération de pêche/Caractéristiques** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 585]](https://gitlab.com/ultreia.io/ird-observe/issues/585) **Changement libellé &quot;Configuration de la base de référence&quot; sur écran &quot;Exporter les données&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.2](https://gitlab.com/ultreia.io/ird-observe/milestones/71)
&#10;&#10;*(from redmine: created on 2016-09-21)*

**Closed at 2016-10-04.**

### Download
* [Application (observe-5.0.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0.2/observe-5.0.2.zip)
* [Serveur (observe-5.0.2.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0.2/observe-5.0.2.war)

### Issues
  * [[Anomalie 293]](https://gitlab.com/ultreia.io/ird-observe/issues/293) **Comportements incorrects du widget de saisie des coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 343]](https://gitlab.com/ultreia.io/ird-observe/issues/343) **Comportement du widget de positions - le quandrant change tout seul lors de la saisie des coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 344]](https://gitlab.com/ultreia.io/ird-observe/issues/344) **Le widget des positions a effacé les degrés lors de l&#39;enregistrement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 426]](https://gitlab.com/ultreia.io/ird-observe/issues/426) **[ObServeLL] l&#39;appli de démarre plus** (Thanks to Philippe Sabarro) (Reported by Tony CHEMIT)
  * [[Anomalie 514]](https://gitlab.com/ultreia.io/ird-observe/issues/514) **Bug d&#39;affichage des scrolling bars dans les listes sous windows** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 537]](https://gitlab.com/ultreia.io/ird-observe/issues/537) **[PS] format champ starttime dans observe_seine.set** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 542]](https://gitlab.com/ultreia.io/ird-observe/issues/542) **[LL] Utilisation des calendriers de filage/virage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 620]](https://gitlab.com/ultreia.io/ird-observe/issues/620) **[WIN] Le serveur ne démarre pas sous Windows en raison d&#39;une erreur POSIX** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 622]](https://gitlab.com/ultreia.io/ird-observe/issues/622) **Toujours générer le fichier de configuration des logs (et pas seulement en dev mode)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 624]](https://gitlab.com/ultreia.io/ird-observe/issues/624) **[GEAR] Affichage des valeurs 0 dans les caractéristiques de type Entier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 627]](https://gitlab.com/ultreia.io/ird-observe/issues/627) **[UI] Assistant de connexion à un serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 630]](https://gitlab.com/ultreia.io/ird-observe/issues/630) **La boîte &quot;A propos&quot; ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 633]](https://gitlab.com/ultreia.io/ird-observe/issues/633) **Dans l&#39;UI de gestion avancée du référentiel, l&#39;affichage des dates est différent à droite et à gauche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 634]](https://gitlab.com/ultreia.io/ird-observe/issues/634) **[GESTION AVANCEE DES MAREES] Identifier les marées qui sont présentes des 2 côtés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 639]](https://gitlab.com/ultreia.io/ird-observe/issues/639) **[GESTION AVANCEE REFERENTIEL] Comportement de l&#39;UI gestion avancée du référentiel suite à migration de bases** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 640]](https://gitlab.com/ultreia.io/ird-observe/issues/640) **[GESTION AVANCEE REFERENTIEL] 2 boutons semblent avoir la même fonction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 132]](https://gitlab.com/ultreia.io/ird-observe/issues/132) **Faire apparaître les topiaid sur les formulaires...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 198]](https://gitlab.com/ultreia.io/ird-observe/issues/198) **Sauver la mise en page de l&#39;UI en quittant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 200]](https://gitlab.com/ultreia.io/ird-observe/issues/200) **Améliorer l&#39;algorithme de calcul des vitesses** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 271]](https://gitlab.com/ultreia.io/ird-observe/issues/271) **Ouverture simultannée de plusieurs marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 307]](https://gitlab.com/ultreia.io/ird-observe/issues/307) **Permettre de persister les configurations ergonomiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 366]](https://gitlab.com/ultreia.io/ird-observe/issues/366) **Erreur de type PSQLException : la valeur d&#39;une clé dupliquée rompt la contrainte... -&gt; rendre le message plus explicite** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 621]](https://gitlab.com/ultreia.io/ird-observe/issues/621) **Amélioration de la documentation sur l&#39;installation serveur web** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.1](https://gitlab.com/ultreia.io/ird-observe/milestones/70)
&#10;&#10;*(from redmine: created on 2016-09-12)*

**Closed at 2016-09-21.**

### Download
* [Application (observe-5.0.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0.1/observe-5.0.1.zip)
* [Serveur (observe-5.0.1.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0.1/observe-5.0.1.war)

### Issues
  * [[Anomalie 106]](https://gitlab.com/ultreia.io/ird-observe/issues/106) **L&#39;application ne s&#39;affiche pas sous Ubuntu Unity dans le cas d&#39;une configuration double-écran** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 507]](https://gitlab.com/ultreia.io/ird-observe/issues/507) **Dépassements de mémoire systématiques lors d&#39;export vers base centrale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 554]](https://gitlab.com/ultreia.io/ird-observe/issues/554) **[UI] Widget position géographique se comprte mal** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 613]](https://gitlab.com/ultreia.io/ird-observe/issues/613) **[UI] Le widget Positions ne se comporte toujours pas correctement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 616]](https://gitlab.com/ultreia.io/ird-observe/issues/616) **L&#39;application ne crée plus de sauvegarde en quittant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 618]](https://gitlab.com/ultreia.io/ird-observe/issues/618) **Le bouton avec le rappel de configuration de source ne se met plus à jour correctement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 619]](https://gitlab.com/ultreia.io/ird-observe/issues/619) **L&#39;application web ne démarre pas sous tomcat 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 130]](https://gitlab.com/ultreia.io/ird-observe/issues/130) **Script de lancement sans fenêtre de terminal** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 256]](https://gitlab.com/ultreia.io/ird-observe/issues/256) **Classement par codes dans les combobox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 309]](https://gitlab.com/ultreia.io/ird-observe/issues/309) **Cosmétique sur la gestion de 2 exceptions bien identifiées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 406]](https://gitlab.com/ultreia.io/ird-observe/issues/406) **En mode connecté à la base centrale, ignorer le mécanisme d&#39;ouverture/fermeture des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 614]](https://gitlab.com/ultreia.io/ird-observe/issues/614) **[VALIDATION] Remettre en route le contrôle des position avec possibilité de le désactiver** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 617]](https://gitlab.com/ultreia.io/ird-observe/issues/617) **Simplification de la configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0-RC-3](https://gitlab.com/ultreia.io/ird-observe/milestones/68)
&#10;&#10;*(from redmine: created on 2016-09-05)*

**Closed at 2016-09-07.**


### Issues
  * [[Anomalie 421]](https://gitlab.com/ultreia.io/ird-observe/issues/421) **Comportement de l&#39;assistant de chargement d&#39;une sauvegarde** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 553]](https://gitlab.com/ultreia.io/ird-observe/issues/553) **[UI] Mauvais fonctionnement dans les caractéristiques d&#39;équipements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 603]](https://gitlab.com/ultreia.io/ird-observe/issues/603) **[Calee/Rejet de thon] Niveau de validation du champ &quot;Poids estimé&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 604]](https://gitlab.com/ultreia.io/ird-observe/issues/604) **[Actions] Mise à jour du référentiel, le bouton suivant est actif même si la base de référence n&#39;est pas configurée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 605]](https://gitlab.com/ultreia.io/ird-observe/issues/605) **[Actions] Exception lors de la validation des données référentiel en serveur distant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 606]](https://gitlab.com/ultreia.io/ird-observe/issues/606) **[Action] Erreur lors de la validation d&#39;une marée LL avec capteur utilisé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 607]](https://gitlab.com/ultreia.io/ird-observe/issues/607) **[Activité de fin de veille] Impossibe de créer une activité de fin de veille à la cloture d&#39;une route** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 608]](https://gitlab.com/ultreia.io/ird-observe/issues/608) **[Arbre] Les changements de routes d&#39;ue marée à l&#39;autre ou d&#39;une activité d&#39;une route à l&#39;autre ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 401]](https://gitlab.com/ultreia.io/ird-observe/issues/401) **[Référentiel] Le passage des champs &quot;code&quot; en caractères n&#39;est pas satisfaisant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0-RC-2](https://gitlab.com/ultreia.io/ird-observe/milestones/67)
&#10;&#10;*(from redmine: created on 2016-09-01)*

**Closed at 2016-09-05.**


### Issues
  * [[Anomalie 105]](https://gitlab.com/ultreia.io/ird-observe/issues/105) **Valeurs nulles autorisées dans les champs contenant des topiaid de clés étrangères** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 112]](https://gitlab.com/ultreia.io/ird-observe/issues/112) **Réplication des données non cloisonnée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 259]](https://gitlab.com/ultreia.io/ird-observe/issues/259) **[PS] Formulaire &quot;rejet de thon&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 503]](https://gitlab.com/ultreia.io/ird-observe/issues/503) **Imposible de renseigner un date si la valeur d&#39;origine est null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 544]](https://gitlab.com/ultreia.io/ird-observe/issues/544) **Id de  virage obligatoire sur sur la schéma de palangre ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 550]](https://gitlab.com/ultreia.io/ird-observe/issues/550) **Import de commentaire dont la ligne fini par un &quot;;&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 567]](https://gitlab.com/ultreia.io/ird-observe/issues/567) **[PS] Rajouter la contrainte de clé étrangère de nontargetlength.sex vers la table sex** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 590]](https://gitlab.com/ultreia.io/ird-observe/issues/590) **[Actions] [Validation] Problème de validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 592]](https://gitlab.com/ultreia.io/ird-observe/issues/592) **[Source de données] Pas de résumé lors de l&#39;import du dernier référentiel chargé (dump)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 593]](https://gitlab.com/ultreia.io/ird-observe/issues/593) **[Source de données] Il manque les templates de wizzard de source de données pour la langue anglaise** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 595]](https://gitlab.com/ultreia.io/ird-observe/issues/595) **[Carte] Il manque des symboles dans la légende et aussi dans la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 596]](https://gitlab.com/ultreia.io/ird-observe/issues/596) **[I18n] Chaines non traduites** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 598]](https://gitlab.com/ultreia.io/ird-observe/issues/598) **[Source de données] Erreur d&#39;import des marées avec PJ (oid/blob?)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 599]](https://gitlab.com/ultreia.io/ird-observe/issues/599) **[Carte] La carte ne s&#39;affiche plus en server distant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 600]](https://gitlab.com/ultreia.io/ird-observe/issues/600) **[DCP] Message de suppression d&#39;un DCP en serveur distant incorrect** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 601]](https://gitlab.com/ultreia.io/ird-observe/issues/601) **Revoir le chargement de l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 602]](https://gitlab.com/ultreia.io/ird-observe/issues/602) **[Reférentiel] Les actions ne sont pas bien connectés avec la sélection dans la liste** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 448]](https://gitlab.com/ultreia.io/ird-observe/issues/448) **Dans les écrans de consultation des données de référence...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 591]](https://gitlab.com/ultreia.io/ird-observe/issues/591) **Optimiser les dépendences** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0-RC-1](https://gitlab.com/ultreia.io/ird-observe/milestones/65)
&#10;&#10;*(from redmine: created on 2016-03-01)*

**Closed at 2016-09-01.**


### Issues
  * [[Evolution 119]](https://gitlab.com/ultreia.io/ird-observe/issues/119) **Synchronisation de marées entre bases centrales** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 120]](https://gitlab.com/ultreia.io/ird-observe/issues/120) **Synchronisation de référentiel : plus de possibilités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 427]](https://gitlab.com/ultreia.io/ird-observe/issues/427) **Migration vers ToPIA 3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 430]](https://gitlab.com/ultreia.io/ird-observe/issues/430) **Gestion de la date de dernière mise à jour des entités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 434]](https://gitlab.com/ultreia.io/ird-observe/issues/434) **[Actions] Migration de l&#39;action de consolidation des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 435]](https://gitlab.com/ultreia.io/ird-observe/issues/435) **[Actions] Migration de l&#39;action de validation des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 438]](https://gitlab.com/ultreia.io/ird-observe/issues/438) **[Actions] Migration de l&#39;action des tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 449]](https://gitlab.com/ultreia.io/ird-observe/issues/449) **Migration des écrans de saisie (utilisation des services)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 452]](https://gitlab.com/ultreia.io/ird-observe/issues/452) **[Actions] Migration des actions d&#39;administration des sources de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 454]](https://gitlab.com/ultreia.io/ird-observe/issues/454) **Migration de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 455]](https://gitlab.com/ultreia.io/ird-observe/issues/455) **[PS] Migration de l&#39;écran d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 456]](https://gitlab.com/ultreia.io/ird-observe/issues/456) **[LL] Migration de l&#39;écran d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 457]](https://gitlab.com/ultreia.io/ird-observe/issues/457) **[PS] Migration de l&#39;écran des équipements d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 458]](https://gitlab.com/ultreia.io/ird-observe/issues/458) **[LL] Migration de l&#39;écran des équipements d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 459]](https://gitlab.com/ultreia.io/ird-observe/issues/459) **[PS] Migration de l&#39;écran des routes d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 460]](https://gitlab.com/ultreia.io/ird-observe/issues/460) **[PS] Migration de l&#39;écran d&#39;une route** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 461]](https://gitlab.com/ultreia.io/ird-observe/issues/461) **[LL] Migration de l&#39;écran des activités d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 462]](https://gitlab.com/ultreia.io/ird-observe/issues/462) **[PS] Migration de l&#39;écran d&#39;une activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 463]](https://gitlab.com/ultreia.io/ird-observe/issues/463) **[LL] Migration de l&#39;écran d&#39;une activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 464]](https://gitlab.com/ultreia.io/ird-observe/issues/464) **[PS] Migration de l&#39;écran des systèmes observés d&#39;une activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 465]](https://gitlab.com/ultreia.io/ird-observe/issues/465) **[PS] Migration de l&#39;écran d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 466]](https://gitlab.com/ultreia.io/ird-observe/issues/466) **[PS] Migration de l&#39;écran d&#39;un objet flottant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 467]](https://gitlab.com/ultreia.io/ird-observe/issues/467) **[PS] Migration de l&#39;écran des opération sur objet flottant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 468]](https://gitlab.com/ultreia.io/ird-observe/issues/468) **[PS] Migration de l&#39;écran de l&#39;estimation du banc objet sur objet flottant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 469]](https://gitlab.com/ultreia.io/ird-observe/issues/469) **[PS] Migration de l&#39;écran de la faune observé sur objet flottant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 470]](https://gitlab.com/ultreia.io/ird-observe/issues/470) **[PS] Migration de l&#39;écran de l&#39;estimation du banc d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 471]](https://gitlab.com/ultreia.io/ird-observe/issues/471) **[PS] Migration de l&#39;écran des captures cibles d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 472]](https://gitlab.com/ultreia.io/ird-observe/issues/472) **[PS] Migration de l&#39;écran des rejets thon d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 473]](https://gitlab.com/ultreia.io/ird-observe/issues/473) **[PS] Migration de l&#39;écran des échantillon thon rejeté d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 474]](https://gitlab.com/ultreia.io/ird-observe/issues/474) **[PS] Migration de l&#39;écran des échantillons thon capturés d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 475]](https://gitlab.com/ultreia.io/ird-observe/issues/475) **[PS] Migration de l&#39;écran des échantillons faune accessoire d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 476]](https://gitlab.com/ultreia.io/ird-observe/issues/476) **[PS] Migration de l&#39;écran de la faune accessoire conservée ou rejetée d&#39;une calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 477]](https://gitlab.com/ultreia.io/ird-observe/issues/477) **[LL] Migration des écrans du détail de l&#39;opération** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 478]](https://gitlab.com/ultreia.io/ird-observe/issues/478) **[LL] Migration de l&#39;écran de la composition globale d&#39;une opération** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 479]](https://gitlab.com/ultreia.io/ird-observe/issues/479) **[LL] Migration de l&#39;écran de définition du schéma de palangre d&#39;une opération** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 480]](https://gitlab.com/ultreia.io/ird-observe/issues/480) **[LL] Migration de l&#39;écran des captures d&#39;une opération** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 481]](https://gitlab.com/ultreia.io/ird-observe/issues/481) **[LL] Migration de l&#39;écran des enregistrements de profondeur d&#39;une opération** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 482]](https://gitlab.com/ultreia.io/ird-observe/issues/482) **[LL] Migration de l&#39;écran des rencontres d&#39;une opération de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 483]](https://gitlab.com/ultreia.io/ird-observe/issues/483) **[LL] Migration de l&#39;écran des capteurs utilisés d&#39;une opération de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 491]](https://gitlab.com/ultreia.io/ird-observe/issues/491) **[Actions] Migration de l&#39;action d&#39;exportation des données observateur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 497]](https://gitlab.com/ultreia.io/ird-observe/issues/497) **Faire paraitre le type de marée dans le nom du programme** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 504]](https://gitlab.com/ultreia.io/ird-observe/issues/504) **[Actions] Migration de l&#39;action de synchronisation du referentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 505]](https://gitlab.com/ultreia.io/ird-observe/issues/505) **Mise à jour des librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 548]](https://gitlab.com/ultreia.io/ird-observe/issues/548) **Serveur : Rendre configurable le delais d&#39;expiration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 549]](https://gitlab.com/ultreia.io/ird-observe/issues/549) **recharger la source de donné apres expiration du token** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 561]](https://gitlab.com/ultreia.io/ird-observe/issues/561) **[Source de données] Migration du changement de source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 562]](https://gitlab.com/ultreia.io/ird-observe/issues/562) **[Source de données] Migration du rechargement de la source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 563]](https://gitlab.com/ultreia.io/ird-observe/issues/563) **[Source de données] Migration de la fermeture de la source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 564]](https://gitlab.com/ultreia.io/ird-observe/issues/564) **[Source de données] Migration de l&#39;écran d&#39;information de la source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 565]](https://gitlab.com/ultreia.io/ird-observe/issues/565) **[Source de données] Migration de l&#39;import de la source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 566]](https://gitlab.com/ultreia.io/ird-observe/issues/566) **[Source de données] Migration de la sauvegarde d&#39;une source de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 582]](https://gitlab.com/ultreia.io/ird-observe/issues/582) **[Source de données] Migration du mode serveur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.6](https://gitlab.com/ultreia.io/ird-observe/milestones/64)
&#10;&#10;*(from redmine: created on 2015-11-04)*

**Closed at 2016-01-13.**

### Download
* [Application (observe-4.0.6.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.6/observe-4.0.6.zip)

### Issues
  * [[Anomalie 506]](https://gitlab.com/ultreia.io/ird-observe/issues/506) **[PS][VALIDATION] Les contrôles de saisie sur les tailles/poids min/max ne fonctionnent plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 511]](https://gitlab.com/ultreia.io/ird-observe/issues/511) **Exception JXPathNotFoundException** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 512]](https://gitlab.com/ultreia.io/ird-observe/issues/512) **[H2] Le problème de corruption à la fermeture ou à l&#39;ouverture de la base locale perdure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 494]](https://gitlab.com/ultreia.io/ird-observe/issues/494) **[PS] Gestion de l&#39;équipement du bateau &amp; affichage des unités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 532]](https://gitlab.com/ultreia.io/ird-observe/issues/532) **Augementer la mémoire à utiliser dans les scripts de démarrage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.5](https://gitlab.com/ultreia.io/ird-observe/milestones/63)
&#10;&#10;*(from redmine: created on 2015-11-03)*

**Closed at 2015-11-04.**

### Download
* [Application (observe-4.0.5.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.5/observe-4.0.5.zip)

### Issues
  * [[Anomalie 489]](https://gitlab.com/ultreia.io/ird-observe/issues/489) **[PS] Tableaux de synthèse - les dernières requêtes mises en place ne fonctionnent plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 493]](https://gitlab.com/ultreia.io/ird-observe/issues/493) **[PS] Calculs taille/poids : Problème sur recalcul** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.4](https://gitlab.com/ultreia.io/ird-observe/milestones/62)
&#10;&#10;*(from redmine: created on 2015-08-27)*

**Closed at 2015-11-03.**

### Download
* [Application (observe-4.0.4.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.4/observe-4.0.4.zip)

### Issues
  * [[Anomalie 418]](https://gitlab.com/ultreia.io/ird-observe/issues/418) **[PS] Petit soucis de gestion d&#39;événement sur la sélection du quadrant** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 437]](https://gitlab.com/ultreia.io/ird-observe/issues/437) **Bug lors du changement de nationalité d&#39;une personne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 450]](https://gitlab.com/ultreia.io/ird-observe/issues/450) **Champ résiduel issu de la traduction FR-&gt;EN** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 453]](https://gitlab.com/ultreia.io/ird-observe/issues/453) **Régulièrement, la ré ouverture du logiciel et de sa base locale se solde par une SQLGrammarException** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 488]](https://gitlab.com/ultreia.io/ird-observe/issues/488) **Plantage des calculs taille/poids lorsque des paramètres de conversion existent pour plusieurs sexes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 490]](https://gitlab.com/ultreia.io/ird-observe/issues/490) **Des clics multiples sur &quot;Charger la source de données/Utiliser&quot; provoquent une exception** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 492]](https://gitlab.com/ultreia.io/ird-observe/issues/492) **Certains Actions sur une base ouverte sont disponibles alors que la base n&#39;est pas ouverte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 432]](https://gitlab.com/ultreia.io/ird-observe/issues/432) **Contrôle des positions** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 439]](https://gitlab.com/ultreia.io/ird-observe/issues/439) **On repasse en niveau INFO les logs pour les modifications des coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 440]](https://gitlab.com/ultreia.io/ird-observe/issues/440) **Faire paraitre le type de marée dans le nom du programme** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.3](https://gitlab.com/ultreia.io/ird-observe/milestones/61)
&#10;&#10;*(from redmine: created on 2015-08-27)*

**Closed at 2015-08-27.**

### Download
* [Application (observe-4.0.3.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.3/observe-4.0.3.zip)

### Issues
  * [[Anomalie 420]](https://gitlab.com/ultreia.io/ird-observe/issues/420) **Problème avec la saisie des positions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 433]](https://gitlab.com/ultreia.io/ird-observe/issues/433) **Souci sur une fonction SQL de la migration 4.0.2 avec PostgreSQL 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.1.1](https://gitlab.com/ultreia.io/ird-observe/milestones/60)
&#10;&#10;*(from redmine: created on 2015-08-14)*

**Closed at 2015-08-14.**

### Download
* [Application (observe-4.0.1.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.1.1/observe-4.0.1.1.zip)

### Issues
  * [[Anomalie 425]](https://gitlab.com/ultreia.io/ird-observe/issues/425) **Correction du chemin de fichier de log** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 275]](https://gitlab.com/ultreia.io/ird-observe/issues/275) **Mise en forme des tableaux comprenant des cases à cocher** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.2](https://gitlab.com/ultreia.io/ird-observe/milestones/59)
&#10;&#10;*(from redmine: created on 2015-08-11)*

**Closed at 2015-08-27.**

### Download
* [Application (observe-4.0.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0.2/observe-4.0.2.zip)

### Issues
  * [[Anomalie 428]](https://gitlab.com/ultreia.io/ird-observe/issues/428) **La migration 4.0-&gt;4.0.1.1 plante sur PostgreSQL 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0.1](https://gitlab.com/ultreia.io/ird-observe/milestones/58)
&#10;&#10;*(from redmine: created on 2015-06-08)*

**Closed at 2015-08-14.**


### Issues
  * [[Anomalie 398]](https://gitlab.com/ultreia.io/ird-observe/issues/398) **[PS/LL] Par défaut les marées de la base locale à sauver ne sont plus sélectionnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 399]](https://gitlab.com/ultreia.io/ird-observe/issues/399) **[Référentiel] Petit défaut de libellé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 400]](https://gitlab.com/ultreia.io/ird-observe/issues/400) **[Référentiel] Problème sur l&#39;édition/validation des relations taille-poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 403]](https://gitlab.com/ultreia.io/ird-observe/issues/403) **[Réferentiel] Impossible d&#39;enregistrer une nouvelle relation taille-poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 404]](https://gitlab.com/ultreia.io/ird-observe/issues/404) **[PS] Il manque quelques contrantes FK** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 405]](https://gitlab.com/ultreia.io/ird-observe/issues/405) **[SECURITE] L&#39;assistant sécurité n&#39;applique pas la sécurité sur les fonctions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 408]](https://gitlab.com/ultreia.io/ird-observe/issues/408) **[PS] La fonction calcul écrase une donnée observée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 409]](https://gitlab.com/ultreia.io/ird-observe/issues/409) **[PS] La fonction de calcul semble ne plus calculer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 411]](https://gitlab.com/ultreia.io/ird-observe/issues/411) **[Référentiel] Ajout d&#39;un observateur/valeur pas défaut de &quot;code&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 413]](https://gitlab.com/ultreia.io/ird-observe/issues/413) **[ObServeLL] les saisies des mesures/poids ne s&#39;enregistrent pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 414]](https://gitlab.com/ultreia.io/ird-observe/issues/414) **[ObServeLL] problème ID de virage négatif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 415]](https://gitlab.com/ultreia.io/ird-observe/issues/415) **[REPLICATION] Messages &quot;exception&quot; dans la console lors des synchros de marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 416]](https://gitlab.com/ultreia.io/ird-observe/issues/416) **[PS] Problème sur la définition des équipements du bateau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 417]](https://gitlab.com/ultreia.io/ird-observe/issues/417) **[MIGRATION] Topiaids relatifs à la conversion de la senne en équipement non uniques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 422]](https://gitlab.com/ultreia.io/ird-observe/issues/422) **[ObServeLL] champs Distance et Nombre inversés dans formulaire Rencontres** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 402]](https://gitlab.com/ultreia.io/ird-observe/issues/402) **Utilisation codelutinpom 2.3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 424]](https://gitlab.com/ultreia.io/ird-observe/issues/424) **Permettre la configuration des logs au niveau utilisateur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC8](https://gitlab.com/ultreia.io/ird-observe/milestones/57)
&#10;&#10;*(from redmine: created on 2015-05-29)*

**Closed at 2015-05-29.**


### Issues
  * [[Anomalie 393]](https://gitlab.com/ultreia.io/ird-observe/issues/393) **La 4 RC7 migrer en RC6, est-ce normal ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC7](https://gitlab.com/ultreia.io/ird-observe/milestones/56)
&#10;&#10;*(from redmine: created on 2015-05-26)*

**Closed at 2015-05-28.**


### Issues
  * [[Anomalie 386]](https://gitlab.com/ultreia.io/ird-observe/issues/386) **Echec de la mise à jour de la version 4.0 RC5 vers RC6 en base locale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 390]](https://gitlab.com/ultreia.io/ird-observe/issues/390) **[PS] Après migration, la senne de toutes les marées anciennes devrait être marquée comme &quot;utilisée pendant la marée&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 392]](https://gitlab.com/ultreia.io/ird-observe/issues/392) **[PS/LL] La fonction Action/Valider les données plante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 387]](https://gitlab.com/ultreia.io/ird-observe/issues/387) **Optimisation des requêtes  pour les tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 388]](https://gitlab.com/ultreia.io/ird-observe/issues/388) **Mise à jour librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 389]](https://gitlab.com/ultreia.io/ird-observe/issues/389) **Quelques ajustements sur les nouvelles requêtes des tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 391]](https://gitlab.com/ultreia.io/ird-observe/issues/391) **Amélioration sur la carte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC6](https://gitlab.com/ultreia.io/ird-observe/milestones/54)
&#10;&#10;*(from redmine: created on 2015-05-06)*

**Closed at 2015-05-23.**


### Issues
  * [[Anomalie 374]](https://gitlab.com/ultreia.io/ird-observe/issues/374) **Problème de lancement de calcul avec la jdk 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 384]](https://gitlab.com/ultreia.io/ird-observe/issues/384) **Problème technique lors de la synchro de référentiel central -&gt; local, lorsqu&#39;une entité de type SpeciesGroup a été modifiée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 385]](https://gitlab.com/ultreia.io/ird-observe/issues/385) **Mauvais topiaid sur les entités GearUseFeaturesMeasurementSeine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 367]](https://gitlab.com/ultreia.io/ird-observe/issues/367) **La synchronisation de marées se base sur la clé métier code bateau#date de débarquement et pas sur le topiaid** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 378]](https://gitlab.com/ultreia.io/ird-observe/issues/378) **[LL] Caractéristiques de la carte LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 379]](https://gitlab.com/ultreia.io/ird-observe/issues/379) **[PS] Petites améliorations souhaitées sur la carte PS** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 380]](https://gitlab.com/ultreia.io/ird-observe/issues/380) **[PS] Orthongel - Nouvelle requête Répartition des calées par cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 381]](https://gitlab.com/ultreia.io/ird-observe/issues/381) **Optimisation des mises à jour de dates de fin sur les marées et routes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 382]](https://gitlab.com/ultreia.io/ird-observe/issues/382) **[PS] Orthongel - Nouvelle requête Distribution des tailles par espèce non ciblées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 383]](https://gitlab.com/ultreia.io/ird-observe/issues/383) **[PS] Orthongel - Nouvelles requêtes Distribution des tailles par espèce ciblées (type de mesure LD1 ou LF)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC5](https://gitlab.com/ultreia.io/ird-observe/milestones/53)
&#10;&#10;*(from redmine: created on 2015-04-22)*

**Closed at 2015-05-11.**


### Issues
  * [[Anomalie 368]](https://gitlab.com/ultreia.io/ird-observe/issues/368) **[ObserveLL] libellé position verticale d&#39;un tdr** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 371]](https://gitlab.com/ultreia.io/ird-observe/issues/371) **[ObservePS] Le controle sur la durée d&#39;une calée ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 373]](https://gitlab.com/ultreia.io/ird-observe/issues/373) **[LL/PS] Problème de libellé lors de la suppression d&#39;un équipement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 375]](https://gitlab.com/ultreia.io/ird-observe/issues/375) **[LL] Définition du schéma de la palangre -&gt; indisponibilité du bouton enregistrer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 369]](https://gitlab.com/ultreia.io/ird-observe/issues/369) **[ObserveLL] Contrôle abusif longueur orin entre sections** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 372]](https://gitlab.com/ultreia.io/ird-observe/issues/372) **Ne pas afficher la carte tant qu&#39;elle n&#39;est pas chargée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC4](https://gitlab.com/ultreia.io/ird-observe/milestones/52)
&#10;&#10;*(from redmine: created on 2015-04-15)*

**Closed at 2015-04-23.**


### Issues
  * [[Anomalie 360]](https://gitlab.com/ultreia.io/ird-observe/issues/360) **[ObServePS] Impossible de rajouter des mises en oeuvre de caractéristique à un équipement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 363]](https://gitlab.com/ultreia.io/ird-observe/issues/363) **2 migrations of 4.0-RC3 ne sont pas jouées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 364]](https://gitlab.com/ultreia.io/ird-observe/issues/364) **[ObServePS] Les équipements ne sont pas synchonisés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 333]](https://gitlab.com/ultreia.io/ird-observe/issues/333) **Tracer une carte de la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 355]](https://gitlab.com/ultreia.io/ird-observe/issues/355) **Nommer les clef étrangères** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 356]](https://gitlab.com/ultreia.io/ird-observe/issues/356) **Améliorer l&#39;affichage des coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 358]](https://gitlab.com/ultreia.io/ird-observe/issues/358) **Pouvoir utiliser la scrollbar même lorsque l&#39;écran est vérouillé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 359]](https://gitlab.com/ultreia.io/ird-observe/issues/359) **[ObServePS] Avoir plus de place pour saisir les mises en oeuvre d&#39;un équipement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 361]](https://gitlab.com/ultreia.io/ird-observe/issues/361) **[ObServePS] Pouvoir supprimer une mise en oeuvre de caractéristique d&#39;un équipement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 362]](https://gitlab.com/ultreia.io/ird-observe/issues/362) **[ObServeLL] Ajout de l&#39;écran des équipements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC3](https://gitlab.com/ultreia.io/ird-observe/milestones/51)
&#10;&#10;*(from redmine: created on 2015-04-10)*

**Closed at 2015-04-17.**


### Issues
  * [[Anomalie 334]](https://gitlab.com/ultreia.io/ird-observe/issues/334) **Problème technique lors de la synchro de référentiel central -&gt; local, lorsqu&#39;une entité a été ajoutée à une display list d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 335]](https://gitlab.com/ultreia.io/ird-observe/issues/335) **La sauvegarde de marées de la base centrale vers la base locale semble ne pas fonctionner** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 337]](https://gitlab.com/ultreia.io/ird-observe/issues/337) **Le bouton &quot;Ajouter l&#39;opération de pêche&quot; ne doit être disponible que pour une activité de type &quot;Opération de pêche&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 338]](https://gitlab.com/ultreia.io/ird-observe/issues/338) **L&#39;export d&#39;une marée vers la base centrale échoue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 339]](https://gitlab.com/ultreia.io/ird-observe/issues/339) **[PS] Problèmes d&#39;utilisation du nouvel écran &#39;Equipements&#39;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 340]](https://gitlab.com/ultreia.io/ird-observe/issues/340) **[PS] Les équipements devraient avoir des caractéristiques par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 345]](https://gitlab.com/ultreia.io/ird-observe/issues/345) **[LL] Comportements erronés du widget des positions en LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 348]](https://gitlab.com/ultreia.io/ird-observe/issues/348) **[LL] Non reprise du quadrant lors de la création d&#39;une opération de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 349]](https://gitlab.com/ultreia.io/ird-observe/issues/349) **[LL] Lors de l&#39;enregistrement d&#39;une opérationde pêche, perte de l&#39;infromation sur les quadrants** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 350]](https://gitlab.com/ultreia.io/ird-observe/issues/350) **Quadrant mal initialisé lors de l&#39;arrvié sur un écran** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 351]](https://gitlab.com/ultreia.io/ird-observe/issues/351) **Renommage du navire Palengirer inconnu en Palengrier inconnu** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 336]](https://gitlab.com/ultreia.io/ird-observe/issues/336) **Problème technique lors de la tentative d&#39;accès au formulaire Rencontres** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 341]](https://gitlab.com/ultreia.io/ird-observe/issues/341) **[PS] Mise en page formulaire Calée / Mesure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 342]](https://gitlab.com/ultreia.io/ird-observe/issues/342) **Menu Action / Calculer les données : ajouter une icône** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 346]](https://gitlab.com/ultreia.io/ird-observe/issues/346) **[PS] Migration des caractéristiques de la senne vers le nouveau système de gestion des équipements** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 347]](https://gitlab.com/ultreia.io/ird-observe/issues/347) **Import Access ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC2](https://gitlab.com/ultreia.io/ird-observe/milestones/50)
&#10;&#10;*(from redmine: created on 2015-04-07)*

**Closed at 2015-04-11.**


### Issues
  * [[Anomalie 323]](https://gitlab.com/ultreia.io/ird-observe/issues/323) **Problème de création de base distante (avec recopie de données)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 324]](https://gitlab.com/ultreia.io/ird-observe/issues/324) **Problème de création d&#39;une nouvelle base (suppression schéma public)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 325]](https://gitlab.com/ultreia.io/ird-observe/issues/325) **Mauvais libéllé lors de la création d&#39;une base distante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 326]](https://gitlab.com/ultreia.io/ird-observe/issues/326) **Problème de création d&#39;une nouvelle base (table tms_version non créée)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 328]](https://gitlab.com/ultreia.io/ird-observe/issues/328) **bug à l&#39;exportation données vers base centrale** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 329]](https://gitlab.com/ultreia.io/ird-observe/issues/329) **Problème sur la type de Program.geartype** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 330]](https://gitlab.com/ultreia.io/ird-observe/issues/330) **Problème lors de la synchronisation des référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 332]](https://gitlab.com/ultreia.io/ird-observe/issues/332) **Une case à cocher est toujours présente mais sans libellé dans les actions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 321]](https://gitlab.com/ultreia.io/ird-observe/issues/321) **Inclure ce fichier observe-report.properties dans la 4.0 RC2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 322]](https://gitlab.com/ultreia.io/ird-observe/issues/322) **Amélioration des temps d&#39;ouverture de l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 327]](https://gitlab.com/ultreia.io/ird-observe/issues/327) **Amélioration du script apply-extra.sh** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 331]](https://gitlab.com/ultreia.io/ird-observe/issues/331) **[ObServeLL] Revue de la fonctionnalité d&#39;export de marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.15.1](https://gitlab.com/ultreia.io/ird-observe/milestones/49)
&#10;&#10;*(from redmine: created on 2015-03-22)*

**Closed at 2015-03-22.**

### Download
* [Application (observe-3.15.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.15.1/observe-3.15.1.zip)

### Issues
  * [[Anomalie 283]](https://gitlab.com/ultreia.io/ird-observe/issues/283) **Les tableaux de synthèse PS ne fonctionnent plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 312]](https://gitlab.com/ultreia.io/ird-observe/issues/312) **Certaines traductions ont disparues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 314]](https://gitlab.com/ultreia.io/ird-observe/issues/314) **Lorsque l&#39;on change de quadrant, cela altère les coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 73]](https://gitlab.com/ultreia.io/ird-observe/issues/73) **Ne pas ouvrir une base locale si sa version est supérieure à celle de l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 195]](https://gitlab.com/ultreia.io/ird-observe/issues/195) **Comportement du masque de saisie des coordonnées + ordre des boutons radio** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 313]](https://gitlab.com/ultreia.io/ird-observe/issues/313) **Amélioration de la migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-RC1](https://gitlab.com/ultreia.io/ird-observe/milestones/48)
&#10;&#10;*(from redmine: created on 2015-03-16)*

**Closed at 2015-04-07.**


### Issues
  * [[Anomalie 266]](https://gitlab.com/ultreia.io/ird-observe/issues/266) **[ObserveLL] La sauvegarde d&#39;une marée de type palangre ne conserve pas les Tdr** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 316]](https://gitlab.com/ultreia.io/ird-observe/issues/316) **[ObServeLL] Echec de la mise à jour de la version 3.14 -&gt; 3.15.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 318]](https://gitlab.com/ultreia.io/ird-observe/issues/318) **[ObServeLL] Mise en page du formulaireComposition détaillée/Détail avançon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 320]](https://gitlab.com/ultreia.io/ird-observe/issues/320) **[ObServePS] Mauvais disposition sur l&#39;écran des activités senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 272]](https://gitlab.com/ultreia.io/ird-observe/issues/272) **LL/PS - Libellé des marées dans l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 317]](https://gitlab.com/ultreia.io/ird-observe/issues/317) **Saisie générique des caractéristiques de l&#39;équipement du bateau &amp; migration senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 319]](https://gitlab.com/ultreia.io/ird-observe/issues/319) **[ObServeLL] Association d&#39;une capture à un TDR** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)

## Version [3.15](https://gitlab.com/ultreia.io/ird-observe/milestones/46)
&#10;&#10;*(from redmine: created on 2015-01-27)*

**Closed at 2015-03-19.**

### Download
* [Application (observe-3.15.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.15/observe-3.15.zip)

### Issues
  * [[Anomalie 241]](https://gitlab.com/ultreia.io/ird-observe/issues/241) **Il manque des espèces dans la combo box espèces du formulaire captures** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 263]](https://gitlab.com/ultreia.io/ird-observe/issues/263) **[ObserveLL] Exception sur l&#39;enregistrement d&#39;une activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 265]](https://gitlab.com/ultreia.io/ird-observe/issues/265) **[ObserveLL] La sauvegarde ne prend pas en compte les marées LL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 269]](https://gitlab.com/ultreia.io/ird-observe/issues/269) **[ObserveLL] Problème sur libellé de la marée LL dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 274]](https://gitlab.com/ultreia.io/ird-observe/issues/274) **Localisation des captures et TDR sur la composition détaillée de la palangre/fonctionnement en entonoir des listes section/basket/branchline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 280]](https://gitlab.com/ultreia.io/ird-observe/issues/280) **Ecran composition détaillée de la palangre : accès à l&#39;onglet &quot;Détail avançon&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 285]](https://gitlab.com/ultreia.io/ird-observe/issues/285) **[ObserveLL] Le mécanisme qui pilote la valeur par défaut de branchline.timer semble ne pas fonctionner** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 264]](https://gitlab.com/ultreia.io/ird-observe/issues/264) **[ObserveLL] Ecran composition détaillée de la palangre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 268]](https://gitlab.com/ultreia.io/ird-observe/issues/268) **[ObserveLL] Le libellé de l&#39;activité dans l&#39;arbre devrait contenir sa date** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 276]](https://gitlab.com/ultreia.io/ird-observe/issues/276) **[ObserveLL] Formulaire Opération de pêche/Enregistreur de profondeur : déplacer des champs sur un onglet supplémentaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 277]](https://gitlab.com/ultreia.io/ird-observe/issues/277) **[ObserveLL] Changement entiers vers décimaux sur entité TDR** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 278]](https://gitlab.com/ultreia.io/ird-observe/issues/278) **[ObserveLL] Mise en page formulaire Opération de pêche/Composition détaillée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 281]](https://gitlab.com/ultreia.io/ird-observe/issues/281) **[ObserveLL] Quelques adaptations sur le formulaire Composition globale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 282]](https://gitlab.com/ultreia.io/ird-observe/issues/282) **[ObserveLL] Quelques adaptations sur le formulaire &quot;Captures&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 286]](https://gitlab.com/ultreia.io/ird-observe/issues/286) **[ObserveLL]  Formulaires Capture et TDR : présentation des 3 listes déroulante en entonoir** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 287]](https://gitlab.com/ultreia.io/ird-observe/issues/287) **Par défaut, replier la réglette du réglage de l&#39;heure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 288]](https://gitlab.com/ultreia.io/ird-observe/issues/288) **[ObserveLL] Supprimer champs Opération de pêche / Caractéristiques / Longueur d&#39;avançon &amp; Longueur traceline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 289]](https://gitlab.com/ultreia.io/ird-observe/issues/289) **[ObserveLL] Appliquer un warning sur Capteur utilisé / Numéro de série** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 291]](https://gitlab.com/ultreia.io/ird-observe/issues/291) **[ObserveLL] Persister les templates sur Opération de pêche / Définition détaillée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 292]](https://gitlab.com/ultreia.io/ird-observe/issues/292) **Améliorer la gestion du focus sur les champs heure/minutes d&#39;un horodatage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 296]](https://gitlab.com/ultreia.io/ird-observe/issues/296) **[ObserveLL] Formulaire Captures : agencement des champs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 297]](https://gitlab.com/ultreia.io/ird-observe/issues/297) **[ObserveLL] Formulaire Captures : Différencier le libellé du bouton &quot;Nouveau&quot; de celui des autres boutons &quot;Nouveau&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 298]](https://gitlab.com/ultreia.io/ird-observe/issues/298) **[ObserveLL] Formulaire Captures : ajouter un onglet qui reprend le contenu de Composition détaillée/Détail avançon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 300]](https://gitlab.com/ultreia.io/ird-observe/issues/300) **[ObserveLL] Formulaire Captures : afficher 3 colonnes supplémentaires dans le tableau récapitulatif des captures** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 302]](https://gitlab.com/ultreia.io/ird-observe/issues/302) **[ObserveLL] Formulaire Activité : Remplir la date de la première activité d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 303]](https://gitlab.com/ultreia.io/ird-observe/issues/303) **[ObserveLL] Formulaire Activité/Opération de pêche/enregistreur de profondeur : permettre la nullité de l&#39;hortadatage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 304]](https://gitlab.com/ultreia.io/ird-observe/issues/304) **Arbre de navigation : quelques améliorations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.14](https://gitlab.com/ultreia.io/ird-observe/milestones/45)
&#10;&#10;*(from redmine: created on 2015-01-22)*

**Closed at 2015-01-27.**

### Download
* [Application (observe-3.14.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.14/observe-3.14.zip)

### Issues
  * [[Anomalie 250]](https://gitlab.com/ultreia.io/ird-observe/issues/250) **La migration change le type du champ activitebateau.code (et d&#39;autres)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 257]](https://gitlab.com/ultreia.io/ird-observe/issues/257) **Lorsque toutes les tailles d&#39;un échantillon sont supprimées, l&#39;échantillon reste en base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 258]](https://gitlab.com/ultreia.io/ird-observe/issues/258) **Mauvaise gestion des flags targetsample.discarded et targetlength.discarded** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 261]](https://gitlab.com/ultreia.io/ird-observe/issues/261) **Impossible d&#39;afficher l&#39;écran Estimation banc** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 255]](https://gitlab.com/ultreia.io/ird-observe/issues/255) **Amélioration ou ajout de contrôles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.13](https://gitlab.com/ultreia.io/ird-observe/milestones/44)
&#10;&#10;*(from redmine: created on 2015-01-16)*

**Closed at 2015-01-19.**

### Download
* [Application (observe-3.13.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.13/observe-3.13.zip)

### Issues
  * [[Anomalie 253]](https://gitlab.com/ultreia.io/ird-observe/issues/253) **[Synchro Référentiel] L&#39;association species.ocean n&#39;est pas synchronisée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 254]](https://gitlab.com/ultreia.io/ird-observe/issues/254) **[Synchro Référentiel] Problème de mise à jour des version** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 235]](https://gitlab.com/ultreia.io/ird-observe/issues/235) **Utilisation des topiaId pour configurer les liste des espèces dans les écrans (via la configuration)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.12](https://gitlab.com/ultreia.io/ird-observe/milestones/43)
&#10;&#10;*(from redmine: created on 2015-01-11)*

**Closed at 2015-01-16.**

### Download
* [Application (observe-3.12.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.12/observe-3.12.zip)

### Issues
  * [[Anomalie 237]](https://gitlab.com/ultreia.io/ird-observe/issues/237) **Synchronisation des ports non fonctionnelle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 248]](https://gitlab.com/ultreia.io/ird-observe/issues/248) **Impossible de scroller dans une double liste sur l&#39;écran n&#39;est pas éditable.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 249]](https://gitlab.com/ultreia.io/ird-observe/issues/249) **Impossible de sauvegarder les modification dans une liste d&#39;espèce** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 238]](https://gitlab.com/ultreia.io/ird-observe/issues/238) **Rajouter des libellés sur l&#39;écran de référentiel Espèce** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 239]](https://gitlab.com/ultreia.io/ird-observe/issues/239) **Passer le port de départ en Warning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 240]](https://gitlab.com/ultreia.io/ird-observe/issues/240) **Ne pas empêcher la clôture d&#39;une marée même si son bateau ou observateur/saisisseur/capitaine est désactivé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 242]](https://gitlab.com/ultreia.io/ird-observe/issues/242) **Ajouter les fonctions PostGIS sur les tables Port et Activté LL** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 247]](https://gitlab.com/ultreia.io/ird-observe/issues/247) **Somme des proportions dans les écran de définition de la composition globale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 251]](https://gitlab.com/ultreia.io/ird-observe/issues/251) **Pouvoir utiliser des champs null sur les champs optionels des navires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 252]](https://gitlab.com/ultreia.io/ird-observe/issues/252) **Ajouter un palangrier inconnu au référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.11](https://gitlab.com/ultreia.io/ird-observe/milestones/42)
&#10;&#10;*(from redmine: created on 2014-12-22)*

**Closed at 2015-01-11.**

### Download
* [Application (observe-3.11.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.11/observe-3.11.zip)

### Issues
  * [[Anomalie 228]](https://gitlab.com/ultreia.io/ird-observe/issues/228) **Problème d&#39;initialisation de la sécurité sur de nouveaux logins** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 230]](https://gitlab.com/ultreia.io/ird-observe/issues/230) **[Sécurité] Bien supprimer les droits d&#39;un utilisateur non utilisé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 223]](https://gitlab.com/ultreia.io/ird-observe/issues/223) **Finaliser l&#39;écran Composition détaillée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 224]](https://gitlab.com/ultreia.io/ird-observe/issues/224) **Suppression de champs obsolètes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 225]](https://gitlab.com/ultreia.io/ird-observe/issues/225) **Ajout de la nationalité de la balise sur le formulaire Opération sur balise** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 229]](https://gitlab.com/ultreia.io/ird-observe/issues/229) **Faire passer le contrôle sur les distances entre activités en warning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 231]](https://gitlab.com/ultreia.io/ird-observe/issues/231) **Réallocation du programme d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 232]](https://gitlab.com/ultreia.io/ird-observe/issues/232) **Rendre le formulaire calée pleinement visible sur petits écrans** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 234]](https://gitlab.com/ultreia.io/ird-observe/issues/234) **Modifier simultannement les états des composants d&#39;heure et de position géographique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.10](https://gitlab.com/ultreia.io/ird-observe/milestones/40)
&#10;&#10;*(from redmine: created on 2014-12-07)*

**Closed at 2015-12-29.**

### Download
* [Application (observe-3.10.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.10/observe-3.10.zip)

### Issues
  * [[Anomalie 185]](https://gitlab.com/ultreia.io/ird-observe/issues/185) **Hauteur des éditeurs de liste** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 194]](https://gitlab.com/ultreia.io/ird-observe/issues/194) **Version de base non reconnue lors de la création d&#39;une nouvelle base locale depuis une base distante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 215]](https://gitlab.com/ultreia.io/ird-observe/issues/215) **Il manque des contraintes FK dans les table &#39;set&#39; et &#39;weightmeasure&#39;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 216]](https://gitlab.com/ultreia.io/ird-observe/issues/216) **Les date/heure/position de l&#39;activité devraient être utilisées pour préremplir les date/heure/position du début de filage du set** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 217]](https://gitlab.com/ultreia.io/ird-observe/issues/217) **La migration crée des Person en doublon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 218]](https://gitlab.com/ultreia.io/ird-observe/issues/218) **LL : Augmenter la taille des champs commentaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 219]](https://gitlab.com/ultreia.io/ird-observe/issues/219) **Problème sur les sensorDataformat** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 222]](https://gitlab.com/ultreia.io/ird-observe/issues/222) **[Écran Opération sur balises] Plus possible d&#39;en créer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 168]](https://gitlab.com/ultreia.io/ird-observe/issues/168) **Écran Composition détaillée de la palangre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 221]](https://gitlab.com/ultreia.io/ird-observe/issues/221) **Pouvoir cliquer sur les onglets lorsque l&#39;écran n&#39;est pas éditable** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.2](https://gitlab.com/ultreia.io/ird-observe/milestones/39)
&#10;&#10;*(from redmine: created on 2014-12-02)*

**Closed at 2014-12-02.**

### Download
* [Application (observe-3.1.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.1.2/observe-3.1.2.zip)

### Issues
  * [[Anomalie 197]](https://gitlab.com/ultreia.io/ird-observe/issues/197) **Mauvais calcul des vitesses dans les activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.9](https://gitlab.com/ultreia.io/ird-observe/milestones/38)
&#10;&#10;*(from redmine: created on 2014-11-27)*

**Closed at 2014-12-11.**

### Download
* [Application (observe-3.9.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.9/observe-3.9.zip)

### Issues
  * [[Anomalie 213]](https://gitlab.com/ultreia.io/ird-observe/issues/213) **Par défaut, afficher le widget des coordonnées en DMS** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 167]](https://gitlab.com/ultreia.io/ird-observe/issues/167) **Écran Composition globale de la palangre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 184]](https://gitlab.com/ultreia.io/ird-observe/issues/184) **Filtre sur les navires** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 201]](https://gitlab.com/ultreia.io/ird-observe/issues/201) **Bien s&#39;assurer que les secondes sont prises en compte dans les éditeurs de position** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 204]](https://gitlab.com/ultreia.io/ird-observe/issues/204) **Pour saisir directement de la date de fin de la marée dès la création d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 205]](https://gitlab.com/ultreia.io/ird-observe/issues/205) **Ajouter des controles sur l&#39;écran Rencontres** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 206]](https://gitlab.com/ultreia.io/ird-observe/issues/206) **Conserver le nom du fichier lors de l&#39;import d&#39;une pièce jointe** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 207]](https://gitlab.com/ultreia.io/ird-observe/issues/207) **Préciser le nom du fichier d&#39;export d&#39;une pièce jointe** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 208]](https://gitlab.com/ultreia.io/ird-observe/issues/208) **Contrôles et valeurs par défaut sur l&#39;écran Calée (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 209]](https://gitlab.com/ultreia.io/ird-observe/issues/209) **Mettre sur les onglets un icon quand il y a des erreurs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 210]](https://gitlab.com/ultreia.io/ird-observe/issues/210) **Écran capture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 211]](https://gitlab.com/ultreia.io/ird-observe/issues/211) **Mettre par defaut la format DMS sur les éditeurs de coordonnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.7.1](https://gitlab.com/ultreia.io/ird-observe/milestones/37)
&#10;&#10;*(from redmine: created on 2014-11-25)*

**Closed at 2014-11-25.**

### Download
* [Application (observe-3.7.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.7.1/observe-3.7.1.zip)

### Issues
  * [[Anomalie 179]](https://gitlab.com/ultreia.io/ird-observe/issues/179) **Bug dans la migration 3.7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 180]](https://gitlab.com/ultreia.io/ird-observe/issues/180) **Problème de génération du site** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.8](https://gitlab.com/ultreia.io/ird-observe/milestones/36)
&#10;&#10;*(from redmine: created on 2014-11-24)*

**Closed at 2014-12-05.**

### Download
* [Application (observe-3.8.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.8/observe-3.8.zip)

### Issues
  * [[Anomalie 176]](https://gitlab.com/ultreia.io/ird-observe/issues/176) **Mauvais déclanchement de la validation des vitesses** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 182]](https://gitlab.com/ultreia.io/ird-observe/issues/182) **Impossible de créer une nouvelle calée seine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 186]](https://gitlab.com/ultreia.io/ird-observe/issues/186) **Revoir le format des éditeurs de position en DMD** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 190]](https://gitlab.com/ultreia.io/ird-observe/issues/190) **Impossible d&#39;ouvrir les détail d&#39;une relation taille-poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 191]](https://gitlab.com/ultreia.io/ird-observe/issues/191) **Validation des plages de temps ne fonctionne plus sur la calée (PS)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 203]](https://gitlab.com/ultreia.io/ird-observe/issues/203) **Mauvaise liste d&#39;espèces utilisées dans certains écrans** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 174]](https://gitlab.com/ultreia.io/ird-observe/issues/174) **Écran des capteurs (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 181]](https://gitlab.com/ultreia.io/ird-observe/issues/181) **Utilisation des doubles listes dans l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 183]](https://gitlab.com/ultreia.io/ird-observe/issues/183) **Ajouter les flag captain et saisisseur à l&#39;utilisateur nommé [inconnu]** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 187]](https://gitlab.com/ultreia.io/ird-observe/issues/187) **Date heure des activités (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 188]](https://gitlab.com/ultreia.io/ird-observe/issues/188) **Écran Calée (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 189]](https://gitlab.com/ultreia.io/ird-observe/issues/189) **Écran Capture (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 192]](https://gitlab.com/ultreia.io/ird-observe/issues/192) **Utilisation des nouveaux éditeurs sur les écrans Seine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 193]](https://gitlab.com/ultreia.io/ird-observe/issues/193) **Écran des TDR (LL)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.7](https://gitlab.com/ultreia.io/ird-observe/milestones/35)
&#10;&#10;*(from redmine: created on 2014-08-25)*

**Closed at 2014-11-24.**

### Download
* [Application (observe-3.7.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.7/observe-3.7.zip)

### Issues
  * [[Anomalie 102]](https://gitlab.com/ultreia.io/ird-observe/issues/102) **L&#39;assistant update-security ne rend pas la main au terminal en fin d&#39;exécution** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 155]](https://gitlab.com/ultreia.io/ird-observe/issues/155) **Libellés erronés sur les écrans de migration d&#39;update-obstuna 3.6** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 156]](https://gitlab.com/ultreia.io/ird-observe/issues/156) **L&#39;espace mémoire que tente de réserver l&#39;assistant update-obstuna 3.6 semble un peu trop important** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 177]](https://gitlab.com/ultreia.io/ird-observe/issues/177) **LL : Augmenter la taille des champs commentaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 124]](https://gitlab.com/ultreia.io/ird-observe/issues/124) **Mettre à jour observe-tools** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 158]](https://gitlab.com/ultreia.io/ird-observe/issues/158) **Renommage des entities Trip, VesselActivity, Activity et Set dans le modèle Seine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 160]](https://gitlab.com/ultreia.io/ird-observe/issues/160) **Ajouter le champs needComment sur tous les référentiels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 161]](https://gitlab.com/ultreia.io/ird-observe/issues/161) **Supprimer le champs TripSeine.organism** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 162]](https://gitlab.com/ultreia.io/ird-observe/issues/162) **Ajout des champs program et ocean sur TripLongline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 163]](https://gitlab.com/ultreia.io/ird-observe/issues/163) **Écran de Marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 165]](https://gitlab.com/ultreia.io/ird-observe/issues/165) **Écran Activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 166]](https://gitlab.com/ultreia.io/ird-observe/issues/166) **Écran Opération de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 169]](https://gitlab.com/ultreia.io/ird-observe/issues/169) **Écran Capture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 170]](https://gitlab.com/ultreia.io/ird-observe/issues/170) **Finalisation de l&#39;entité SetLongline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 171]](https://gitlab.com/ultreia.io/ird-observe/issues/171) **Finalisation de l&#39;entité CatchLongline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 173]](https://gitlab.com/ultreia.io/ird-observe/issues/173) **Finalisation de l&#39;entité Tdr** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 175]](https://gitlab.com/ultreia.io/ird-observe/issues/175) **Mise à jour des listes d&#39;espèces pour le modèle ObserveLL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.6](https://gitlab.com/ultreia.io/ird-observe/milestones/34)
Intégration des référentiels longline + diverses évolutions et corrections&#10;&#10;*(from redmine: created on 2014-07-24)*

**Closed at 2014-08-06.**

### Download
* [Application (observe-3.6.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.6/observe-3.6.zip)

### Issues
  * [[Anomalie 21]](https://gitlab.com/ultreia.io/ird-observe/issues/21) **Anomalie relative à la synchronisation de référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 128]](https://gitlab.com/ultreia.io/ird-observe/issues/128) **Exception due à un probleme de date d&#39;activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 129]](https://gitlab.com/ultreia.io/ird-observe/issues/129) **NulPointerException sur une base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 134]](https://gitlab.com/ultreia.io/ird-observe/issues/134) **Contrôle doublonné avec avertissement+erreur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 135]](https://gitlab.com/ultreia.io/ird-observe/issues/135) **Mauvaise interprétation des logins postgres nécessitant d&#39;être échappés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 98]](https://gitlab.com/ultreia.io/ird-observe/issues/98) **Restauration de marées lors de la création d&#39;une base centrale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 110]](https://gitlab.com/ultreia.io/ird-observe/issues/110) **Libellé à modifier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 150]](https://gitlab.com/ultreia.io/ird-observe/issues/150) **Stabilisation du modèle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 151]](https://gitlab.com/ultreia.io/ird-observe/issues/151) **Ajout des référentiels longline** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 152]](https://gitlab.com/ultreia.io/ird-observe/issues/152) **Changement du type reference.code en String** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 154]](https://gitlab.com/ultreia.io/ird-observe/issues/154) **Ajout d&#39;un champs gearType sur les programmes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0](https://gitlab.com/ultreia.io/ird-observe/milestones/33)
ObserveLL&#10;&#10;*(from redmine: created on 2014-06-23)*

**Closed at 2015-06-08.**

### Download
* [Application (observe-4.0.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/4.0/observe-4.0.zip)

### Issues
  * [[Anomalie 199]](https://gitlab.com/ultreia.io/ird-observe/issues/199) **Comportement du contrôle sur les vitesses entre activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 370]](https://gitlab.com/ultreia.io/ird-observe/issues/370) **[ObserveLL] Schéma de palangre (composition détaillée) : problème de clic droit sous Windows (empêche la suppression de paniers ou avancons)** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 394]](https://gitlab.com/ultreia.io/ird-observe/issues/394) **Erreur de légende pour la cartographie des marées ObserveLL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 125]](https://gitlab.com/ultreia.io/ird-observe/issues/125) **ObserveLL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 172]](https://gitlab.com/ultreia.io/ird-observe/issues/172) **Ajout de la nationalité de la balise sur le formulaire Opération sur balise** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 178]](https://gitlab.com/ultreia.io/ird-observe/issues/178) **Suppression de champs obsolètes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 245]](https://gitlab.com/ultreia.io/ird-observe/issues/245) **Amélioration ou ajout de contrôles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 396]](https://gitlab.com/ultreia.io/ird-observe/issues/396) **Ajout d&#39;une migration de base pour être en 4.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0](https://gitlab.com/ultreia.io/ird-observe/milestones/32)
Mise en place web services&#10;&#10;*(from redmine: created on 2014-02-05)*

**Closed at 2016-09-12.**

### Download
* [Application (observe-5.0.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0/observe-5.0.zip)
* [Serveur (observe-5.0.war)](http://repo1.maven.org/maven2/fr/ird/observe/observe/5.0/observe-5.0.war)

### Issues
  * [[Anomalie 131]](https://gitlab.com/ultreia.io/ird-observe/issues/131) **Problème de création d&#39;une base locale depuis la base centrale si trop de marées sélectionnées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 451]](https://gitlab.com/ultreia.io/ird-observe/issues/451) **[LL] l&#39;export de marées LL est exagérément long** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 498]](https://gitlab.com/ultreia.io/ird-observe/issues/498) **Certains Actions sur une base ouverte sont disponibles alors que la base n&#39;est pas ouverte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 499]](https://gitlab.com/ultreia.io/ird-observe/issues/499) **Des clics multiples sur &quot;Charger la source de données/Utiliser&quot; provoquent une exception** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 501]](https://gitlab.com/ultreia.io/ird-observe/issues/501) **[PS] Calculs taille/poids : Problème sur recalcul** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 509]](https://gitlab.com/ultreia.io/ird-observe/issues/509) **[EQUIPEMENT BATEAU] Mieux gérer le cas où une caractéristique n&#39;a pas de mesure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 534]](https://gitlab.com/ultreia.io/ird-observe/issues/534) **[PS] Problème carte ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 539]](https://gitlab.com/ultreia.io/ird-observe/issues/539) **La fonction &quot;Créer l&#39;activité de fin de veille plante&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 541]](https://gitlab.com/ultreia.io/ird-observe/issues/541) **Dans Route/Activité/Calée/Estimation du banc, un champ ne devrait pas être obligatoire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 556]](https://gitlab.com/ultreia.io/ird-observe/issues/556) **[EQUIPEMENT] La version 4.0.6 permet toujours de saisir des caractéristiques d&#39;équipement sans valeur associée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 583]](https://gitlab.com/ultreia.io/ird-observe/issues/583) **[LL] Problème de valeur nulle dans le schéma de palangres, haulingIdentifier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 588]](https://gitlab.com/ultreia.io/ird-observe/issues/588) **[UI] Impossible d&#39;ajouter son pays à un port** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 609]](https://gitlab.com/ultreia.io/ird-observe/issues/609) **[GEAR][PS][LL] Le formulaire d&#39;enregistrement des équipements peut produire des enregistrements corrompus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 610]](https://gitlab.com/ultreia.io/ird-observe/issues/610) **Regroupement de petits défauts sur les messages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 121]](https://gitlab.com/ultreia.io/ird-observe/issues/121) **Menu contextuel sur l&#39;arbre de navigation &amp; traitements par lots** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 164]](https://gitlab.com/ultreia.io/ird-observe/issues/164) **Pouvoir sélectionner un ensemble de routes contiguës et les déplacer dans une autre marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 226]](https://gitlab.com/ultreia.io/ird-observe/issues/226) **Pouvoir sélectionner un ensemble d&#39;activités contiguës et les déplacer dans une autre route** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 227]](https://gitlab.com/ultreia.io/ird-observe/issues/227) **Mise en place de menus contextuels sur l&#39;arbre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 429]](https://gitlab.com/ultreia.io/ird-observe/issues/429) **Supprimer en base la notion d&#39;ouverture ou fermeture de marées, routes, actitivés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 436]](https://gitlab.com/ultreia.io/ird-observe/issues/436) **Création du modèle de sécurité et gestion des utilisateurs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 446]](https://gitlab.com/ultreia.io/ird-observe/issues/446) **Mise à jour du référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 484]](https://gitlab.com/ultreia.io/ird-observe/issues/484) **Ajout de nouvelles fonctionnalités sur la liste des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 485]](https://gitlab.com/ultreia.io/ird-observe/issues/485) **[PS] Ajout de nouvelles fonctionnalités sur la liste des routes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 486]](https://gitlab.com/ultreia.io/ird-observe/issues/486) **[PS] Ajout de nouvelles fonctionnalités sur la liste des activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 487]](https://gitlab.com/ultreia.io/ird-observe/issues/487) **[LL] Ajout de nouvelles fonctionnalités sur la liste des activités** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 496]](https://gitlab.com/ultreia.io/ird-observe/issues/496) **Suppression du champs TripSeine#datearriveeport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 502]](https://gitlab.com/ultreia.io/ird-observe/issues/502) **Revue de l&#39;algorithme de recherche d&#39;une RTP** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 533]](https://gitlab.com/ultreia.io/ird-observe/issues/533) **Dans la gestion des équipements bateau, il est impossible d&#39;ajouter une caractéristique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 536]](https://gitlab.com/ultreia.io/ird-observe/issues/536) **Faire disparaître les anciens codes espèces 3L de l&#39;affichage dans les listes d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 611]](https://gitlab.com/ultreia.io/ird-observe/issues/611) **[SYNCHRO AVANCEE] Ergonomie sur l&#39;écran de gestion avancée du référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-2](https://gitlab.com/ultreia.io/ird-observe/milestones/31)
Contrat &quot;Armateurs&quot;&#10;&#10;*(from redmine: created on 2014-01-30)*

**Closed at 2015-06-08.**


### Issues
  * [[Anomalie 103]](https://gitlab.com/ultreia.io/ird-observe/issues/103) **[Synchro Référentiel] L&#39;association species.ocean n&#39;est pas synchronisée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 85]](https://gitlab.com/ultreia.io/ird-observe/issues/85) **Réallocation du programme d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 109]](https://gitlab.com/ultreia.io/ird-observe/issues/109) **Rendre le formulaire calée pleinement visible sur petits écrans** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 111]](https://gitlab.com/ultreia.io/ird-observe/issues/111) **Amélioration de performances** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 113]](https://gitlab.com/ultreia.io/ird-observe/issues/113) **Tracer une carte de la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 114]](https://gitlab.com/ultreia.io/ird-observe/issues/114) **Ajout de caractéristiques bateau sur une marée (et non dans le référentiel)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 115]](https://gitlab.com/ultreia.io/ird-observe/issues/115) **Programmation de 10 requêtes supplémentaires dans les tableaux de synthèse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 117]](https://gitlab.com/ultreia.io/ird-observe/issues/117) **Gestion du pool de balises d&#39;objets flottants exploité par le bateau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 118]](https://gitlab.com/ultreia.io/ird-observe/issues/118) **Saisie générique des caractéristiques de l&#39;équipement du bateau &amp; migration senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 133]](https://gitlab.com/ultreia.io/ird-observe/issues/133) **Faire passer le contrôle sur les distances entre activités en warning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.5](https://gitlab.com/ultreia.io/ird-observe/milestones/30)
Traduction modèle + ajout modèle Longline&#10;&#10;*(from redmine: created on 2014-01-09)*

**Closed at 2014-07-06.**

### Download
* [Application (observe-3.5.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.5/observe-3.5.zip)

### Issues
  * [[Anomalie 127]](https://gitlab.com/ultreia.io/ird-observe/issues/127) **Bug dans le calcul des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 107]](https://gitlab.com/ultreia.io/ird-observe/issues/107) **Traduire le schéma de la base (tables/champs) en anglais** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 108]](https://gitlab.com/ultreia.io/ird-observe/issues/108) **Réunir les espèces thon et les espèces faune dans une même table et créer la notion de listes d&#39;affichage** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 116]](https://gitlab.com/ultreia.io/ird-observe/issues/116) **Modélisation senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 140]](https://gitlab.com/ultreia.io/ird-observe/issues/140) **Mise en place modèle ObServeLL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 141]](https://gitlab.com/ultreia.io/ird-observe/issues/141) **Mise en place des listes d&#39;espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 142]](https://gitlab.com/ultreia.io/ird-observe/issues/142) **Réusinage des Personnes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 143]](https://gitlab.com/ultreia.io/ird-observe/issues/143) **Migration VentBeaufort** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 144]](https://gitlab.com/ultreia.io/ird-observe/issues/144) **Ajout champs captain et dataEntryOperator sur la marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 145]](https://gitlab.com/ultreia.io/ird-observe/issues/145) **Add Sex common reference + rename gender field to sex** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 146]](https://gitlab.com/ultreia.io/ird-observe/issues/146) **Add FpaZone reference** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 147]](https://gitlab.com/ultreia.io/ird-observe/issues/147) **Ajout des référentiels du modèle Palangre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 148]](https://gitlab.com/ultreia.io/ird-observe/issues/148) **utilisation de schéma sql** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.1](https://gitlab.com/ultreia.io/ird-observe/milestones/29)
&#10;&#10;*(from redmine: created on 2013-10-10)*

**Closed at 2013-10-10.**

### Download
* [Application (observe-3.1.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.1.1/observe-3.1.1.zip)

### Issues
No issue.

## Version [3.1](https://gitlab.com/ultreia.io/ird-observe/milestones/28)
&#10;&#10;*(from redmine: created on 2013-07-18)*

**Closed at 2013-10-10.**

### Download
* [Application (observe-3.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.1/observe-3.1.zip)

### Issues
  * [[Anomalie 91]](https://gitlab.com/ultreia.io/ird-observe/issues/91) **Problèmes de création de nouvelle base, si la base est sous Postgis 2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 94]](https://gitlab.com/ultreia.io/ird-observe/issues/94) **Problème sur la migration des flags xxxcalcule ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 95]](https://gitlab.com/ultreia.io/ird-observe/issues/95) **Bug potentiel sur la contrainte d&#39;unicité du formulaire &quot;Faune accessoire conservée ou rejetée&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 97]](https://gitlab.com/ultreia.io/ird-observe/issues/97) **Doublons et absence de clé primaire dans especefaune_ocean et especethon_ocean** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 99]](https://gitlab.com/ultreia.io/ird-observe/issues/99) **Gros problèmes avec une base locale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 96]](https://gitlab.com/ultreia.io/ird-observe/issues/96) **Ajouter ce fichier à la distribution** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-rc-4](https://gitlab.com/ultreia.io/ird-observe/milestones/27)
&#10;&#10;*(from redmine: created on 2013-07-17)*

**Closed at 2013-07-18.**


### Issues
  * [[Anomalie 79]](https://gitlab.com/ultreia.io/ird-observe/issues/79) **Ecran Capture faune : libellés d&#39;erreur/warning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 80]](https://gitlab.com/ultreia.io/ird-observe/issues/80) **Clic sur engrenage pour réinitialiser à &quot;observé&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 82]](https://gitlab.com/ultreia.io/ird-observe/issues/82) **Calcul des données capture faune associée : tout n&#39;est pas calculé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 83]](https://gitlab.com/ultreia.io/ird-observe/issues/83) **Manque la vitesse sur le controle des vitesse au niveau de la route** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 86]](https://gitlab.com/ultreia.io/ird-observe/issues/86) **Export central -&gt; local : est-ce une erreur ?** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 88]](https://gitlab.com/ultreia.io/ird-observe/issues/88) **Ajout d&#39;un nouvel onglet dans l&#39;écran À propos pour les traductions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-rc-3](https://gitlab.com/ultreia.io/ird-observe/milestones/26)
&#10;&#10;*(from redmine: created on 2013-07-12)*

**Closed at 2013-07-17.**


### Issues
  * [[Anomalie 75]](https://gitlab.com/ultreia.io/ird-observe/issues/75) **Contrôle des vitesses au niveau activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 76]](https://gitlab.com/ultreia.io/ird-observe/issues/76) **Libellé du message sur test de vitesse au niveau activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 77]](https://gitlab.com/ultreia.io/ird-observe/issues/77) **Pb avec update-obstuna.sh et le SSL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 78]](https://gitlab.com/ultreia.io/ird-observe/issues/78) **Problème de migration 3.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 74]](https://gitlab.com/ultreia.io/ird-observe/issues/74) **Signalement d&#39;une position d&#39;activité suspecte (test de vitesse) -&gt; diverses difficultés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-rc-2](https://gitlab.com/ultreia.io/ird-observe/milestones/25)
&#10;&#10;*(from redmine: created on 2013-07-04)*

**Closed at 2013-07-12.**


### Issues
  * [[Anomalie 8]](https://gitlab.com/ultreia.io/ird-observe/issues/8) **Un contrôle empêche la saisie d&#39;une activite 6-Début de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 13]](https://gitlab.com/ultreia.io/ird-observe/issues/13) **Calcul de données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 70]](https://gitlab.com/ultreia.io/ird-observe/issues/70) **Dans la fenêtre perte du contenu de certains onglets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 71]](https://gitlab.com/ultreia.io/ird-observe/issues/71) **[Validation] Les contrôles de vitesse ne fonctionnent pas bien dans l&#39;action de validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 72]](https://gitlab.com/ultreia.io/ird-observe/issues/72) **Taille du zip anormalement grande** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0](https://gitlab.com/ultreia.io/ird-observe/milestones/24)
&#10;&#10;*(from redmine: created on 2013-07-04)*

**Closed at 2013-09-12.**

### Download
* [Application (observe-3.0.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/3.0/observe-3.0.zip)

### Issues
  * [[Anomalie 90]](https://gitlab.com/ultreia.io/ird-observe/issues/90) **Création BD centrale sans référentiel -&gt; initialisation d&#39;un programme -&gt; l&#39;arbre ne montre pas le programme** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 84]](https://gitlab.com/ultreia.io/ird-observe/issues/84) **Ajout champs Maree#iders** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-rc-1](https://gitlab.com/ultreia.io/ird-observe/milestones/23)
3.0 Release candidate 1&#10;&#10;*(from redmine: created on 2013-06-25)*

**Closed at 2013-07-04.**


### Issues
  * [[Anomalie 24]](https://gitlab.com/ultreia.io/ird-observe/issues/24) **[TECH] L&#39;appli v2.5 permet de se connecter à une base centrale v2.1 sans message d&#39;avertissement ou d&#39;erreur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 60]](https://gitlab.com/ultreia.io/ird-observe/issues/60) **[TECH] Problème de mise à jour d&#39;une base distante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 61]](https://gitlab.com/ultreia.io/ird-observe/issues/61) **[TECH] Problème d&#39;application de la sécurité sur des roles avec des - dans leur nom** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 68]](https://gitlab.com/ultreia.io/ird-observe/issues/68) **[TECH] Action validation ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 22]](https://gitlab.com/ultreia.io/ird-observe/issues/22) **[Validation] Contrôle de la véracité de la position d&#39;une activité au regard de la précédente et de la vitesse maxi d&#39;un bateau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 25]](https://gitlab.com/ultreia.io/ird-observe/issues/25) **Ajouter le nom de l&#39;observateur dans l&#39;identification des marées dans l&#39;arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 26]](https://gitlab.com/ultreia.io/ird-observe/issues/26) **Classer partout les listes d&#39;espèce (thons et faune associée) par code FAO par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 27]](https://gitlab.com/ultreia.io/ird-observe/issues/27) **Inverser les codes 1 et 2 correspondant aux types de banc** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 28]](https://gitlab.com/ultreia.io/ird-observe/issues/28) **[Validation] Empêcher l&#39;utilisation d&#39;une espèce dans un océan si elle n&#39;y est pas marquée dans le référentiel comme présente** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 30]](https://gitlab.com/ultreia.io/ird-observe/issues/30) **Inclure cette fonction SQL sur le dépôt des sources et dans la distribution** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 31]](https://gitlab.com/ultreia.io/ird-observe/issues/31) **[EchantillonThon] Bug sur la saisie des échantillons de thons, saisie par individu** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 32]](https://gitlab.com/ultreia.io/ird-observe/issues/32) **Correction d&#39;un libellé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 33]](https://gitlab.com/ultreia.io/ird-observe/issues/33) **Intégrer ce fichier de requêtes dans la distribution courante** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 34]](https://gitlab.com/ultreia.io/ird-observe/issues/34) **Passer la relation objetflottant_operation de 0..n à 1..1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 36]](https://gitlab.com/ultreia.io/ird-observe/issues/36) **[Validation] Passer &quot;Devenir de l&#39;objet&quot; en warning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 37]](https://gitlab.com/ultreia.io/ird-observe/issues/37) **[TECH] Voir s&#39;il est possible de simplifier la connexion SSL** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 38]](https://gitlab.com/ultreia.io/ird-observe/issues/38) **Modélisation de la senne** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 39]](https://gitlab.com/ultreia.io/ird-observe/issues/39) **Pouvoir choisir directement l&#39;année sur le composant Date** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 40]](https://gitlab.com/ultreia.io/ird-observe/issues/40) **Passer sur des vrais champs Date ou heure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 41]](https://gitlab.com/ultreia.io/ird-observe/issues/41) **[Validation] ne pas permettre de rajouter deux routes à la même date** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 42]](https://gitlab.com/ultreia.io/ird-observe/issues/42) **[Validation] ne pas permettre de rajouter deux activités à la même heure** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 43]](https://gitlab.com/ultreia.io/ird-observe/issues/43) **[Validation] passer en warning tous les contrôles sur des intervalles de poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 44]](https://gitlab.com/ultreia.io/ird-observe/issues/44) **[Validation] Changer le contrôle pour exiger deux champs minimum dont au moins un des deux champs (poids estimé ou nombre  estimé) soit renseigné (Faune accessoire ou rejeté)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 45]](https://gitlab.com/ultreia.io/ird-observe/issues/45) **[Validation] ne pas pouvoir faire d&#39;échantillon s&#39;il n&#39;y a pas d&#39;individus capturés (thon et faune)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 46]](https://gitlab.com/ultreia.io/ird-observe/issues/46) **[Validation] ne pas forcer d&#39;avoir une activité de fin de pêche pour pouvoir en créer une nouvelle** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 47]](https://gitlab.com/ultreia.io/ird-observe/issues/47) **Pouvoir changer le status d&#39;une donnée calculée en donnée observée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 48]](https://gitlab.com/ultreia.io/ird-observe/issues/48) **Valeurs à la création** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 49]](https://gitlab.com/ultreia.io/ird-observe/issues/49) **Décoration des espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 50]](https://gitlab.com/ultreia.io/ird-observe/issues/50) **Ajouter un nouveau champ Calee#profondeurMesure (en m)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 51]](https://gitlab.com/ultreia.io/ird-observe/issues/51) **Correction de la traduction espagnole validator.captureFaune.bound.poidsMoyen** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 52]](https://gitlab.com/ultreia.io/ird-observe/issues/52) **[EchantillonFaune] Pouvoir saisir des echantillons faunes associés en mode effectif ou par individu** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 53]](https://gitlab.com/ultreia.io/ird-observe/issues/53) **Ajout d&#39;une champ Programme#commentaire** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 54]](https://gitlab.com/ultreia.io/ird-observe/issues/54) **Supprimer unicité sur Espece#codeFAO** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 55]](https://gitlab.com/ultreia.io/ird-observe/issues/55) **Déplacer le champs ObjetFlottant#appartenance vers BaliseLue#appartenance** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 56]](https://gitlab.com/ultreia.io/ird-observe/issues/56) **Problème d&#39;affichage des arbres sous jdk 7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 57]](https://gitlab.com/ultreia.io/ird-observe/issues/57) **Amélioration de l&#39;initialisation du référentiel d&#39;une base centrale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 58]](https://gitlab.com/ultreia.io/ird-observe/issues/58) **Décorer en rouge les messages d&#39;avertissement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 59]](https://gitlab.com/ultreia.io/ird-observe/issues/59) **Permettre le changement des dates de fin de calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 62]](https://gitlab.com/ultreia.io/ird-observe/issues/62) **[TECH] Ne permettre l&#39;utilisation lors de la création de la même base pour faire l&#39;import du référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 63]](https://gitlab.com/ultreia.io/ird-observe/issues/63) **[EchantillonThon] Pouvoir modifier l&#39;espèce et la classe de taille en mode effectif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 64]](https://gitlab.com/ultreia.io/ird-observe/issues/64) **Algorithme de colmatage des données de taille/poids manquantes sur les captures de faune associée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1](https://gitlab.com/ultreia.io/ird-observe/milestones/21)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2009-09-04.**


### Issues
No issue.

## Version [1.0](https://gitlab.com/ultreia.io/ird-observe/milestones/20)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2009-07-08.**


### Issues
No issue.

## Version [2.5](https://gitlab.com/ultreia.io/ird-observe/milestones/19)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2012-09-27.**

### Download
* [Application (observe-2.5.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.5/observe-2.5.zip)

### Issues
  * [[Anomalie 2]](https://gitlab.com/ultreia.io/ird-observe/issues/2) **Can not edit calee#vitesseCourant as a decimal** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 4]](https://gitlab.com/ultreia.io/ird-observe/issues/4) **Deux bug génants autours de parametrageTaillePoidsFaune** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 5]](https://gitlab.com/ultreia.io/ird-observe/issues/5) **Bug sur calcul de données avec JVM Java 7 (Linux/Ubuntu 11.10/IcedTea)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 6]](https://gitlab.com/ultreia.io/ird-observe/issues/6) **bug d&#39;affichage Java 7 : arbre de navigation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 7]](https://gitlab.com/ultreia.io/ird-observe/issues/7) **Contrôles de position inactifs dans le formulaire Activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 9]](https://gitlab.com/ultreia.io/ird-observe/issues/9) **Remplacer le fichier observe-reports.properties par celui fourni ici** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 11]](https://gitlab.com/ultreia.io/ird-observe/issues/11) **Modification d&#39;un contrôle sur formulaire Activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 12]](https://gitlab.com/ultreia.io/ird-observe/issues/12) **Modification de contrôle sur formulaire Calée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 14]](https://gitlab.com/ultreia.io/ird-observe/issues/14) **Mettre à jour le fichier de traduction espagnol** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 15]](https://gitlab.com/ultreia.io/ird-observe/issues/15) **Exception au changement du sexe d&#39;un paramétrage taille-poids faune** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 16]](https://gitlab.com/ultreia.io/ird-observe/issues/16) **Revoir les contrôles d&#39;unicité des clefs métiers des entités ParametrageTaillePoidsFaune et ParametrageTaillePoidsThon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 17]](https://gitlab.com/ultreia.io/ird-observe/issues/17) **Suppression impossible dans le référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 18]](https://gitlab.com/ultreia.io/ird-observe/issues/18) **Mauvais libellé dans le référentiel ParamétrageTaillePoidsThon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 19]](https://gitlab.com/ultreia.io/ird-observe/issues/19) **Lors d&#39;une erreur dans les actions, on ne voit pas bien les erreurs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 20]](https://gitlab.com/ultreia.io/ird-observe/issues/20) **L&#39;ajout de nouvelles entitées via la synchronisation ne fonctionne pas pour les liasions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 10]](https://gitlab.com/ultreia.io/ird-observe/issues/10) **Mettre à jour la doc avec les PJ ici présentes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.4](https://gitlab.com/ultreia.io/ird-observe/milestones/18)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-06-27.**

### Download
* [Application (observe-2.4.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.4/observe-2.4.zip)

### Issues
No issue.

## Version [2.3](https://gitlab.com/ultreia.io/ird-observe/milestones/17)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-04-15.**

### Download
* [Application (observe-2.3.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.3/observe-2.3.zip)

### Issues
No issue.

## Version [2.2](https://gitlab.com/ultreia.io/ird-observe/milestones/16)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-02-22.**

### Download
* [Application (observe-2.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.2/observe-2.2.zip)

### Issues
No issue.

## Version [2.1](https://gitlab.com/ultreia.io/ird-observe/milestones/15)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-01-27.**

### Download
* [Application (observe-2.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.1/observe-2.1.zip)

### Issues
No issue.

## Version [2.0.1](https://gitlab.com/ultreia.io/ird-observe/milestones/14)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-01-21.**

### Download
* [Application (observe-2.0.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.0.1/observe-2.0.1.zip)

### Issues
No issue.

## Version [2.0](https://gitlab.com/ultreia.io/ird-observe/milestones/13)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2011-01-03.**

### Download
* [Application (observe-2.0.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/2.0/observe-2.0.zip)

### Issues
No issue.

## Version [1.9.1](https://gitlab.com/ultreia.io/ird-observe/milestones/12)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-10-30.**

### Download
* [Application (observe-1.9.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.9.1/observe-1.9.1.zip)

### Issues
No issue.

## Version [1.9](https://gitlab.com/ultreia.io/ird-observe/milestones/11)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-10-18.**

### Download
* [Application (observe-1.9.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.9/observe-1.9.zip)

### Issues
No issue.

## Version [1.8](https://gitlab.com/ultreia.io/ird-observe/milestones/10)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-09-30.**

### Download
* [Application (observe-1.8.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.8/observe-1.8.zip)

### Issues
No issue.

## Version [1.7](https://gitlab.com/ultreia.io/ird-observe/milestones/9)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-09-22.**

### Download
* [Application (observe-1.7.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.7/observe-1.7.zip)

### Issues
No issue.

## Version [1.6.1](https://gitlab.com/ultreia.io/ird-observe/milestones/8)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-09-20.**

### Download
* [Application (observe-1.6.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.6.1/observe-1.6.1.zip)

### Issues
No issue.

## Version [1.6](https://gitlab.com/ultreia.io/ird-observe/milestones/7)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-09-20.**

### Download
* [Application (observe-1.6.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.6/observe-1.6.zip)

### Issues
No issue.

## Version [1.5](https://gitlab.com/ultreia.io/ird-observe/milestones/6)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-09-16.**

### Download
* [Application (observe-1.5.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.5/observe-1.5.zip)

### Issues
No issue.

## Version [1.4](https://gitlab.com/ultreia.io/ird-observe/milestones/5)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-07-06.**

### Download
* [Application (observe-1.4.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.4/observe-1.4.zip)

### Issues
No issue.

## Version [1.3.1](https://gitlab.com/ultreia.io/ird-observe/milestones/4)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-05-07.**

### Download
* [Application (observe-1.3.1.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.3.1/observe-1.3.1.zip)

### Issues
No issue.

## Version [1.3](https://gitlab.com/ultreia.io/ird-observe/milestones/3)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-03-31.**

### Download
* [Application (observe-1.3.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.3/observe-1.3.zip)

### Issues
No issue.

## Version [1.2](https://gitlab.com/ultreia.io/ird-observe/milestones/2)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-03-13.**

### Download
* [Application (observe-1.2.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.2/observe-1.2.zip)

### Issues
No issue.

## Version [1.1.0](https://gitlab.com/ultreia.io/ird-observe/milestones/1)
&#10;&#10;*(from redmine: created on 2011-08-02)*

**Closed at 2010-01-27.**

### Download
* [Application (observe-1.1.0.zip)](http://repo1.maven.org/maven2/fr/ird/observe/observe/1.1.0/observe-1.1.0.zip)

### Issues
No issue.

