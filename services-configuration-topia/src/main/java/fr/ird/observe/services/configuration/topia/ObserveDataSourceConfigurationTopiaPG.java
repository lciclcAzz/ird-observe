package fr.ird.observe.services.configuration.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Configuration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationTopiaPG extends ObserveDataSourceConfigurationTopiaSupport {

    private static final long serialVersionUID = 1L;

    /**
     * L'url jdbc de la source.
     */
    private String jdbcUrl;

    /**
     * Est-ce que l'on doit utiliser une connexion ssl ?
     */
    private boolean useSsl;

    @Override
    public boolean isH2Database() {
        return false;
    }

    @Override
    public boolean isPostgresDatabase() {
        return true;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public boolean isUseSsl() {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveDataSourceConfigurationTopiaPG)) return false;
        ObserveDataSourceConfigurationTopiaPG that = (ObserveDataSourceConfigurationTopiaPG) o;
        return Objects.equals(getLabel(), that.getLabel()) &&
               Objects.equals(jdbcUrl, that.jdbcUrl) &&
               Objects.equals(getUsername(), that.getUsername()) &&
               Arrays.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLabel(), jdbcUrl, getUsername(), getPassword());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("label", getLabel())
                .add("jdbcUrl", jdbcUrl)
                .add("username", getUsername())
                .add("password", "***")
                .add("useSsl", useSsl)
                .toString();
    }

    @Override
    public ObserveDataSourceConfigurationTopiaPG clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationTopiaPG) super.clone();
    }

}
