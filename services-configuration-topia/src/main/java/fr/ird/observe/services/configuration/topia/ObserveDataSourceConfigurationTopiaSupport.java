package fr.ird.observe.services.configuration.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Configuration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceType;
import org.nuiton.version.Version;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ObserveDataSourceConfigurationTopiaSupport implements ObserveDataSourceConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Le nom de la data source.
     */
    private String label;

    /**
     * L'utilisateur pour se connecter à la source.
     */
    private String username;

    /**
     * Le mot de passe pour se connecter à la source.
     */
    private char[] password;

    //FIXME deplacer ça dans l'objet de création ou d'ouverture de base
    /**
     * Est-ce que la base doit être migrée à son ouverture?
     */
    private boolean canMigrate;

    //FIXME deplacer ça dans l'objet de création ou d'ouverture de base
    /**
     * Est-ce que l'on peut voir la progression lors d'une migration ?
     */
    private boolean showMigrationProgression;

    //FIXME deplacer ça dans l'objet de création ou d'ouverture de base
    /**
     * Est-ce que l'on peut voir le script sql de migration à appliquer ?
     */
    private boolean showMigrationSql;

    private boolean autoMigrate;

    private Version modelVersion;

    public abstract boolean isH2Database();

    public abstract boolean isPostgresDatabase();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char... password) {
        this.password = password;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isCanMigrate() {
        return canMigrate;
    }

    public void setCanMigrate(boolean canMigrate) {
        this.canMigrate = canMigrate;
    }

    public boolean isShowMigrationProgression() {
        return showMigrationProgression;
    }

    public void setShowMigrationProgression(boolean showMigrationProgression) {
        this.showMigrationProgression = showMigrationProgression;
    }

    public boolean isShowMigrationSql() {
        return showMigrationSql;
    }

    public void setShowMigrationSql(boolean showMigrationSql) {
        this.showMigrationSql = showMigrationSql;
    }

    @Override
    public boolean isAutoMigrate() {
        return autoMigrate;
    }

    public void setAutoMigrate(boolean autoMigrate) {
        this.autoMigrate = autoMigrate;
    }

    @Override
    public Version getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(Version modelVersion) {
        this.modelVersion = modelVersion;
    }

    @Override
    public ObserveDataSourceConfigurationTopiaSupport clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationTopiaSupport) super.clone();
    }

    @Override
    public ObserveDataSourceType getType() {
        return ObserveDataSourceType.SQL;
    }
}
