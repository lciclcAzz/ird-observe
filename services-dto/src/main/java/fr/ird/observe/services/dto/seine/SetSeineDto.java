package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.util.DateUtil;

import java.util.Date;

public class SetSeineDto extends GeneratedSetSeineDto {

    private static final long serialVersionUID = 3546973062604141414L;

    public void setStartSetDate(Date startSetDate) {
        if (startTime != null) {
            Date dateAndTime = DateUtil.getDateAndTime(startSetDate, startTime, false, false);
            setStartTime(dateAndTime);
        }
    }

    public void setStartSetTime(Date startSetTime) {
        if (startTime != null) {
            Date dateAndTime = DateUtil.getDateAndTime(startTime, startSetTime, false, false);
            setStartTime(dateAndTime);
        }
    }

    public void setEndSetDate(Date endSetDate) {
        if (endSetTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endSetDate, endSetTimeStamp, false, false);
            setEndSetTimeStamp(dateAndTime);
        }
    }

    public void setEndSetTime(Date endSetTime) {
        if (endSetTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endSetTimeStamp, endSetTime, false, false);
            setEndSetTimeStamp(dateAndTime);
        }
    }

    public void setEndPursingDate(Date endPursingDate) {
        if (endPursingTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endPursingDate, endPursingTimeStamp, false, false);
            setEndPursingTimeStamp(dateAndTime);
        }
    }

    public void setEndPursingTime(Date endPursingTime) {
        if (endPursingTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endPursingTimeStamp, endPursingTime, false, false);
            setEndPursingTimeStamp(dateAndTime);
        }
    }

    public Date getStartSetDate() {
        return startTime == null ? null : DateUtil.getDay(startTime);
    }

    public Date getStartSetTime() {
        return startTime == null ? null : DateUtil.getTime(startTime, false, false);
    }

    public Date getEndSetDate() {
        return endSetTimeStamp == null ? null : DateUtil.getDay(endSetTimeStamp);
    }

    public Date getEndSetTime() {
        return endSetTimeStamp == null ? null : DateUtil.getTime(endSetTimeStamp, false, false);
    }

    public Date getEndPursingDate() {
        return endPursingTimeStamp == null ? null : DateUtil.getDay(endPursingTimeStamp);
    }

    public Date getEndPursingTime() {
        return endPursingTimeStamp == null ? null : DateUtil.getTime(endPursingTimeStamp, false, false);
    }

}
