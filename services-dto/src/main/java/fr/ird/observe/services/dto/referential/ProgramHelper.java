package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.constants.GearType;

import java.util.List;
import java.util.stream.Collectors;

public class ProgramHelper extends GeneratedProgramHelper {

    public static boolean isProgramLongline(ReferentialReference<ProgramDto> programDtoRef) {

        boolean result = false;

        if (programDtoRef.getPropertyNames().contains(ProgramDto.PROPERTY_GEAR_TYPE)) {

            GearType gearType = (GearType) programDtoRef.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE);

            result = GearType.longline.equals(gearType);
        }

        return result;
    }

    public static boolean isProgramSeine(ReferentialReference<ProgramDto> programDtoRef) {

        boolean result = false;

        if (programDtoRef.getPropertyNames().contains(ProgramDto.PROPERTY_GEAR_TYPE)) {

            GearType gearType = (GearType) programDtoRef.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE);

            result = GearType.seine.equals(gearType);
        }

        return result;
    }

    public static List<ReferentialReference<ProgramDto>> filterReferencesByGearType(List<ReferentialReference<ProgramDto>> programs, GearType gearType) {
        return programs.stream()
                       .filter(r -> gearType.equals(r.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE)))
                       .collect(Collectors.toList());
    }

}
