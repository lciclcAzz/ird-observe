/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.constants;


import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.I18nReferentialHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Locale;

/**
 * Pour définir les différentes langues du référentiel.
 *
 * La position de chaque constante définit l'index du champs libelleXXX à
 * utiliser.
 *
 * En base on a actuellement 8 langues possibles, pour gérer une nouvelle
 * langue, il faut définir une nouvelle constante ici.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum ReferentialLocale {
    /**
     * correspond a la propriete {@code label1} d'un {@link I18nReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel1()
     */
    UK(Locale.UK),
    /**
     * correspond a la propriete {@code label2} d'un {@link ReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel2()
     */
    FR(Locale.FRANCE),
    /**
     * correspond a la propriete {@code label3} d'un {@link ReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel3()
     */
    ES(new Locale("es", "ES"));

    private final Locale locale;

    ReferentialLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLibelle() {
        return "label" + (ordinal() + 1);
    }

    public static ReferentialLocale valueOf(Locale locale) {
        for (ReferentialLocale anEnum : values()) {
            if (locale.equals(anEnum.getLocale())) {
                return anEnum;
            }
        }
        throw new IllegalArgumentException(
                "could not find referentiel locale from locale " + locale);
    }

    public String getColumnName() {
        return "label" + (ordinal() + 1);
    }

    @Override
    public String toString() {
        return "<ReferentialLocale: " + locale + ", index: " + name() + ">";
    }

    public <E extends I18nReferentialDto> void setLabel(String label, E i18nDto) {

        switch (ordinal() + 1) {
            case 1:
                i18nDto.setLabel1(label);
                break;
            case 2:
                i18nDto.setLabel2(label);
                break;
            case 3:
                i18nDto.setLabel3(label);
                break;
            case 4:
                i18nDto.setLabel4(label);
                break;
            case 5:
                i18nDto.setLabel5(label);
                break;
            case 6:
                i18nDto.setLabel6(label);
                break;
            case 7:
                i18nDto.setLabel7(label);
                break;
            case 8:
                i18nDto.setLabel8(label);
                break;
        }

    }

    public <E extends I18nReferentialDto> String getLabel(E i18nEntity) {

        return I18nReferentialHelper.decorate(ordinal(), i18nEntity);

    }
}
