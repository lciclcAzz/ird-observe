package fr.ird.observe.services.dto.gson;

/*-
 * #%L
 * ObServe :: Services DTO
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.dto.ObserveBlobsContainer;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created on 10/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public class ObserveBlobsContainerAdapter implements JsonDeserializer<ObserveBlobsContainer> {

    @Override
    public ObserveBlobsContainer deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        String tableName = jsonObject.get("tableName").getAsString();
        String columnName = jsonObject.get("columnName").getAsString();

        Type mapType = mapOf(TypeToken.of(String.class), TypeToken.of(byte[].class)).getType();
        Map<String, byte[]> blobsById = context.deserialize(jsonObject.get("blobsById"), mapType);

        return new ObserveBlobsContainer(tableName, columnName, ImmutableMap.copyOf(blobsById));
    }

    static <K, V> TypeToken<Map<K, V>> mapOf(TypeToken<K> keyType, TypeToken<V> valueType) {
        return new TypeToken<Map<K, V>>() {
        }
                .where(new TypeParameter<K>() {
                }, keyType)
                .where(new TypeParameter<V>() {
                }, valueType);
    }

}

