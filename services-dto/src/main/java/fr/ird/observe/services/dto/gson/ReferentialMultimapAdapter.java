package fr.ird.observe.services.dto.gson;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author Samuel Maisonneuve - maisonneuve@codelutin.com
 */
public class ReferentialMultimapAdapter implements JsonSerializer<ReferentialMultimap>, JsonDeserializer<ReferentialMultimap> {


    @Override
    public JsonElement serialize(ReferentialMultimap src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.asMap());
    }

    @Override
    public ReferentialMultimap deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        ReferentialMultimap.Builder builder = ReferentialMultimap.builder();

        Set<Map.Entry<String, JsonElement>> entries = json.getAsJsonObject().entrySet();
        for (Map.Entry<String, JsonElement> entry : entries) {
            String keyEntry = entry.getKey();
            JsonObject keyObject = new JsonObject();
            keyObject.addProperty("key", keyEntry);

            // Deserialize the key
            Class key = context.deserialize(keyObject.getAsJsonPrimitive("key"), Class.class);

            // Deserialize the collection associated to the key
            Type typeToken = collectionOf(key).getType();
            Collection values = context.deserialize(entry.getValue(), typeToken);

            // Add the key/values to the multimap
            builder.putAll(key, values);
        }

        return builder.build();

    }

    static <E> TypeToken<Collection<E>> collectionOf(Class<E> subType) {
        return new TypeToken<Collection<E>>() {}
                .where(new TypeParameter<E>() {}, subType);
    }
}
