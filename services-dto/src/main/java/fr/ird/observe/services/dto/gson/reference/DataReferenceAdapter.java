package fr.ird.observe.services.dto.gson.reference;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceSetDefinitions;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;

import java.io.Serializable;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataReferenceAdapter<D extends DataDto> extends AbstractReferenceAdapter<D, DataReference<D>> {

    @Override
    protected ReferenceSetDefinition<D> getDefinition(Class<D> dtoType) {
        return DataReferenceSetDefinitions.getDefinition(dtoType);
    }

    @Override
    protected DataReference<D> newReference(Class<D> dtoType, String[] propertyNames, Serializable... propertyValues) {
        DataReference<D> reference = new DataReference<>();
        reference.init(dtoType, propertyNames, propertyValues);
        return reference;
    }

}
