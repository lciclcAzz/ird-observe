package fr.ird.observe.services.dto.gson.reference;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * Note: Pour une référence, on ne sérialize pas les méta-données (noms et types des propriétés), on les récupère à la
 * désérialisation via {@link ReferenceSetDefinition}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class AbstractReferenceAdapter<D extends IdDto, R extends AbstractReference<D>> implements JsonDeserializer<R>, JsonSerializer<R> {

    @Override
    public final R deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Class<D> dtoType = context.deserialize(jsonObject.get(AbstractReference.PROPERTY_TYPE), Class.class);

        ReferenceSetDefinition<D> definition = getDefinition(dtoType);

        String[] propertyNames = definition.getPropertyNames();
        Class<?>[] propertyTypes = definition.getPropertyTypes();

        return deserialize(jsonObject, context, dtoType, propertyNames, propertyTypes);

    }

    public R deserialize(JsonObject jsonObject, JsonDeserializationContext context, Class<D> dtoType, String[] propertyNames, Class<?>... propertyTypes) {

        JsonArray jsonPropertyValues = jsonObject.get(AbstractReference.PROPERTY_LABEL_PROPERTY_VALUES).getAsJsonArray();

        int propertiesSize = propertyNames.length;

        Serializable[] propertyValues = new Serializable[propertiesSize];

        for (int i = 0; i < propertiesSize; i++) {

            Class type = propertyTypes[i];
            JsonElement jsonValue = jsonPropertyValues.get(i);
            Serializable value = context.deserialize(jsonValue, type);
            propertyValues[i] = value;

        }

        R reference = newReference(dtoType, propertyNames, propertyValues);

        String id = context.deserialize(jsonObject.get(AbstractReference.PROPERTY_ID), String.class);
        reference.setId(id);

        JsonElement versionElement = jsonObject.get(AbstractReference.PROPERTY_VERSION);
        if (versionElement != null) {
            long version = versionElement.getAsLong();
            reference.setVersion(version);
        }

        JsonElement createDateElement = jsonObject.get(AbstractReference.PROPERTY_CREATE_DATE);
        if (createDateElement != null) {
            Date createDate = context.deserialize(createDateElement, Date.class);
            reference.setCreateDate(createDate);
        }

        return reference;

    }

    @Override
    public JsonObject serialize(R src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject jsonReference = new JsonObject();

        jsonReference.add(AbstractReference.PROPERTY_TYPE, context.serialize(src.getType()));
        jsonReference.add(AbstractReference.PROPERTY_ID, context.serialize(src.getId()));
        jsonReference.add(AbstractReference.PROPERTY_LABEL_PROPERTY_VALUES, context.serialize(src.getLabelPropertyValues()));

        if (src.getVersion() != 0) {
            jsonReference.add(AbstractReference.PROPERTY_VERSION, context.serialize(src.getVersion()));
        }

        if (src.getCreateDate() != null) {
            jsonReference.add(AbstractReference.PROPERTY_CREATE_DATE, context.serialize(src.getCreateDate()));
        }

        return jsonReference;

    }

    protected abstract ReferenceSetDefinition<D> getDefinition(Class<D> dtoType);

    protected abstract R newReference(Class<D> dtoType, String[] propertyNames, Serializable... propertyValues);

}
