package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.IdHelper;

import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

public class TripSeineDto extends GeneratedTripSeineDto {

    private static final long serialVersionUID = 3976788848141361507L;

    public boolean isDateAvailable(String routeId, Date date) {

        Predicate<RouteStubDto> routeStubDtoPredicate = RouteStubHelper.newDatePredicate(date);
        Predicate<IdDto> idDtoPredicate = IdHelper.newIdPredicate(routeId);
        Optional<RouteStubDto> optional = getRoute().stream()
                                                    .filter(r -> routeStubDtoPredicate.test(r) && !idDtoPredicate.test(r))
                                                    .findFirst();

//        Optional<RouteStubDto> optional = Iterables.tryFind(getRoute(),
//                Predicates.and(RouteStubDtos.newDatePredicate(date),
//                        Predicates.not(IdHelper.newIdPredicate(routeId))));

        return !optional.isPresent();
    }


}
