package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;

import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;

import static fr.ird.observe.services.dto.AbstractReference.newIdPredicate;

public class IdHelper extends GeneratedIdHelper {

    public static <BeanType extends IdDto> BeanType findById(Collection<BeanType> source, String id) {
        return source.stream().filter(newIdPredicate(id)::test).findFirst().orElse(null);
//        return Iterables.find(source, newIdPredicate(id));
    }

    public static <BeanType extends IdDto> Predicate<BeanType> newTripSeinePredicate() {
        return input -> isTripSeineId(input.getId());
    }

    public static <BeanType extends IdDto> Predicate<BeanType> newTripLonglinePredicate() {
        return input -> isTripLonglineId(input.getId());
    }

    public static <BeanType extends IdDto> boolean exists(Collection<BeanType> source, String id) {
        return source.stream().filter(newIdPredicate(id)::test).findFirst().isPresent();
//        return Iterables.tryFind(source, newIdPredicate(id)).isPresent();
    }

    public static boolean isProgram(IdDto dto) {
        return isProgramId(dto.getId());
    }

    public static boolean isProgramId(String id) {
        return id.contains("Program");
    }

    public static boolean isTrip(IdDto dto) {
        return isTripSeineId(dto.getId()) || isTripLonglineId(dto.getId());
    }

    public static boolean isTrip(AbstractReference dto) {
        return isTripSeineId(dto.getId()) || isTripLonglineId(dto.getId());
    }

    public static boolean isTripClass(Class<?> dtoClass) {
        return TripSeineDto.class.equals(dtoClass) || TripLonglineDto.class.equals(dtoClass);
    }

//    public static boolean isTripLongline(IdDto dto) {
//        return isTripLonglineId(dto.getId());
//    }

    public static boolean isTripLonglineId(String id) {
        return id.contains("TripLongline");
    }

//    public static boolean isTripSeine(IdDto dto) {
//        return isTripSeineId(dto.getId());
//    }

    public static boolean isTripSeineId(String id) {
        return id.contains("TripSeine");
    }

//    public static boolean isRoute(IdDto dto) {
//        return isRouteId(dto.getId());
//    }

    public static boolean isRouteId(String id) {
        return id.contains("Route");
    }

    public static boolean isActivitySeine(IdDto dto) {
        return isActivitySeineId(dto.getId());
    }

    public static boolean isActivitySeineId(String id) {
        return id.contains("ActivitySeine");
    }

    public static boolean isActivityLongline(IdDto dto) {
        return isActivityLonglineId(dto.getId());
    }

    public static boolean isActivityLonglineId(String id) {
        return id.contains("ActivityLongline");
    }

    public static boolean isActivityClass(Class<?> dtoClass) {
        return ActivitySeineDto.class.equals(dtoClass) || ActivityLonglineDto.class.equals(dtoClass);
    }

    public static boolean isSetSeine(IdDto dto) {
        return isSetSeineId(dto.getId());
    }

    public static boolean isSetSeineId(String id) {
        return id.contains("SetSeine");
    }

    public static boolean isSetLongline(IdDto dto) {
        return isSetLonglineId(dto.getId());
    }

    public static boolean isSetLonglineId(String id) {
        return id.contains("SetLongline");
    }

    public static boolean isSetClass(Class<?> dtoClass) {
        return SetSeineDto.class.equals(dtoClass) || SetLonglineDto.class.equals(dtoClass);
    }

    public static boolean isFloatingObject(IdDto dto) {
        return isFloatingObjectId(dto.getId());
    }

    public static boolean isFloatingObjectId(String id) {
        return id.contains("FloatingObject");
    }

    public static boolean isSeineId(String id) {
        return id.contains("Seine");
    }

    public static boolean isLonglineId(String id) {
        return id.contains("Longline");
    }

    public static <BeanType extends IdDto> Predicate<BeanType> newIdsPredicate(final Collection<String> ids) {
        return input -> ids.contains(input.getId());
    }

    public static <BeanType extends IdDto> Map<String, BeanType> splitById(Collection<BeanType> dtos) {
        return Maps.uniqueIndex(dtos, ID_FUNCTION::apply);
    }

} //IdHelper
