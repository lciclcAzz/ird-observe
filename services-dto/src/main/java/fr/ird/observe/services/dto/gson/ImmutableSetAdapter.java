package fr.ird.observe.services.dto.gson;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ImmutableSetAdapter implements JsonDeserializer<ImmutableSet<?>> {

    @Override
    public ImmutableSet deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type actualTypeArgument = actualTypeArguments[0];

        Type type2 = setOf(TypeToken.of(actualTypeArgument)).getType();
        Set set = context.deserialize(json, type2);

        return ImmutableSet.copyOf(set);
    }

    static <E> TypeToken<Set<E>> setOf(TypeToken<E> subType) {
        return new TypeToken<Set<E>>() {}
                .where(new TypeParameter<E>() {}, subType);
    }
}
