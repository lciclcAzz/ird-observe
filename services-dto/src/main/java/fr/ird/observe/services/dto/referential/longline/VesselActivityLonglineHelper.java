package fr.ird.observe.services.dto.referential.longline;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialReference;

public class VesselActivityLonglineHelper extends GeneratedVesselActivityLonglineHelper {

    private static final String VESSEL_ACTIVITY_ID_FOR_SET = "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.1";

    public static boolean isSetOpreration(String id) {
        return VESSEL_ACTIVITY_ID_FOR_SET.equals(id);
    }

    public static boolean isSetOperation(VesselActivityLonglineDto vesselActivityLonglineDto) {
        return vesselActivityLonglineDto != null && isSetOpreration(vesselActivityLonglineDto.getId());
    }

    public static boolean isSetOperation(ReferentialReference<VesselActivityLonglineDto> vesselActivityLonglineRef) {
        return vesselActivityLonglineRef != null && isSetOpreration(vesselActivityLonglineRef.getId());
    }


} //VesselActivityLonglineHelper
