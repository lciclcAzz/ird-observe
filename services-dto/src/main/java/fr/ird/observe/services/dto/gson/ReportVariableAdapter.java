package fr.ird.observe.services.dto.gson;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ReportVariableAdapter implements JsonDeserializer<ReportVariable> {

    @Override
    public ReportVariable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject reportVariableJson = json.getAsJsonObject();

        String name = context.deserialize(reportVariableJson.get(ReportVariable.PROPERTY_NAME), String.class);
        Class<?> type = context.deserialize(reportVariableJson.get(ReportVariable.PROPERTY_TYPE), Class.class);
        String request = context.deserialize(reportVariableJson.get(ReportVariable.PROPERTY_REQUEST), String.class);

        ReportVariable reportVariable = new ReportVariable<>(name, type, request);

        Class valueType = type;

        if (DataDto.class.isAssignableFrom(type)) {
            valueType = DataReference.class;
        }
        if (ReferentialDto.class.isAssignableFrom(type)) {
            valueType = ReferentialReference.class;
        }

        JsonElement valuesJson = reportVariableJson.get(ReportVariable.PROPERTY_VALUES);

        if (valuesJson != null) {
            Set values = Sets.newHashSet();
            for (JsonElement valueJson : valuesJson.getAsJsonArray()) {
                Object value = context.deserialize(valueJson, valueType);
                values.add(value);
            }

            reportVariable.setValues(values);
        }

        Object selectedValue = context.deserialize(reportVariableJson.get(ReportVariable.PROPERTY_SELECTED_VALUE), valueType);

        reportVariable.setSelectedValue(selectedValue);

        return reportVariable;
    }
}
