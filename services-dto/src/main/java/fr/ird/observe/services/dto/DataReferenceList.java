package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

public class DataReferenceList<R extends DataDto> extends ReferenceCollectionSupport<R, DataReference<R>, ImmutableList<DataReference<R>>> {

    private static final long serialVersionUID = 1L;

    public static <R extends DataDto> DataReferenceList<R> of(Class<R> type,
                                                              ImmutableList<DataReference<R>> references) {

        return new DataReferenceList<>(type, references);

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add(PROPERTY_TYPE, type.getSimpleName())
                          .toString();
    }

    protected DataReferenceList(Class<R> type, ImmutableList<DataReference<R>> references) {
        super(type, references);
    }

}
