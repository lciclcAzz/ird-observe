package fr.ird.observe.services.dto.reference.request;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.io.Serializable;

/**
 * Pour définir une demande de récupération d'ensemble de références.
 *
 * Created on 11/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferenceSetRequestDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    private final ImmutableSet<ReferenceSetDefinition<? extends DataDto>> dataReferenceSetDefinitions;

    private final Class<? extends IdDto> type;

    private final ImmutableSet<ReferenceSetRequestKeyDefinition<?>> propertyDefinitions;

    private final ImmutableSet<ReferenceSetDefinition<? extends ReferentialDto>> referentialReferenceSetDefinitions;

    public static Builder builder(Class<? extends IdDto> type) {
        return new Builder(type);
    }

    public ReferenceSetRequestDefinition(Class<? extends IdDto> type, ImmutableSet<ReferenceSetRequestKeyDefinition<?>> propertyDefinitions, ImmutableSet<ReferenceSetDefinition<? extends ReferentialDto>> referentialReferenceSetDefinitions,
                                         ImmutableSet<ReferenceSetDefinition<? extends DataDto>> dataReferenceSetDefinitions) {
        this.type = type;
        this.propertyDefinitions = propertyDefinitions;
        this.referentialReferenceSetDefinitions = referentialReferenceSetDefinitions;
        this.dataReferenceSetDefinitions = dataReferenceSetDefinitions;
    }

    public ImmutableSet<ReferenceSetDefinition<? extends ReferentialDto>> getReferentialReferenceSetDefinitions() {
        return referentialReferenceSetDefinitions;
    }

    public ImmutableSet<ReferenceSetDefinition<? extends DataDto>> getDataReferenceSetDefinitions() {
        return dataReferenceSetDefinitions;
    }

    public ImmutableSet<ReferenceSetRequestKeyDefinition<?>> getPropertyDefinitions() {
        return propertyDefinitions;
    }

    public Class<? extends IdDto> getType() {
        return type;
    }
    
    public static class Builder {

        private final Class<? extends IdDto> type;

        private final ImmutableSet.Builder<ReferenceSetRequestKeyDefinition<?>> propertiesBuilder;

        private final ImmutableSet.Builder<ReferenceSetDefinition<? extends ReferentialDto>> referentialSetsBuilder;

        private final ImmutableSet.Builder<ReferenceSetDefinition<? extends DataDto>> dataSetsBuilder;

        public Builder(Class<? extends IdDto> type) {
            this.type = type;
            this.propertiesBuilder = ImmutableSet.builder();
            this.referentialSetsBuilder = ImmutableSet.builder();
            this.dataSetsBuilder = ImmutableSet.builder();
        }

        public Builder addKey(String name, ReferentialReferenceSetDefinitions propertyDefinition) {
            ReferenceSetDefinition definition = propertyDefinition.getDefinition();
            addKey(name, definition);
            return this;
        }

        public ReferenceSetRequestDefinition build() {
            return new ReferenceSetRequestDefinition(type, propertiesBuilder.build(), referentialSetsBuilder.build(), dataSetsBuilder.build());
        }

        protected <D extends IdDto> void addKey(String name, ReferenceSetDefinition<D> definition) {
            ReferenceSetRequestKeyDefinition<D> propertyDefinition = new ReferenceSetRequestKeyDefinition<>(definition, name);
            propertiesBuilder.add(propertyDefinition);
            if (ReferentialDto.class.isAssignableFrom(definition.getType())) {
                referentialSetsBuilder.add((ReferenceSetDefinition) definition);

            } else {
                dataSetsBuilder.add((ReferenceSetDefinition) definition);
            }
        }
    }
}
