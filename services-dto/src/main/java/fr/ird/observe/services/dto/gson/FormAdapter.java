package fr.ird.observe.services.dto.gson;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;

import java.lang.reflect.Type;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FormAdapter<R extends IdDto> implements JsonDeserializer<Form<R>> {

    @Override
    public Form<R> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonForm = json.getAsJsonObject();

        JsonElement jsonType = jsonForm.get(Form.PROPERTY_TYPE);
        Class<R> type = context.deserialize(jsonType, Class.class);

        JsonElement jsonObject = jsonForm.get(Form.PROPERTY_OBJECT);
        R object = context.deserialize(jsonObject, type);

        JsonElement jsonRef = jsonForm.get(Form.PROPERTY_REFERENTIAL_REFERENCE_SETS_REQUEST_NAME);
        String referentialReferenceSetsRequestName = context.deserialize(jsonRef, String.class);

        JsonElement jsonData = jsonForm.get(Form.PROPERTY_DATA_REFERENCE_SETS_REQUEST_NAME);
        String dataReferenceSetsRequestName = context.deserialize(jsonData, String.class);

        return Form.newFormDto(type, object, referentialReferenceSetsRequestName, dataReferenceSetsRequestName);

    }
}
