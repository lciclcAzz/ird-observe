package fr.ird.observe.services.dto.gson.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;

/**
 * Created on 23/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataReferenceSetAdapter<D extends DataDto> extends ReferenceCollectionSupportAdapter<D, DataReference<D>, ImmutableSet<DataReference<D>>, DataReferenceSet<D>> {

    protected final DataReferenceAdapter<D> referenceAdapter = new DataReferenceAdapter<>();

    @Override
    protected ImmutableCollection.Builder<DataReference<D>> createBuilder() {
        return ImmutableSet.builder();
    }

    @Override
    protected ReferenceSetDefinition<D> getDefintion(Class<D> dtoType) {
        return referenceAdapter.getDefinition(dtoType);
    }

    @Override
    protected DataReferenceSet<D> newReferenceSet(Class<D> dtoType, ImmutableSet<DataReference<D>> references, JsonObject jsonObject, JsonDeserializationContext context) {
        return DataReferenceSet.of(dtoType, references);
    }

    @Override
    protected DataReference<D> deserializeReference(JsonElement referenceJsonElement, JsonDeserializationContext context, Class<D> dtoType, String[] propertyNames, Class<?>... propertyTypes) {
        return referenceAdapter.deserialize(referenceJsonElement, dtoType, context);
    }

}
