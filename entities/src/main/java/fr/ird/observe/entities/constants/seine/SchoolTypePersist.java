/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.constants.seine;

/**
 * Une énumération pour caractériser les valeurs d'un type de banc d'une calée.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public enum SchoolTypePersist {

    /** banc indéterminé (valeur par défaut) // was 3 before version 4.0 */
    undefined,

    /** banc objet // was 1 before version 4.0 */
    objet,

    /** banc libre // was 2 before version 4.0 */
    libre

}
