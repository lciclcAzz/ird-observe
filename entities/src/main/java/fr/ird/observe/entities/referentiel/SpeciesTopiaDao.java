package fr.ird.observe.entities.referentiel;

/*-
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.CatchLonglineTopiaDao;
import fr.ird.observe.entities.longline.Encounter;
import fr.ird.observe.entities.longline.EncounterTopiaDao;
import fr.ird.observe.entities.longline.Tdr;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;
import fr.ird.observe.entities.referentiel.seine.WeightCategoryTopiaDao;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetCatchTopiaDao;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetLengthTopiaDao;
import fr.ird.observe.entities.seine.ObjectObservedSpecies;
import fr.ird.observe.entities.seine.ObjectObservedSpeciesTopiaDao;
import fr.ird.observe.entities.seine.ObjectSchoolEstimate;
import fr.ird.observe.entities.seine.ObjectSchoolEstimateTopiaDao;
import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.entities.seine.SchoolEstimateTopiaDao;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetLengthTopiaDao;
import org.hibernate.Session;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;

import java.util.List;

public class SpeciesTopiaDao extends AbstractSpeciesTopiaDao<Species> {

    //FIXME Le code généré par topia n'est pas bon, mais on ne fait plus de maintenance sur cette version de Topia...
    @Override
    public void delete(Species entity) {
        if (!entity.isPersisted()) {
            throw new IllegalArgumentException("entity " + entity + " is not persisted, you can't delete it");
        }

        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();
        Session hibernateSession = hibernateSupport.getHibernateSession();

        {
            String sql =
                    "SELECT main.*  FROM OBSERVE_LONGLINE.Catch main, OBSERVE_LONGLINE.catch_predator secondary " +
                    " WHERE main.topiaId=secondary.catch AND secondary.predator='" + entity.getTopiaId() + "'";
            List<CatchLongline> list = hibernateSession
                    .createSQLQuery(sql)
                    .addEntity("main", ObserveEntityEnum.CatchLongline.getImplementation())
                    .list();

            for (CatchLongline item : list) {
                item.removePredator(entity);
            }
        }

        {
            String sql =
                    "SELECT main.*  FROM OBSERVE_LONGLINE.tdr main, OBSERVE_LONGLINE.species_tdr secondary " +
                    " WHERE main.topiaId=secondary.tdr  AND secondary.species='" + entity.getTopiaId() + "'";
            List<Tdr> list = hibernateSession
                    .createSQLQuery(sql)
                    .addEntity("main", ObserveEntityEnum.Tdr.getImplementation())
                    .list();

            for (Tdr item : list) {
                item.removeSpecies(entity);
            }
        }

        {
            String sql =
                    "SELECT main.*  FROM OBSERVE_COMMON.speciesList main, OBSERVE_COMMON.species_specieslist secondary " +
                            " WHERE main.topiaId=secondary.speciesList  AND secondary.species='" + entity.getTopiaId() + "'";
            List<SpeciesList> list = hibernateSession
                    .createSQLQuery(sql)
                    .addEntity("main", ObserveEntityEnum.SpeciesList.getImplementation())
                    .list();

            for (SpeciesList item : list) {
                item.removeSpecies(entity);
            }
        }

        {
            CatchLonglineTopiaDao dao = topiaDaoSupplier
                    .getDao(CatchLongline.class, CatchLonglineTopiaDao.class);
            List<CatchLongline> list = dao
                    .forProperties(CatchLongline.PROPERTY_SPECIES_CATCH, entity)
                    .findAll();
            for (CatchLongline item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpeciesCatch())) {
                    item.setSpeciesCatch(null);
                }

            }
        }

        {
            EncounterTopiaDao dao = topiaDaoSupplier
                    .getDao(Encounter.class, EncounterTopiaDao.class);
            List<Encounter> list = dao
                    .forProperties(Encounter.PROPERTY_SPECIES, entity)
                    .findAll();
            for (Encounter item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            LengthWeightParameterTopiaDao dao = topiaDaoSupplier
                    .getDao(LengthWeightParameter.class, LengthWeightParameterTopiaDao.class);
            List<LengthWeightParameter> list = dao
                    .forProperties(LengthWeightParameter.PROPERTY_SPECIES, entity)
                    .findAll();
            for (LengthWeightParameter item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            NonTargetCatchTopiaDao dao = topiaDaoSupplier
                    .getDao(NonTargetCatch.class, NonTargetCatchTopiaDao.class);
            List<NonTargetCatch> list = dao
                    .forProperties(NonTargetCatch.PROPERTY_SPECIES, entity)
                    .findAll();
            for (NonTargetCatch item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            ObjectObservedSpeciesTopiaDao dao = topiaDaoSupplier
                    .getDao(ObjectObservedSpecies.class, ObjectObservedSpeciesTopiaDao.class);
            List<ObjectObservedSpecies> list = dao
                    .forProperties(ObjectObservedSpecies.PROPERTY_SPECIES, entity)
                    .findAll();
            for (ObjectObservedSpecies item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            NonTargetLengthTopiaDao dao = topiaDaoSupplier
                    .getDao(NonTargetLength.class, NonTargetLengthTopiaDao.class);
            List<NonTargetLength> list = dao
                    .forProperties(NonTargetLength.PROPERTY_SPECIES, entity)
                    .findAll();
            for (NonTargetLength item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            TargetLengthTopiaDao dao = topiaDaoSupplier
                    .getDao(TargetLength.class, TargetLengthTopiaDao.class);
            List<TargetLength> list = dao
                    .forProperties(TargetLength.PROPERTY_SPECIES, entity)
                    .findAll();
            for (TargetLength item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            ObjectSchoolEstimateTopiaDao dao = topiaDaoSupplier
                    .getDao(ObjectSchoolEstimate.class, ObjectSchoolEstimateTopiaDao.class);
            List<ObjectSchoolEstimate> list = dao
                    .forProperties(ObjectSchoolEstimate.PROPERTY_SPECIES, entity)
                    .findAll();
            for (ObjectSchoolEstimate item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            SchoolEstimateTopiaDao dao = topiaDaoSupplier
                    .getDao(SchoolEstimate.class, SchoolEstimateTopiaDao.class);
            List<SchoolEstimate> list = dao
                    .forProperties(SchoolEstimate.PROPERTY_SPECIES, entity)
                    .findAll();
            for (SchoolEstimate item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        {
            WeightCategoryTopiaDao dao = topiaDaoSupplier
                    .getDao(WeightCategory.class, WeightCategoryTopiaDao.class);
            List<WeightCategory> list = dao
                    .forProperties(WeightCategory.PROPERTY_SPECIES, entity)
                    .findAll();
            for (WeightCategory item : list) {

                // sletellier : Set null only if target is concerned by deletion
                if (entity.equals(item.getSpecies())) {
                    item.setSpecies(null);
                }

            }
        }

        // On applique le super.delete (mais on ne peut pas passer par le parent car le code est erroné...)
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
        topiaFiresSupport.notifyEntityDeleted(entity);
    }
}
