package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.seine.ObjectType;
import fr.ird.observe.entities.referentiel.seine.ObjectTypeImpl;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class FloatingObjectTopiaDao extends AbstractFloatingObjectTopiaDao<FloatingObject> {

    public List<FloatingObject> findAllStubByActivityId(String activityId, int referenceLocale) {

        return StubSqlQuery.findAll(topiaSqlSupport, activityId, referenceLocale);

    }

    public FloatingObject findStubByTopiaId(String floatingObjectId, int referenceLocale) {

        return StubSqlQuery.find(topiaSqlSupport, floatingObjectId, referenceLocale);

    }

    private static class StubSqlQuery extends TopiaSqlQuery<FloatingObject> {

        private final String sql;

        private final String id;

        private final int referenceLocale;

        static List<FloatingObject> findAll(TopiaSqlSupport context, String activityId, int referenceLocale) {

            String sql = "SELECT" +
                         " fo.topiaId," +
                         " ot." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_seine.floatingobject fo, observe_seine.objecttype ot" +
                         " WHERE " +
                         " fo.activity = ?" +
                         " AND fo.objecttype = ot.topiaid" +
                         " ORDER BY ot." + I18nReferenceEntities.getPropertyName(referenceLocale);

            StubSqlQuery request = new StubSqlQuery(sql, activityId, referenceLocale);
            return context.findMultipleResult(request);

        }

        static FloatingObject find(TopiaSqlSupport context, String floatingObjectId, int referenceLocale) {

            String sql = "SELECT" +
                         " fo.topiaId," +
                         " ot." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_seine.floatingobject fo, observe_seine.objecttype ot" +
                         " WHERE " +
                         " fo.topiaId = ?" +
                         " AND fo.objecttype = ot.topiaid" +
                         " ORDER BY ot." + I18nReferenceEntities.getPropertyName(referenceLocale);

            StubSqlQuery request = new StubSqlQuery(sql, floatingObjectId, referenceLocale);
            return context.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id, int referenceLocale) {
            this.sql = sql;
            this.id = id;
            this.referenceLocale = referenceLocale;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            return preparedStatement;
        }

        @Override
        public FloatingObject prepareResult(ResultSet set) throws SQLException {

            FloatingObject floatingObject = new FloatingObjectImpl();
            floatingObject.setTopiaId(set.getString(1));

            String objectTypeLabel = set.getString(2);
            ObjectType objectType = new ObjectTypeImpl();
            I18nReferenceEntities.setLabel(referenceLocale, objectType, objectTypeLabel);
            floatingObject.setObjectType(objectType);

            return floatingObject;

        }

    }

}
