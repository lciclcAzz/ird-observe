/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.gps;

/**
 * Helper to deal with coordinates.
 *
 * @author tchemit - dev@tchemit.fr
 * @since 1.2
 */
public class CoordinateHelper {

    /**
     * Calcule le quadrant à partir d'une {@code longitude} et {@code latitude}.
     *
     * @param longitude la longitude décimale
     * @param latitude  la latitude décimale
     * @return la valeur du quadrant ou {@code null} si l'une des deux
     *         coordonnées est {@code null}.
     * @since 1.2
     */
    public static Integer getQuadrant(Float longitude, Float latitude) {
        if (longitude == null || latitude == null) {
            return null;
        }
        int result;

        if (latitude > 0) {
            result = longitude > 0 ? 1 : 4;
        } else {
            result = longitude > 0 ? 2 : 3;
        }
        return result;
    }

}
