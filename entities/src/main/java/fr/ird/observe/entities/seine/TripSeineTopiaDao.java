/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import fr.ird.observe.entities.TripMapPoint;
import fr.ird.observe.entities.constants.TripMapPointTypePersist;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.PersonImpl;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.ProgramImpl;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.VesselImpl;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Add some user methods.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.2
 */
public class TripSeineTopiaDao extends AbstractTripSeineTopiaDao<TripSeine> {

    public static final String ACTIVITY_HARBOUR_ID = "fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1239832675349#0.363119635949572";

    //FIXME Si on veut juste récupérer les positions, on fait une requete spécifique :(
    public int findPositionByProgramId(String programId, String tripId) {
        int result = 0;
        List<TripSeine> list = findAllStubByProgramId(programId, 1);
        for (TripSeine e : list) {
            if (tripId.equals(e.getTopiaId())) {
                return result;
            }
            result++;
        }

        // not found
        return -1;
    }

    public List<TripSeine> findAllStubByProgramId(String programId, int referenceLocale) {

        return StubSqlQuery.findAll(topiaSqlSupport, programId, referenceLocale);

    }

    public TripSeine findStubByTopiaId(String tripId, int referenceLocale) {

        return StubSqlQuery.find(topiaSqlSupport, tripId, referenceLocale);

    }

    public LinkedHashSet<TripMapPoint> extractTripMapActivityPoints(String tripId) {

        TripSeine tripSeine = forTopiaIdEquals(tripId).findUnique();

        LinkedHashSet<TripMapPoint> tripMapPoints = new LinkedHashSet<>();

        // add departure harbours
        Harbour departureHarbour = tripSeine.getDepartureHarbour();
        if (departureHarbour != null && departureHarbour.getLatitude() != null && departureHarbour.getLongitude() != null) {
            TripMapPoint departurePoint = new TripMapPoint();
            departurePoint.setTime(tripSeine.getStartDate());
            departurePoint.setLatitude(departureHarbour.getLatitude());
            departurePoint.setLongitude(departureHarbour.getLongitude());
            departurePoint.setType(TripMapPointTypePersist.seineDepartureHarbour);
            tripMapPoints.add(departurePoint);
        }

        // Add Activities
        TripMapActivityPointQuery tripMapActivityPointQuery = new TripMapActivityPointQuery(tripId);
        tripMapPoints.addAll(topiaSqlSupport.findMultipleResult(tripMapActivityPointQuery));

        // add landing harbours
        Harbour landingHarbour = tripSeine.getLandingHarbour();
        if (landingHarbour != null && landingHarbour.getLatitude() != null && landingHarbour.getLongitude() != null) {
            TripMapPoint landingPoint = new TripMapPoint();
            landingPoint.setTime(tripSeine.getEndDate());
            landingPoint.setLatitude(landingHarbour.getLatitude());
            landingPoint.setLongitude(landingHarbour.getLongitude());
            landingPoint.setType(TripMapPointTypePersist.seineLandingHarbour);
            tripMapPoints.add(landingPoint);
        }

        return tripMapPoints;
    }

    private static class TripMapActivityPointQuery extends TopiaSqlQuery<TripMapPoint> {

        private static final String SQL = "SELECT" +
                " r.date, " +
                " a.time, " +
                " a.latitude," +
                " a.longitude," +
                " a.vesselactivity, " +
                " s.schoolType" +
                " FROM observe_seine.route r" +
                " INNER JOIN observe_seine.activity a" +
                " ON a.route = r.topiaId" +
                " LEFT OUTER JOIN observe_seine.set s" +
                " ON s.topiaId = a.set" +
                " WHERE r.trip = ?" +
                " ORDER BY r.date, a.time";

        protected final String tripId;

        public TripMapActivityPointQuery(String tripId) {
            this.tripId = tripId;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, tripId);
            return preparedStatement;
        }

        @Override
        public TripMapPoint prepareResult(ResultSet resultSet) throws SQLException {

            TripMapPoint point = new TripMapPoint();
            Date time = DateUtil.getDateAndTime(resultSet.getDate(1), resultSet.getTime(2), true, false);
            point.setTime(time);
            point.setLatitude(resultSet.getFloat(3));
            point.setLongitude(resultSet.getFloat(4));
            if (ACTIVITY_HARBOUR_ID.equals(resultSet.getString(5))) {
                point.setType(TripMapPointTypePersist.seineActivityInHarbour);
            } else if (resultSet.getString(6) == null) {
                point.setType(TripMapPointTypePersist.seineActivity);
            } else {
                SchoolTypePersist schoolType = SchoolTypePersist.values()[resultSet.getInt(6)];
                switch (schoolType) {
                    case libre:
                        point.setType(TripMapPointTypePersist.seineActivityWithFreeSchoolType);
                        break;
                    case objet:
                        point.setType(TripMapPointTypePersist.seineActivityWithObjectSchoolType);
                        break;
                    default:
                        point.setType(TripMapPointTypePersist.seineActivity);
                }

            }

            return point;
        }
    }


    private static class StubSqlQuery extends TopiaSqlQuery<TripSeine> {

        private final String sql;

        private final String id;

        private final int referenceLocale;

        static <E extends TripSeine> List<TripSeine> findAll(TopiaSqlSupport context, String programId, int referenceLocale) {

            String sql = "SELECT" +
                    " t.topiaId," +
                    " t.startDate," +
                    " t.endDate," +
                    " pr.topiaId," +
                    " p.lastName," +
                    " p.firstName," +
                    " v.topiaId, " +
                    " v." + I18nReferenceEntities.getPropertyName(referenceLocale) + "," +
                    " (select count(*) from observe_seine.route r where r.trip = t.topiaId) as routeCount" +
                    " FROM observe_seine.trip t, observe_common.person p, observe_common.vessel v,observe_common.program pr " +
                    " WHERE " +
                    " t.program = ?" +
                    " AND t.program = pr.topiaId" +
                    " AND t.observer = p.topiaId" +
                    " AND t.vessel = v.topiaId" +
                    " ORDER BY t.startDate";

            StubSqlQuery request = new StubSqlQuery(sql, programId, referenceLocale);
            return context.findMultipleResult(request);

        }

        static TripSeine find(TopiaSqlSupport context, String tripId, int referenceLocale) {

            String sql = "SELECT" +
                    " t.topiaId," +
                    " t.startDate," +
                    " t.endDate," +
                    " pr.topiaId," +
                    " p.lastName," +
                    " p.firstName," +
                    " v.topiaId," +
                    " v." + I18nReferenceEntities.getPropertyName(referenceLocale) + "," +
                    " (select count(*) from observe_seine.route r where r.trip = t.topiaId) as routeCount" +
                    " FROM observe_seine.trip t, observe_common.person p, observe_common.vessel v, observe_common.program pr" +
                    " WHERE " +
                    " t.topiaId = ?" +
                    " AND t.program = pr.topiaId" +
                    " AND t.observer = p.topiaId" +
                    " AND t.vessel = v.topiaId" +
                    " ORDER BY t.startDate";

            StubSqlQuery request = new StubSqlQuery(sql, tripId, referenceLocale);
            return context.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id, int referenceLocale) {
            this.sql = sql;
            this.id = id;
            this.referenceLocale = referenceLocale;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            return preparedStatement;
        }

        @Override
        public TripSeine prepareResult(ResultSet set) throws SQLException {

            TripSeineImpl trip = new TripSeineImpl();

            trip.setTopiaId(set.getString(1));
            trip.setStartDate(set.getDate(2));
            trip.setEndDate(set.getDate(3));

            Program program = new ProgramImpl();
            program.setTopiaId(set.getString(4));
            trip.setProgram(program);

            String observerLastName = set.getString(5);
            String observerFirstName = set.getString(6);
            Person observer = new PersonImpl();
            observer.setFirstName(observerFirstName);
            observer.setLastName(observerLastName);
            trip.setObserver(observer);

            Vessel vessel = new VesselImpl();
            String vesselId = set.getString(7);
            vessel.setTopiaId(vesselId);
            String label = set.getString(8);
            I18nReferenceEntities.setLabel(referenceLocale, vessel, label);
            trip.setVessel(vessel);
            trip.setRouteCount(set.getInt(9));


            return trip;

        }

    }

    public boolean updateEndDate(TripSeine trip) {

        boolean wasUpdated = false;

        // la date de fin theorique (date de la dernière activité de la marée)
        Date theoricalEndDate = getTheoricalEndDate(trip);

        // la date de fin actuelle
        Date realEndDate = trip.getEndDate();

        if (realEndDate == null || theoricalEndDate.after(realEndDate)) {

            // on utilise la nouvelle date theorique car l'ancienne n'existe pas
            // ou est antérieure à la date de fin théoriquue
            trip.setEndDate(theoricalEndDate);
            wasUpdated = true;

        }

        return wasUpdated;

    }

    public Date getTheoricalEndDate(TripSeine trip) {

        Date d;

        Date lastRouteDate = TheoricalEndOfDateSqlQuery.find(topiaSqlSupport, trip.getTopiaId());

        if (lastRouteDate == null) {

            // pas de route, donc la date de fin est la date de debut
            d = trip.getStartDate();

        } else {

            // date de la dernière route de la marée
            d = lastRouteDate;

        }

        // on conserve la date epuree (pas de notion de temps dans la date)
        d = DateUtil.getEndOfDay(d);
        return d;

    }

    private static class TheoricalEndOfDateSqlQuery extends TopiaSqlQuery<Date> {

        private final String sql;

        private final String tripId;

        TheoricalEndOfDateSqlQuery(String sql, String tripId) {
            this.sql = sql;
            this.tripId = tripId;
        }

        public static Date find(TopiaSqlSupport context, String tripId) {

            String sql = "SELECT max(r.date) FROM observe_seine.route r WHERE r.trip = ?";

            TheoricalEndOfDateSqlQuery request = new TheoricalEndOfDateSqlQuery(sql, tripId);
            return context.findSingleResult(request);

        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, tripId);
            return preparedStatement;

        }

        @Override
        public Date prepareResult(ResultSet set) throws SQLException {

            return set.getDate(1);

        }

    }

}
