package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.Person;

/**
 * Created on 29/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripSeines {

    public static String decorate(int referenceLocale, TripSeine tripSeine) {

        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%1$td/%1$tm/%1$tY", tripSeine.getStartDate()));
        builder.append(" - ").append(String.format("%1$td/%1$tm/%1$tY", tripSeine.getEndDate()));
        builder.append(" - ").append(I18nReferenceEntities.getLabel(referenceLocale, tripSeine.getVessel()));

        Person observer = tripSeine.getObserver();
        builder.append(" - ").append(observer == null ? "" : observer.getLastName() + " " + observer.getFirstName());

        return builder.toString();

    }

}
