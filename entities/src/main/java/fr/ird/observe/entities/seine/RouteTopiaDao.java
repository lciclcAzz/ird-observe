package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class RouteTopiaDao extends AbstractRouteTopiaDao<Route> {

    public List<Route> findAllStubByTripId(String tripSeineId) {

        return StubSqlQuery.findAll(topiaSqlSupport, tripSeineId);

    }

    public Route findStubByTopiaId(String routeId) {

        return StubSqlQuery.find(topiaSqlSupport, routeId);

    }


    private static class StubSqlQuery extends TopiaSqlQuery<Route> {

        private final String sql;

        private final String id;

        static List<Route> findAll(TopiaSqlSupport context, String tripSeineId) {

            String sql = "SELECT" +
                         " r.topiaId," +
                         " r.date" +
                         " FROM observe_seine.route r" +
                         " WHERE " +
                         " r.trip = ?" +
                         " ORDER BY r.date";

            StubSqlQuery request = new StubSqlQuery(sql, tripSeineId);
            return context.findMultipleResult(request);

        }

        static Route find(TopiaSqlSupport context, String routeId) {

            String sql = "SELECT" +
                         " r.topiaId," +
                         " r.date" +
                         " FROM observe_seine.route r" +
                         " WHERE " +
                         " r.topiaId = ?" +
                         " ORDER BY r.date";

            StubSqlQuery request = new StubSqlQuery(sql, routeId);
            return context.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id) {
            this.id = id;
            this.sql = sql;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            return preparedStatement;
        }

        @Override
        public Route prepareResult(ResultSet set) throws SQLException {

            Route trip = new RouteImpl();
            trip.setTopiaId(set.getString(1));
            trip.setDate(set.getDate(2));

            return trip;

        }
    }

    /**
     * To update date part of {@link SetSeine#getEndPursingTimeStamp()} and {@link SetSeine#getEndSetTimeStamp()} with
     * the day date of the route.
     *
     * @param routeId id of the route to use
     * @since 4.0
     */
    public void updateActivitiesDate(final String routeId) {

        Objects.requireNonNull(routeId, "routeId can't be null");

        Route route = forTopiaIdEquals(routeId).findUnique();

        final Date date = new Date(route.getDate().getTime());

        topiaSqlSupport.doSqlWork(connection -> {

            String sql = "UPDATE observe_seine.SET" +
                         " SET ENDPURSINGTIMESTAMP = (? || ' ' || ENDPURSINGTIMESTAMP::time)::timestamp," +
                         "     ENDSETTIMESTAMP     = (? || ' ' || ENDSETTIMESTAMP::time)::timestamp" +
                         " WHERE TOPIAID IN " +
                         " (" +
                         "  SELECT s.TOPIAID" +
                         "  FROM observe_seine.ACTIVITY a, observe_seine.SET s" +
                         "  WHERE a.ROUTE = ? AND a.SET = s.TOPIAID" +
                         " );";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setDate(1, date);
            ps.setDate(2, date);
            ps.setString(3, routeId);
            ps.executeUpdate();

        });

    }

}
