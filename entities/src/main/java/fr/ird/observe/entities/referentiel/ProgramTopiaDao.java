package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.constants.GearTypePersist;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class ProgramTopiaDao extends AbstractProgramTopiaDao<Program> {

    public List<Program> findAllStub(int referenceLocale) {
        return StubSqlQuery.findAll(topiaSqlSupport, referenceLocale);

    }

    public Program findStubByTopiaId(String programId, int referentielLocale) {
        return StubSqlQuery.find(topiaSqlSupport, programId, referentielLocale);
    }


    private static class StubSqlQuery extends TopiaSqlQuery<Program> {

        private final String sql;

        private final String id;

        private final int referenceLocale;

        static List<Program> findAll(TopiaSqlSupport topiaSqlSupport, int referenceLocale) {

            String sql = "SELECT" +
                         " p.topiaId," +
                         " p.gearType," +
                         " p." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_common.program p" +
                         " ORDER BY p." + I18nReferenceEntities.getPropertyName(referenceLocale);

            StubSqlQuery request = new StubSqlQuery(sql, null, referenceLocale);
            return topiaSqlSupport.findMultipleResult(request);

        }

        static Program find(TopiaSqlSupport topiaSqlSupport, String tripId, int referenceLocale) {

            String sql = "SELECT" +
                         " p.topiaId," +
                         " p.gearType," +
                         " p." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_common.program p" +
                         " WHERE " +
                         " p.topiaId = ?" +
                         " ORDER BY p." + I18nReferenceEntities.getPropertyName(referenceLocale);

            StubSqlQuery request = new StubSqlQuery(sql, tripId, referenceLocale);
            return topiaSqlSupport.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id, int referenceLocale) {
            this.sql = sql;
            this.id = id;
            this.referenceLocale = referenceLocale;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            if (id != null) {
                preparedStatement.setString(1, id);
            }
            return preparedStatement;
        }

        @Override
        public Program prepareResult(ResultSet set) throws SQLException {

            Program program = new ProgramImpl();
            program.setTopiaId(set.getString(1));

            int gearTypeOrdinal = set.getInt(2);
            GearTypePersist gearType = GearTypePersist.fromOrdinal(gearTypeOrdinal);
            program.setGearType(gearType);

            String label = set.getString(3);
            I18nReferenceEntities.setLabel(referenceLocale, program, label);

            return program;

        }

    }


}
