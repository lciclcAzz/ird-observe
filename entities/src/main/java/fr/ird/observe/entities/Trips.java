package fr.ird.observe.entities;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.seine.TripSeine;

/**
 * Created on 2/4/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.15
 */
public class Trips {

    public static boolean isTripId(String id) {

        return id.startsWith(TripSeine.class.getName()) || id.startsWith(TripLongline.class.getName());

    }

}
