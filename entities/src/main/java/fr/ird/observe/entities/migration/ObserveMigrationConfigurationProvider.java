package fr.ird.observe.entities.migration;

/*-
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;
import org.reflections.Reflections;

import java.util.Set;

/**
 * Created on 01/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class ObserveMigrationConfigurationProvider {

    private static ObserveMigrationConfigurationProvider INSTANCE;

    public abstract Version[] getAvailableVersions();

    public abstract Version getLastVersion();

    public abstract Version getMinimumVersion();

    public abstract ImmutableList<Version> getVersionsAfter(Version current);

    public abstract Class<? extends TopiaMigrationCallbackByClass> getMigrationClassBack(boolean h2);

    public static ObserveMigrationConfigurationProvider get() {
        if (INSTANCE == null) {

            Set<Class<? extends ObserveMigrationConfigurationProvider>> impls = new Reflections("fr.ird.observe.entities.migration").getSubTypesOf(ObserveMigrationConfigurationProvider.class);
            if (impls.isEmpty()) {
                throw new ExceptionInInitializerError("No migration configuration provider found.");
            }
            Class<? extends ObserveMigrationConfigurationProvider> impl = impls.iterator().next();
            try {
                INSTANCE = impl.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new ExceptionInInitializerError(e);
            }
        }
        return INSTANCE;
    }

}
