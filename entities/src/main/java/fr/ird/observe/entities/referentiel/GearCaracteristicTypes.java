package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearCaracteristicTypes {

    public static final Set<String> INTEGER_IDS = Sets.newHashSet(
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.3",
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.5"
    );

    public static final Set<String> FLOAT_IDS = Sets.newHashSet(
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.4",
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.6"
    );


    public static boolean isBoolean(GearCaracteristicType type) {
        return "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.2".equals(type.getTopiaId());
    }

    public static boolean isInteger(GearCaracteristicType type) {
        return INTEGER_IDS.contains(type.getTopiaId());
    }

    public static boolean isFloat(GearCaracteristicType type) {
        return FLOAT_IDS.contains(type.getTopiaId());
    }

    public static Object getTypeValue(GearCaracteristicType type, Object value) {

        if (value != null) {

            if (isBoolean(type)) {

                value = Boolean.valueOf((String) value);

            } else if (isInteger(type)) {

                value = Float.valueOf((String) value).intValue();

            } else if (isFloat(type)) {

                value = Float.valueOf((String) value);

            }
        }

        return value;

    }

}
