/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import fr.ird.observe.entities.constants.seine.NonTargetCatchComputedValueSourcePersist;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.NumberUtil;

/**
 * L'implantation par defaut d'une discarded faune.
 *
 * Une discarded faune implante le contrat {@link LengthWeightParameter} sur les champs
 * <ul>
 * <li>{@link #meanWeight}</li>
 * <li>{@link #meanLength}</li>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class NonTargetCatchImpl extends NonTargetCatchAbstract {


    /** Logger */
    private static final Log log = LogFactory.getLog(NonTargetCatchImpl.class);

    private static final long serialVersionUID = 1L;

    @Override
    public Float getLength() {
        return getMeanLength();
    }

    @Override
    public void setLength(Float length) {
        setMeanLength(length);
    }

    @Override
    public boolean isLengthSource() {
        return isMeanLengthComputed();
    }

    @Override
    public void setLengthSource(boolean lengthSource) {
        setMeanLengthComputedSource(NonTargetCatchComputedValueSourcePersist.fromData);
    }

    @Override
    public Float getWeight() {
        return getMeanWeight();
    }

    @Override
    public void setWeight(Float weight) {
        setMeanWeight(weight);
    }

    @Override
    public boolean isWeightSource() {
        return isMeanWeightComputed();
    }

    @Override
    public void setWeightSource(boolean weightSource) {
        setMeanWeightComputedSource(NonTargetCatchComputedValueSourcePersist.fromData);
    }

    @Override
    public void setMeanWeight(Float meanWeight) {
        if (meanWeight != null) {

            // on arrondit à 2 décimales
            meanWeight = NumberUtil.roundTwoDigits(meanWeight);
            if (log.isDebugEnabled()) {
                log.debug("weight moyen : " + meanWeight);
            }
        }
        super.setMeanWeight(meanWeight);
    }

    @Override
    public void setCatchWeight(Float catchWeight) {
        if (catchWeight != null) {

            // on arrondit à 3 décimales
            catchWeight = NumberUtil.roundThreeDigits(catchWeight);
            if (log.isDebugEnabled()) {
                log.debug("weight estime : " + catchWeight);
            }
        }
        super.setCatchWeight(catchWeight);
    }

    @Override
    public void setMeanLength(Float meanLength) {
        if (meanLength != null) {

            // on arrondit à 1 décimale
            meanLength = NumberUtil.roundOneDigit(meanLength);
        }
        super.setMeanLength(meanLength);
    }

    @Override
    public boolean isCatchWeightComputed() {
        return catchWeightComputedSource != null;
    }

    @Override
    public boolean isMeanWeightComputed() {
        return meanWeightComputedSource != null;
    }

    @Override
    public boolean isTotalCountComputed() {
        return totalCountComputedSource != null;
    }

    @Override
    public boolean isMeanLengthComputed() {
        return meanLengthComputedSource != null;
    }
}
