/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referentiel;

import fr.ird.observe.ObserveTopiaPersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.util.NumberUtil;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe utilitaire pour les traitements communs des implantations de
 * {@link LengthWeightParameter}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 * @deprecated Utiliser à la place LengthWeightParemeters
 */
@Deprecated
public class LengthWeightParemeterHelper {

    private static final Pattern COEFFICIENTS_PATTERN =
            Pattern.compile("(.+)=(.+)");

    /** Logger */
    private static final Log log =
            LogFactory.getLog(LengthWeightParemeterHelper.class);

    /** moteur d'évaluation d'expression */
    protected static ScriptEngine scriptEngine;

    /** variable weight à utiliser dans la relation taille */
    public static final String VARIABLE_POIDS = "P";

    /** variable taille à utiliser dans la relation weight */
    public static final String VARIABLE_TAILLE = "L";

    private static final String COEFFICIENT_A = "a";

    private static final String COEFFICIENT_B = "b";

    protected static ScriptEngine getScriptEngine() {
        if (scriptEngine == null) {
            ScriptEngineManager factory = new ScriptEngineManager();

            scriptEngine = factory.getEngineByExtension("js");
        }
        return scriptEngine;
    }

    public static Map<String, Double> getCoefficientValues(LengthWeightParameter parametrage) {

        Map<String, Double> result = new TreeMap<>();
        String coefficients = parametrage.getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher = COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                if (log.isDebugEnabled()) {
                    log.debug("constant to test = " + coefficientDef);
                }
                if (matcher.matches()) {

                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        if (log.isDebugEnabled()) {
                            log.debug("detects coefficient " + key + '=' + val);
                        }
                    } catch (NumberFormatException e) {
                        // pas pu recupere le count...
                        if (log.isWarnEnabled()) {
                            log.warn("could not parse dou" + COEFFICIENT_B + "le " + val + " for coefficient " + key);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static boolean validateWeightRelation(LengthWeightParameter parametrage) {
        return validateRelation(parametrage,
                                parametrage.getLengthWeightFormula(),
                                VARIABLE_TAILLE
        );
    }

    public static boolean validateLengthRelation(LengthWeightParameter parametrage) {
        return validateRelation(parametrage,
                                parametrage.getWeightLengthFormula(),
                                VARIABLE_POIDS
        );
    }

    public static Float computeLength(LengthWeightParameter parametrage,
                                      float weight) {
        Double b = parametrage.getCoefficientValue(COEFFICIENT_B);
        if (b == 0) {

            // ce cas limite ne permet pas de calculer la taille a partir du weight
            return null;
        }
        Float o = computeValue(parametrage,
                               parametrage.getWeightLengthFormula(),
                               VARIABLE_POIDS,
                               weight
        );

        if (o != null) {
            o = NumberUtil.roundOneDigit(o);
        }
        return o;
    }

    public static Float computeWeight(LengthWeightParameter parametrage,
                                      float taille) {

        Float o = computeValue(parametrage,
                               parametrage.getLengthWeightFormula(),
                               VARIABLE_TAILLE,
                               taille
        );

        if (o != null) {
            o = NumberUtil.roundTwoDigits(o);
        }
        return o;
    }

    @SuppressWarnings({"unchecked"})
    public static <P extends LengthWeightParameter> List<P> findBySpecies(ObserveTopiaPersistenceContext tx,
                                                                          Species taillePoidsAble) {

        // le type de resultat recherche
        Class<P> entityClass = (Class<P>) LengthWeightParameter.class;

        TopiaDao<P> dao = tx.getDao(entityClass);

        List<P> list = dao.forProperties(LengthWeightParameter.PROPERTY_SPECIES, taillePoidsAble).findAll();

        // on supprime les paramétrages qui ont a=0 ou a=null ou b=0 ou b = null
        Iterator<P> itr = list.iterator();
        while (itr.hasNext()) {
            P p = itr.next();
            Double a = p.getCoefficientValue(COEFFICIENT_A);
            if (a == null || a == 0) {
                itr.remove();
                continue;
            }

            Double b = p.getCoefficientValue(COEFFICIENT_B);
            // on autorise d'avoir b à 0 (mais cela ne permet plus de calculer la taille à partir du poids)
//            if (b == null || b == 0) {
            if (b == null) {
                itr.remove();
            }
        }
        return list;

    }

    public static <P extends LengthWeightParameter> List<P> filterByOcean(List<P> list, Ocean ocean) {
        List<P> result = new ArrayList<>();
        if (ocean == null) {

            // on n'accepte que les parametrage sans ocean
            for (P parametrageLengthWeight : list) {
                if (parametrageLengthWeight.getOcean() == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            for (P parametrageLengthWeight : list) {
                if (ocean.equals(parametrageLengthWeight.getOcean())) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

    public static <P extends LengthWeightParameter> List<P> filterBySexe(List<P> list, Sex sex) {
        List<P> result = new ArrayList<>();

        for (P parametrageLengthWeight : list) {
            if (parametrageLengthWeight.getSex() == sex) {
                result.add(parametrageLengthWeight);
            }
        }
        return result;
    }

    public static <P extends LengthWeightParameter> List<P> filterByDateDebutValidite(List<P> list, Date startDate) {
        List<P> result = new ArrayList<>();

        for (P parametrageLengthWeight : list) {

            if (parametrageLengthWeight.getStartDate().before(startDate) ||
                parametrageLengthWeight.getStartDate().equals(startDate)) {
                result.add(parametrageLengthWeight);
            }
        }
        return result;
    }

    public static <P extends LengthWeightParameter> List<P> filterByDateFinValidite(List<P> list, Date endDate) {
        List<P> result = new ArrayList<>();

        if (endDate == null) {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            for (P parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            // - ceux dont la date de fin est avant la date de fin donnée
            for (P parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null ||
                    date.after(endDate) ||
                    date.equals(endDate)) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

    protected static boolean validateRelation(LengthWeightParameter parametrage, String relation, String variable) {
        boolean result = false;
        if (!StringUtils.isEmpty(relation)) {

            Map<String, Double> coeffs = parametrage.getCoefficientValues();

            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
                String key = entry.getKey();
                Double value = entry.getValue();
                bindings.put(key, value);

                if (log.isDebugEnabled()) {
                    log.debug("add constant " + key + '=' + value);
                }
            }
            bindings.put(variable, 1);

            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Double o = (Double) engine.eval("parseFloat(" + relation + ")");
                if (log.isDebugEnabled()) {
                    log.debug("evaluation ok : " + relation + " (" + variable + "=1) = " + o);
                }
                result = true;
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("evalution ko : " + relation + ", reason : " + e.getMessage());
                }
            }
        }
        return result;
    }

    public static Float computeValue(LengthWeightParameter parametrage, String relation, String variable, float taille) {
        Map<String, Double> coeffs = parametrage.getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);

            if (log.isDebugEnabled()) {
                log.debug("add constant " + key + '=' + value);
            }
        }
        bindings.put(variable, taille);
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Double o = null;
        try {
            o = (Double) engine.eval("parseFloat(" + relation + ")");
        } catch (ScriptException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not compute value from " + relation);
            }
        }
        return o == null ? null : o.floatValue();
    }
}
