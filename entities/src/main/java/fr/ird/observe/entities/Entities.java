package fr.ird.observe.entities;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.ObserveEntityEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.NumberUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 8/27/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class Entities {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Entities.class);

    public static final ObserveEntityEnum[] REFERENCE_ENTITIES =
            new ObserveEntityEnum[]{
                    ObserveEntityEnum.VesselSizeCategory,
                    ObserveEntityEnum.Country,
                    ObserveEntityEnum.Harbour,
                    ObserveEntityEnum.VesselType,
                    ObserveEntityEnum.Vessel,
                    ObserveEntityEnum.SpeciesGroup,
                    ObserveEntityEnum.Ocean,
                    ObserveEntityEnum.Species,
                    ObserveEntityEnum.Sex,
                    ObserveEntityEnum.FpaZone,
                    ObserveEntityEnum.SpeciesList,
                    ObserveEntityEnum.Person,
                    ObserveEntityEnum.Organism,
                    ObserveEntityEnum.LengthWeightParameter,
                    ObserveEntityEnum.Program,
                    ObserveEntityEnum.GearCaracteristicType,
                    ObserveEntityEnum.GearCaracteristic,
                    ObserveEntityEnum.Gear,
                    ObserveEntityEnum.LastUpdateDate,

                    ObserveEntityEnum.VesselActivitySeine,
                    ObserveEntityEnum.SurroundingActivity,
                    ObserveEntityEnum.ReasonForNullSet,
                    ObserveEntityEnum.ReasonForNoFishing,
                    ObserveEntityEnum.SpeciesFate,
                    ObserveEntityEnum.ObjectFate,
                    ObserveEntityEnum.WeightCategory,
                    ObserveEntityEnum.DetectionMode,
                    ObserveEntityEnum.TransmittingBuoyOperation,
                    ObserveEntityEnum.ObjectOperation,
                    ObserveEntityEnum.ReasonForDiscard,
                    ObserveEntityEnum.SpeciesStatus,
                    ObserveEntityEnum.ObservedSystem,
                    ObserveEntityEnum.TransmittingBuoyType,
                    ObserveEntityEnum.ObjectType,
                    ObserveEntityEnum.Wind,

                    ObserveEntityEnum.BaitHaulingStatus,
                    ObserveEntityEnum.BaitSettingStatus,
                    ObserveEntityEnum.BaitType,
                    ObserveEntityEnum.CatchFateLongline,
                    ObserveEntityEnum.EncounterType,
                    ObserveEntityEnum.Healthness,
                    ObserveEntityEnum.HookPosition,
                    ObserveEntityEnum.HookSize,
                    ObserveEntityEnum.HookType,
                    ObserveEntityEnum.ItemVerticalPosition,
                    ObserveEntityEnum.ItemHorizontalPosition,
                    ObserveEntityEnum.LightsticksColor,
                    ObserveEntityEnum.LightsticksType,
                    ObserveEntityEnum.LineType,
                    ObserveEntityEnum.MaturityStatus,
                    ObserveEntityEnum.MitigationType,
                    ObserveEntityEnum.SensorBrand,
                    ObserveEntityEnum.SensorDataFormat,
                    ObserveEntityEnum.SensorType,
                    ObserveEntityEnum.SettingShape,
                    ObserveEntityEnum.SizeMeasureType,
                    ObserveEntityEnum.StomacFullness,
                    ObserveEntityEnum.TripType,
                    ObserveEntityEnum.VesselActivityLongline,
                    ObserveEntityEnum.WeightMeasureType
            };

    public static final List<ObserveEntityEnum> REFERENCE_ENTITIES_LIST =
            Collections.unmodifiableList(Arrays.asList(REFERENCE_ENTITIES));

    public static final ObserveEntityEnum[] DATA_ENTITIES =
            new ObserveEntityEnum[]{
                    ObserveEntityEnum.TargetSample,
                    ObserveEntityEnum.TargetLength,
                    ObserveEntityEnum.NonTargetSample,
                    ObserveEntityEnum.NonTargetLength,
                    ObserveEntityEnum.NonTargetCatch,
                    ObserveEntityEnum.SetSeine,
                    ObserveEntityEnum.TargetCatch,
                    ObserveEntityEnum.TransmittingBuoy,
                    ObserveEntityEnum.ObjectObservedSpecies,
                    ObserveEntityEnum.SchoolEstimate,
                    ObserveEntityEnum.ObjectSchoolEstimate,
                    ObserveEntityEnum.FloatingObject,
                    ObserveEntityEnum.ActivitySeine,
                    ObserveEntityEnum.Route,
                    ObserveEntityEnum.GearUseFeaturesMeasurementSeine,
                    ObserveEntityEnum.GearUseFeaturesSeine,
                    ObserveEntityEnum.TripSeine,

                    ObserveEntityEnum.HooksComposition,
                    ObserveEntityEnum.BranchlinesComposition,
                    ObserveEntityEnum.BaitsComposition,
                    ObserveEntityEnum.FloatlinesComposition,
                    ObserveEntityEnum.SizeMeasure,
                    ObserveEntityEnum.WeightMeasure,
                    ObserveEntityEnum.SetLongline,
                    ObserveEntityEnum.Branchline,
                    ObserveEntityEnum.Basket,
                    ObserveEntityEnum.Section,
                    ObserveEntityEnum.CatchLongline,
                    ObserveEntityEnum.Tdr,
                    ObserveEntityEnum.Encounter,
                    ObserveEntityEnum.SensorUsed,
                    ObserveEntityEnum.ActivityLongline,
                    ObserveEntityEnum.TdrRecord,
                    ObserveEntityEnum.GearUseFeaturesMeasurementLongline,
                    ObserveEntityEnum.GearUseFeaturesLongline,
                    ObserveEntityEnum.TripLongline
            };

    /**
     * FIXME : cela n'est pas vrai !!! (voir l'algortihme de replication qui
     * detecte tout seul l'ordre a utiliser).
     *
     * la liste des entités non référentiel dans l'ordre de précédence.
     *
     * Ainsi on pourra lors des duplications de base parcourir cet ordre sans
     * avoir à se préoccuper d'éventuels viloations d'intégrités en base (sur
     * les clef étrangères des associations).
     */
    public static final List<ObserveEntityEnum> DATA_ENTITIES_LIST =
            Collections.unmodifiableList(Arrays.asList(DATA_ENTITIES));

    public static final Predicate<String> IS_SEINE_ID = Entities::isSeineId;
    public static final Predicate<String> IS_LONGLINE_ID = Entities::isLonglineId;

    /**
     * @param klass le type d'entite a tester
     * @return {@code true} si c'est une classe du référentiel.
     */
    public static boolean isReferentielClass(Class<?> klass) {
        if (!ObserveEntity.class.isAssignableFrom(klass)) {
            return false;
        }
        klass = ObserveEntityEnum.getContractClass((Class) klass);
        for (ObserveEntityEnum c : REFERENCE_ENTITIES_LIST) {
            if (c.getContract().equals(klass)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSeineId(String id) {
        return id.contains("Seine");
    }

    public static boolean isLonglineId(String id) {
        return id.contains("Longline");
    }

    public static void printDebugInformations(String propertyName, Object instance, Integer value) {
        printDebugInformations0(propertyName, instance, NumberUtil.NULL_OR_ZERO_INTEGER::apply, value);
    }

    public static void printDebugInformations(String propertyName, Object instance, Float value) {
        printDebugInformations0(propertyName, instance, NumberUtil.NULL_OR_ZERO_FLOAT_THREE_DIGITS::apply, value);
    }

    private static <O> void printDebugInformations0(String propertyName, Object instance, Predicate<O> nullPredicate, O value) {
        if (log.isDebugEnabled()) {
            String debugInformations = String.format("[%s] %s changed to %s", instance, propertyName, value);
            if (nullPredicate.test(value)) {
                StackTraceElement[] stackTraceElements = new Throwable().getStackTrace();
                List<StackTraceElement> stackTraceElementList = Lists.newArrayList(stackTraceElements);
                // on retire les deux premiers appels (printDebugInformations et printDebugInformations0)
                stackTraceElementList.remove(0);
                stackTraceElementList.remove(0);
                String stackTrace = Joiner.on("\n\t").join(Arrays.stream(stackTraceElements).filter(new StackTraceElementPredicate()).collect(Collectors.toList()));
                debugInformations += "\n\t" + stackTrace;
            }
            log.debug(debugInformations);
        }

    }

    private static class StackTraceElementPredicate implements Predicate<StackTraceElement> {

        static final ImmutableSet<String> matchingPatterns = ImmutableSet.of("org.nuiton.", "fr.ird");

        @Override
        public boolean test(StackTraceElement input) {
            String className = input.getClassName();
            boolean keep = false;
            for (String matchingPattern : matchingPatterns) {
                if (className.contains(matchingPattern)) {
                    keep = true;
                    break;
                }
            }
            return keep;
        }
    }
}
