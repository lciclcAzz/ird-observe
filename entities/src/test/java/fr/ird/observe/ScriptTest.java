/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.StringUtil;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class ScriptTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(ScriptTest.class);

    final double a = 0.2f;

    final double b = 1.5f;

    final double x = 10f;

    protected static final String EXPRESSION = "a*(x^b)";

    protected static final String EXPRESSION_RESULT = "[%1$-10s] '%2$s' nb eval %3$-5s in %4$-10s (~ %5$s)";

    protected ScriptEngineManager factory;

    @Before
    public void init() {
        factory = new ScriptEngineManager();
        List<ScriptEngineFactory> scriptEngineFactoryList = factory.getEngineFactories();
        for (ScriptEngineFactory factory : scriptEngineFactoryList) {
            if (log.isInfoEnabled()) {
                log.info("available factory [engine: " +
                         factory.getEngineName() + " v." +
                         factory.getEngineVersion() + "] [language: " +
                         factory.getLanguageName() + ", version:" +
                         factory.getLanguageVersion() + "], mime-types : " +
                         "?" + factory.getMimeTypes() +
                         "?" + factory.getExtensions());
            }
        }
    }

    @Test
    public void testScripts() throws Exception {

        doEvaluate(EXPRESSION, 1);
        doEvaluate(EXPRESSION, 10);
        doEvaluate(EXPRESSION, 100);
        doEvaluate(EXPRESSION, 1000);

        doEvaluate(EXPRESSION, 1000);
        doEvaluate(EXPRESSION, 100);
        doEvaluate(EXPRESSION, 10);
        doEvaluate(EXPRESSION, 1);
    }

    protected void doEvaluate(String expression, int nb) throws ScriptException {
        ScriptEngine engine = factory.getEngineByExtension("js");

        long t0 = System.nanoTime();
        for (int i = 0; i < nb; i++) {
            ScriptContext c = engine.getContext();

            c.setAttribute("a", a, ScriptContext.GLOBAL_SCOPE);
            c.setAttribute("x", x, ScriptContext.GLOBAL_SCOPE);
            c.setAttribute("b", b, ScriptContext.GLOBAL_SCOPE);
            Double o = (Double) engine.eval("parseFloat(a*x^b)");
            Assert.assertEquals(a * Math.pow(x, b), o, 5);
        }
        long t1 = System.nanoTime();
        String result = String.format(EXPRESSION_RESULT,
                                      "js",
                                      expression,
                                      nb,
                                      StringUtil.convertTime(t1 - t0),
                                      StringUtil.convertTime((t1 - t0) / nb));
        if (log.isInfoEnabled()) {
            log.info(result);
        }
    }
}
