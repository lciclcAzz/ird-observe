/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referentiel;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test de la classe {@link LengthWeightParemeterHelper}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.8
 */
public class LengthWeightParemeterHelperTest {

    @Test
    public void testComputeValue() {
        Float weight;

        LengthWeightParameter parametrage = new LengthWeightParameterImpl();
        parametrage.setCoefficients("a=3.8e-5:b=2.78 ");
        parametrage.setLengthWeightFormula("a * Math.pow(L, b)");
        Assert.assertTrue(parametrage.isLengthWeightFormulaValid());
        weight = LengthWeightParemeterHelper.computeWeight(parametrage, 84.0f);

        Double excepted = Math.pow(84.0, 2.78) * 3.8e-5;
        Assert.assertEquals(excepted, weight, 2);
    }
}
