package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.HibernateException;
import org.hibernate.dialect.Dialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class DropSchemaAction extends AbstractSchemaAction<DropSchemaRequest> {

    public static final String DROP_SCHEMA_STATEMENT = "\nDROP SCHEMA %s;";

    public DropSchemaAction(DropSchemaRequest request) {
        super(request);
    }

    @Override
    protected String produceSql(Class<? extends Dialect> dialectType, Path temporaryDirectory) throws IOException {

        try {

            Path sqlScriptFile = temporaryDirectory.resolve("dropSchema_" + System.nanoTime() + ".sql");

            generateSqlInFile(dialectType, sqlScriptFile, SchemaExport.Action.DROP);

            String sqlStatements = new String(Files.readAllBytes(sqlScriptFile), StandardCharsets.UTF_8);
            Files.delete(sqlScriptFile);

            if (request.isDropSchema()) {
                for (String schemaName : getSchemaNames()) {
                    sqlStatements += String.format(DROP_SCHEMA_STATEMENT, schemaName);
                }
            }

            return sqlStatements;

        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not drop schema for reason: %s", eee.getMessage()), eee);
        }

    }

}
