package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class UpdateTablesRequest extends AbstractTablesRequest {

    private TopiaMetadataModel topiaMetaModel;
    private boolean  h2;

    public TopiaMetadataModel getTopiaMetaModel() {
        return topiaMetaModel;
    }

    public boolean isH2() {
        return h2;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractTablesRequestBuilder<Builder, UpdateTablesRequest> {

        public Builder() {
            super(new UpdateTablesRequest());
        }

        public Builder setTopiaMetaModel(TopiaMetadataModel topiaMetadataModel) {
            request.topiaMetaModel = topiaMetadataModel;
            return this;
        }

        public Builder setH2(boolean h2) {
            request.h2 = h2;
            return this;
        }

    }
}

