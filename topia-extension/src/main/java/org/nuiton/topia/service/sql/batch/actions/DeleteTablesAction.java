package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTable;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class DeleteTablesAction extends AbstractTablesAction<DeleteTablesRequest> {

    public static final String DELETE_ASSOCIATION_STATEMENT = "DELETE FROM %s.%s WHERE %s = '%%s';\n";

    public static final String DELETE_STATEMENT = "DELETE FROM %s.%s WHERE topiaId = '%%s';\n";
    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(DeleteTablesAction.class);

    public DeleteTablesAction(DeleteTablesRequest request) {
        super(request);
    }

    @Override
    protected void executeOnTable(DeleteTablesRequest request, TopiaSqlTable table, PreparedStatement readStatement) throws SQLException {

        ResultSet readResultSet = readStatement.getResultSet();

        ResultSetMetaData readResultSetMetaData = readResultSet.getMetaData();
        int columnCount = readResultSetMetaData.getColumnCount();

        List<String> columnNames = getColumnNames(readResultSetMetaData, columnCount, null);

        String topiaIdColumnName = TopiaEntity.PROPERTY_TOPIA_ID.toLowerCase();
        int topiaIdColumnIndex = columnNames.indexOf(topiaIdColumnName) + 1;

        if (table.isAssociationTable()) {
            topiaIdColumnIndex = 1;
        }

        boolean useOutputWriter = useOutputWriter();
        boolean useOutputDb = useOutputDb();

        PreparedStatement writeStatement = null;

        String deleteStatementSql = newDeleteStatementSql(table);

        if (useOutputDb) {

            String sql = String.format(deleteStatementSql, "?");
            writeStatement = targetConnection.prepareStatement(sql);

        }

        int writeBatchSize = request.getWriteBatchSize();
        String tableName = table.getFullyTableName();

        long index = 0;
        while (readResultSet.next()) {

            String topiaId = readResultSet.getString(topiaIdColumnIndex);

            if (log.isTraceEnabled()) {
                log.trace("Delete " + topiaId);
            }

            if (useOutputDb) {

                writeStatement.clearParameters();
                writeStatement.setString(1, topiaId);
                writeStatement.addBatch();

            }

            if (useOutputWriter) {

                try {

                    String sql = String.format(deleteStatementSql, topiaId);
                    writer.append(sql);

                } catch (IOException e) {
                    throw new TopiaException("Could not deleteRow", e);
                }

            }

            if ((++index % writeBatchSize) == 0) {
                flush(writeStatement, writer, tableName, index);
            }

        }

        flush(writeStatement, writer, tableName, index);

    }

    @Override
    protected TopiaSqlTables getTables() {
        return request.getTables().reverse();

    }

    protected String newDeleteStatementSql(TopiaSqlTable table) {

        String sql;
        if (table.isAssociationTable()) {
            sql = String.format(DELETE_ASSOCIATION_STATEMENT, table.getSchemaName(), table.getTableName(), table.getJoinColumnName());
        } else {
            sql = String.format(DELETE_STATEMENT, table.getSchemaName(), table.getTableName());
        }
        if (log.isDebugEnabled()) {
            log.debug("Delete sql: " + sql);
        }

        return sql;

    }

}
