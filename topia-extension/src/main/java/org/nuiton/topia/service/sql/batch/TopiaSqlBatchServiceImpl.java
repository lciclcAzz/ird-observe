package org.nuiton.topia.service.sql.batch;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.sql.batch.actions.AbstractSqlAction;
import org.nuiton.topia.service.sql.batch.actions.AbstractSqlRequest;
import org.nuiton.topia.service.sql.batch.actions.BlobsContainer;
import org.nuiton.topia.service.sql.batch.actions.CreateSchemaAction;
import org.nuiton.topia.service.sql.batch.actions.CreateSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.DeleteTablesAction;
import org.nuiton.topia.service.sql.batch.actions.DeleteTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.DropSchemaAction;
import org.nuiton.topia.service.sql.batch.actions.DropSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesAction;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.UpdateTablesAction;
import org.nuiton.topia.service.sql.batch.actions.UpdateTablesRequest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 04/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class TopiaSqlBatchServiceImpl implements TopiaSqlBatchService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaSqlBatchServiceImpl.class);

    private static final ImmutableMap<Class, Class> ACTION_MAPPING = ImmutableMap
            .<Class, Class>builder()
            .put(CreateSchemaRequest.class, CreateSchemaAction.class)
            .put(DropSchemaRequest.class, DropSchemaAction.class)
            .put(ReplicateTablesRequest.class, ReplicateTablesAction.class)
            .put(UpdateTablesRequest.class, UpdateTablesAction.class)
            .put(DeleteTablesRequest.class, DeleteTablesAction.class)
            .build();

    protected TopiaApplicationContext topiaApplicationContext;

    protected TopiaSqlBatchServiceConfiguration configuration;

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {

        this.topiaApplicationContext = topiaApplicationContext;
        this.configuration = new TopiaSqlBatchServiceConfiguration();

        String readFetchSizeStr = serviceConfiguration.get(TopiaSqlBatchServiceConfiguration.PROPERTY_READ_FETCH_SIZE);
        int readFetchSize = readFetchSizeStr != null
                ? Integer.valueOf(readFetchSizeStr)
                : TopiaSqlBatchServiceConfiguration.DEFAULT_READ_FETCH_SIZE;
        configuration.setReadFetchSize(readFetchSize);

        String writeBatchSizeStr = serviceConfiguration.get(TopiaSqlBatchServiceConfiguration.PROPERTY_WRITE_BATCH_SIZE);
        int writeBatchSize = writeBatchSizeStr == null
                ? TopiaSqlBatchServiceConfiguration.DEFAULT_WRITE_BATCH_SIZE
                : Integer.valueOf(writeBatchSizeStr);
        configuration.setWriteBatchSize(writeBatchSize);

    }

    @Override
    public void close() {

    }

    @Override
    public TopiaSqlBatchServiceConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public SqlRequests.Builder requestBuilder() {
        return SqlRequests.builder().from(topiaApplicationContext);
    }

    @Override
    public CreateSchemaRequest.Builder createSchemaRequestBuilder() {
        return CreateSchemaRequest.builder().from(topiaApplicationContext);
    }

    @Override
    public DropSchemaRequest.Builder dropSchemaRequestBuilder() {
        return DropSchemaRequest.builder().from(topiaApplicationContext);
    }

    @Override
    public ReplicateTablesRequest.Builder replicateTablesRequestBuilder() {
        return ReplicateTablesRequest
                .builder()
                .from(topiaApplicationContext)
                .setReadFetchSize(configuration.getReadFetchSize())
                .setWriteBatchSize(configuration.getWriteBatchSize());
    }

    @Override
    public UpdateTablesRequest.Builder updateTablesRequestBuilder() {
        return UpdateTablesRequest
                .builder()
                .from(topiaApplicationContext)
                .setReadFetchSize(configuration.getReadFetchSize())
                .setWriteBatchSize(configuration.getWriteBatchSize());
    }

    @Override
    public DeleteTablesRequest.Builder deleteTablesRequestBuilder() {
        return DeleteTablesRequest
                .builder()
                .from(topiaApplicationContext)
                .setReadFetchSize(configuration.getReadFetchSize())
                .setWriteBatchSize(configuration.getWriteBatchSize());
    }

    @Override
    public SqlResult execute(SqlRequests requests) {

        Map<String, BlobsContainer.Builder> blobContainersBuilder = new TreeMap<>();

        Iterator<AbstractSqlRequest> sqlRequestIterator = requests.iterator();
        boolean hasNext = sqlRequestIterator.hasNext();
        while (hasNext) {

            AbstractSqlRequest sqlRequest = sqlRequestIterator.next();

            try (AbstractSqlAction<?> action = createAction(sqlRequest)) {

                action.run();

                Optional<Set<BlobsContainer.Builder>> optionalBlobsContainers = action.getBlobsContainersBuilder();
                if (optionalBlobsContainers.isPresent()) {
                    Set<BlobsContainer.Builder> blobsContainers = optionalBlobsContainers.get();

                    for (BlobsContainer.Builder blobsContainerBuilder : blobsContainers) {
                        String tableName = blobsContainerBuilder.getTableName();
                        String columnName = blobsContainerBuilder.getColumnName();

                        String key = tableName + "##" + columnName;
                        if (blobContainersBuilder.containsKey(key)) {
                            BlobsContainer.Builder blobsContainerBuilder1 = blobContainersBuilder.get(key);
                            blobsContainerBuilder1.addAllBlob(blobsContainerBuilder.build().getBlobsById());
                        } else {
                            blobContainersBuilder.put(tableName, blobsContainerBuilder);
                        }
                    }
                }
                //FIXME Review transaction management

                hasNext = sqlRequestIterator.hasNext();
                if (!hasNext) {
                    action.commit();
                }
            }

        }

        ImmutableMap.Builder<String, BlobsContainer> blobsContainersBuilder = ImmutableMap.builder();

        for (Map.Entry<String, BlobsContainer.Builder> entry : blobContainersBuilder.entrySet()) {

            String tableName = entry.getKey();
            BlobsContainer blobsContainer = entry.getValue().build();
            if (!blobsContainer.isEmpty()) {
                if (log.isInfoEnabled()) {
                    log.info("Register blobsContainer for table: " + tableName + ", with " + blobsContainer.getBlobsById().size() + " blob(s)");
                }
                blobsContainersBuilder.put(tableName, blobsContainer);
            }
        }

        ImmutableSet<BlobsContainer> blobsContainers = ImmutableSet.copyOf(blobsContainersBuilder.build().values());
        return new SqlResult(blobsContainers);

    }

    protected <R extends AbstractSqlRequest, A extends AbstractSqlAction<R>> A createAction(R request) {

        Objects.requireNonNull(request, "Request can't be null");
        Class<A> actionType = ACTION_MAPPING.get(request.getClass());
        Objects.requireNonNull(actionType, "Could not find action for request type: " + request.getClass().getName());

        Constructor<A> constructor;
        try {
            constructor = actionType.getConstructor(request.getClass());
        } catch (NoSuchMethodException e) {
            throw new TopiaException(e);
        }
        try {
            return constructor.newInstance(request);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new TopiaException(e);
        }

    }

    @Override
    public void execute(CreateSchemaRequest request) {
        executeOneRequest(request);
    }

    @Override
    public void execute(DropSchemaRequest request) {
        executeOneRequest(request);
    }

    @Override
    public SqlResult execute(ReplicateTablesRequest request) {
        return executeOneRequest(request);

    }

    @Override
    public SqlResult execute(UpdateTablesRequest request) {
        return executeOneRequest(request);
    }

    @Override
    public void execute(DeleteTablesRequest request) {
        executeOneRequest(request);
    }

    protected SqlResult executeOneRequest(AbstractSqlRequest request) {
        SqlRequests sqlRequests = SqlRequests.of(request);
        execute(sqlRequests);
        return new SqlResult(ImmutableSet.of());
    }
}
