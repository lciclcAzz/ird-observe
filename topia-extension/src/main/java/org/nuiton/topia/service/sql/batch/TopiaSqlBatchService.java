package org.nuiton.topia.service.sql.batch;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.service.sql.batch.actions.CreateSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.DeleteTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.DropSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.UpdateTablesRequest;

/**
 * Created on 04/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public interface TopiaSqlBatchService extends TopiaService {

    /**
     * @return the service configuration.
     */
    TopiaSqlBatchServiceConfiguration getConfiguration();

    /**
     * @return Starts a new {@link SqlRequests} builder.
     */
    SqlRequests.Builder requestBuilder();

    /**
     * @return Starts a new {@link CreateSchemaRequest} builder.
     */
    CreateSchemaRequest.Builder createSchemaRequestBuilder();

    /**
     * @return Starts a new {@link DropSchemaRequest} builder.
     */
    DropSchemaRequest.Builder dropSchemaRequestBuilder();

    /**
     * @return Starts a new {@link ReplicateTablesRequest} builder.
     */
    ReplicateTablesRequest.Builder replicateTablesRequestBuilder();

    /**
     * @return Starts a new {@link UpdateTablesRequest} builder.
     */
    UpdateTablesRequest.Builder updateTablesRequestBuilder();


    /**
     * @return Starts a new {@link DeleteTablesRequest} builder.
     */
    DeleteTablesRequest.Builder deleteTablesRequestBuilder();

    /**
     * Execute a {@link SqlRequests}.
     *
     * @param requests the request to execute
     */
    SqlResult execute(SqlRequests requests);

    /**
     * Execute a {@link CreateSchemaRequest}.
     *
     * @param request the request to execute
     */
    void execute(CreateSchemaRequest request);

    /**
     * Execute a {@link DropSchemaRequest}.
     *
     * @param request the request to execute
     */
    void execute(DropSchemaRequest request);

    /**
     * Execute a {@link ReplicateTablesRequest}.
     *
     * @param request the request to execute
     */
    SqlResult execute(ReplicateTablesRequest request);

    /**
     * Execute a {@link UpdateTablesRequest}.
     *
     * @param request the request to execute
     */
    SqlResult execute(UpdateTablesRequest request);

    /**
     * Execute a {@link DeleteTablesRequest}.
     *
     * @param request the request to execute
     */
    void execute(DeleteTablesRequest request);

}
