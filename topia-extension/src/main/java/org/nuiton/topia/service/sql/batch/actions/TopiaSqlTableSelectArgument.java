package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * Created on 02/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class TopiaSqlTableSelectArgument {

    protected final ImmutableSet<String> ids;

    protected TopiaSqlTableSelectArgument(Iterable<String> ids) {
        this.ids = ImmutableSet.copyOf(ids);
    }

    public static TopiaSqlTableSelectArgument of(String... ids) {
        return ids.length == 0 ? null : new TopiaSqlTableSelectArgument(Sets.newHashSet(ids));
    }

    public static TopiaSqlTableSelectArgument of(Iterable<String> ids) {
        return new TopiaSqlTableSelectArgument(ids);
    }

    public ImmutableSet<String> getIds() {
        return ids;
    }
}
