package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;

/**
 * Validateurs sur la taille d'une species.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class SpeciesLengthFieldValidator extends AbstractSpeciesFieldValidator {

    @Override
    protected Float getBoundMin(Species referentiel) {
        return referentiel.getMinLength();
    }

    @Override
    protected Float getBoundMax(Species referentiel) {
        return referentiel.getMaxLength();
    }

    @Override
    public String getValidatorType() {
        return "species_length";
    }
}
