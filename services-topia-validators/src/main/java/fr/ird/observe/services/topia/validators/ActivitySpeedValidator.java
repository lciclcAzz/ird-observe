package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.util.GPSPoint;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.xwork2.field.CollectionFieldExpressionValidator;

/**
 * <!-- START SNIPPET: javadoc --> ActivityspeedValidator vérifie que
 * la cohérence de vitesses entre toutes les activités d'une route. <!-- END SNIPPET: javadoc
 * -->
 *
 *
 * <!-- START SNIPPET: parameters --> <ul> <li>fieldName - The field name this
 * validator is validating. Required if using Plain-Validator Syntax otherwise
 * not required</li> </ul> <!-- END SNIPPET: parameters -->
 *
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="invalidLochMatin"&gt;
 *             &lt;param name="fieldName"&gt;startLogValue&lt;/param&gt;
 *             &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *         &lt;/validator&gt;
 *
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="startLogValue"&gt;
 *         	  &lt;field-validator type="invalidLochMatin"&gt;
 *                 &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivitySpeedValidator extends FieldValidatorSupport {

    /** Logger. */
    private static final Log LOG = LogFactory.getLog(ActivitySimpleSpeedValidator.class);

    private CollectionFieldExpressionValidator delegate;

    private Float speed;

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public void setValueStack(ValueStack stack) {
        this.stack = stack;
        super.setValueStack(stack);
    }

    protected String invalidActivity;

    public String getInvalidActivity() {
        return invalidActivity;
    }

    //FIXME
    protected String decorate(ActivitySeine activitySeine) {
        return activitySeine.toString();
    }

    //FIXME
    protected String decorate(GPSPoint currentPoint) {
        return currentPoint.toString();
    }

    public CollectionFieldExpressionValidator getDelegate(final Route route) {
        if (delegate == null) {
            delegate = new CollectionFieldExpressionValidator() {

                @Override
                protected boolean validateOneEntry(Object object) {

                    c.addCurrent(object);

                    ActivitySeine previousActivity = (ActivitySeine) c.getPrevious();
                    ActivitySeine currentActivity = (ActivitySeine) c.getCurrent();

                    if (previousActivity == null) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("No previous activity for current activity : " + decorate(currentActivity) + ", skip speed computation");
                        }
                        return true;
                    }

                    if (previousActivity.getLatitude() == null || previousActivity.getLongitude() == null) {
                        // cas limite (pas de précédent ou position non renseigne)

                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Missing latitude or longitude on previous activity : " + decorate(previousActivity) + ", skip speed computation");
                        }

                        return true;
                    }

                    if (currentActivity.getLongitude() == null || currentActivity.getLatitude() == null) {
                        // cas limite (pas de précédent ou position non renseigne)
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Missing latitude or longitude on current activity : " + decorate(currentActivity) + ", skip speed computation");
                        }
                        return true;
                    }

                    GPSPoint previousPoint = GPSPoint.newPoint(route.getDate(), previousActivity.getTime(), previousActivity.getLatitude(), previousActivity.getLongitude());
                    GPSPoint currentPoint = GPSPoint.newPoint(route.getDate(), currentActivity.getTime(), currentActivity.getLatitude(), currentActivity.getLongitude());

                    float computedSpeed = previousPoint.getSpeed(currentPoint);

                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Speed computed between previous activity point " + decorate(previousPoint) + " to current activity point " + decorate(currentPoint) + ", speed is : " + computedSpeed);
                    }

                    boolean valid = computedSpeed <= speed;

                    if (!valid) {
                        stack.set("foundSpeed", computedSpeed);

                        invalidActivity = decorate(currentActivity);

                        if (LOG.isInfoEnabled()) {
                            LOG.info("Speed from " +
                                     decorate(previousActivity) +
                                     " to " + invalidActivity +
                                     " is " + computedSpeed +
                                     " which is more thant authorized one " +
                                     speed);
                        }
                    }
                    return valid;
                }

                @Override
                public String getMessage(Object object) {
                    boolean pop = false;
                    if (!stack.getRoot().contains(ActivitySpeedValidator.this)) {
                        stack.push(ActivitySpeedValidator.this);
                        pop = true;
                    }
                    try {
                        return super.getMessage(object);
                    } finally {
                        if (pop) {
                            stack.pop();
                        }
                    }
                }
            };
            delegate.setCollectionFieldName(Route.PROPERTY_ACTIVITY_SEINE);
            delegate.setMode(CollectionFieldExpressionValidator.Mode.ALL);
            delegate.setValueStack(stack);
            delegate.setUseSensitiveContext(true);
            delegate.setExpressionForFirst(null);
            delegate.setExpressionForLast(null);
            delegate.setFieldName(getFieldName());
            delegate.setExpression("true");
            delegate.setMessageKey(getMessageKey());
            delegate.setDefaultMessage(getDefaultMessage());
            delegate.setValidatorContext(getValidatorContext());
        }
        return delegate;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (speed == null) {
            throw new ValidationException("le parametre speed est obligatoire");
        }

        invalidActivity = null;

        getDelegate((Route) object).validate(object);
    }

    @Override
    public String getValidatorType() {
        return "activitySpeed";
    }
}
