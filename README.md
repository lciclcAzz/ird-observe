# ObServe

[![Maven Central status](https://img.shields.io/maven-central/v/fr.ird.observe/observe.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22fr.ird.observe%22%20AND%20a%3A%22observe%22)
![Build Status](https://gitlab.com/ultreia.io/ird-observe/badges/develop-5.x/build.svg)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreia.io/ird-observe/blob/develop-5.x/CHANGELOG.md)
* [Documentation](http://observe.ultreia.io)

# Demo

* [Demo (latest)](https://demo.ultreia.io/observe-latest)
* [Demo (5.x)](https://demo.ultreia.io/observe-5.3.1)
* [Demo (6.x)](https://demo.ultreia.io/observe-6.0-RC-3)

# Community

* [Mailing-list (Devel)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/observe-devel)
* [Mailing-list (Commits)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/observe-commits)
* [Contact](mailto:dev@tchemit.fr)
